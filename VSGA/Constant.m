//
//  Constant.m
//  VStudy
//
//  Created by TamNVM on 22/06/2023.
//

#import "Constant.h"

const CGFloat kTopicViewItemSpacing = 2;
const CGFloat kTopicViewItemPadding = 2;
const CGFloat kTopicViewContentInset = 5;

const CGFloat kTopicViewDeleteButtonSize = 17;
const CGFloat kTopicViewDeleteButtonHPadding = 5;

const CGFloat kTopicViewMaxSelectedCount = 5;
