//
//  UserHandler.h
//  VStudy
//
//  Created by LAP14011 on 23/04/2023.
//

#import <Foundation/Foundation.h>
#import "User.h"
NS_ASSUME_NONNULL_BEGIN

@interface UserHandler : NSObject

- (void)getProfileAndUpdateCurrentUserInfoWithCompletionHandler:(void (^)(NSError * _Nullable))completion;

- (void)promoteMember:(User *)member inRoom:(NSString *)roomCode completionHandler:(void (^)(NSError * _Nullable))completion;

@end

NS_ASSUME_NONNULL_END
