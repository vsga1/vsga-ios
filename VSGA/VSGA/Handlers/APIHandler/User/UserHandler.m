//
//  UserHandler.m
//  VStudy
//
//  Created by LAP14011 on 23/04/2023.
//

#import "UserHandler.h"
#import "Macro.h"
#import "OAuth.h"
#import "Scope.h"
#import "APIResponse.h"
#import "AccountManager.h"
@implementation UserHandler

#pragma mark - Get Profile

- (void)getProfileAndUpdateCurrentUserInfoWithCompletionHandler:(void (^)(NSError * _Nullable))completion {
    NSURLRequest *request = [self _getProfileRequest];
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            completion(error);
            return;
        }
        if (!data) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNetworkError
                                             userInfo:nil];
            completion(error);
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSUTF8StringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kServerError
                                             userInfo:nil];
            completion(error);
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNoDataError
                                             userInfo:nil];
            completion(error);
            return;
        }
        [self parseAndUpdateCurrentUserInfoFromJSON:jsonObject];
        completion(nil);
    }] resume];
}

- (NSURLRequest *)_getProfileRequest {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", [[AccountManager sharedManager] getCurrentUserAccessToken]]};
    
    NSURLRequest *request = [OAuth URLRequestURL:kGetProfile
                                      parameters:nil
                                     accessToken:nil
                                   requestMethod:@"GET"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

- (void)parseAndUpdateCurrentUserInfoFromJSON:(id)jsonObject {
    NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
    NSDictionary *data = jsonDictionary[@"data"];

    User *currentUser = [[AccountManager sharedManager] getCurrentUser];
    
    NSString *fullName = [data objectForKey:@"full_name"];
    currentUser.fullName = fullName == (id)[NSNull null] ? nil : fullName;
    NSString *email = [data objectForKey:@"email"];
    currentUser.email = email;
    NSString *avatarURLString = [data objectForKey:@"avatar_img_url"];
    currentUser.avatarURLString = avatarURLString;
    NSString *code = [data objectForKey:@"code"];
    currentUser.userID = code;
}
#pragma mark - Promote
- (void)promoteMember:(User *)member inRoom:(NSString *)roomCode completionHandler:(void (^)(NSError * _Nullable))completion; {
    NSURLRequest *request = [self _postPromoteMember:member roomCode:roomCode];
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            completion(error);
            return;
        }
        if (!data) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNetworkError
                                             userInfo:nil];
            completion(error);
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSUTF8StringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kServerError
                                             userInfo:nil];
            completion(error);
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNoDataError
                                             userInfo:nil];
            completion(error);
            return;
        }
        completion(nil);
    }] resume];
}

- (NSURLRequest *)_postPromoteMember:(User *)member roomCode:(NSString *)roomCode {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", [[AccountManager sharedManager] getCurrentUserAccessToken]]};
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:roomCode forKey:@"room_code"];
    [params setObject:member.username forKey:@"username"];
    
    NSURLRequest *request = [OAuth URLRequestURL:kPromoteMember
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"POST"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}
@end
