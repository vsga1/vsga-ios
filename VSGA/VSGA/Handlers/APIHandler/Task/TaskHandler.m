//
//  TaskHandler.m
//  VStudy
//
//  Created by LAP14011 on 01/04/2023.
//

#import "TaskHandler.h"
#import "Macro.h"
#import "OAuth.h"
#import "Scope.h"
#import "AccountManager.h"
#import "APIResponse.h"
@implementation TaskHandler

#pragma mark - Get Task

- (void)getTaskWithRoomCode:(NSString *)roomCode
                       type:(TaskType)type
                     status:(TaskStatus)status
          completionHandler:(void (^)(NSArray<TaskItem *> * _Nullable, NSError * _Nullable))completion {
    NSURLRequest *request = [self _getTaskListRequestRoomCode:roomCode
                                                         type:type
                                                       status:status];
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            completion(nil, error);
            return;
        }
        if (!data) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNetworkError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSUTF8StringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kServerError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNoDataError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        NSArray<Task *> *taskList = [self _parseGetTaskListResponseFromJSON:jsonObject];
        NSArray<TaskItem *> *taskItemList = [self _converToTaskItems:taskList];
        completion(taskItemList, nil);
    }] resume];
}

#pragma mark - Add Task

- (void)addNewTask:(TaskItem *)item completionHandler:(nonnull void (^)(TaskItem * _Nullable, NSError * _Nullable))completion {
    NSURLRequest *request = [self _postTaskListRequestWithItem:item];
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            completion(nil, error);
            return;
        }
        if (!data) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNetworkError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSUTF8StringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kServerError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNoDataError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        Task *task = [self _parsePostTaskResponseFromJSON:jsonObject];
        TaskItem *item = [[TaskItem alloc] initWithTask:task];
        completion(item, nil);
    }] resume];
}

#pragma mark - Update Task

- (void)updateTask:(TaskItem *)item completionHandler:(void (^)(NSError * _Nullable))completion {
    NSURLRequest *request = [self
                             _updateTaskListRequestWithItem:item];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            completion(error);
            return;
        }
        if (!data) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNetworkError
                                             userInfo:nil];
            completion(error);
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSUTF8StringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kServerError
                                             userInfo:nil];
            completion(error);
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNoDataError
                                             userInfo:nil];
            completion(error);
            return;
        }
        completion(nil);
    }] resume];
}

- (void)postRoomTaskDoneWithItem:(TaskItem *)item completionHandler:(void (^)(RoomTaskItem * _Nullable, NSError * _Nullable))completion {
    NSURLRequest *request = [self _postRoomTaskDoneRequestWithItem:item];
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            completion(nil, error);
            return;
        }
        if (!data) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNetworkError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSUTF8StringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kServerError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNoDataError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        RoomTaskItem *roomTaskItem = [self _parseRoomTaskDoneResponseFromJSON:jsonObject withTaskItem:item];
        completion(roomTaskItem, nil);
    }] resume];
}

#pragma mark - Get Task Request and Response

- (NSURLRequest *)_getTaskListRequestRoomCode:(NSString *)roomCode
                                         type:(TaskType)type
                                       status:(TaskStatus)status {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", [[AccountManager sharedManager] getCurrentUserAccessToken]]};
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:roomCode forKey:@"room_code"];
    NSString *typeString;
    switch (type) {
        case TaskTypeRoom:
            typeString = @"ROOM_TASK";
            break;
        case TaskTypePersonal:
            typeString = @"PERSONAL_TASK";
            break;
        default:
            break;
    }
    NSString *statusString;
    switch (status) {
        case TaskStatusDone:
            statusString = @"DONE";
            break;
        case TaskStatusUndone:
            statusString = @"UNDONE";
            break;
        default:
            break;
    }
    [params setObject:typeString forKey:@"type"];
    [params setObject:statusString forKey:@"status"];
    
    NSURLRequest *request = [OAuth URLRequestURL:kTaskURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"GET"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

- (NSArray *)_parseGetTaskListResponseFromJSON:(id)jsonObject {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
    
    NSArray *noteListJSON = jsonDictionary[@"data"];
    for (NSDictionary *metaData in noteListJSON) {
        NSString *code = metaData[@"code"];
        NSString *roomCode = metaData[@"room_code"];
        NSString *desc = metaData[@"description"];
        NSString *scheme = metaData[@"scheme"];
        NSString *status = metaData[@"status"];
        NSString *type = metaData[@"type"];
        Task *task = [[Task alloc] initWithCode:code
                                       roomCode:roomCode
                                         scheme:scheme
                                           desc:desc
                                         status:status
                                           type:type];
        
        [result addObject:task];
        
    }
    return result;
}

- (NSArray *)_converToTaskItems:(NSArray *)taskList {
    NSMutableArray *taskItemList = [[NSMutableArray alloc] init];
    for (Task *task in taskList) {
        TaskItem *taskItem = [[TaskItem alloc] initWithTask:task];
        [taskItemList addObject:taskItem];
    }
    return taskItemList;
}

#pragma mark - Add Task Request and Response

- (NSURLRequest *)_postTaskListRequestWithItem:(TaskItem *)item {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", [[AccountManager sharedManager] getCurrentUserAccessToken]]};
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:item.roomCode forKey:@"room_code"];
    [params setObject:item.desc forKey:@"description"];
    NSString *type;
    if (item.type == TaskTypePersonal) {
        type = @"PERSONAL_TASK";
    }
    else {
        type = @"ROOM_TASK";
    }
    [params setObject:type forKey:@"type"];
    if (item.scheme) {
        [params setObject:item.scheme forKey:@"scheme"];
    }
    NSURLRequest *request = [OAuth URLRequestURL:kTaskURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"POST"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

- (Task *)_parsePostTaskResponseFromJSON:(id)jsonObject {
    NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
    
    NSDictionary *metaData = jsonDictionary[@"data"];
    NSString *code = metaData[@"code"];
    NSString *roomCode = metaData[@"room_code"];
    NSString *desc = metaData[@"description"];
    NSString *scheme = metaData[@"scheme"];
    NSString *status = metaData[@"status"];
    NSString *type = metaData[@"type"];
    Task *task = [[Task alloc] initWithCode:code
                                   roomCode:roomCode
                                     scheme:scheme
                                       desc:desc
                                     status:status
                                       type:type];
    return task;
}

#pragma mark - Update Task Request and Response

- (NSURLRequest *)_updateTaskListRequestWithItem:(TaskItem *)item {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", [[AccountManager sharedManager] getCurrentUserAccessToken]]};
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:item.roomCode forKey:@"room_code"];
    [params setObject:item.code forKey:@"code"];
    [params setObject:item.desc forKey:@"description"];
    NSString *type;
    if (item.type == TaskTypePersonal) {
        type = @"PERSONAL_TASK";
    }
    else {
        type = @"ROOM_TASK";
    }
    [params setObject:type forKey:@"type"];
    
    NSString *status;
    if (item.status == TaskStatusDone) {
        status = @"DONE";
    }
    else if (item.status == TaskStatusUndone)  {
        status = @"UNDONE";
    }
    else {
        status = @"DELETED";
    }
    [params setObject:status forKey:@"status"];
    
    if (item.scheme) {
        [params setObject:item.scheme forKey:@"scheme"];
    }
    NSURLRequest *request = [OAuth URLRequestURL:kTaskURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"PUT"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

#pragma mark - Post Room Task Request and Response

- (NSURLRequest *)_postRoomTaskDoneRequestWithItem:(TaskItem *)item {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", [[AccountManager sharedManager] getCurrentUserAccessToken]]};
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:item.roomCode forKey:@"room_code"];
    [params setObject:item.scheme forKey:@"scheme"];
    NSURLRequest *request = [OAuth URLRequestURL:kRoomTaskDoneURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"POST"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

- (RoomTaskItem *)_parseRoomTaskDoneResponseFromJSON:(id)jsonObject withTaskItem:(TaskItem *)item {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
    
    NSDictionary *userInfoData = jsonDictionary[@"data"];
    NSArray *userData = userInfoData[@"members"];
    for (NSString *username in userData) {
        
//        NSString *username = metaData[@"username"];
        
        //TODO:find user in roomManager
        
        User *user = [[User alloc] initWithUsername:username
                                    avatarURLString:nil
                                               role:@"MEMBER"];
        
        [result addObject:user];
    }
    RoomTaskItem *roomTaskItem = [[RoomTaskItem alloc] initWithTaskItem:item doneUserArray:result];
    return roomTaskItem;
}

@end
