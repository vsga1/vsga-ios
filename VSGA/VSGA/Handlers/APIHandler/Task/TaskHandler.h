//
//  TaskHandler.h
//  VStudy
//
//  Created by LAP14011 on 01/04/2023.
//

#import <Foundation/Foundation.h>
#import "Task.h"
#import "TaskItem.h"
#import "RoomTaskItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface TaskHandler : NSObject

//MARK: Get Task

- (void)getTaskWithRoomCode:(NSString *)roomCode
                       type:(TaskType)type
                     status:(TaskStatus)status
          completionHandler:(void (^)(NSArray<TaskItem *> * _Nullable, NSError * _Nullable))completion;

- (void)addNewTask:(TaskItem *)item completionHandler:(void (^)(TaskItem * _Nullable, NSError * _Nullable))completion;

- (void)updateTask:(TaskItem *)item completionHandler:(void (^)(NSError * _Nullable))completion;

- (void)postRoomTaskDoneWithItem:(TaskItem *)item completionHandler:(void (^)(RoomTaskItem * _Nullable, NSError * _Nullable))completion;

@end

NS_ASSUME_NONNULL_END
