//
//  RoomHandler.m
//  VStudy
//
//  Created by LAP14011 on 06/03/2023.
//

#import "RoomHandler.h"

#import "Macro.h"

#import "Scope.h"
#import "OAuth.h"
#import "APIResponse.h"

#import "RoomManager.h"
#import "AccountManager.h"

#import "Topic.h"

@implementation RoomHandler

- (void)getRoomListWithPage:(NSInteger)pageNum
                       text:(NSString *)text
                     topics:(NSArray *)topics
          completionHandler:(void (^)(NSArray<RoomItem *> * _Nullable, NSInteger, NSError * _Nullable))completion {
    NSURLRequest *request = [self _getRoomListRequestWithPage:pageNum text:text topics:topics];

    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            completion(nil, 0, error);
            return;
        }
        if (!data) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNetworkError
                                             userInfo:nil];
            completion(nil, 0, error);
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSUTF8StringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kServerError
                                             userInfo:nil];
            completion(nil, 0, error);
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNoDataError
                                             userInfo:nil];
            completion(nil, 0, error);
            return;
        }
        NSArray<Room *> *roomList = [self _parseGetRoomListResponseFromJSON:jsonObject];
        NSArray<RoomItem *> *roomItemList = [self _converToItem:roomList];
        NSNumber *total = jsonObject[@"meta"][@"total"];
        completion(roomItemList, [total integerValue], nil);
    }] resume];
}

- (void)getLastestRoomListWithCompletionHandler:(void (^)(NSArray<RoomItem *> * _Nullable, NSError * _Nullable))completion {
    NSURLRequest *request = [self _getLatestRoomListRequest];
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            completion(nil, error);
            return;
        }
        if (!data) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNetworkError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSUTF8StringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kServerError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNoDataError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        
        NSArray<Room *> *roomList = [self _parseGetRoomListResponseFromJSON:jsonObject];
        NSArray<RoomItem *> *roomItemList = [self _converToItem:roomList];
        completion(roomItemList, nil);
    }] resume];
}

- (void)getRoomListWithSearchParams:(NSDictionary *)searchParams
                  completionHandler:(void (^)(NSArray<RoomItem *> * _Nullable, NSError * _Nullable))completion {
    NSURLRequest *request = [self _getRoomListRequestWithSearchParams:searchParams];
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            completion(nil, error);
            return;
        }
        if (!data) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNetworkError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSUTF8StringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kServerError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNoDataError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        
        NSArray<Room *> *roomList = [self _parseGetRoomListResponseFromJSON:jsonObject];
        NSArray<RoomItem *> *roomItemList = [self _converToItem:roomList];
        completion(roomItemList, nil);
    }] resume];
}

- (void)postJoinRoomEventWithInfo:(NSDictionary *)info {
    NSString *roomCode = info[@"roomCode"];
    NSString *password = info[@"password"];
    
    NSURLRequest *request = [self _postJoinRoomEventRequestWithRoomCode:roomCode
                                                               password:password];
    
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            [self.joinRoomDelegate onFinishJoinRoomWithError:RoomHandlerServerError];
            return;
        }
        if (!data) {
            [self.joinRoomDelegate onFinishJoinRoomWithError:RoomHandlerServerError];
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSASCIIStringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            [self.joinRoomDelegate onFinishJoinRoomWithError:RoomHandlerServerError];
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            long code = [APIResponse getCodeResponseFromJSON:jsonObject];
            if (code == 4005003) {
                [self.joinRoomDelegate onFinishJoinRoomWithError:RoomHandlerWrongPassword];
            }
            else if (code == 4005008) {
                [self.joinRoomDelegate onFinishJoinRoomWithError:RoomHandlerWaitForHost];
            }
            else {
                [self.joinRoomDelegate onFinishJoinRoomWithError:RoomHandlerInvalidDataError];
            }
            return;
        }
        [self parseAndStoreRoomDataFromJSON:jsonObject];
        // Confirm there is no error
        [self.joinRoomDelegate onFinishJoinRoomWithError:RoomHandlerNoError];
    }] resume];
}

- (void)postCreateNewRoomWithInfo:(NSDictionary *)info {
    NSURLRequest *request = [self _postCreateNewRoomWithInfo:info];
    
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            [self.createRoomDelegate onFinishCreateNewRoomWithError:RoomHandlerServerError newRoomItem:nil];
            return;
        }
        if (!data) {
            [self.createRoomDelegate onFinishCreateNewRoomWithError:RoomHandlerServerError newRoomItem:nil];
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSASCIIStringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            [self.createRoomDelegate onFinishCreateNewRoomWithError:RoomHandlerServerError newRoomItem:nil];
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            [self.createRoomDelegate onFinishCreateNewRoomWithError:RoomHandlerInvalidDataError newRoomItem:nil];
            return;
        }
        RoomItem *roomItem = [self _convertToItemFromJSON:jsonObject];
        
        User *currentUser = [[AccountManager sharedManager] getCurrentUser];
        [currentUser updateRole:@"HOST"];
        // Confirm there is no error
        [self.createRoomDelegate onFinishCreateNewRoomWithError:RoomHandlerNoError newRoomItem:roomItem];
    }] resume];
}

#pragma mark - Get Room List

- (NSURLRequest *)_getRoomListRequestWithPage:(NSInteger)pageNum
                                         text:(NSString *)text
                                       topics:(NSArray *)topics {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@",[[AccountManager sharedManager] getCurrentUserAccessToken]]};
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    if (pageNum > 0) {
        NSInteger offset = pageNum * kLoadRoomSize;
        [params setObject:[NSNumber numberWithInteger:offset] forKey:@"offset"];
    }
   
    [params setObject:[NSNumber numberWithInteger:kLoadRoomSize] forKey:@"limit"];
    if (text.length > 0) {
        [params setObject:text forKey:@"name"];
    }
    if (topics.count > 0) {
        [params setObject:topics forKey:@"topics"];
    }
    NSURLRequest *request = [OAuth URLRequestURL:kSearchRoomListURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"GET"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

- (NSArray *)_parseGetRoomListResponseFromJSON:(id)jsonObject {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
    NSArray *roomListJSON = jsonDictionary[@"data"];
    for (NSDictionary *metaData in roomListJSON) {
        NSString *code = metaData[@"code"];
        NSString *name = metaData[@"name"];
        NSString *roomTypeString = metaData[@"room_type"];
        RoomType roomType = RoomTypePublic;
        if ([roomTypeString isEqualToString:@"PUBLIC"]) {
            roomType = RoomTypePublic;
        }
        else if ([roomTypeString isEqualToString:@"PRIVATE"]) {
            roomType = RoomTypePrivate;
        }
        NSString *desc = metaData[@"description"];
        int capacity = [metaData[@"capacity"] intValue];
        int totalMember = [metaData[@"total_member"] intValue];

        NSMutableArray<Topic *> *topics = [NSMutableArray new];
        id topicData = metaData[@"topics"];
        if (topicData != [NSNull null]) {
            NSArray *topicArray = (NSArray *)topicData;
            for (NSDictionary *topic in topicArray) {
                NSString *name = topic[@"name"];
                NSString *color = topic[@"color"];
                Topic *topic = [[Topic alloc] initWithName:name colorString:color];
                [topics addObject:topic];
            }
        }
        NSDictionary *config = (NSDictionary *)metaData[@"configuration"];
        BOOL isProtected = [config[@"is_protected"] boolValue];
        BOOL isFavored = [metaData[@"is_favored"] boolValue];
        Room *room = [[Room alloc] initWithCode:code
                                           name:name
                                           desc:desc
                                       roomType:roomType
                                       capacity:capacity
                                    totalMember:totalMember
                                         topics:topics
                                    isProtected:isProtected
                                      isFavored:isFavored];
        [result addObject:room];
        
    }
    return result;
}

- (NSArray *)_converToItem:(NSArray *)roomList {
    NSMutableArray *roomItemList = [[NSMutableArray alloc] init];
    for (Room *room in roomList) {
        RoomItem *roomItem = [[RoomItem alloc] initWithRoom:room];
        [roomItemList addObject:roomItem];
    }
    return roomItemList;
}

#pragma mark - Get Latest Room List

- (NSURLRequest *)_getLatestRoomListRequest {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", [[AccountManager sharedManager] getCurrentUserAccessToken]]};
    NSURLRequest *request = [OAuth URLRequestURL:kGetLatestRoomListURL
                                      parameters:nil
                                     accessToken:nil
                                   requestMethod:@"GET"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

#pragma mark - Get Room List With Search

- (NSURLRequest *)_getRoomListRequestWithSearchParams:(NSDictionary *)searchParams {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", [[AccountManager sharedManager] getCurrentUserAccessToken]]};
    NSURLRequest *request = [OAuth URLRequestURL:kSearchRoomListURL
                                      parameters:searchParams
                                     accessToken:nil
                                   requestMethod:@"GET"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

#pragma mark - Join Room

- (NSURLRequest *)_postJoinRoomEventRequestWithRoomCode:(NSString *)roomCode
                                               password:(NSString *)password {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", [[AccountManager sharedManager] getCurrentUserAccessToken]]};
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:roomCode forKey:@"room_code"];
    if (password) {
        [params setObject:password forKey:@"password"];
    }
    
    NSURLRequest *request = [OAuth URLRequestURL:kJoinRoomEventURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"POST"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

- (void)parseAndStoreRoomDataFromJSON:(id)jsonObject {
    NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
    NSDictionary *data = jsonDictionary[@"data"];
    
//    NSArray *membersInfo = [data objectForKey:@"members"];
//    [self _parseAndStoreMemberInfo:membersInfo];
    
    NSString *currentUserRole = [data objectForKey:@"role"];
    User *currentUser = [[AccountManager sharedManager] getCurrentUser];
    [currentUser updateRole:currentUserRole];
}

- (void)_parseAndStoreMemberInfo:(NSArray *)membersInfo {
    NSMutableArray *memberArray = [[NSMutableArray alloc] init];
    for (NSDictionary *memberInfo in membersInfo) {
        //TODO: age, description, display_name, full_name, gender
        NSString *avatarURLString = [memberInfo objectForKey:@"avatar_img_url"] == [NSNull null] ? nil : [memberInfo objectForKey:@"avatar_img_url"];
        NSString *role = [memberInfo objectForKey:@"role"];
        NSString *username = [memberInfo objectForKey:@"username"];
        
        User *user = [[User alloc] initWithUsername:username
                                    avatarURLString:avatarURLString
                                               role:role];
        [memberArray addObject:user];
    }
    [[RoomManager sharedManager] createMemberArrayWithArray:memberArray];
}

#pragma mark - Create New Room

- (NSURLRequest *)_postCreateNewRoomWithInfo:(NSDictionary *)info {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", [[AccountManager sharedManager] getCurrentUserAccessToken]]};
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    NSString *name = info[@"name"];
    [params setObject:name forKey:@"name"];
    
    NSString *description = info[@"description"];
    if (description) {
        [params setObject:description forKey:@"description"];
    }
    
    NSString *roomType = info[@"roomType"];
    [params setObject:roomType forKey:@"room_type"];
    
    NSString *capacity = info[@"capacity"];
    [params setObject:capacity forKey:@"capacity"];
    
    NSString *password = info[@"password"];
    if (password) {
        [params setObject:password forKey:@"password"];
    }
    NSArray *topics = info[@"topics"];
    [params setObject:topics forKey:@"topics"];
    
    NSURLRequest *request = [OAuth URLRequestURL:kCreateNewRoomURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"POST"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

- (RoomItem *)_convertToItemFromJSON:(id)jsonObject {
    
    NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
    NSDictionary *metaData = jsonDictionary[@"data"];
    
    NSString *code = metaData[@"code"];
    NSString *name = metaData[@"name"];
    NSString *roomTypeString = metaData[@"room_type"];
    RoomType roomType = RoomTypePublic;
    if ([roomTypeString isEqualToString:@"PUBLIC"]) {
        roomType = RoomTypePublic;
    }
    else if ([roomTypeString isEqualToString:@"PRIVATE"]) {
        roomType = RoomTypePrivate;
    }
    NSString *desc = metaData[@"description"];
    int capacity = [metaData[@"capacity"] intValue];
    int totalMember = [metaData[@"total_member"] intValue];

    NSMutableArray *topics = [NSMutableArray new];
    id topicData = metaData[@"topics"];
    if (topicData != [NSNull null]) {
        NSArray *topicArray = (NSArray *)topicData;
        for (NSDictionary *topic in topicArray) {
            NSString *name = topic[@"name"];
            NSString *color = topic[@"color"];
            Topic *topic = [[Topic alloc] initWithName:name colorString:color];
            [topics addObject:topic];
        }
    }
    NSDictionary *config = (NSDictionary *)metaData[@"configuration"];
    BOOL isProtected = [config[@"is_protected"] boolValue];
    BOOL isFavored = [metaData[@"is_favored"] boolValue];
    
    Room *room = [[Room alloc] initWithCode:code
                                       name:name
                                       desc:desc
                                   roomType:roomType
                                   capacity:capacity
                                totalMember:totalMember
                                     topics:[NSArray arrayWithArray:topics]
                                isProtected:isProtected
                                  isFavored:isFavored];
    RoomItem *item = [[RoomItem alloc] initWithRoom:room];
    return item;
}

@end
