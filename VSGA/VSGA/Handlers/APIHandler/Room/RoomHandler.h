//
//  RoomHandler.h
//  VStudy
//
//  Created by LAP14011 on 06/03/2023.
//

#import <Foundation/Foundation.h>
#import "Room.h"
#import "RoomItem.h"

typedef NS_ENUM(NSUInteger, RoomHandlerErrorCode) {
    RoomHandlerServerError,
    RoomHandlerInvalidDataError,
    RoomHandlerWrongPassword,
    RoomHandlerWaitForHost,
    RoomHandlerNoError
};

@protocol JoinRoomDelegate <NSObject>

- (void)onFinishJoinRoomWithError:(RoomHandlerErrorCode)errorCode;

@end

@protocol NewRoomDelegate <NSObject>

- (void)onFinishCreateNewRoomWithError:(RoomHandlerErrorCode)errorCode newRoomItem:(RoomItem *_Nullable)item;

@end

@interface RoomHandler : NSObject

@property (nonatomic, weak) id<JoinRoomDelegate> _Nullable joinRoomDelegate;

@property (nonatomic, weak) id<NewRoomDelegate> _Nullable createRoomDelegate;

- (void)getRoomListWithPage:(NSInteger)pageNum
                       text:(NSString *_Nonnull)text
                     topics:(NSArray *_Nonnull)topics
          completionHandler:(void (^_Nonnull)(NSArray<RoomItem *> * _Nullable, NSInteger,
                                      NSError * _Nullable))completion;

- (void)getLastestRoomListWithCompletionHandler:(void (^_Nonnull)(NSArray<RoomItem *> * _Nullable,
                                                                  NSError * _Nullable))completion;

- (void)getRoomListWithSearchParams:(NSDictionary *_Nonnull)searchParams
                  completionHandler:(void (^_Nonnull)(NSArray<RoomItem *> * _Nullable, NSError * _Nullable))completion;

- (void)postJoinRoomEventWithInfo:(NSDictionary *_Nonnull)info;

- (void)postCreateNewRoomWithInfo:(NSDictionary *_Nonnull)info;

@end
