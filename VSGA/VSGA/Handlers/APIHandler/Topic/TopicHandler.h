//
//  TopicHandler.h
//  VStudy
//
//  Created by LAP14011 on 07/05/2023.
//

#import <Foundation/Foundation.h>
#import "Topic.h"
#import "FilterTopicItem.h"
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, TopicHandlerErrorCode) {
    TopicHandlerServerError,
    TopicHandlerInvalidDataError,
    TopicHandlerNoError
};

@interface TopicHandler : NSObject

- (void)getSuggestTopicWithCompletionHandler:(void (^_Nonnull)(NSArray<FilterTopicItem *> * _Nullable,
                                                               NSError * _Nullable))completion;

- (void)searchTopicWithText:(NSString *)text
          completionHandler:(void (^_Nonnull)(NSArray<FilterTopicItem *> * _Nullable, NSError * _Nullable))completion;
@end

NS_ASSUME_NONNULL_END
