//
//  TopicHandler.m
//  VStudy
//
//  Created by LAP14011 on 07/05/2023.
//

#import "TopicHandler.h"
#import "Macro.h"
#import "OAuth.h"
#import "Scope.h"
#import "APIResponse.h"

#import "AccountManager.h"
@implementation TopicHandler

- (void)getSuggestTopicWithCompletionHandler:(void (^)(NSArray<FilterTopicItem *> * _Nullable, NSError * _Nullable))completion {
    NSURLRequest *request = [self _getSuggestTopicList];

    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            completion(nil, error);
            return;
        }
        if (!data) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNetworkError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSUTF8StringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kServerError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNoDataError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        NSArray<Topic *> *topicList = [self _parseGetSuggestTopicListResponseFromJSON:jsonObject];
        NSArray<FilterTopicItem *> *selectTopicItemList = [self _converToItem:topicList];
        completion(selectTopicItemList, nil);
    }] resume];
}

- (void)searchTopicWithText:(NSString *)text completionHandler:(void (^)(NSArray<FilterTopicItem *> * _Nullable, NSError * _Nullable))completion {
    NSURLRequest *request = [self _searchTopicListWithText:text];

    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            completion(nil, error);
            return;
        }
        if (!data) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNetworkError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSUTF8StringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kServerError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNoDataError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        NSArray<Topic *> *topicList = [self _parseGetSuggestTopicListResponseFromJSON:jsonObject];
        NSArray<FilterTopicItem *> *selectTopicItemList = [self _converToItem:topicList];
        completion(selectTopicItemList, nil);
    }] resume];
}
#pragma mark - NSURLRequest

- (NSURLRequest *)_getSuggestTopicList {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@",[[AccountManager sharedManager] getCurrentUserAccessToken]]};
    
    NSURLRequest *request = [OAuth URLRequestURL:kGetSuggestTopicURL
                                      parameters:nil
                                     accessToken:nil
                                   requestMethod:@"GET"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

- (NSURLRequest *)_searchTopicListWithText:(NSString *)text {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@",[[AccountManager sharedManager] getCurrentUserAccessToken]]};
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:[text lowercaseString] forKey:@"name"];
    
    NSURLRequest *request = [OAuth URLRequestURL:kSearchTopicURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"GET"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

#pragma mark - Parse
- (NSArray *)_parseGetSuggestTopicListResponseFromJSON:(id)jsonObject {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
    NSArray *topicListJSON = jsonDictionary[@"data"];
    for (NSDictionary *metaData in topicListJSON) {
        NSString *name = metaData[@"name"];
        NSString *color = metaData[@"color"];
        
        Topic *topic = [[Topic alloc] initWithName:name colorString:color];
        [result addObject:topic];
    }
    return result;
}

- (NSArray *)_converToItem:(NSArray *)topicList {
    NSMutableArray *topicItemList = [[NSMutableArray alloc] init];
    for (Topic *topic in topicList) {
        FilterTopicItem *topicItem = [[FilterTopicItem alloc] initWithName:topic.name colorString:topic.colorString];
        [topicItemList addObject:topicItem];
    }
    return topicItemList;
}

@end
