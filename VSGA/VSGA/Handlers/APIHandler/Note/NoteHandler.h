//
//  NoteHandler.h
//  VStudy
//
//  Created by LAP14011 on 16/03/2023.
//

#import <Foundation/Foundation.h>

#import "PersonalNote.h"
#import "PersonalNoteItem.h"

#import "RoomNote.h"
#import "RoomNoteItem.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, NoteHandlerErrorCode) {
    NoteHandlerError,
    NoteHandlerNoError
};

@protocol PersonalNoteHandlerDelegate <NSObject>

- (void)onFinishAddNewNote:(PersonalNoteItem *_Nullable)newNote error:(NoteHandlerErrorCode)errorCode;

- (void)onFinishUpdateNote:(PersonalNoteItem *_Nullable)note error:(NoteHandlerErrorCode)errorCode;

@end

@protocol RoomNoteHandlerDelegate <NSObject>

- (void)onFinishUpdateNote:(RoomNoteItem *_Nullable)note error:(NoteHandlerErrorCode)errorCode;

@end

@interface NoteHandler : NSObject

@property (nonatomic, weak) id<PersonalNoteHandlerDelegate> personalNoteDelegate;

@property (nonatomic, weak) id<RoomNoteHandlerDelegate> roomNoteDelegate;

//MARK: Personal Note

- (void)getPersonalNoteListWithRoomCode:(NSString *)roomCode
              completionHandler:(void (^)(NSArray<PersonalNoteItem *> * _Nullable, NSError * _Nullable))completion;

- (void)addPersonalNoteWithInfo:(NSDictionary *)info;

- (void)updatePersonalNoteWithInfo:(NSDictionary *)info;

//MARK: Room Note

- (void)getRoomNoteWithRoomCode:(NSString *)roomCode
              completionHandler:(void (^)(RoomNoteItem * _Nullable, NSError * _Nullable))completion;

- (void)updateRoomNoteWithInfo:(NSDictionary *)info;

@end

NS_ASSUME_NONNULL_END
