//
//  NoteHandler.m
//  VStudy
//
//  Created by LAP14011 on 16/03/2023.
//

#import "NoteHandler.h"
#import "AccountManager.h"
#import "Scope.h"
#import "APIResponse.h"
#import "OAuth.h"
#import "Macro.h"
@implementation NoteHandler

#pragma mark - Personal Note

- (void)getPersonalNoteListWithRoomCode:(NSString *)roomCode
                      completionHandler:(void (^)(NSArray<PersonalNoteItem *> * _Nullable, NSError * _Nullable))completion {
    NSURLRequest *request = [self _getPersonalNoteListRequestWithRoomCode:roomCode];
    
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            completion(nil, error);
            return;
        }
        if (!data) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNetworkError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSUTF8StringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kServerError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNoDataError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        NSArray<PersonalNote *> *noteList = [self _parseGetPersonalNoteListResponseFromJSON:jsonObject];
        NSArray<PersonalNoteItem *> *noteItemList = [self _converToPersonalItems:noteList];
        completion(noteItemList, nil);
    }] resume];
}

- (void)addPersonalNoteWithInfo:(NSDictionary *)info {
    NSString *header = info[@"header"];
    NSString *desc = info[@"desc"];
    NSString *roomCode = info[@"roomCode"];
    
    NSURLRequest *request = [self _postPersonalNoteRequestWithRoomCode:roomCode
                                                                header:header
                                                                  desc:desc];
    
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            [self.personalNoteDelegate onFinishAddNewNote:nil error:NoteHandlerError];
            return;
        }
        if (!data) {
            [self.personalNoteDelegate onFinishAddNewNote:nil error:NoteHandlerError];
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSASCIIStringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            [self.personalNoteDelegate onFinishAddNewNote:nil error:NoteHandlerError];
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            [self.personalNoteDelegate onFinishAddNewNote:nil error:NoteHandlerError];
            return;
        }
        PersonalNote *note = [self _parsePostPersonalNoteResponseFromJSON:jsonObject];
        PersonalNoteItem *noteItem = [[PersonalNoteItem alloc] initWithNote:note];
        // Confirm there is no error
        [self.personalNoteDelegate onFinishAddNewNote:noteItem error:NoteHandlerNoError];
    }] resume];
}

- (void)updatePersonalNoteWithInfo:(NSDictionary *)info {
    NSString *code = info[@"code"];
    NSString *header = info[@"header"];
    NSString *desc = info[@"desc"];
    NSString *roomCode = info[@"roomCode"];
    
    NSURLRequest *request = [self _putPersonalNoteRequestWithRoomCode:roomCode
                                                                 code:code
                                                               header:header
                                                                 desc:desc];
    
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            [self.personalNoteDelegate onFinishUpdateNote:nil error:NoteHandlerError];
            return;
        }
        if (!data) {
            [self.personalNoteDelegate onFinishUpdateNote:nil error:NoteHandlerError];
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSASCIIStringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            [self.personalNoteDelegate onFinishUpdateNote:nil error:NoteHandlerError];
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            [self.personalNoteDelegate onFinishUpdateNote:nil error:NoteHandlerError];
            return;
        }
        PersonalNote *note = [self _parsePutPersonalNoteResponseFromJSON:jsonObject];
        PersonalNoteItem *noteItem = [[PersonalNoteItem alloc] initWithNote:note];
        // Confirm there is no error
        [self.personalNoteDelegate onFinishUpdateNote:noteItem error:NoteHandlerNoError];
    }] resume];
}

#pragma mark - Get Personal Note

- (NSURLRequest *)_getPersonalNoteListRequestWithRoomCode:(NSString *)roomCode {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", [[AccountManager sharedManager] getCurrentUserAccessToken]]};
    NSString *getNoteListURL = [NSString stringWithFormat:@"%@/v1/room/%@/notes", kHost, roomCode];
    NSURLRequest *request = [OAuth URLRequestURL:getNoteListURL
                                      parameters:nil
                                     accessToken:nil
                                   requestMethod:@"GET"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

- (NSArray *)_parseGetPersonalNoteListResponseFromJSON:(id)jsonObject {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
    
    NSArray *noteListJSON = jsonDictionary[@"data"];
    for (NSDictionary *metaData in noteListJSON) {
        NSString *code = metaData[@"code"];
        NSString *roomCode = metaData[@"room_code"];
        NSString *header = metaData[@"header"];
        NSString *desc = metaData[@"description"];
        
        PersonalNote *note = [[PersonalNote alloc] initWithRoomCode:roomCode
                                                               code:code
                                                             header:header
                                                               desc:desc];
        
        [result addObject:note];
        
    }
    return result;
}

- (NSArray *)_converToPersonalItems:(NSArray *)noteList {
    NSMutableArray *noteItemList = [[NSMutableArray alloc] init];
    for (PersonalNote *note in noteList) {
        PersonalNoteItem *noteItem = [[PersonalNoteItem alloc] initWithNote:note];
        [noteItemList addObject:noteItem];
    }
    return noteItemList;
}

#pragma mark - Post Personal Note

- (NSURLRequest *)_postPersonalNoteRequestWithRoomCode:(NSString *)roomCode
                                                header:(NSString *)header
                                                  desc:(NSString *)desc {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", [[AccountManager sharedManager] getCurrentUserAccessToken]]};
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:header forKey:@"header"];
    [params setObject:desc forKey:@"description"];
    
    NSString *postNoteURL = [NSString stringWithFormat:@"%@/v1/room/%@/notes", kHost, roomCode];
    NSURLRequest *request = [OAuth URLRequestURL:postNoteURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"POST"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

- (PersonalNote *)_parsePostPersonalNoteResponseFromJSON:(id)jsonObject {
    NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
    
    NSDictionary *metaData = jsonDictionary[@"data"];
    NSString *code = metaData[@"code"];
    NSString *roomCode = metaData[@"room_code"];
    NSString *header = metaData[@"header"];
    NSString *desc = metaData[@"description"];
    
    PersonalNote *note = [[PersonalNote alloc] initWithRoomCode:roomCode
                                                           code:code
                                                         header:header
                                                           desc:desc];
    return note;
}

#pragma mark - Update Personal Note

- (NSURLRequest *)_putPersonalNoteRequestWithRoomCode:(NSString *)roomCode
                                                 code:(NSString *)code
                                               header:(NSString *)header
                                                 desc:(NSString *)desc {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", [[AccountManager sharedManager] getCurrentUserAccessToken]]};
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:code forKey:@"code"];
    [params setObject:header forKey:@"header"];
    [params setObject:desc forKey:@"description"];
    
    NSString *postNoteURL = [NSString stringWithFormat:@"%@/v1/room/%@/notes", kHost, roomCode];
    NSURLRequest *request = [OAuth URLRequestURL:postNoteURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"PUT"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

- (PersonalNote *)_parsePutPersonalNoteResponseFromJSON:(id)jsonObject {
    NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
    
    NSDictionary *metaData = jsonDictionary[@"data"];
    NSString *code = metaData[@"code"];
    NSString *roomCode = metaData[@"room_code"];
    NSString *header = metaData[@"header"];
    NSString *desc = metaData[@"description"];
    
    PersonalNote *note = [[PersonalNote alloc] initWithRoomCode:roomCode
                                                           code:code
                                                         header:header
                                                           desc:desc];
    return note;
}

#pragma mark - Room Note

- (void)getRoomNoteWithRoomCode:(NSString *)roomCode
              completionHandler:(void (^)(RoomNoteItem * _Nullable, NSError * _Nullable))completion {
    NSURLRequest *request = [self _getRoomNoteRequestWithRoomCode:roomCode];
    
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            completion(nil, error);
            return;
        }
        if (!data) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNetworkError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSUTF8StringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kServerError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            NSError *error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier]
                                                 code:kNoDataError
                                             userInfo:nil];
            completion(nil, error);
            return;
        }
        RoomNote *roomNote = [self _parseGetRoomNoteResponseFromJSON:jsonObject];
        RoomNoteItem *roomNoteItem = [[RoomNoteItem alloc] initWithNote:roomNote];
        completion(roomNoteItem, nil);
    }] resume];
}

- (void)updateRoomNoteWithInfo:(NSDictionary *)info {
    
    NSString *status = info[@"status"];
    NSString *header = info[@"header"];
    NSString *desc = info[@"desc"];
    NSString *roomCode = info[@"roomCode"];
    
    NSURLRequest *request = [self _putRoomNoteRequestWithRoomCode:roomCode
                                                           header:header
                                                             desc:desc
                                                           status:status];
    
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            [self.roomNoteDelegate onFinishUpdateNote:nil error:NoteHandlerError];
            return;
        }
        if (!data) {
            [self.roomNoteDelegate onFinishUpdateNote:nil error:NoteHandlerError];
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSASCIIStringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            [self.roomNoteDelegate onFinishUpdateNote:nil error:NoteHandlerError];
            return;
        }
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            [self.roomNoteDelegate onFinishUpdateNote:nil error:NoteHandlerError];
            return;
        }
        RoomNote *note = [self _parsePutRoomNoteResponseFromJSON:jsonObject];
        RoomNoteItem *noteItem = [[RoomNoteItem alloc] initWithNote:note];
        // Confirm there is no error
        [self.roomNoteDelegate onFinishUpdateNote:noteItem error:NoteHandlerNoError];
    }] resume];
}

#pragma mark - Get Room Note

- (NSURLRequest *)_getRoomNoteRequestWithRoomCode:(NSString *)roomCode {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", [[AccountManager sharedManager] getCurrentUserAccessToken]]};
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:roomCode forKey:@"room_code"];
    
    NSURLRequest *request = [OAuth URLRequestURL:kGetRoomNoteURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"GET"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

- (RoomNote *)_parseGetRoomNoteResponseFromJSON:(id)jsonObject {
    NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
    
    NSDictionary *metaData = jsonDictionary[@"data"];
    
    NSString *status = metaData[@"status"];
    NSString *header = metaData[@"header"];
    NSString *desc = metaData[@"description"];
    
    RoomNote *note = [[RoomNote alloc] initWithHeader:header
                                                 desc:desc
                                               status:status];
    return note;
}

#pragma mark - Update Room Note

- (NSURLRequest *)_putRoomNoteRequestWithRoomCode:(NSString *)roomCode
                                           header:(NSString *)header
                                             desc:(NSString *)desc
                                           status:(NSString *)status {
    NSDictionary *headerValues = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", [[AccountManager sharedManager] getCurrentUserAccessToken]]};
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:roomCode forKey:@"room_code"];
    if (header) {
        [params setObject:header forKey:@"header"];
    }
    if (desc) {
        [params setObject:desc forKey:@"description"];
    }
    [params setObject:status forKey:@"status"];
    
    NSURLRequest *request = [OAuth URLRequestURL:kPutRoomNoteURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"PUT"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:headerValues];
    return request;
}

- (RoomNote *)_parsePutRoomNoteResponseFromJSON:(id)jsonObject {
    NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
    
    NSDictionary *metaData = jsonDictionary[@"data"];
    
    NSString *header = metaData[@"header"];
    NSString *desc = metaData[@"description"];
    NSString *status = metaData[@"status"];
    
    RoomNote *note = [[RoomNote alloc] initWithHeader:header
                                                 desc:desc
                                               status:status];
    return note;
}

@end
