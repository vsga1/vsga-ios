//
//  SignUpHandler.m
//  VStudy
//
//  Created by LAP14011 on 19/02/2023.
//

#import "SignUpHandler.h"

#import "Macro.h"

#import "Scope.h"

#import "OAuth.h"

#import "APIResponse.h"

@implementation SignUpHandler

- (void)signUpWithInfo:(NSDictionary *)info {
    NSString *email = info[@"email"];
    NSString *username = info[@"username"];
    NSString *password = info[@"password"];
    
    NSURLRequest *request = [self _getSignUpURLRequestWithEmail:email
                                                       username:username
                                                       password:password];
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            [self.delegate onFinishSignUpWithErrorCode:SignUpHandlerErrorNotValidData message:@"Error! Try again"];
            return;
        }
        if (!data) {
            [self.delegate onFinishSignUpWithErrorCode:SignUpHandlerErrorServerError message:@"Error! Try again"];
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSASCIIStringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            [self.delegate onFinishSignUpWithErrorCode:SignUpHandlerErrorNotValidData message:@"Error! Try again"];
            return;
        }
        NSLog(@"[DEBUG] %s : sign up response: %@", __func__, responseDataString);
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
            NSDictionary *metaData = jsonDictionary[@"meta"];
            NSString *message = metaData[@"message"];
            [self.delegate onFinishSignUpWithErrorCode:SignUpHandlerErrorNotValidData message:message];
            return;
        }
    
        // Confirm there is no error
        [self.delegate onFinishSignUpWithErrorCode:SignUpHandlerNoError message:@"Sign up successfully"];
    }] resume];
}

- (NSURLRequest *)_getSignUpURLRequestWithEmail:(NSString *)email
                             username:(NSString *)username
                             password:(NSString *)password {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:username forKey:@"username"];
    [params setObject:password forKey:@"password"];
    [params setObject:email forKey:@"email"];
    
    NSURLRequest *request = [OAuth URLRequestURL:kSignUpURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"POST"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:nil];
    return request;
}

@end
