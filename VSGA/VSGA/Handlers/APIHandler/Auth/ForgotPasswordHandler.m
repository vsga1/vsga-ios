//
//  ForgotPasswordHandler.m
//  VStudy
//
//  Created by LAP14011 on 19/02/2023.
//

#import "ForgotPasswordHandler.h"

#import "Macro.h"

#import "Scope.h"

#import "OAuth.h"

#import "APIResponse.h"

@implementation ForgotPasswordHandler

- (void)forgotPasswordWithInfo:(NSDictionary *)info {
    NSString *email = info[@"email"];
    
    NSURLRequest *request = [self _getForgotURLRequestWithEmail:email];

    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            [self.delegate onFinishForgotPasswordWithErrorCode:ForgotPasswordHandlerErrorNotValidData];
            return;
        }
        if (!data) {
            [self.delegate onFinishForgotPasswordWithErrorCode:ForgotPasswordHandlerErrorServerError];
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSASCIIStringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            [self.delegate onFinishForgotPasswordWithErrorCode:ForgotPasswordHandlerErrorNotValidData];
            return;
        }
        NSLog(@"[DEBUG] %s : forgot password response: %@", __func__, responseDataString);
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            [self.delegate onFinishForgotPasswordWithErrorCode:ForgotPasswordHandlerErrorNotValidData];
            return;
        }
    
        // Confirm there is no error
        [self.delegate onFinishForgotPasswordWithErrorCode:ForgotPasswordHandlerNoError];
    }] resume];
}

- (NSURLRequest *)_getForgotURLRequestWithEmail:(NSString *)email {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:email forKey:@"email"];
    
    NSURLRequest *request = [OAuth URLRequestURL:kForgotPasswordURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"GET"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:nil];
    return request;
}

@end
