//
//  SignUpHandler.h
//  VStudy
//
//  Created by LAP14011 on 19/02/2023.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SignUpHandlerErrorCode) {
    SignUpHandlerErrorInvalidURL,
    SignUpHandlerErrorNetworkError,
    SignUpHandlerErrorNotValidData,
    SignUpHandlerErrorServerError,
    SignUpHandlerNoError
};

@protocol SignUpHandlerDelegate <NSObject>

- (void)onFinishSignUpWithErrorCode:(SignUpHandlerErrorCode)errorCode message:(NSString *)message;

@end

@interface SignUpHandler : NSObject

@property (nonatomic, weak) id<SignUpHandlerDelegate> delegate;

- (void)signUpWithInfo:(NSDictionary *)info;

@end
