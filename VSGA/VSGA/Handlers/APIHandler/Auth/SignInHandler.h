//
//  SignInHandler.h
//  VStudy
//
//  Created by LAP14011 on 18/02/2023.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SignInHandlerErrorCode) {
    SignInHandlerErrorInvalidURL,
    SignInHandlerErrorNetworkError,
    SignInHandlerErrorNotValidData,
    SignInHandlerErrorServerError,
    SignInHandlerNoError
};

typedef NS_ENUM(NSUInteger, AuthenticationState) {
    GettingAccessToken,
    SavingUserInfo
};


NS_ASSUME_NONNULL_BEGIN

@protocol SignInHandlerDelegate <NSObject>

- (void)onFinishGettingAccessTokenWithErrorCode:(SignInHandlerErrorCode)errorCode;

- (void)onFinishSavingUserInfo:(SignInHandlerErrorCode)errorCode;

@end

@interface SignInHandler : NSObject

@property (nonatomic, weak) id<SignInHandlerDelegate> delegate;

- (void)startAuthenticationProcessWithInfo:(NSDictionary *)info;

NS_ASSUME_NONNULL_END

@end
