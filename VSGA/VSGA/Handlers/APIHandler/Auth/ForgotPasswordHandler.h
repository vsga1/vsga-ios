//
//  ForgotPasswordHandler.h
//  VStudy
//
//  Created by LAP14011 on 19/02/2023.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ForgotPasswordHandlerErrorCode) {
    ForgotPasswordHandlerErrorInvalidURL,
    ForgotPasswordHandlerErrorNetworkError,
    ForgotPasswordHandlerErrorNotValidData,
    ForgotPasswordHandlerErrorServerError,
    ForgotPasswordHandlerNoError
};

@protocol ForgotPasswordHandlerDelegate <NSObject>

- (void)onFinishForgotPasswordWithErrorCode:(ForgotPasswordHandlerErrorCode)errorCode;

@end

@interface ForgotPasswordHandler : NSObject

@property (nonatomic, weak) id<ForgotPasswordHandlerDelegate> delegate;

- (void)forgotPasswordWithInfo:(NSDictionary *)info;

@end


