//
//  ResetPasswordHandler.m
//  VStudy
//
//  Created by LAP14011 on 20/02/2023.
//

#import "ResetPasswordHandler.h"

#import "Macro.h"

#import "Scope.h"

#import "OAuth.h"

#import "APIResponse.h"

@implementation ResetPasswordHandler

- (void)resetPasswordWithInfo:(NSDictionary *)info {
    NSString *token = info[@"token"];
    NSString *password = info[@"password"];
    
    NSURLRequest *request = [self _putResetPasswordURLRequestWithPassword:password token:token];

    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            [self.delegate onFinishResetPasswordWithErrorCode:ResetPasswordHandlerErrorNotValidData];
            return;
        }
        if (!data) {
            [self.delegate onFinishResetPasswordWithErrorCode:ResetPasswordHandlerErrorServerError];
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSASCIIStringEncoding];
        id jsonObject = [APIResponse isValidResponse:responseDataString];
        if (!jsonObject) {
            [self.delegate onFinishResetPasswordWithErrorCode:ResetPasswordHandlerErrorNotValidData];
            return;
        }
        NSLog(@"[DEBUG] %s : reset password response: %@", __func__, responseDataString);
        
        BOOL isSuccessResponse = [APIResponse parseResponseSuccessFromJSON:jsonObject];
        if (!isSuccessResponse) {
            [self.delegate onFinishResetPasswordWithErrorCode:ResetPasswordHandlerErrorNotValidData];
            return;
        }
    
        // Confirm there is no error
        [self.delegate onFinishResetPasswordWithErrorCode:ResetPasswordHandlerNoError];
    }] resume];
}

- (NSURLRequest *)_putResetPasswordURLRequestWithPassword:(NSString *)password token:(NSString *)token {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:password forKey:@"password"];
    [params setObject:token forKey:@"token"];
    
    NSURLRequest *request = [OAuth URLRequestURL:kResetPasswordURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"PUT"
                                    dataEncoding:OAuthContentTypeJsonObject
                                    headerValues:nil];
    return request;
}

@end
