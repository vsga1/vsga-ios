//
//  SignInHandler.m
//  VStudy
//
//  Created by LAP14011 on 18/02/2023.
//

#import "SignInHandler.h"

#import "Macro.h"

#import "Scope.h"

#import "OAuth.h"

#import "AccountManager.h"

@interface SignInHandler () {
    NSString *accessToken;
    AuthenticationState currentState;
}

- (NSURLRequest *)_getAccessTokenURLRequestWithUsername:(NSString *)username
                                               password:(NSString *)password;

@end

@implementation SignInHandler

- (instancetype)init {
    self = [super init];
    if (self) {
        // Initialize ivar
       
        accessToken = @"";
        
        currentState = GettingAccessToken;
    }
    return self;
}

#pragma mark - Operations

- (void)startAuthenticationProcessWithInfo:(NSDictionary *)info {
    switch (currentState) {
        case GettingAccessToken:
            // Get the access token
            [self _getAccessTokenWithInfo:info];
            NSLog(@"[DEBUG] %s : currentState: GettingAccessToken", __func__);
            break;
        case SavingUserInfo:
            [self _saveUserInfoWithInfo:info];
            NSLog(@"[DEBUG] %s: currentState: SavingUserInfo", __func__);
            break;
        default:
            break;
    }
}

#pragma mark - Private methods

- (void)_getAccessTokenWithInfo:(NSDictionary *)info {
    if (currentState != GettingAccessToken) {
        NSLog(@"[ERROR] %s : NOT CORRECT STATE OR NOT SUFFICENT INFO", __func__);
        return;
    }
    NSString *username = info[@"username"];
    NSString *password = info[@"password"];
    
    NSURLRequest *request = [self _getAccessTokenURLRequestWithUsername:username
                                                               password:password];
    @weakify(self)
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data,
                                                         NSURLResponse *response,
                                                         NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"[ERROR] %s : error: %@",
                  __func__,
                  error.localizedDescription);
            [self.delegate onFinishGettingAccessTokenWithErrorCode:SignInHandlerErrorServerError];
            [self _resetVariables];
            return;
        }
        if (!data) {
            [self.delegate onFinishGettingAccessTokenWithErrorCode:SignInHandlerErrorServerError];
            [self _resetVariables];
            return;
        }
        NSString *responseDataString = [[NSString alloc] initWithData:data
                                                             encoding:NSASCIIStringEncoding];
        id jsonObject = [self _isValidAccessTokenResponse:responseDataString];
        if (!jsonObject) {
            [self.delegate onFinishGettingAccessTokenWithErrorCode:SignInHandlerErrorNotValidData];
            [self _resetVariables];
            return;
        }
        NSLog(@"[DEBUG] %s : request access token response: %@", __func__, responseDataString);
        
        BOOL isAccessTokenParsed = [self _isParseAccessTokenSuccessfulFromJSON:jsonObject];
        if (!isAccessTokenParsed) {
            [self.delegate onFinishGettingAccessTokenWithErrorCode:SignInHandlerErrorNotValidData];
            [self _resetVariables];
            return;
        }
        
        NSLog(@"[DEBUG] %s : token parsed: %@",
              __func__,
              self->accessToken);
        // Confirm there is no error
        [self.delegate onFinishGettingAccessTokenWithErrorCode:SignInHandlerNoError];
        // Changing state
        self->currentState = SavingUserInfo;
        
        [self startAuthenticationProcessWithInfo:info];
    }] resume];
}

- (void)_saveUserInfoWithInfo:(NSDictionary *)info {
    if (currentState != SavingUserInfo || [accessToken isEqualToString:@""]) {
        NSLog(@"[ERROR] %s : NOT CORRECT STATE OR NOT SUFFICENT INFO", __func__);
        return;
    }
    NSString *username = info[@"username"];
    BOOL isRemember = [info[@"isRemember"] boolValue];
    @weakify(self)
    [[AccountManager sharedManager] setAccountInfoWithUsername:username
                                               userAccessToken:accessToken
                                                    isRemember:isRemember
                                                    completion:^{
        @strongify(self)
        if (![[AccountManager sharedManager] isUserLoggedIn]) {
            [self.delegate onFinishSavingUserInfo:SignInHandlerErrorNotValidData];
            self->currentState = GettingAccessToken;
            [self _resetVariables];
            return;
        }
        // Changing state
        self->currentState = GettingAccessToken;
        [self.delegate onFinishSavingUserInfo:SignInHandlerNoError];
    }];
}

#pragma mark - Token URL & URLRequest

- (NSURLRequest *)_getAccessTokenURLRequestWithUsername:(NSString *)username
                                               password:(NSString *)password {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:username forKey:@"username"];
    [params setObject:password forKey:@"password"];

    NSURLRequest *request = [OAuth URLRequestURL:kSignInURL
                                      parameters:params
                                     accessToken:nil
                                   requestMethod:@"POST"
                                    dataEncoding:OAuthContentTypeUrlEncodedForm
                                    headerValues:nil];
    return request;
}

- (BOOL)_isParseAccessTokenSuccessfulFromJSON:(id)jsonObject {
    BOOL isSuccessful = NO;
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSAssert(NO, @"JSON return array");
    }
    else {
        NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
        accessToken = jsonDictionary[@"access_token"];
        if (![accessToken isEqualToString:@""]) {
            isSuccessful = YES;
        }
    }
    
    return isSuccessful;
}

- (id)_isValidAccessTokenResponse:(NSString *)responseString {
    if (responseString == nil) {
        return nil;
    }
    NSData *jsonData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    
    /// Note that JSONObjectWithData will return either an NSDictionary or an NSArray, depending whether your JSON string represents an a dictionary or an array.
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    if (error) {
        NSLog(@"Error parsing JSON: %@", error);
        return nil;
    }
    else {
        if ([jsonObject isKindOfClass:[NSArray class]]) {
            NSArray *jsonArray = (NSArray *)jsonObject;
            return jsonArray;
            
        }
        else {
            NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
            accessToken = jsonDictionary[@"access_token"];
            if (!jsonDictionary[@"access_token"]) {
                return nil;
            }
            return jsonDictionary;
        }
    }
}

- (void)_resetVariables {
    accessToken = @"";
}
@end
