//
//  ResetPasswordHandler.h
//  VStudy
//
//  Created by LAP14011 on 20/02/2023.
//


#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ResetPasswordHandlerErrorCode) {
    ResetPasswordHandlerErrorInvalidURL,
    ResetPasswordHandlerErrorNetworkError,
    ResetPasswordHandlerErrorNotValidData,
    ResetPasswordHandlerErrorServerError,
    ResetPasswordHandlerNoError
};

@protocol ResetPasswordHandlerDelegate <NSObject>

- (void)onFinishResetPasswordWithErrorCode:(ResetPasswordHandlerErrorCode)errorCode;

@end

@interface ResetPasswordHandler : NSObject

@property (nonatomic, weak) id<ResetPasswordHandlerDelegate> delegate;

- (void)resetPasswordWithInfo:(NSDictionary *)info;

@end
