//
//  AppDelegate.h
//  VSGA
//
//  Created by LAP14011 on 31/01/2023.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@property (class, readonly, strong) AppDelegate *shared;

- (void)updateView;

@end

