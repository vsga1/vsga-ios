//
//  NSUserDefaults+Extension.h
//  VStudy
//
//  Created by LAP14011 on 18/02/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSUserDefaults (Extension)

- (NSError * _Nullable)saveUserObject:(id<NSCoding>)object
                                  key:(NSString *)key;

- (id<NSCoding>)loadUserObjectWithKey:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
