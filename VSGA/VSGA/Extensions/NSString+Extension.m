//
//  NSString+Extension.m
//  VSGA
//
//  Created by LAP14011 on 06/02/2023.
//

#import "NSString+Extension.h"

@implementation NSString (Extension)

- (CGSize)sizeOfStringWithStyledFont:(UIFont *)styledFont withSize:(CGSize)size {
    
    CGSize estimatedSize = size;
    if (CGSizeEqualToSize(size, CGSizeZero)) {
        estimatedSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, CGFLOAT_MAX);
    }
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:self attributes:@{NSFontAttributeName:styledFont}];
    CGSize tempSize = [attributedText boundingRectWithSize:estimatedSize
                                                   options:(NSStringDrawingUsesLineFragmentOrigin)
                                                   context:nil].size;
    return CGSizeMake(ceilf(tempSize.width), ceilf(tempSize.height));
}

- (CGSize)calculateSizeWithTargetSize:(CGSize)targetSize withFont:(UIFont *)font {
    CGSize size = [self boundingRectWithSize:targetSize
                                    options:NSStringDrawingUsesLineFragmentOrigin
                                 attributes:@{NSFontAttributeName : font}
                                     context:nil].size;
    return CGSizeMake(ceil(size.width), ceil(size.height));
}
@end
