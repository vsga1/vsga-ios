//
//  UIImage+Extension.h
//  VStudy
//
//  Created by TamNVM on 10/06/2023.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Extension)

- (UIImage *)resizeWithTargetSize:(CGSize)targetSize;

@end

NS_ASSUME_NONNULL_END
