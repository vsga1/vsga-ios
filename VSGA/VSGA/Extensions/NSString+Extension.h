//
//  NSString+Extension.h
//  VSGA
//
//  Created by LAP14011 on 06/02/2023.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Extension)

- (CGSize)sizeOfStringWithStyledFont:(UIFont *)styledFont withSize:(CGSize)size;

- (CGSize)calculateSizeWithTargetSize:(CGSize)targetSize withFont:(UIFont *)font;

@end

NS_ASSUME_NONNULL_END
