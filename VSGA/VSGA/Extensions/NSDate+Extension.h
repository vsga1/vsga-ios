//
//  NSDate+Extension.h
//  VStudy
//
//  Created by LAP14011 on 05/03/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDate (Extension)

@property NSInteger currentDay;

@property NSInteger day;

- (NSString *)convertToNSStringWithDateFormat:(NSString *)dateFormat;

- (NSString *)convertToTimestampStringShowFull;

- (NSString *)convertToTimestampStringShowLess;

@end

NS_ASSUME_NONNULL_END
