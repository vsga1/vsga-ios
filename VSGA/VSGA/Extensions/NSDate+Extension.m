//
//  NSDate+Extension.m
//  VStudy
//
//  Created by LAP14011 on 05/03/2023.
//

#import "NSDate+Extension.h"
#import "Macro.h"

@implementation NSDate (Extension)

- (NSInteger)currentDay {
    NSDateComponents *component = [[NSCalendar currentCalendar] components: NSCalendarUnitDay
                                                                   fromDate:[NSDate date]];
    return component.day;
}

- (void)setCurrentDay:(NSInteger)currentDay {
    if (currentDay) {
        self.currentDay = currentDay;
    }
}

- (NSInteger)day {
    NSDateComponents *component = [[NSCalendar currentCalendar] components: NSCalendarUnitDay
                                                                   fromDate:self];
    return component.day;
}

- (void)setDay:(NSInteger)day {
    if (day) {
        self.day = day;
    }
}

- (NSString *)convertToNSStringWithDateFormat:(NSString *)dateFormat {
    NSString * deviceLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    NSLocale * locale = [[NSLocale alloc] initWithLocaleIdentifier:deviceLanguage];
    [dateFormatter setLocale:locale];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:dateFormat];
    NSString * dateString = [dateFormatter stringFromDate:self];
    return dateString;
}

- (NSString *)convertToTimestampStringShowFull {
    NSDateComponents *timeDurationComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:self toDate:[NSDate date] options:0];
    NSString *timestampString;
    NSInteger duration = [timeDurationComponents day];
    if (duration == 0 && self.day == self.currentDay) {
        //today
        timestampString = [self convertToNSStringWithDateFormat:kTimestampShowLess];
        timestampString = [timestampString stringByAppendingString:kTimestampTodayText];
        return timestampString;
    }
    //different day
    timestampString = [self convertToNSStringWithDateFormat:kTimestampShowFull];
    return timestampString;
}

- (NSString *)convertToTimestampStringShowLess {
    NSString *timestampString = [self convertToNSStringWithDateFormat:kTimestampShowLess];
    return timestampString;
}

@end
