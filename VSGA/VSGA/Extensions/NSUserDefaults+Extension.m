//
//  NSUserDefaults+Extension.m
//  VStudy
//
//  Created by LAP14011 on 18/02/2023.
//

#import "NSUserDefaults+Extension.h"

#import "User.h"

@implementation NSUserDefaults (Extension)


- (NSError *)saveUserObject:(id<NSCoding>)object
                        key:(NSString *)key {
    NSError *localError = nil;
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object
                                                  requiringSecureCoding:YES
                                                                  error:&localError];
    if (localError) {
        NSLog(@"[DEBUG] %s : error occured: %@",
              __func__,
              localError);
        return localError;
    }
    [self setObject:encodedObject forKey:key];
    [self synchronize];
    return nil;
}

- (id<NSCoding>)loadUserObjectWithKey:(NSString *)key {
    NSError *localError = nil;
    NSData *encodedObject = [self objectForKey:key];
    NSSet *setOfClasses = [NSSet setWithObjects:[User class], [NSString class], nil];
    id<NSCoding> object = [NSKeyedUnarchiver unarchivedObjectOfClasses:setOfClasses
                                                              fromData:encodedObject
                                                                 error:&localError];
    if (localError) {
        NSLog(@"[DEBUG] %s : error occured: %@",
              __func__,
              localError);
        return nil;
    }
    
    return object;
}

@end
