//
//  UIImage+Extension.m
//  VStudy
//
//  Created by TamNVM on 10/06/2023.
//

#import "UIImage+Extension.h"

@implementation UIImage (Extension)

- (UIImage *)resizeWithTargetSize:(CGSize)targetSize {
    CGSize size = self.size;

    CGFloat widthRatio = targetSize.width / size.width;
    CGFloat heightRatio = targetSize.height / size.height;

    CGFloat scaleFactor = MAX(widthRatio, heightRatio);
    CGSize newSize = CGSizeMake(size.width * scaleFactor, size.height * scaleFactor);

    UIGraphicsImageRenderer *renderer = [[UIGraphicsImageRenderer alloc] initWithSize:newSize];
    UIImage *resizedImage = [renderer imageWithActions:^(UIGraphicsImageRendererContext * _Nonnull rendererContext) {
        [self drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    }];

    return resizedImage;
}

@end
