//
//  RoomManager.m
//  VStudy
//
//  Created by LAP14011 on 16/03/2023.
//

#import "RoomManager.h"

@interface RoomManager()

@property (strong, nonatomic) TSMutableArray <User *> *memberArray;

@property (strong, nonatomic) RoomItem *currentRoom;

@property (nonatomic, strong) dispatch_queue_t queue;

@end

@implementation RoomManager

+ (instancetype)sharedManager {
    static dispatch_once_t onceToken;
    static RoomManager *sharedManager = nil;
    dispatch_once(&onceToken, ^{
        sharedManager = [[RoomManager alloc] init];
        sharedManager.queue = dispatch_queue_create("com.example.RoomManager", DISPATCH_QUEUE_CONCURRENT);
    });
    return sharedManager;
}

- (RoomItem *)getCurrentRoom {
    __block RoomItem *currentRoom = nil;
    dispatch_sync(self.queue, ^{
        if (self.currentRoom) {
            currentRoom = self.currentRoom;
        }
    });
    return currentRoom;
}

- (NSString *)getRoomCode {
    __block NSString *currentRoomCode = nil;
    dispatch_sync(self.queue, ^{
        if (self.currentRoom) {
            currentRoomCode = self.currentRoom.code;
        }
    });
    return currentRoomCode;
}

- (void)joinRoom:(RoomItem *)room {
    dispatch_barrier_async(self.queue, ^{
        self.currentRoom = room;
    });
}

- (void)leaveRoom {
    dispatch_barrier_async(self.queue, ^{
        self.currentRoom = nil;
    });
}

- (void)createMemberArrayWithArray:(NSArray *)memberArray {
    dispatch_barrier_async(self.queue, ^{
        self.memberArray = [[TSMutableArray alloc] initWithArray:memberArray];
        // Check if the delegate is set and respond to the delegate method
        if ([self.delegate respondsToSelector:@selector(didUpdateMemberArray:)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate didUpdateMemberArray:self.memberArray];
            });
        }
    });
}

//- (void)addMember:(User *)member {
//    dispatch_barrier_async(self.queue, ^{
//        [self.memberArray addObject:member];
//    });
//}

- (NSArray *)getMemberArray {
    __block NSArray *memberArray = nil;
    dispatch_sync(self.queue, ^{
        memberArray = self.memberArray;
    });
    return memberArray;
}

@end
