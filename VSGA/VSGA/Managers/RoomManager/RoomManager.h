//
//  RoomManager.h
//  VStudy
//
//  Created by LAP14011 on 16/03/2023.
//

#import <Foundation/Foundation.h>
#import "TSMutableArray.h"
#import "RoomItem.h"
#import "User.h"

NS_ASSUME_NONNULL_BEGIN

@protocol RoomManagerDelegate <NSObject>

@optional

- (void)didUpdateMemberArray:(TSMutableArray <User *> *)memberArray;

- (void)didUpdateRoomItem:(RoomItem *)roomItem;

@end

@interface RoomManager : NSObject

@property (nonatomic, weak) id<RoomManagerDelegate> delegate;

+ (instancetype)sharedManager;

- (RoomItem *)getCurrentRoom;

- (NSString *)getRoomCode;

- (void)joinRoom:(RoomItem *)room;

- (void)leaveRoom;

- (void)createMemberArrayWithArray:(NSArray *)memberArray;

//- (void)addMember:(User *)member;

- (NSArray *)getMemberArray;

@end

NS_ASSUME_NONNULL_END
