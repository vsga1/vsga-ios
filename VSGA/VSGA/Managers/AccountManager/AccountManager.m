//
//  AccountManager.m
//  VStudy
//
//  Created by LAP14011 on 18/02/2023.
//


#import "AccountManager.h"

#import "NSUserDefaults+Extension.h"

#define kCurrentUserKey @"currentUser"
#define kRememberUsername @"rememberUsername"
#define kIsLoggedIn @"isLoggedIn"

@interface AccountManager()

@property (nonatomic, strong) User *currentUser;

@property (nonatomic, strong) NSString *rememberUsername;

@property (nonatomic, assign) BOOL isLoggedIn;

@property (nonatomic, strong) dispatch_queue_t queue;

@end

@implementation AccountManager

+ (instancetype)sharedManager {
    static dispatch_once_t onceToken;
    static AccountManager *sharedManager = nil;
    dispatch_once(&onceToken, ^{
        sharedManager = [[AccountManager alloc] init];
    });
    return sharedManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.queue = dispatch_queue_create("com.example.AccountManager", DISPATCH_QUEUE_CONCURRENT);
        self.currentUser = (User *)[NSUserDefaults.standardUserDefaults loadUserObjectWithKey:kCurrentUserKey];
        self.rememberUsername = [[NSUserDefaults standardUserDefaults] objectForKey:kRememberUsername];
        self.isLoggedIn = [NSUserDefaults.standardUserDefaults boolForKey:kIsLoggedIn];
    }
    return self;
}

- (User *)getCurrentUser {
    __block User *currentUser = nil;
    dispatch_sync(self.queue, ^{
        if (self.currentUser) {
            currentUser = self.currentUser;
        }
    });
    return currentUser;
}

- (NSString *)getCurrentUsername {
    __block NSString *currentUsername = nil;
    dispatch_sync(self.queue, ^{
        if (self.currentUser) {
            currentUsername = self.currentUser.username;
        }
    });
    return currentUsername;
}

- (NSString *)getCurrentUserAccessToken {
    __block NSString *currentAccessToken = nil;
    dispatch_sync(self.queue, ^{
        if (self.currentUser) {
            currentAccessToken = self.currentUser.accessToken;
        }
    });
    return currentAccessToken;
}

- (NSString *)getRememberUsername {
    __block NSString *rememberUsername = nil;
    dispatch_sync(self.queue, ^{
        if (self.rememberUsername) {
            rememberUsername = self.rememberUsername;
        }
    });
    return rememberUsername;
}

- (BOOL)isUserLoggedIn {
    return self.isLoggedIn;
}

- (void)setAccountInfoWithUsername:(NSString *)username
                   userAccessToken:(NSString *)accessToken
                        isRemember:(BOOL)isRemember
                    completion:(void (^)(void))completion {
    dispatch_barrier_async(self.queue, ^{
        /// Save user is logged in
        if (self.isLoggedIn) {
            NSLog(@"[ERROR] logged in already, log out before using this");
            return;
        }

        User *user = [[User alloc] initWithUsername:username
                                        accessToken:accessToken];

        NSError *error = [NSUserDefaults.standardUserDefaults saveUserObject:user
                                                                         key:kCurrentUserKey];
        if (error == nil) {
            self.currentUser = user;
            [NSUserDefaults.standardUserDefaults setBool:YES forKey:kIsLoggedIn];
            self.isLoggedIn = YES;
        }

        /// Save username when click remember me
        if (isRemember) {
            [[NSUserDefaults standardUserDefaults] setObject:username forKey:kRememberUsername];
            self.rememberUsername = username;
        }
        else {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:kRememberUsername];
            self.rememberUsername = nil;
        }
        
        if (completion) {
            completion();
        }
    });
}

- (void)removeAccountInfoWithCompletion:(void (^)(void))completion {
    dispatch_barrier_async(self.queue, ^{
        [NSUserDefaults.standardUserDefaults removeObjectForKey:kCurrentUserKey];
        [NSUserDefaults.standardUserDefaults removeObjectForKey:kIsLoggedIn];
        self.currentUser = nil;
        self.isLoggedIn = FALSE;
        if (completion) {
            completion();
        }
    });
}

@end
