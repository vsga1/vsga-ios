//
//  AccountManager.h
//  VStudy
//
//  Created by LAP14011 on 18/02/2023.
//

#import <Foundation/Foundation.h>
#import "User.h"
NS_ASSUME_NONNULL_BEGIN

@interface AccountManager : NSObject

+ (instancetype)sharedManager;

- (User *)getCurrentUser;

- (NSString *)getCurrentUsername;

- (NSString *)getCurrentUserAccessToken;

- (NSString *)getRememberUsername;

- (BOOL)isUserLoggedIn;

- (void)setAccountInfoWithUsername:(NSString *)username
                   userAccessToken:(NSString *)accessToken
                        isRemember:(BOOL)isRemember
                        completion:(void (^)(void))completion;

- (void)removeAccountInfoWithCompletion:(void (^)(void))completion;


@end

NS_ASSUME_NONNULL_END
