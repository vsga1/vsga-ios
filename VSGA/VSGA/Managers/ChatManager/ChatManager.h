//
//  ChatManager.h
//  VStudy
//
//  Created by LAP14011 on 12/03/2023.
//

#import <Foundation/Foundation.h>
#import "MessageItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface ChatManager : NSObject

+ (instancetype)sharedManager;

- (void)insertAtFirst:(MessageItem *)message;
- (NSArray *)getMessages;
- (void)resetMessages;

- (void)increaseUnreadCount:(NSInteger)number;
- (NSInteger)getUnreadCount;
- (void)resetUnreadCount;

@end

NS_ASSUME_NONNULL_END
