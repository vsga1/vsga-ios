//
//  ChatManager.m
//  VStudy
//
//  Created by LAP14011 on 12/03/2023.
//

#import "ChatManager.h"

@interface ChatManager()

@property (nonatomic, strong) NSMutableArray<MessageItem *> *items;

@property (nonatomic, assign) NSInteger unreadCount;

@property (nonatomic, strong) dispatch_queue_t queue;

@end

@implementation ChatManager

+ (instancetype)sharedManager {
    static dispatch_once_t onceToken;
    static ChatManager *sharedManager = nil;
    dispatch_once(&onceToken, ^{
        sharedManager = [[ChatManager alloc] init];
        sharedManager.items = [NSMutableArray array];
        sharedManager.unreadCount = 0;
        sharedManager.queue = dispatch_queue_create("com.example.ChatManager", DISPATCH_QUEUE_CONCURRENT);
    });
    return sharedManager;
}

- (void)insertAtFirst:(MessageItem *)message {
    dispatch_barrier_async(self.queue, ^{
        [self.items insertObject:message atIndex:0];
    });
}

- (void)addMessage:(MessageItem *)message {
    dispatch_barrier_async(self.queue, ^{
        [self.items addObject:message];
    });
}

- (void)resetMessages {
    dispatch_barrier_async(self.queue, ^{
        [self.items removeAllObjects];
    });
}

- (NSArray *)getMessages {
    __block NSArray *messages = nil;
    dispatch_sync(self.queue, ^{
        messages = [NSArray arrayWithArray:self.items];
    });
    return messages;
}

- (void)increaseUnreadCount:(NSInteger)number{
    self.unreadCount += number;
}

- (NSInteger)getUnreadCount {
    return self.unreadCount;
}

- (void)resetUnreadCount {
    self.unreadCount = 0;
}

@end
