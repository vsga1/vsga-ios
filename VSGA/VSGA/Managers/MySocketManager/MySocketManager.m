//
//  MySocketManager.m
//  VStudy
//
//  Created by LAP14011 on 07/03/2023.
//

#import "MySocketManager.h"
#import <SocketIO/SocketIO-Swift.h>

#import "Macro.h"
#import "MessageItem.h"
#import "AccountManager.h"

@interface MySocketManager ()

@property (nonatomic, strong) SocketManager *manager;

@property (nonatomic, strong) SocketIOClient *socket;

@property (nonatomic, assign) BOOL isSocketConnected;

@end

@implementation MySocketManager {
    
    NSMutableArray *_pendingEvents;
}

+ (instancetype)sharedInstance {
    static MySocketManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MySocketManager alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    if (self = [super init]) {
        NSString *urlString = kSocketHost;
        NSString *token = [[AccountManager sharedManager] getCurrentUserAccessToken];
        // Create the URL for the socket connection
        NSURL *url = [NSURL URLWithString:kSocketHost];

        // Create the configuration dictionary with custom headers
        NSDictionary *headers = @{@"Authorization": [NSString stringWithFormat:@"Bearer %@", token]};
        NSDictionary *config = @{@"log": @YES, @"compress": @YES, @"extraHeaders": headers};

        // Create the SocketManager with the URL and configuration
        self.manager = [[SocketManager alloc] initWithSocketURL:url config:config];
        self.socket = [self.manager defaultSocket];
        _pendingEvents = [[NSMutableArray alloc] init];
        [self _addHandler];
    }
    return self;
}

- (void)_addHandler {
    __weak typeof(self) weakSelf = self;
    [self.socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        NSLog(@"tamnvm>> Socket connected");
        strongSelf.isSocketConnected = YES;
        [self emitPendingEvents];
    }];
}

- (void)connect {
    [self.socket connect];
}

- (void)disconnect {
    [self.socket disconnect];
    self.isSocketConnected = FALSE;
}


- (void)emit:(NSString *)event with:(NSDictionary *)data completion:(nullable void (^)(BOOL success))completion {
    if (self.isSocketConnected) {
        [self.socket emit:event with:@[data] completion:^{
            if (completion) {
                completion(YES);
            }
        }];
    } else {
        // Socket is not yet connected, so store the event to emit later
        [self addPendingEvent:event withData:data];
    }
}

- (void)addPendingEvent:(NSString *)eventName withData:(NSDictionary *)eventData {
    // Create a dictionary to store the event name and data
    NSMutableDictionary *event = [NSMutableDictionary dictionary];
    event[@"name"] = eventName;
    event[@"data"] = eventData;
    
    // Add the event to an array
    [_pendingEvents addObject:event];
}

- (void)emitPendingEvents {
    // Loop through the pending events and emit them
    for (NSDictionary *event in _pendingEvents) {
        NSString *eventName = event[@"name"];
        NSDictionary *eventData = event[@"data"];
        [self emit:eventName with:eventData completion:^(BOOL success) {
            
        }];
    }
    
    // Clear the pending events array
    [_pendingEvents removeAllObjects];
}

- (void)off:(NSString *)event{
    [self.socket off:event];
}

- (void)listenForJoinRoomEventWithCompletion:(void (^)(BOOL))completion {
    [self.socket on:kJoinRoomEvent callback:^(NSArray* data, SocketAckEmitter* ack) {
        if (completion) {
            completion(TRUE);
        }
    }];
}

- (void)listenForRoomDataEventWithCompletion:(void (^)(NSArray <User *> *))completion {
    __weak typeof(self) weakSelf = self;
    [self.socket on:kRoomDataEvent callback:^(NSArray * _Nonnull data, SocketAckEmitter * _Nonnull ack) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            NSDictionary *result = [data firstObject];
            NSArray *usersInfo = [result objectForKey:@"users"];
            NSMutableArray *userArray = [[NSMutableArray alloc] init];
            for (NSDictionary *userInfo in usersInfo) {
                NSString *username = [userInfo objectForKey:@"username"];
                NSString *userID = [userInfo objectForKey:@"id"];
                NSString *role = [userInfo objectForKey:@"role"];
                id avatarInfo = [userInfo objectForKey:@"avatar_img_url"];
//                NSString *roomCode = [userInfo objectForKey:@"roomCode"];
                
                NSString *avatarURLString = nil;
                if (avatarInfo != [NSNull null] && avatarInfo) {
                    avatarURLString = (NSString *)avatarInfo;
                }
                
                User *user = [[User alloc] initWithUsername:username
                                                     userID:userID
                                            avatarURLString:avatarURLString
                                                       role:role];
                [userArray addObject:user];
            }
            if (completion) {
                completion(userArray);
            }
        }
        
    }];
}

- (void)listenForReceiveMessageEventWithCompletion:(void (^)(MessageItem * _Nonnull))completion {
    __weak typeof(self) weakSelf = self;
    [self.socket on:kReceiveMessageEvent callback:^(NSArray * _Nonnull data, SocketAckEmitter * _Nonnull ack) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            NSDictionary *result = [data firstObject];
            NSString *text = [result objectForKey:@"text"];
            NSString *receiver = [result objectForKey:@"receiver"];
            NSString *sender = [result objectForKey:@"sender"];
            NSString *type = [result objectForKey:@"type"];
            MessageItem *message = [[MessageItem alloc] initWithMessaageID:[[NSUUID UUID] UUIDString]
                                                         andSenderUsername:sender
                                                               andReceiver:receiver
                                                                   andText:text
                                                                   andType:type];
            if (completion) {
                completion(message);
            }
        }
        
    }];
}

- (void)listenForOpenEditNoteWithCompletion:(void (^)(RoomNoteItem * _Nonnull))completion {
    __weak typeof(self) weakSelf = self;
    [self.socket on:kOpenEditNote callback:^(NSArray * _Nonnull data, SocketAckEmitter * _Nonnull ack) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            NSDictionary *result = [data firstObject];
            NSString *header = [result objectForKey:@"header"];
            NSString *desc = [result objectForKey:@"desc"];
    
            RoomNote *note = [[RoomNote alloc] initWithHeader:header
                                                         desc:desc
                                                       status:@"OPEN"];
            RoomNoteItem *item = [[RoomNoteItem alloc] initWithNote:note];
            
            if (completion) {
                completion(item);
            }
        }
        
    }];
}

- (void)listenForCloseEditNoteWithCompletion:(void (^)(BOOL))completion {
    __weak typeof(self) weakSelf = self;
    [self.socket on:kCloseEditNote callback:^(NSArray * _Nonnull data, SocketAckEmitter * _Nonnull ack) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            if (completion) {
                completion(TRUE);
            }
        }
        
    }];
}

- (void)listenForDoneRoomTaskWithCompletion:(void (^)(NSString *, NSString *, User * _Nonnull))completion {
    __weak typeof(self) weakSelf = self;
    [self.socket on:kDoneRoomTask callback:^(NSArray * _Nonnull data, SocketAckEmitter * _Nonnull ack) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            NSDictionary *result = [data firstObject];
            
            NSString *code = [result objectForKey:@"code"];
            NSString *username = [result objectForKey:@"username"];
            NSString *scheme = [result objectForKey:@"scheme"];
            NSString *avatar = [result objectForKey:@"avatar"];
            //TODO: find user in room manager
            User *user = [[User alloc] initWithUsername:username avatarURLString:avatar role:@"MEMBER"];
            
            if (completion) {
                completion(code, scheme, user);
            }
        }
        
    }];
}

- (void)listenForGetNewRoomTaskWithCompletion:(void (^)(RoomTaskItem * _Nonnull))completion {
    __weak typeof(self) weakSelf = self;
    [self.socket on:kGetNewRoomTask callback:^(NSArray * _Nonnull data, SocketAckEmitter * _Nonnull ack) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            NSDictionary *result = [data firstObject];
            NSDictionary *taskInfo = [result objectForKey:@"task"];
            
            NSString *code = [taskInfo objectForKey:@"code"];
            NSString *desc = [taskInfo objectForKey:@"description"];
            NSString *roomCode = [taskInfo objectForKey:@"room_code"];
            NSString *scheme = [taskInfo objectForKey:@"scheme"];
            NSString *status = [taskInfo objectForKey:@"status"];
            NSString *type = [taskInfo objectForKey:@"type"];
            
            Task *task = [[Task alloc] initWithCode:code
                                           roomCode:roomCode
                                             scheme:scheme
                                               desc:desc
                                             status:status
                                               type:type];
            RoomTaskItem *item = [[RoomTaskItem alloc] initWithTask:task doneUserArray:nil];
            
            if (completion) {
                completion(item);
            }
        }
        
    }];
}

@end
