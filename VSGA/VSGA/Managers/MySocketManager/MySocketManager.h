//
//  MySocketManager.h
//  VStudy
//
//  Created by LAP14011 on 07/03/2023.
//

#import <Foundation/Foundation.h>
#import "MessageItem.h"
#import "RoomNoteItem.h"
#import "RoomTaskItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface MySocketManager : NSObject

+ (instancetype)sharedInstance;

- (void)connect;

- (void)disconnect;

- (void)emit:(NSString *)event with:(NSDictionary *)data completion:(nullable void (^)(BOOL success))completion;

- (void)off:(NSString *)event;

//Room

- (void)listenForJoinRoomEventWithCompletion:(void (^)(BOOL success))completion;

- (void)listenForRoomDataEventWithCompletion:(void (^)(NSArray <User *> *))completion;

//Chat

- (void)listenForReceiveMessageEventWithCompletion:(void (^)(MessageItem *))completion;

//Note

- (void)listenForOpenEditNoteWithCompletion:(void (^)(RoomNoteItem *))completion;

- (void)listenForCloseEditNoteWithCompletion:(void (^)(BOOL))completion;

//Task

- (void)listenForDoneRoomTaskWithCompletion:(void (^)(NSString *, NSString *, User * _Nonnull))completion;

- (void)listenForGetNewRoomTaskWithCompletion:(void (^)(RoomTaskItem *))completion;

//Member

- (void)listenForPromoteMemberWithCompletion:(void (^)(NSString *, NSString *, User * _Nonnull))completion;

@end

NS_ASSUME_NONNULL_END
