//
//  MyMessage.m
//  VStudy
//
//  Created by LAP14011 on 11/03/2023.
//

#import "MyMessage.h"

@implementation MyMessage

- (instancetype)initWithText:(NSString *)text withSender:(NSString *)sender withReceiver:(id)receiver {
    self = [super init];
    if (self) {
        self.text = text;
        self.sender = sender;
        
    }
    return self;
}

@end
