//
//  MyMessage.h
//  VStudy
//
//  Created by LAP14011 on 11/03/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, MessageType) {
    MessageTypePublic,
    MessageTypeDirect
};

@interface MyMessage : NSObject

@property (strong, nonatomic) NSString *text;

@property (strong, nonatomic) NSString *sender;

@property (strong, nonatomic, nullable) id receiver;

@property (nonatomic, assign) MessageType messageType;

- (instancetype)initWithText:(NSString *)text
                  withSender:(NSString *)sender
                withReceiver:(id)receiver;
@end

NS_ASSUME_NONNULL_END
