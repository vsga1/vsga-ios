//
//  Room.h
//  VStudy
//
//  Created by LAP14011 on 06/03/2023.
//

#import <Foundation/Foundation.h>
#import "TSMutableArray.h"
#import "Topic.h"
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, RoomType) {
    RoomTypePublic,
    RoomTypePrivate
};

@interface Room : NSObject

@property (nonatomic, strong) NSString *code;

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *desc;

@property (nonatomic, assign) RoomType roomType;

@property (nonatomic, assign) int capacity;

@property (nonatomic, assign) int totalMember;

@property (nonatomic, assign) BOOL isProtected;

@property (nonatomic, assign) BOOL isFavored;

@property (nonatomic, strong, nullable) NSArray<Topic *> *topics;

- (instancetype)initWithCode:(NSString *)code
                        name:(NSString *)name
                        desc:(NSString *)desc
                    roomType:(RoomType)roomType
                    capacity:(int)capacity
                 totalMember:(int)totalMember
                      topics:(NSArray<Topic *> *)topics
                 isProtected:(BOOL)isProtected
                   isFavored:(BOOL)isFavored;


@end

NS_ASSUME_NONNULL_END
