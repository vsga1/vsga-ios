//
//  Room.m
//  VStudy
//
//  Created by LAP14011 on 06/03/2023.
//

#import "Room.h"

@implementation Room

- (instancetype)initWithCode:(NSString *)code
                        name:(NSString *)name
                        desc:(NSString *)desc
                    roomType:(RoomType)roomType
                    capacity:(int)capacity
                 totalMember:(int)totalMember
                      topics:(NSArray<Topic *> *)topics
                 isProtected:(BOOL)isProtected
                   isFavored:(BOOL)isFavored {
    self = [super init];
    if (self) {
        self.code = code;
        self.name = name;
        self.desc = desc;
        self.roomType = roomType;
        self.capacity = capacity;
        self.totalMember = totalMember;
        self.topics = topics;
        self.isProtected = isProtected;
        self.isFavored = isFavored;
    }
    return self;
}

@end
