//
//  Topic.h
//  VStudy
//
//  Created by LAP14011 on 07/05/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Topic : NSObject

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *colorString;

- (instancetype)initWithName:(NSString *)name colorString:(NSString *)colorString;

@end

NS_ASSUME_NONNULL_END
