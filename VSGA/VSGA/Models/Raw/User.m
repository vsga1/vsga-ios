//
//  User.m
//  VStudy
//
//  Created by LAP14011 on 18/02/2023.
//

#import "User.h"

@implementation User

- (instancetype)initWithUsername:(NSString *)username
                     accessToken:(NSString *)accessToken {
    if ((self = [super init])) {
        self.username = username;
        self.accessToken = accessToken;
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.username forKey:@"username"];
    [encoder encodeObject:self.accessToken forKey:@"accessToken"];
}

+ (BOOL)supportsSecureCoding {
    return YES;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.username = [decoder decodeObjectForKey:@"username"];
        self.accessToken = [decoder decodeObjectForKey:@"accessToken"];
    }
    return self;
}

- (instancetype)initWithUsername:(NSString *)username
                          userID:(NSString *)userID
                 avatarURLString:(nullable NSString *)avatarURLString
                            role:(NSString *)role {
    self = [super init];
    if (self) {
        self.username = username;
        self.userID = userID;
        self.avatarURLString = avatarURLString;
        if (avatarURLString) {
            self.avatarURL = [NSURL URLWithString:avatarURLString];
        }
        self.role = role;
        if ([role isEqualToString:@"HOST"]) {
            self.roleType = UserRoleHost;
        }
        else if ([role isEqualToString:@"CO_HOST"]){
            self.roleType = UserRoleCoHost;
        }
        else if ([role isEqualToString:@"MEMBER"]) {
            self.roleType = UserRoleMember;
        }
    }
    return self;
}

- (instancetype)initWithUsername:(NSString *)username
                 avatarURLString:(nullable NSString *)avatarURLString
                            role:(NSString *)role {
    self = [super init];
    if (self) {
        self.username = username;
        self.avatarURLString = avatarURLString;
        if (avatarURLString) {
            self.avatarURL = [NSURL URLWithString:avatarURLString];
        }
        self.role = role;
        if ([role isEqualToString:@"HOST"]) {
            self.roleType = UserRoleHost;
        }
        else if ([role isEqualToString:@"CO_HOST"]){
            self.roleType = UserRoleCoHost;
        }
        else if ([role isEqualToString:@"MEMBER"]) {
            self.roleType = UserRoleMember;
        }
    }
    return self;
}

- (instancetype)initFakeUserEveryone {
    self = [super init];
    if (self) {
        self.userID = [[NSUUID UUID] UUIDString];
        self.username = @"everyone";
        self.userType = UserTypeFakeEveryone;
    }
    return self;
}

- (instancetype)initFakeUserImageWithImageName:(NSString *)imageName {
    self = [super init];
    if (self) {
        self.userID = [[NSUUID UUID] UUIDString];
        self.userType = UserTypeFakeImage;
        self.avatarURLString = imageName;
    }
    return self;
}

- (void)updateRole:(NSString *)role {
    self.role = role;
    if ([role isEqualToString:@"HOST"]) {
        self.roleType = UserRoleHost;
    }
    else if ([role isEqualToString:@"CO_HOST"]){
        self.roleType = UserRoleCoHost;
    }
    else if ([role isEqualToString:@"MEMBER"]) {
        self.roleType = UserRoleMember;
    }
}

- (void)setAvatarURLString:(NSString *)avatarURLString {
    if (avatarURLString) {
        _avatarURLString = avatarURLString;
        _avatarURL = [NSURL URLWithString:avatarURLString];
    }
}
#pragma mark -

- (NSUInteger)hash {
    return self.userID.hash;
}

- (BOOL)isEqual:(id)object {
    if (object == self) {
        return YES;
    } else if (![super isEqual:object]) {
        return NO;
    } else if (![object isKindOfClass:[User class]]) {
        return NO;
    } else {
        User *other = (User *)object;
        return other.hash == self.hash;
    }
}

@end
