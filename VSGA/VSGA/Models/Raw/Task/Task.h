//
//  Task.h
//  VStudy
//
//  Created by LAP14011 on 29/03/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Task : NSObject

@property (nonatomic, strong) NSString *roomCode;

@property (nonatomic, strong) NSString *code;

@property (nonatomic, strong) NSString *desc;

@property (nonatomic, strong) NSString *status;

@property (nonatomic, strong) NSString *type;

@property (nonatomic, strong) NSString *scheme;

- (instancetype)initWithCode:(NSString *)code
                    roomCode:(NSString *)roomCode
                      scheme:(NSString *)scheme
                        desc:(NSString *)desc
                      status:(NSString *)status
                        type:(NSString *)type;

@end

NS_ASSUME_NONNULL_END
