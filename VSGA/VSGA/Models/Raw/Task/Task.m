//
//  Task.m
//  VStudy
//
//  Created by LAP14011 on 29/03/2023.
//

#import "Task.h"

@implementation Task

- (instancetype)initWithCode:(NSString *)code
                    roomCode:(NSString *)roomCode
                      scheme:(NSString *)scheme
                        desc:(NSString *)desc
                      status:(NSString *)status
                        type:(NSString *)type {
    self = [super init];
    if (self) {
        self.code = code;
        self.roomCode = roomCode;
        self.scheme = scheme;
        self.desc = desc;
        self.status = status;
        self.type = type;
    }
    return self;
}


@end
