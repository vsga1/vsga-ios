//
//  Topic.m
//  VStudy
//
//  Created by LAP14011 on 07/05/2023.
//

#import "Topic.h"

@implementation Topic

- (instancetype)initWithName:(NSString *)name colorString:(NSString *)colorString {
    self = [super init];
    if (self) {
        self.name = name;
        self.colorString = colorString;
    }
    return self;
}

@end
