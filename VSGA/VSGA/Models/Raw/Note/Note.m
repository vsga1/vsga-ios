//
//  Note.m
//  VStudy
//
//  Created by LAP14011 on 16/03/2023.
//

#import "Note.h"

@implementation Note

- (instancetype)initWithHeader:(NSString *)header
                          desc:(NSString *)desc {
    self = [super init];
    if (self) {
        self.header = header;
        self.desc = desc;
    }
    return self;
}

@end
