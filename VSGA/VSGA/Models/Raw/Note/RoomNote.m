//
//  RoomNote.m
//  VStudy
//
//  Created by LAP14011 on 21/03/2023.
//

#import "RoomNote.h"

@implementation RoomNote

- (instancetype)initWithHeader:(NSString *)header
                          desc:(NSString *)desc
                        status:(NSString *)status {
    self = [super initWithHeader:header
                            desc:desc];
    if (self) {
        self.status = status;
    }
    return self;
}


@end
