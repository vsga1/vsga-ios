//
//  RoomNote.h
//  VStudy
//
//  Created by LAP14011 on 21/03/2023.
//

#import <Foundation/Foundation.h>
#import "Note.h"

NS_ASSUME_NONNULL_BEGIN

@interface RoomNote : Note

@property (nonatomic, strong) NSString *status;

- (instancetype)initWithHeader:(NSString *)header
                          desc:(NSString *)desc
                        status:(NSString *)status;

@end

NS_ASSUME_NONNULL_END
