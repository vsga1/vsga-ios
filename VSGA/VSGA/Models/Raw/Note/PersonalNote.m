//
//  PersonalNote.m
//  VStudy
//
//  Created by LAP14011 on 21/03/2023.
//

#import "PersonalNote.h"

@implementation PersonalNote

- (instancetype)initWithRoomCode:(NSString *)roomCode
                            code:(NSString *)code
                          header:(NSString *)header
                            desc:(NSString *)desc {
    self = [super initWithHeader:header
                            desc:desc];
    if (self) {
        self.roomCode = roomCode;
        self.code = code;
    }
    return self;
}

@end
