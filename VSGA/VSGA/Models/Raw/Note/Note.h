//
//  Note.h
//  VStudy
//
//  Created by LAP14011 on 16/03/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Note : NSObject

@property (nonatomic, strong) NSString *header;

@property (nonatomic, strong) NSString *desc;

- (instancetype)initWithHeader:(NSString *)header
                          desc:(NSString *)desc;

@end

NS_ASSUME_NONNULL_END
