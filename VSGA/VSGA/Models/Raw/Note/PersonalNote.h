//
//  PersonalNote.h
//  VStudy
//
//  Created by LAP14011 on 21/03/2023.
//

#import <Foundation/Foundation.h>
#import "Note.h"

NS_ASSUME_NONNULL_BEGIN

@interface PersonalNote : Note

@property (nonatomic, strong) NSString *roomCode;

@property (nonatomic, strong) NSString *code;

- (instancetype)initWithRoomCode:(NSString *)roomCode
                            code:(NSString *)code
                          header:(NSString *)header
                            desc:(NSString *)desc;

@end

NS_ASSUME_NONNULL_END
