//
//  User.h
//  VStudy
//
//  Created by LAP14011 on 18/02/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, UserRole) {
    UserRoleMember,
    UserRoleHost,
    UserRoleCoHost
};

typedef NS_ENUM(NSInteger, UserType) {
    UserTypeReal,
    UserTypeFakeEveryone,
    UserTypeFakeImage
};

@interface User : NSObject <NSCoding, NSSecureCoding>

@property (nonatomic, strong) NSString *userID;

@property (nonatomic, strong) NSString *username;

@property (nonatomic, strong) NSString *fullName;

@property (nonatomic, strong) NSString *email;

@property (nonatomic, strong) NSString *accessToken;

@property (nonatomic, strong) NSString *role;

@property (nonatomic, assign) UserRole roleType;

@property (nonatomic, strong) NSURL *avatarURL;

@property (nonatomic, strong) NSString *avatarURLString;

@property (nonatomic, assign) UserType userType;

- (instancetype)initWithUsername:(NSString *)username
                          userID:(NSString *)userID
                 avatarURLString:(nullable NSString *)avatarURLString
                            role:(NSString *)role;

- (instancetype)initWithUsername:(NSString *)username
                 avatarURLString:(nullable NSString *)avatarURLString
                            role:(NSString *)role;

- (instancetype)initWithUsername:(NSString *)username
                     accessToken:(NSString *)accessToken;

- (instancetype)initFakeUserEveryone;

- (instancetype)initFakeUserImageWithImageName:(NSString *)imageName;

- (void)updateRole:(NSString *)role;

@end

NS_ASSUME_NONNULL_END
