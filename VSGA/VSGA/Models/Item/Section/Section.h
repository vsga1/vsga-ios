//
//  Section.h
//  VStudy
//
//  Created by LAP14011 on 20/02/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, ItemSection) {
    SectionMain
};

@interface Section : NSObject

@property (nonatomic) ItemSection sectionType;

- (instancetype)initWithSection:(ItemSection)section;

@end

NS_ASSUME_NONNULL_END
