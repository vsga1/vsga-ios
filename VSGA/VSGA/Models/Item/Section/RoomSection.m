//
//  RoomSection.m
//  VStudy
//
//  Created by LAP14011 on 04/04/2023.
//

#import "RoomSection.h"

@implementation RoomSection

- (instancetype)initWithSection:(RoomSectionType)section isExpand:(BOOL)isExpand {
    self = [super init];
    if (self) {
        self.sectionType = section;
        self.isExpand = isExpand;
    }
    return self;
}

@end
