//
//  RoomSection.h
//  VStudy
//
//  Created by LAP14011 on 04/04/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, RoomSectionType) {
    RoomSectionLastVisted,
    RoomSectionOwner,
    RoomSectionFavorite
};

@interface RoomSection : NSObject

@property (nonatomic, assign) RoomSectionType sectionType;

@property (nonatomic, assign) BOOL isExpand;

- (instancetype)initWithSection:(RoomSectionType)section isExpand:(BOOL)isExpand;

@end

NS_ASSUME_NONNULL_END
