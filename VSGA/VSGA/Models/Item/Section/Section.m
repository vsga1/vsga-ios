//
//  Section.m
//  VStudy
//
//  Created by LAP14011 on 20/02/2023.
//


#import "Section.h"

@implementation Section

- (instancetype)initWithSection:(ItemSection)section {
    self = [super init];
    if (self) {
        self.sectionType = section;
    }
    return self;
}

@end

