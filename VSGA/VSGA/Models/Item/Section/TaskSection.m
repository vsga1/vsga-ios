//
//  TaskSection.m
//  VStudy
//
//  Created by LAP14011 on 01/04/2023.
//

#import "TaskSection.h"

@implementation TaskSection

- (instancetype)initWithSection:(TaskSectionType)section isExpand:(BOOL)isExpand {
    self = [super init];
    if (self) {
        self.sectionType = section;
        self.isExpand = isExpand;
    }
    return self;
}

@end
