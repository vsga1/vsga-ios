//
//  TaskSection.h
//  VStudy
//
//  Created by LAP14011 on 01/04/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, TaskSectionType) {
    TaskSectionUndone,
    TaskSectionDone
};

@interface TaskSection : NSObject

@property (nonatomic, assign) TaskSectionType sectionType;

@property (nonatomic, assign) BOOL isExpand;

- (instancetype)initWithSection:(TaskSectionType)section isExpand:(BOOL)isExpand;

@end

NS_ASSUME_NONNULL_END
