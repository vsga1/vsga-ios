//
//  TopicItem.m
//  VStudy
//
//  Created by LAP14011 on 07/03/2023.
//

#import "TopicItem.h"
#import "Macro.h"
#import "NSString+Extension.h"
const CGFloat kMaxLabelHeight = 20;
const CGFloat kLabelPadding = 3;
@implementation TopicItem

- (instancetype)initWithName:(NSString *)name colorString:(NSString *)colorString {
    self = [super init];
    if (self) {
        self.name = name;
        self.nameFont = kMedium14;
        self.backgroundColor = [self convertFromHexString:colorString];
        self.textColor = [self contrastingColorForColor:self.backgroundColor];
        [self calculateSize];
    }
    return self;
}

- (void)calculateSize {
    CGSize labelSize = [self.name sizeOfStringWithStyledFont:self.nameFont withSize:CGSizeMake(CGFLOAT_MAX,kMaxLabelHeight)];
    self.labelSize = CGSizeMake(labelSize.width, kMaxLabelHeight);
    self.itemSize = CGSizeMake(labelSize.width + 6 * kLabelPadding, kMaxLabelHeight + 2 * kLabelPadding);
}

- (UIColor *)convertFromHexString:(NSString *)hexString {
    
    // Remove the '#' character
    NSString *cleanHexString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];

    // Convert the hex string to an integer
    unsigned int hexInt = 0;
    NSScanner *scanner = [NSScanner scannerWithString:cleanHexString];
    [scanner scanHexInt:&hexInt];

    // Extract the individual red, green, and blue component values from the integer
    CGFloat red = ((hexInt & 0xFF0000) >> 16) / 255.0;
    CGFloat green = ((hexInt & 0x00FF00) >> 8) / 255.0;
    CGFloat blue = (hexInt & 0x0000FF) / 255.0;

    // Create and return the UIColor object
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0];

}

- (UIColor *)contrastingColorForColor:(UIColor *)color {
    CGFloat red, green, blue, alpha;
    [color getRed:&red green:&green blue:&blue alpha:&alpha];
    CGFloat luminance = 0.2126 * pow(red, 2.2) + 0.7152 * pow(green, 2.2) + 0.0722 * pow(blue, 2.2);
    return luminance > 0.179 ? [UIColor blackColor] : [UIColor whiteColor];
}


- (UIColor *)getTextColorFromBGColor:(UIColor *)bgColor {
    
    CGFloat red, green, blue, alpha;
    [bgColor getRed:&red green:&green blue:&blue alpha:&alpha];

    CGFloat y = (red * 0.299f) + (green * 0.587f) + (blue * 0.114f);
    CGFloat i = (red * 0.596f) - (green * 0.274f) - (blue * 0.322f);
    CGFloat q = (red * 0.211f) - (green * 0.523f) + (blue * 0.312f);
    
    CGFloat yContrast = (y >= 0.5f) ? (y * 0.7f) : (1.0f - (y * 0.7f));
    CGFloat iqContrast = (i * i) + (q * q);
    
    CGFloat contrast = sqrt((yContrast * yContrast) + iqContrast);
//    UIColor *textColor = [chroma contrast:color with:@"white"] > 1.5 ? [UIColor whiteColor] : [UIColor blackColor];

    return (contrast >= 4.5) ? [UIColor whiteColor] : [UIColor blackColor];
}

@end
