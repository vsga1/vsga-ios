//
//  RoomItem.m
//  VStudy
//
//  Created by LAP14011 on 06/03/2023.
//

#import "RoomItem.h"
#import "Topic.h"
@implementation RoomItem

- (instancetype)initWithRoom:(Room *)room {
    self = [super init];
    if (self) {
        self.code = room.code;
        self.name = [room.name stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        self.desc = room.desc;
        self.roomType = room.roomType;
        self.capacity = room.capacity;
        self.totalMember = room.totalMember;
        self.isProtected = room.isProtected;
        self.isFavored = room.isFavored;
        NSMutableArray *topicList = [[NSMutableArray alloc] init];
        for (Topic *topic in room.topics) {
            TopicItem *item = [[TopicItem alloc] initWithName:topic.name colorString:topic.colorString];
            [topicList addObject:item];
        }
        self.topicItemList = [NSArray arrayWithArray:topicList];
    }
    return self;
}

@end
