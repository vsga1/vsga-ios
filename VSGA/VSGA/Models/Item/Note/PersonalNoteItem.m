//
//  PersonalNoteItem.m
//  VStudy
//
//  Created by LAP14011 on 15/03/2023.
//

#import "PersonalNoteItem.h"
#import "Macro.h"
#import "NoteListCollectionViewCell.h"

@implementation PersonalNoteItem


- (instancetype)initWithNote:(PersonalNote *)note {
    self = [super init];
    if (self) {
        self.code = note.code;
        self.roomCode = note.roomCode;
        self.header = note.header;
        self.desc = note.desc;
    }
    return self;
}

- (NSUInteger)hash {
    return self.code.hash;
}

- (BOOL)isEqual:(id)object {
    if (object == self) {
        return YES;
    } else if (![super isEqual:object]) {
        return NO;
    } else if (![object isKindOfClass:[PersonalNoteItem class]]) {
        return NO;
    } else {
        PersonalNoteItem *other = (PersonalNoteItem *)object;
        return other.hash == self.hash;
    }
}

//MARK: Get Class

- (Class)getCellClass {
    return NoteListCollectionViewCell.class;
}

//MARK: Display in List View Controller

- (void)setupDisplayInListView {
    self.headerFont = kSemiBold18;
    self.descFont = kRegular16;
}

//MARK: Display in Detail View Controller

- (void)setupDisplayInDetailView {
    self.headerFont = kSemiBold18;
    self.descFont = kRegular16;
}

@end
