//
//  RoomNoteItem.h
//  VStudy
//
//  Created by LAP14011 on 21/03/2023.
//

#import <Foundation/Foundation.h>
#import "RoomNote.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, RoomNoteStatus) {
    RoomNoteStatusEdit,
    RoomNoteStatusOpen,
};

@interface RoomNoteItem : NSObject

@property (nonatomic, strong) NSString *header;

@property (nonatomic, strong) NSString *desc;

@property (nonatomic, assign) RoomNoteStatus statusType;

- (instancetype)initWithNote:(RoomNote *)note;

@end

NS_ASSUME_NONNULL_END
