//
//  PersonalNoteItem.h
//  VStudy
//
//  Created by LAP14011 on 15/03/2023.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PersonalNote.h"
NS_ASSUME_NONNULL_BEGIN

@interface PersonalNoteItem : NSObject

@property (strong, nonatomic) NSString *code;

@property (nonatomic, strong) NSString *roomCode;

@property (nonatomic, strong) NSString *header;

@property (nonatomic, strong) NSString *desc;

// MARK: Info to show UI

@property (nonatomic, strong) UIFont *headerFont;

@property (nonatomic, strong) UIFont *descFont;

- (instancetype)initWithNote:(PersonalNote *)note;

- (Class)getCellClass;

- (void)setupDisplayInListView;

@end

NS_ASSUME_NONNULL_END
