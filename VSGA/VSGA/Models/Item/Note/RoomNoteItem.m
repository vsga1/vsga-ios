//
//  RoomNoteItem.m
//  VStudy
//
//  Created by LAP14011 on 21/03/2023.
//

#import "RoomNoteItem.h"

@implementation RoomNoteItem

- (instancetype)initWithNote:(RoomNote *)note {
    self = [super init];
    if (self) {
        self.header = note.header;
        self.desc = note.desc;
        if ([note.status isEqualToString:@"EDIT"]) {
            self.statusType = RoomNoteStatusEdit;
        }
        else if ([note.status isEqualToString:@"OPEN"]) {
            self.statusType = RoomNoteStatusOpen;
        }
    }
    return self;
}

@end
