//
//  FilterTopicItem.h
//  VStudy
//
//  Created by TamNVM on 21/05/2023.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FilterTopicItem : NSObject

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, strong) UIColor *textColor;

@property (nonatomic, strong) UIFont *nameFont;

@property (nonatomic, assign) BOOL isSelected;

@property (nonatomic, assign) CGSize selectedLabelSize;

@property (nonatomic, assign) CGSize selectedContainerSize;

@property (nonatomic, assign) CGSize selectedCellSize;

- (instancetype)initWithName:(NSString *)name colorString:(NSString *)colorString;

@end

NS_ASSUME_NONNULL_END
