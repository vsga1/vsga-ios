//
//  RoomTaskItem.h
//  VStudy
//
//  Created by LAP14011 on 02/04/2023.
//

#import "TaskItem.h"
#import "User.h"

NS_ASSUME_NONNULL_BEGIN

@interface RoomTaskItem : TaskItem

@property (strong, nonatomic, nullable) NSArray *doneUserArray;

- (instancetype)initWithTaskItem:(TaskItem *)taskItem doneUserArray:(nullable NSArray *)doneUserArray;

- (instancetype)initWithTask:(Task *)task doneUserArray:(nullable NSArray *)doneUserArray;

- (void)addMoreDoneUser:(User *)user;

@end

NS_ASSUME_NONNULL_END
