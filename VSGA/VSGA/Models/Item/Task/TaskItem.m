//
//  TaskItem.m
//  VStudy
//
//  Created by LAP14011 on 29/03/2023.
//

#import "TaskItem.h"
#import "Macro.h"
@implementation TaskItem

- (instancetype)initWithTask:(Task *)task {
    self = [super init];
    if (self) {
        self.code = task.code;
        self.roomCode = task.roomCode;
        self.scheme = task.scheme;
        self.desc = task.desc;
        if ([task.status isEqual:@"DONE"]) {
            self.status = TaskStatusDone;
        }
        else if ([task.status isEqual:@"UNDONE"]) {
            self.status = TaskStatusUndone;
        }
        else if ([task.status isEqual:@"DELETED"]){
            self.status = TaskStatusDeleted;
        }
        if ([task.type isEqual:@"PERSONAL_TASK"]) {
            self.type = TaskTypePersonal;
        }
        else if ([task.type isEqual:@"ROOM_TASK"]) {
            self.type = TaskTypeRoom;
        }
        self.isTempItem = FALSE;
    }
    return self;
}

- (NSUInteger)hash {
    return self.code.hash;
}

- (BOOL)isEqual:(id)object {
    if (object == self) {
        return YES;
    } else if (![super isEqual:object]) {
        return NO;
    } else if (![object isKindOfClass:[TaskItem class]]) {
        return NO;
    } else {
        TaskItem *other = (TaskItem *)object;
        return other.hash == self.hash;
    }
}

- (instancetype)initTempItemWithType:(TaskType)type {
    self = [super init];
    if (self) {
        self.code = [[NSUUID UUID] UUIDString];
        self.roomCode = nil;
        self.scheme = nil;
        self.desc = @"";
        self.status = TaskStatusUndone;
        self.type = type;
        self.isTempItem = TRUE;
    }
    return self;
}

- (void)updateTempItemWithRoomCode:(NSString *)roomCode scheme:(nullable NSString *)scheme desc:(nonnull NSString *)desc {
    self.roomCode = roomCode;
    self.scheme = scheme;
    self.desc = desc;
    self.isTempItem = FALSE;
}

//MARK: Display in View Controller

- (void)setupDisplayInView {
    self.descFont = kRegular16;
}


@end
