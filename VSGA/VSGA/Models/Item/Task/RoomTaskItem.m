//
//  RoomTaskItem.m
//  VStudy
//
//  Created by LAP14011 on 02/04/2023.
//

#import "RoomTaskItem.h"

@implementation RoomTaskItem

- (instancetype)initWithTaskItem:(TaskItem *)taskItem doneUserArray:(nullable NSArray *)doneUserArray {
    self = [super init];
    if (self) {
        self.code = taskItem.code;
        self.roomCode = taskItem.roomCode;
        self.scheme = taskItem.scheme;
        self.desc = taskItem.desc;
        self.status = taskItem.status;
        self.type = taskItem.type;
        self.isTempItem = taskItem.isTempItem;
        self.descFont = taskItem.descFont;
        self.doneUserArray = doneUserArray;
        
    }
    return self;
}

- (instancetype)initWithTask:(Task *)task doneUserArray:(nullable NSArray *)doneUserArray {
    self = [super initWithTask:task];
    if (self) {
        self.doneUserArray = doneUserArray;
    }
    return self;
}

- (void)addMoreDoneUser:(User *)user {
    NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:self.doneUserArray];

    // Add the new item to the mutable array
    [mutableArray addObject:user];

    // Assign the updated mutable array to the original NSArray variable
    self.doneUserArray = [NSArray arrayWithArray:mutableArray];
}

@end
