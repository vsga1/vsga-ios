//
//  TaskItem.h
//  VStudy
//
//  Created by LAP14011 on 29/03/2023.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Task.h"
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, TaskStatus) {
    TaskStatusUndone,
    TaskStatusDone,
    TaskStatusDeleted
};

typedef NS_ENUM(NSInteger, TaskType) {
    TaskTypePersonal,
    TaskTypeRoom
};

@interface TaskItem : NSObject

@property (nonatomic, strong, nullable) NSString *roomCode;

@property (nonatomic, strong) NSString *code;

@property (nonatomic, strong) NSString *desc;

@property (nonatomic, assign) TaskStatus status;

@property (nonatomic, assign) TaskType type;

@property (nonatomic, strong, nullable) NSString *scheme;

@property (nonatomic, assign) BOOL isTempItem;

// MARK: Info to show UI

@property (nonatomic, strong) UIFont *descFont;

- (instancetype)initWithTask:(Task *)task;

- (instancetype)initTempItemWithType:(TaskType)type;

- (void)updateTempItemWithRoomCode:(NSString *)roomCode scheme:(nullable NSString *)scheme desc:(NSString *)desc;

- (void)setupDisplayInView;

@end

NS_ASSUME_NONNULL_END
