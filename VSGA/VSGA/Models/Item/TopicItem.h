//
//  TopicItem.h
//  VStudy
//
//  Created by LAP14011 on 07/03/2023.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TopicItem : NSObject

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) UIFont *nameFont;

@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, strong) UIColor *textColor;

@property (nonatomic, assign) CGSize labelSize;

@property (nonatomic, assign) CGSize itemSize;

- (instancetype)initWithName:(NSString *)name colorString:(NSString *)colorString;

@end

NS_ASSUME_NONNULL_END
