//
//  MessageItem.m
//  VStudy
//
//  Created by LAP14011 on 04/03/2023.
//

#import "MessageItem.h"
#import "Macro.h"
#import "AccountManager.h"
#import "NSDate+Extension.h"
#import "NSString+Extension.h"

#import "MessageCollectionViewCell.h"
@implementation MessageItem

//MARK: Init

- (instancetype)initWithMessaageID:(NSString *)messageID
                 andSenderUsername:(NSString *)senderUsername
                       andReceiver:(nullable NSString *)receiverUsername
                           andText:(NSString *)text
                           andType:(NSString *)type {
    self = [super init];
    if (self) {
        self.messageID = messageID;
        self.text = text;
        self.timestamp = [NSDate date];
        
        self.senderUsername = senderUsername;
        if ([senderUsername isEqualToString:[[AccountManager sharedManager] getCurrentUsername]]) {
            self.isSendMessage = TRUE;
            self.isShowAvatar = FALSE;
            self.messageName = [NSString stringWithFormat:@"From me"];
        }
        else {
            self.isSendMessage = FALSE;
            self.isShowAvatar = TRUE;
            self.messageName = [NSString stringWithFormat:@"From %@", senderUsername];
        }
        
        if ([type isEqualToString:@"ALL"]) {
            self.messageType = MessageTypePublic;
            self.messageName = [self.messageName stringByAppendingFormat:@" to everyone"];
            self.receiverUsername = nil;
        }
        else {
            self.messageType = MessageTypeDirect;
            if ([receiverUsername isEqualToString:[[AccountManager sharedManager] getCurrentUsername]]) {
                self.messageName = [self.messageName stringByAppendingFormat:@" to me"];
            }
            else {
                self.messageName = [self.messageName stringByAppendingFormat:@" to %@", receiverUsername];
            }
            self.receiverUsername = receiverUsername;
        }
    }
    return self;
}



- (NSUInteger)hash {
    return self.messageID.hash;
}

- (BOOL)isEqual:(id)object {
    if (object == self) {
        return YES;
    } else if (![super isEqual:object]) {
        return NO;
    } else if (![object isKindOfClass:[MessageItem class]]) {
        return NO;
    } else {
        MessageItem *other = (MessageItem *)object;
        return other.hash == self.hash;
    }
}

//MARK: Get Class

- (Class)getCellClass {
    return MessageCollectionViewCell.class;
}

//MARK: Calculate size and info

- (void)calculateSizeAndInfoWithTargetSize:(CGSize)targetSize {
    [self calculateAllSizeWithTargetSize:targetSize];
    [self calculateLayoutInfo];
}

//MARK: Calculate size

- (void)calculateAllSizeWithTargetSize:(CGSize)targetSize {
    //1. calculate message name (max width = screen width - avatar)
    CGFloat cellWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat maxMessageNameWidth = cellWidth - kChatAvatarSize - 2 * kChatAvatarMargin;
    [self calculateMessageNameWithTargetSize:CGSizeMake(maxMessageNameWidth, targetSize.height)];
    //2. calculate main content size + padding
    [self calculateMainContentWithTargetSize:targetSize];
    //3. calculate timestamp + padding + margin
    [self calculateTimestampSizeWithTargetSize:targetSize];
    //3. calculate bubble and cell height
    [self calculateBubbleAndCellHeight];
}

- (void)calculateMessageNameWithTargetSize:(CGSize)targetSize {
    self.messageNameFont = kRegular16;
    switch (self.messageType) {
        case MessageTypePublic:
            self.messageNameTextColor = UIColor.blueColor;
            break;
        case MessageTypeDirect:
            self.messageNameTextColor = UIColor.redColor;
            break;
        default:
            self.messageNameTextColor = UIColor.blueColor;
            break;
    }
    CGSize size = [self.messageName calculateSizeWithTargetSize:CGSizeMake(targetSize.width, targetSize.height) withFont:self.messageNameFont];
    self.messageNameSize = CGSizeMake(size.width, size.height);
}

- (void)calculateMainContentWithTargetSize:(CGSize)targetSize {
    self.textFont = kRegular18;
    CGFloat maxTextWidth = targetSize.width - 2 * kTextMessagePadding;
    CGSize size = [self.text calculateSizeWithTargetSize:CGSizeMake(maxTextWidth, targetSize.height)
                                                withFont:self.textFont];
    if (size.width < kBubbleMinWidth) {
        self.mainContentSize = CGSizeMake(kBubbleMinWidth, size.height);
    }
    else {
        self.mainContentSize = CGSizeMake(size.width, size.height);
    }
}

- (void)calculateTimestampSizeWithTargetSize:(CGSize)targetSize {
    self.timestampFont = kRegular14;
    
    self.timestampString = [self.timestamp convertToTimestampStringShowLess];
    CGSize size = [self.timestampString calculateSizeWithTargetSize:CGSizeMake(targetSize.width, targetSize.height) withFont:self.timestampFont];
    
    self.timestampSize = CGSizeMake(size.width + 2 * kOutsideTimestampPadding,
                                    size.height + kOutsideTimestampMargin);
}

- (void)calculateBubbleAndCellHeight {
    CGSize mainContentWithPadding = CGSizeMake(self.mainContentSize.width + 2 * kTextMessagePadding, self.mainContentSize.height + 2 * kTextMessagePadding);
    self.bubbleSize = CGSizeMake(mainContentWithPadding.width, mainContentWithPadding.height);
    
    self.cellHeight = self.messageNameSize.height + kMessageNameMargin + self.bubbleSize.height + self.timestampSize.height + kOutsideTimestampMargin;
}

//MARK: Calculate Layout info

- (void)calculateLayoutInfo {
    CGFloat cellWidth = [UIScreen mainScreen].bounds.size.width;
    CGRect cellFrame = CGRectMake(0,
                                  0,
                                  cellWidth,
                                  self.cellHeight);
    LayoutInfo *cellLayout = [[LayoutInfo alloc] initWithFrame:cellFrame children:nil];
    LayoutInfo *avatarLayout = nil;
    LayoutInfo *messageNameLayout = nil;
    LayoutInfo *bubbleMessageLayout = nil;
    LayoutInfo *mainContentLayout = nil;
    LayoutInfo *timestampLayout = nil;
    
    //avatar
    if (self.isShowAvatar) {
        avatarLayout = [cellLayout addChildWithOrigin:CGPointMake(kChatAvatarMargin, cellLayout.top)
                                                 size:CGSizeMake(kChatAvatarSize, kChatAvatarSize)
                                               forKey:kAvatarLayout];
    }
    
    //message name
    CGPoint messageNameOrigin = CGPointZero;
    messageNameOrigin.y = cellLayout.top;
    messageNameOrigin.x = self.isSendMessage ?
    cellLayout.right - kBubbleTrailingPadding - self.messageNameSize.width :
    avatarLayout.right + kChatAvatarMargin;
    messageNameLayout = [cellLayout addChildWithOrigin:messageNameOrigin
                                                  size:CGSizeMake(self.messageNameSize.width, self.messageNameSize.height)
                                                forKey:kMessageNameLayout];
    
    //bubble
    CGRect bubbleMessageFrame = CGRectMake(0, 0, 0, 0);
    bubbleMessageFrame.size.width = self.bubbleSize.width;
    bubbleMessageFrame.size.height = self.bubbleSize.height;
    bubbleMessageFrame.origin.y = messageNameLayout.bottom + kMessageNameMargin;
    bubbleMessageFrame.origin.x = self.isSendMessage ?
    cellLayout.right - kBubbleTrailingPadding - self.bubbleSize.width :
    avatarLayout.right + kChatAvatarMargin;
    
    bubbleMessageLayout = [cellLayout addChildWithFrame:bubbleMessageFrame forKey:kBubbleLayout];
    
    //main content layout
    CGRect mainContentFrame = CGRectMake(kTextMessagePadding,
                                         kTextMessagePadding,
                                         self.mainContentSize.width,
                                         self.mainContentSize.height);
    mainContentLayout = [bubbleMessageLayout addChildWithFrame:mainContentFrame
                                                        forKey:kMainContentLayout];
    
    //timestamp
    CGRect timestampFrame = CGRectMake(bubbleMessageLayout.left,
                                       bubbleMessageLayout.bottom + kOutsideTimestampMargin,
                                       self.timestampSize.width,
                                       self.timestampSize.height);
    timestampLayout = [cellLayout addChildWithFrame:timestampFrame forKey:kTimestampLayout];
    
    self.layoutInfo = cellLayout;
}

@end
