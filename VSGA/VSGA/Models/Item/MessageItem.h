//
//  MessageItem.h
//  VStudy
//
//  Created by LAP14011 on 04/03/2023.
//

#import <Foundation/Foundation.h>

#import "LayoutInfo.h"
#import "MyMessage.h"

#import "User.h"
NS_ASSUME_NONNULL_BEGIN

@interface MessageItem : NSObject

@property (strong, nonatomic) NSString *messageID;

@property (strong, nonatomic) NSString *senderUsername;

@property (strong, nonatomic, nullable) NSString *receiverUsername;

@property (strong, nonatomic) NSDate *timestamp;

@property (strong, nonatomic) NSString *text;

// MARK: Info to show UI

@property (strong, nonatomic) NSURL *senderAvatarURL;

@property (assign, nonatomic) MessageType messageType;

@property (assign, nonatomic) BOOL isSendMessage;

@property (assign, nonatomic) BOOL isShowAvatar;

@property (strong, nonatomic) NSString *timestampString;

@property (strong, nonatomic) UIFont* timestampFont;

@property (strong, nonatomic) NSString *messageName; //From...To

@property (strong, nonatomic) UIFont* messageNameFont;

@property (strong, nonatomic) UIColor* messageNameTextColor;

@property (strong, nonatomic) UIFont* textFont;

// MARK: Layout props

@property (strong, nonatomic) LayoutInfo *layoutInfo;

@property (assign, nonatomic) CGFloat cellHeight;

@property (assign, nonatomic) CGSize messageNameSize;

@property (assign, nonatomic) CGSize bubbleSize;

@property (assign, nonatomic) CGSize mainContentSize;

@property (assign, nonatomic) CGSize timestampSize;

- (instancetype)initWithMessaageID:(NSString *)messageID
                 andSenderUsername:(NSString *)senderUsername
                       andReceiver:(nullable NSString *)receiverUsername
                           andText:(NSString *)text
                           andType:(NSString *)type;

- (Class)getCellClass;

- (void)calculateSizeAndInfoWithTargetSize:(CGSize)targetSize;

@end

NS_ASSUME_NONNULL_END
