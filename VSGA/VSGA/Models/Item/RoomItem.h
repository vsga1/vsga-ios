//
//  RoomItem.h
//  VStudy
//
//  Created by LAP14011 on 06/03/2023.
//

#import <Foundation/Foundation.h>
#import "Room.h"
#import "TopicItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface RoomItem : NSObject

@property (nonatomic, strong) NSString *code;

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *desc;

@property (nonatomic, assign) RoomType roomType;

@property (nonatomic, assign) int capacity;

@property (nonatomic, assign) int totalMember;

@property (nonatomic, strong) NSArray *topicItemList;

@property (nonatomic, assign) BOOL isProtected;

@property (nonatomic, assign) BOOL isFavored;

- (instancetype)initWithRoom:(Room *)room;

@end

NS_ASSUME_NONNULL_END
