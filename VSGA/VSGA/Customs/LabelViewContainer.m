//
//  LabelViewContainer.m
//  VStudy
//
//  Created by LAP14011 on 04/03/2023.
//
#import <Masonry/Masonry.h>

#import "LabelViewContainer.h"

@implementation LabelViewContainer

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initLabel];
    }
    return self;
}

- (instancetype)initLabelEdgesEqualToViewWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initLabel];
        [self setupLabelCenterToView];
    }
    return self;
}

- (instancetype)initWithLabelCenterToView {
    self = [super init];
    if (self) {
        [self initLabel];
        [self setupLabelCenterToView];
    }
    return self;
}

- (instancetype)initWithLabelEdgesEqualToView {
    self = [super init];
    if (self) {
        [self initLabel];
        [self setupLabelEdgesEqualToView];
    }
    return self;
}

- (instancetype)initWithLabelEdgesEqualToViewWithPadding:(UIEdgeInsets)padding {
    self = [super init];
    if (self) {
        [self initLabel];
        [self setupLabelEdgesEqualToViewWithPadding:padding];
    }
    return self;
}

- (void)initLabel {
    self.label = [[UILabel alloc] init];
    [self addSubview:self.label];
}

- (void)setupLabelCenterToView {
    //autolayout
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
    }];
}

- (void)setupLabelEdgesEqualToView {
    //autolayout
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

- (void)setupLabelEdgesEqualToViewWithPadding:(UIEdgeInsets)padding {
    //autolayout
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(padding);
    }];
}

- (void)displayLabelWithText:(NSString *)text
                             withFont:(UIFont *)font
                        withTextColor:(UIColor *)textColor {
    self.label.text = text;
    self.label.font = font;
    self.label.textColor = textColor;
}

- (void)displayLabelWithFont:(UIFont *)font
           withTextColor:(UIColor *)textColor {
    self.label.font = font;
    self.label.textColor = textColor;
}

@end
