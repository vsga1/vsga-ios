//
//  ErrorLabel.m
//  VStudy
//
//  Created by LAP14011 on 20/02/2023.
//

#import "ErrorLabel.h"
#import "UIView+Extension.h"
#import "NSString+Extension.h"

@implementation ErrorLabel {
    CGFloat _maxWidth;
}

- (instancetype)initWithOrigin:(CGPoint)origin
                      maxWidth:(CGFloat)maxWidth
                     titleFont:(UIFont *)titleFont {
    CGSize titleSize = [@"" sizeOfStringWithStyledFont:self.font withSize:CGSizeMake(maxWidth, CGFLOAT_MAX)];
    self = [super initWithFrame:CGRectMake(origin.x,
                                           origin.y,
                                           titleSize.width,
                                           titleSize.height)
                          title:@""
                      titleFont:titleFont
                     titleColor:UIColor.redColor];
    if (self) {
        self.textAlignment = NSTextAlignmentCenter;
        _maxWidth = maxWidth;
    }
    return self;
}

- (void)setErrorLabelWithTitle:(NSString *)title {
    if (!title) {
        return;
    }
    
    CGSize titleSize = [title sizeOfStringWithStyledFont:self.font withSize:CGSizeMake(_maxWidth, CGFLOAT_MAX)];
    
    CGRect frame = CGRectMake((_maxWidth - titleSize.width)/2, self.y, titleSize.width, titleSize.height);
    
    [self setFrame:frame title:title];
}

@end
