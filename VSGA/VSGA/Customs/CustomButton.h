//
//  CustomButton.h
//  VStudy
//
//  Created by LAP14011 on 15/02/2023.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomButton : UIButton

- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)title
                   titleColor:(UIColor *)titleColor
                    titleFont:(UIFont *)titleFont
              backgroundColor:(UIColor *)backgroundColor
                  borderColor:(CGColorRef)borderColor;

- (void)showLoading;

- (void)hideLoading;

- (void)showAndHideLoadingInDuration:(CGFloat)time;

@end

NS_ASSUME_NONNULL_END
