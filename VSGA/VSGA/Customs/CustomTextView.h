//
//  CustomTextView.h
//  VStudy
//
//  Created by LAP14011 on 04/03/2023.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomTextView : UITextView

@property (strong, nonatomic) UILabel *placeholder;

- (instancetype)initWithFont:(UIFont *)font
               withTextColor:(UIColor *)textColor
         withPlaceholderText:(NSString *)placeholderText
    withPlaceholderTextColor:(UIColor *)placeholderTextColor;

- (instancetype)initWithPlaceholderText:(NSString *)placeholderText
               withPlaceholderTextColor:(UIColor *)placeholderTextColor;

- (void)placeholderTopLeftPadding:(CGFloat)padding;

- (BOOL)isEmpty;

- (BOOL)isDisplayPlaceholder;

- (void)displayPlaceholder;

- (void)displayContent;

- (void)displayContentOrPlaceholder;

- (void)setFont:(UIFont *)font textColor:(UIColor *)textColor;

@end

NS_ASSUME_NONNULL_END
