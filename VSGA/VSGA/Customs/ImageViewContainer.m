//
//  ImageViewContainer.m
//  VStudy
//
//  Created by LAP14011 on 04/03/2023.
//
#import <Masonry/Masonry.h>
#import "ImageViewContainer.h"

@implementation ImageViewContainer

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initImageView];
    }
    return self;
}

- (instancetype)initImageViewCenterToViewWithNormalTintColor:(nullable UIColor *)normalTintColor
                                       withSelectedTintColor:(nullable UIColor *)selectedTintColor {
    self = [super init];
    if (self) {
        [self initImageView];
        [self setupImageViewCenterToView];
        self.normalTintColor = normalTintColor;
        self.selectedTintColor = selectedTintColor;
        self.isSelected = FALSE;
    }
    return self;
}

- (void)initImageView {
    self.imageView = [[UIImageView alloc] init];
    [self addSubview:self.imageView];
}

- (void)setupImageViewCenterToView {
    //autolayout
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
    }];
}

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    if (_isSelected) {
        if (!self.selectedTintColor) return;
        self.imageView.image = [self.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.imageView.tintColor = self.selectedTintColor;
    }
    else {
        if (!self.normalTintColor) return;
        self.imageView.tintColor = self.normalTintColor;
    }
}


@end
