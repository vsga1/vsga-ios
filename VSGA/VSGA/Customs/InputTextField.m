//
//  InputTextField.m
//  VSGA
//
//  Created by LAP14011 on 07/02/2023.
//

#import "InputTextField.h"

@implementation InputTextField

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.edgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
        
        CALayer *bottomLine = [CALayer layer];
        bottomLine.frame = CGRectMake(0,
                                      self.frame.size.height - 1,
                                      self.frame.size.width,
                                      1);
        bottomLine.backgroundColor = [UIColor blackColor].CGColor;

        [self setBorderStyle:UITextBorderStyleNone];
        [self.layer addSublayer:bottomLine];
        
        
    }
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [super editingRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
