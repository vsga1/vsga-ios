//
//  TitleAndToggleButton.m
//  VStudy
//
//  Created by TamNVM on 18/06/2023.
//

#import "TitleAndToggleButton.h"
#import "Macro.h"

@implementation TitleAndToggleButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialize label
        self.titleLabel = [[CustomLabel alloc] initWithFrame:CGRectZero];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.font = kMedium20;
        [self addSubview:self.titleLabel];
        
        // Initialize disclosure button
//        self.disclosureButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        
//        [self.disclosureButton addTarget:self action:@selector(handleDisclosureButtonTap) forControlEvents:UIControlEventTouchUpInside];
//        [self addSubview:self.disclosureButton];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    // Position label and button
    CGFloat padding = 15.0;
    CGFloat buttonSize = 20.0;
    self.titleLabel.frame = CGRectMake(padding,
                                       0,
                                       self.bounds.size.width - 2 * padding - buttonSize,
                                       self.bounds.size.height);
    self.disclosureButton.frame = CGRectMake(self.bounds.size.width - buttonSize - padding,
                                             (self.bounds.size.height - buttonSize) / 2,
                                             buttonSize,
                                             buttonSize);
}

- (void)handleDisclosureButtonTap {
    if (self.toggleHandler) {
        self.toggleHandler();
    }
}

- (void)setObjectWithTaskSection:(RoomSection *)section {
    switch (section.sectionType) {
        case RoomSectionLastVisted:
            self.titleLabel.text = @"Last visited rooms";
            break;
        case RoomSectionOwner:
            self.titleLabel.text = @"My rooms";
            break;
        case RoomSectionFavorite:
            self.titleLabel.text = @"My favorite rooms";
            break;
        default:
            break;
    }
    
    UIImage *image;
    if (section.isExpand) {
        image = [UIImage imageNamed:@"ExpandArrow"];
    }
    else {
        image = [UIImage imageNamed:@"CollapseArrow"];
    }
    [self.disclosureButton setImage:image forState:UIControlStateNormal];
}

@end
