//
//  TimestampView.m
//  VStudy
//
//  Created by LAP14011 on 05/03/2023.
//

#import "TimestampView.h"

@implementation TimestampView

- (instancetype)init {
    self = [super initWithLabelCenterToView];
    if (self) {
    }
    return self;
}

- (instancetype)initWithBackgroundColor:(UIColor *)backgroundColor {
    self = [super initWithLabelCenterToView];
    if (self) {
        if (backgroundColor) {
            self.backgroundColor = backgroundColor;
        }
    }
    return self;
}

@end
