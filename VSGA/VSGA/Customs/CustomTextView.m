//
//  CustomTextView.m
//  VStudy
//
//  Created by LAP14011 on 04/03/2023.
//
#import <Masonry/Masonry.h>
#import "CustomTextView.h"

#import "Macro.h"
@implementation CustomTextView


- (instancetype)initWithFont:(UIFont *)font
               withTextColor:(UIColor *)textColor
         withPlaceholderText:(NSString *)placeholderText
    withPlaceholderTextColor:(UIColor *)placeholderTextColor {
    self = [super init];
    if (self) {
        self.font = font;
        self.textColor = textColor;
        self.showsHorizontalScrollIndicator = NO;
        
        [self initPlaceholder];
        self.placeholder.text = placeholderText;
        self.placeholder.font = font;
        self.placeholder.textColor = placeholderTextColor;
    }
    return self;
}

- (instancetype)initWithPlaceholderText:(NSString *)placeholderText
               withPlaceholderTextColor:(UIColor *)placeholderTextColor {
    self = [super init];
    if (self) {
        [self initPlaceholder];
        self.placeholder.text = placeholderText;
        self.placeholder.textColor = placeholderTextColor;
    }
    return self;
}

- (void)placeholderTopLeftPadding:(CGFloat)padding {
    [self.placeholder mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(padding);
        make.top.mas_equalTo(padding);
    }];
}

- (void)initPlaceholder {
    self.placeholder = [[UILabel alloc] init];
    [self addSubview:self.placeholder];
}

- (BOOL)isEmpty {
    if (self.text.length == 0) {
        return TRUE;
    }
    return FALSE;
}

- (void)displayPlaceholder {
    self.text = @"";
    self.placeholder.hidden = FALSE;
}

- (void)displayContent {
    self.placeholder.hidden = TRUE;
}

- (BOOL)isDisplayPlaceholder {
    if (self.placeholder.hidden == TRUE) {
        return FALSE;
    }
    return TRUE;
}

- (void)displayContentOrPlaceholder {
    if ([self isEmpty]) {
        [self displayPlaceholder];
    }
    else {
        [self displayContent];
    }
}

- (void)setFont:(UIFont *)font textColor:(UIColor *)textColor {
    self.placeholder.font = font;
    self.font = font;
    self.textColor = textColor;
    self.showsHorizontalScrollIndicator = NO;
}
@end
