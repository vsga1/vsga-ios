//
//  ErrorLabel.h
//  VStudy
//
//  Created by LAP14011 on 20/02/2023.
//

#import "CustomLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ErrorLabel : CustomLabel

- (instancetype)initWithOrigin:(CGPoint)origin
                      maxWidth:(CGFloat)maxWidth
                     titleFont:(UIFont *)titleFont;

- (void)setErrorLabelWithTitle:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
