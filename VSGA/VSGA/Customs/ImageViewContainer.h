//
//  ImageViewContainer.h
//  VStudy
//
//  Created by LAP14011 on 04/03/2023.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageViewContainer : UIView

@property (strong, nonatomic) UIImageView *imageView;

@property (nonatomic) BOOL isSelected;

@property (nonatomic) UIColor *normalTintColor;

@property (nonatomic) UIColor *selectedTintColor;

- (instancetype)initImageViewCenterToViewWithNormalTintColor:(nullable UIColor *)normalTintColor
                                       withSelectedTintColor:(nullable UIColor *)selectedTintColor;

@end

NS_ASSUME_NONNULL_END
