//
//  CustomButton.m
//  VStudy
//
//  Created by LAP14011 on 15/02/2023.
//

#import "CustomButton.h"

@interface CustomButton ()

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) NSString *originalButtonText;

@property (nonatomic, strong) UIColor *normalBackgroundColor;

@property (nonatomic, strong) UIColor *normalBorderColor;
@end

@implementation CustomButton

- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)title
                   titleColor:(UIColor *)titleColor
                    titleFont:(UIFont *)titleFont
              backgroundColor:(UIColor *)backgroundColor
                  borderColor:(CGColorRef)borderColor {
    self = [super initWithFrame:frame];
    if (self) {
        [self setTitle:title forState:UIControlStateNormal];
        [self setTitleColor:titleColor forState:UIControlStateNormal];
        [self.titleLabel setFont:titleFont];
    
        self.originalButtonText = self.titleLabel.text;
        
        self.backgroundColor = backgroundColor;
        
        self.layer.borderColor = borderColor;
        self.layer.borderWidth  = 0.5;
        self.layer.cornerRadius = 10;
        
        self.normalBackgroundColor = self.backgroundColor;
        self.normalBorderColor = [UIColor colorWithCGColor:self.layer.borderColor];
        
        /// shadow
        self.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.25] CGColor];
        self.layer.shadowOpacity = 0.8;
        self.layer.shadowRadius = 4;
        self.layer.shadowOffset = CGSizeMake(0, 2);
    }
    return self;
}

- (void)showLoading {
    [self setTitle:@""
          forState:UIControlStateNormal];
    [self showSpinning];
}

- (void)hideLoading {
    [self setTitle:self.originalButtonText
          forState:UIControlStateNormal];
    [self.activityIndicator stopAnimating];
    self.enabled = YES;
}

- (void)showAndHideLoadingInDuration:(CGFloat)time {
    self.originalButtonText = self.titleLabel.text;
    [self setTitle:@""
          forState:UIControlStateNormal];
    [self showSpinning];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, time * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self setTitle:self.originalButtonText
              forState:UIControlStateNormal];
        [self.activityIndicator stopAnimating];
        self.enabled = YES;
    });
}

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    if (!enabled) {
        self.backgroundColor = UIColor.lightGrayColor;
        self.layer.borderColor = UIColor.lightGrayColor.CGColor;
    }
    else {
        self.backgroundColor = self.normalBackgroundColor;
        self.layer.borderColor = self.normalBorderColor.CGColor;
    }
}

#pragma mark - Private methods

- (void)showSpinning {
    self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:self.activityIndicator];
    [self setActivityIndicatorInCenter];
    [self.activityIndicator startAnimating];
    self.enabled = NO;
}

- (void)setActivityIndicatorInCenter {
    NSLayoutConstraint *xCenterConstraint = [NSLayoutConstraint constraintWithItem:self
                                                                         attribute:NSLayoutAttributeCenterX
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.activityIndicator
                                                                         attribute:NSLayoutAttributeCenterX
                                                                        multiplier:1
                                                                          constant:0];
    [self addConstraint:xCenterConstraint];
    NSLayoutConstraint *yCenterConstraint = [NSLayoutConstraint constraintWithItem:self
                                                                         attribute:NSLayoutAttributeCenterY
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.activityIndicator
                                                                         attribute:NSLayoutAttributeCenterY
                                                                        multiplier:1
                                                                          constant:0];
    [self addConstraint:yCenterConstraint];
}

#pragma mark - Custom accessors

- (UIActivityIndicatorView *)activityIndicator {
    if (_activityIndicator) return _activityIndicator;
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleLarge];
    _activityIndicator.color = UIColor.whiteColor;
    _activityIndicator.hidesWhenStopped = YES;
    return _activityIndicator;
}

@end
