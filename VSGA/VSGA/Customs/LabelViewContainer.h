//
//  LabelViewContainer.h
//  VStudy
//
//  Created by LAP14011 on 04/03/2023.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LabelViewContainer : UIView

@property (strong, nonatomic) UILabel *label;

- (instancetype)initLabelEdgesEqualToViewWithFrame:(CGRect)frame;

- (instancetype)initWithLabelEdgesEqualToView;

- (instancetype)initWithLabelEdgesEqualToViewWithPadding:(UIEdgeInsets)padding;

- (instancetype)initWithLabelCenterToView;

- (void)displayLabelWithText:(NSString *)text
                    withFont:(UIFont *)font
               withTextColor:(UIColor *)textColor;

- (void)displayLabelWithFont:(UIFont *)font
           withTextColor:(UIColor *)textColor;

@end

NS_ASSUME_NONNULL_END
