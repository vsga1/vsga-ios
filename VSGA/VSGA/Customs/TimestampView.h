//
//  TimestampView.h
//  VStudy
//
//  Created by LAP14011 on 05/03/2023.
//

#import "LabelViewContainer.h"

NS_ASSUME_NONNULL_BEGIN

@interface TimestampView : LabelViewContainer

- (instancetype)init;

- (instancetype)initWithBackgroundColor:(UIColor *)backgroundColor;

@end

NS_ASSUME_NONNULL_END
