//
//  CustomLabel.h
//  VStudy
//
//  Created by LAP14011 on 16/02/2023.
//

#import <UIKit/UIKit.h>
#import "NSString+Extension.h"
NS_ASSUME_NONNULL_BEGIN

@interface CustomLabel : UILabel

@property (nonatomic, assign) UIEdgeInsets insets;

- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)title
                    titleFont:(UIFont *)titleFont
                   titleColor:(UIColor *)titleColor;

- (void)setFrame:(CGRect)frame
           title:(NSString *)title;

- (void)setTitle:(NSString *)title
       titleFont:(UIFont *)titleFont
      titleColor:(UIColor *)titleColor;

@end

NS_ASSUME_NONNULL_END
