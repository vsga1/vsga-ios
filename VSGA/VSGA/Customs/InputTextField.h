//
//  InputTextField.h
//  VSGA
//
//  Created by LAP14011 on 07/02/2023.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InputTextField : UITextField

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@end

NS_ASSUME_NONNULL_END
