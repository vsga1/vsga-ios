//
//  TitleAndToggleButton.h
//  VStudy
//
//  Created by TamNVM on 18/06/2023.
//

#import <UIKit/UIKit.h>
#import "CustomLabel.h"
#import "RoomSection.h"

NS_ASSUME_NONNULL_BEGIN

@interface TitleAndToggleButton : UIView

@property (nonatomic, strong) CustomLabel *titleLabel;

@property (nonatomic, strong) UIButton *disclosureButton;

@property (nonatomic, copy) void (^toggleHandler)(void);

- (void)setObjectWithTaskSection:(RoomSection *)section;


@end

NS_ASSUME_NONNULL_END
