//
//  CustomLabel.m
//  VStudy
//
//  Created by LAP14011 on 16/02/2023.
//

#import "CustomLabel.h"

@implementation CustomLabel

- (instancetype)init {
    self = [super init];
    if (self) {
        self.numberOfLines = 0;
        self.textAlignment = NSTextAlignmentLeft;
        self.insets = UIEdgeInsetsZero;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)title
                    titleFont:(UIFont *)titleFont
                   titleColor:(UIColor *)titleColor {
    self = [super initWithFrame:frame];
    if (self) {
        self.numberOfLines = 0;
        self.textAlignment = NSTextAlignmentLeft;
        self.font = titleFont;
        self.text = title;
        self.textColor = titleColor;
        self.insets = UIEdgeInsetsZero;
    }
    return self;
}

- (void)setFrame:(CGRect)frame
           title:(NSString *)title {
    self.frame = frame;
    self.text = title;
}

- (void)setTitle:(NSString *)title
       titleFont:(UIFont *)titleFont
      titleColor:(UIColor *)titleColor {
    self.font = titleFont;
    self.text = title;
    self.textColor = titleColor;
}

- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = self.insets;
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}


@end
