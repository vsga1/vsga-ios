//
//  AppDelegate.m
//  VSGA
//
//  Created by LAP14011 on 31/01/2023.
//

#import "AppDelegate.h"

@import GoogleSignIn;
@import FBSDKCoreKit;

#import "SplashScreenView.h"

#import "GetStartedViewController.h"
#import "HomeViewController.h"

#import "AccountManager.h"

#import "MySocketManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

+ (AppDelegate *)shared {
    return (AppDelegate *)UIApplication.sharedApplication.delegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] init];
    [self updateView];
    
    CGRect mainFrame = [UIScreen mainScreen].bounds;
    SplashScreenView *splashScreenView = [[SplashScreenView alloc] initWithFrame:mainFrame];
    [[self.window.rootViewController view] addSubview:splashScreenView];
    [[self.window.rootViewController view] bringSubviewToFront:splashScreenView];
    
    [self.window makeKeyAndVisible];
    
    [UIView transitionWithView:self.window
                      duration:1.0f
                       options:UIViewAnimationOptionTransitionNone
                    animations:^(void){
        splashScreenView.alpha=0.0f;
    }
                    completion:^(BOOL finished){
        [splashScreenView removeFromSuperview];
    }];
    
    /// Initialize Facebook SDK
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];


    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
//    [[MySocketManager sharedInstance] disconnect];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
//    [[MySocketManager sharedInstance] connect];
}

- (void)updateView {
    if ([[AccountManager sharedManager] isUserLoggedIn]) {
        [self switchToHomeView];
    } else {
        [self switchToGetStartedView];
    }
    [UIView transitionWithView:self.window
                      duration:1.0f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];
}

#pragma mark - Operations
- (void)switchToGetStartedView {
    if (self.window.rootViewController != nil) {
        [self.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
    }
    
    GetStartedViewController *getStartedVC = [[GetStartedViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:getStartedVC];
    self.window.rootViewController = navigationController;
    
}

- (void)switchToHomeView {
    if (self.window.rootViewController != nil) {
        [self.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
    }
    
    HomeViewController *homeViewController = [[HomeViewController alloc] init];
    self.window.rootViewController = homeViewController;
}

#pragma mark - Google Sign In

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    
    BOOL googleHandled = [GIDSignIn.sharedInstance handleURL:url];
    BOOL facebookHandled = [FBSDKApplicationDelegate.sharedInstance application:app
                                                                        openURL:url
                                                                        options:options];
    if (googleHandled || facebookHandled) {
        return YES;
    }
    return NO;
}

@end
