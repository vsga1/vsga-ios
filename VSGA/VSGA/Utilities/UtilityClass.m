//
//  UtilityClass.m
//  VStudy
//
//  Created by TamNVM on 10/06/2023.
//

#import "UtilityClass.h"
#import <Toast/UIView+Toast.h>
#import "Macro.h"

@implementation UtilityClass

+ (UIWindow *)getWindow {
    UIWindow *window = nil;
    
    if (@available(iOS 13.0, *)) {
        for (UIWindowScene *scene in [UIApplication sharedApplication].connectedScenes) {
            if (scene.activationState == UISceneActivationStateForegroundActive) {
                window = scene.windows.firstObject;
                break;
            }
        }
    } else {
        window = [UIApplication sharedApplication].keyWindow;
    }
    
    return window;
}


+ (CGFloat)getStatusBarHeight {
    CGFloat statusBarHeight = 0.0;
    UIWindow *window = [self getWindow];
    if (window.windowScene.statusBarManager.statusBarFrame.size.height > 0.0) {
        statusBarHeight = window.windowScene.statusBarManager.statusBarFrame.size.height;
    }
    return statusBarHeight;
}

+ (CGFloat)getSafeAreaBottomHeight {
    CGFloat safeAreaBottomHeight = 0.0;
    UIWindow *window = [self getWindow];
    if (window.safeAreaInsets.bottom > 0.0) {
        safeAreaBottomHeight = window.safeAreaInsets.bottom;
    }
    return safeAreaBottomHeight;
    
}

+ (void)showSuccessToast:(NSString *)message
            centerInView:(UIView *)view
              completion:(void (^)(void))completion {
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.backgroundColor = [UIColor colorWithRed:62/255.0 green:176/255.0 blue:87/255.0 alpha:1.0];
    style.messageColor = [UIColor whiteColor];
    style.cornerRadius = 8.0;
    style.messageFont = kRegular16;
    style.messageAlignment = NSTextAlignmentCenter;
    
    [view makeToast:message
           duration:1.0
           position:CSToastPositionCenter
              title:nil
              image:nil
              style:style
         completion:^(BOOL didTap) {
        if (completion) {
            completion();
        }
    }];
}

+ (void)showFailureToast:(NSString *)message
            centerInView:(UIView *)view
              completion:(void (^)(void))completion {
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.backgroundColor = [UIColor colorWithRed:0.8 green:0.0 blue:0.0 alpha:0.8];
    style.messageColor = [UIColor whiteColor];
    style.cornerRadius = 8.0;
    style.messageFont = kRegular16;
    style.messageAlignment = NSTextAlignmentCenter;
    
    [view makeToast:message
           duration:1.0
           position:CSToastPositionCenter
              title:nil
              image:nil
              style:style
         completion:^(BOOL didTap) {
        if (completion) {
            completion();
        }
    }];
}

+ (void)showLoadingToastCenterInView:(UIView *)view {
    [view makeToastActivity:CSToastPositionCenter];
}

+ (void)hideLoadingToastOfView:(UIView *)view {
    [view hideToastActivity];
}

@end
