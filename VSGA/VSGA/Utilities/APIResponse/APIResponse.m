//
//  APIResponse.m
//  VStudy
//
//  Created by LAP14011 on 19/02/2023.
//

#import "APIResponse.h"

@implementation APIResponse

+ (id)isValidResponse:(NSString *)responseString {
    if (responseString == nil) {
        return nil;
    }
    NSData *jsonData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    
    /// Note that JSONObjectWithData will return either an NSDictionary or an NSArray, depending whether your JSON string represents an a dictionary or an array.
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    if (error) {
        NSLog(@"Error parsing JSON: %@", error);
        return nil;
    }
    else {
        if ([jsonObject isKindOfClass:[NSArray class]]) {
            NSArray *jsonArray = (NSArray *)jsonObject;
            NSLog(@"jsonArray - %@",jsonArray);
            return jsonArray;
            
        }
        else {
            NSLog(@"it is a dictionary");
            NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
            return jsonDictionary;
        }
    }
}

+ (BOOL)parseResponseSuccessFromJSON:(id)jsonObject {
    BOOL isSuccessful = NO;
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
//        NSArray *jsonArray = (NSArray *)jsonObject;
        
    }
    else {
        NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
        NSDictionary *metaData = jsonDictionary[@"meta"];
        long code = [metaData[@"code"] longValue];
        if (code == 200) {
            isSuccessful = YES;
        }
    }
    
    return isSuccessful;
}

+ (long)getCodeResponseFromJSON:(id)jsonObject {
    NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
    NSDictionary *metaData = jsonDictionary[@"meta"];
    long code = [metaData[@"code"] longValue];
    return code;
}
@end
