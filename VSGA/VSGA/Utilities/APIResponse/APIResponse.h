//
//  APIResponse.h
//  VStudy
//
//  Created by LAP14011 on 19/02/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface APIResponse : NSObject

+ (id)isValidResponse:(NSString *)responseString;

+ (BOOL)parseResponseSuccessFromJSON:(id)jsonObject;

+ (long)getCodeResponseFromJSON:(id)jsonObject;

@end

NS_ASSUME_NONNULL_END
