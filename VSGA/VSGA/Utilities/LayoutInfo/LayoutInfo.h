//
//  LayoutInfo.h
//  VStudy
//
//  Created by LAP14011 on 05/03/2023.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TSMutableDictionary.h"

NS_ASSUME_NONNULL_BEGIN

@interface LayoutInfo : NSObject

@property (strong, nonatomic) TSMutableDictionary *children;

@property (assign, nonatomic) CGRect frame;

@property (nonatomic) CGFloat left;

@property (nonatomic) CGFloat top;

@property (nonatomic) CGFloat right;

@property (nonatomic) CGFloat bottom;

@property (nonatomic) CGFloat width;

@property (nonatomic) CGFloat height;

- (instancetype)initWithFrame:(CGRect)frame children:(NSDictionary * _Nullable)children;

- (void)addChild:(LayoutInfo *)childLayout forKey:(NSString *)key;

- (LayoutInfo *)addChildWithOrigin:(CGPoint)origin
                              size:(CGSize)size
                            forKey:(NSString *)key;

- (LayoutInfo *)addChildWithFrame:(CGRect)frame
                           forKey:(NSString *)key;

- (CGRect)childFrameForKey:(NSString *)key;

+ (CGRect)findFrameLayoutIn:(LayoutInfo *)layout forKey:(NSString *)key;

+ (LayoutInfo *)findLayoutIn:(LayoutInfo *)layout forKey:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
