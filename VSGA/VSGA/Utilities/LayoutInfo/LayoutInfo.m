//
//  LayoutInfo.m
//  VStudy
//
//  Created by LAP14011 on 05/03/2023.
//

#import "LayoutInfo.h"

@implementation LayoutInfo

- (instancetype)initWithFrame:(CGRect)frame children:(TSMutableDictionary *)children {
    self = [super init];
    if (self) {
        self.frame = frame;
        self.children = children;
    }
    return self;
}

- (void)addChild:(LayoutInfo *)childLayout forKey:(NSString *)key {
    if (!self.children) {
        self.children = [[TSMutableDictionary alloc] init];
    }
    [self.children setObject:childLayout forKey:key];
}

- (LayoutInfo *)addChildWithOrigin:(CGPoint)origin
                              size:(CGSize)size
                            forKey:(NSString *)key {
    CGRect childFrame = CGRectMake(origin.x,
                                   origin.y,
                                   size.width,
                                   size.height);
    LayoutInfo *child = [[LayoutInfo alloc] initWithFrame:childFrame children:nil];
    [self addChild:child forKey:key];
    return child;
}

- (LayoutInfo *)addChildWithFrame:(CGRect)frame forKey:(NSString *)key {
    LayoutInfo *child = [[LayoutInfo alloc] initWithFrame:frame children:nil];
    [self addChild:child forKey:key];
    return child;
}

- (CGRect)childFrameForKey:(NSString *)key {
    LayoutInfo *layout = [self.children objectForKey:key];
    return layout.frame;
}

+ (CGRect)findFrameLayoutIn:(LayoutInfo *)layout forKey:(NSString *)key {
    if (!layout.children) {
        return CGRectNull;
    }
    LayoutInfo *child = [layout.children objectForKey:key];
    if (child) {
        return child.frame;
    }
    CGRect result = CGRectNull;
    for (LayoutInfo *child in [layout.children allValues]) {
        result = [LayoutInfo findFrameLayoutIn:child forKey:key];
        if (!CGRectIsEmpty(result)) {
            return result;
        }
    }
    return result;
}

+ (LayoutInfo *)findLayoutIn:(LayoutInfo *)layout forKey:(NSString *)key {
    if (!layout.children) {
        return nil;
    }
    LayoutInfo *child = [layout.children objectForKey:key];
    if (child) {
        return child;
    }
    LayoutInfo *result = nil;
    for (LayoutInfo *child in [layout.children allValues]) {
        result = [LayoutInfo findLayoutIn:child forKey:key];
        if (result) {
            return result;
        }
    }
    return result;
}

- (CGFloat)top {
    return self.frame.origin.y;
}

- (CGFloat)left {
    return self.frame.origin.x;
}

- (CGFloat)bottom {
    return self.frame.origin.y + self.frame.size.height;
}

- (CGFloat)right {
    return self.frame.origin.x + self.frame.size.width;
}

- (CGFloat)width {
    return self.frame.size.width;
}

- (CGFloat)height {
    return self.frame.size.height;
}

@end
