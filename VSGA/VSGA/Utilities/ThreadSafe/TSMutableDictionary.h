//
//  TSMutableDictionary.h
//  VStudy
//
//  Created by LAP14011 on 05/03/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSMutableDictionary<NSCopying, NSObject> : NSMutableDictionary

@end

NS_ASSUME_NONNULL_END
