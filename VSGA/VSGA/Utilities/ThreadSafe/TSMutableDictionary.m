//
//  TSMutableDictionary.m
//  VStudy
//
//  Created by LAP14011 on 05/03/2023.
//

#import "TSMutableDictionary.h"

@interface TSMutableDictionary()

@property (strong, nonatomic) dispatch_queue_t concurrent_queue;

@property (strong, nonatomic) NSMutableDictionary *dictionary;

@end

@implementation TSMutableDictionary

- (instancetype)init{
    self = [super init];
    if (self) {
        self.concurrent_queue = dispatch_queue_create("com.domain.tsmutabledictionary", DISPATCH_QUEUE_CONCURRENT);
        self.dictionary = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)setObject:(id)anObject
           forKey:(id<NSCopying>)aKey {
    dispatch_barrier_async(self.concurrent_queue, ^{
        [self.dictionary setObject:anObject forKey:aKey];
    });
}

- (void)removeObjectForKey:(id)aKey {
    dispatch_barrier_async(self.concurrent_queue, ^{
        [self.dictionary removeObjectForKey:aKey];
    });
}

- (instancetype)initWithObjects:(id  _Nonnull const [])objects
                        forKeys:(id<NSCopying>  _Nonnull const [])keys
                          count:(NSUInteger)cnt {
    self = [super initWithObjects:objects forKeys:keys count:cnt];
    return self;
}

- (NSUInteger)count {
    __block NSUInteger count;
    dispatch_sync(self.concurrent_queue, ^{
        count = self.dictionary.count;
    });
    return count;
}

- (id)objectForKey:(id)aKey {
    __block id obj;
    dispatch_sync(self.concurrent_queue, ^{
        obj = [self.dictionary objectForKey:aKey];
    });
    return obj;
}

- (NSEnumerator *)keyEnumerator {
    __block NSEnumerator * enu;
    dispatch_sync(self.concurrent_queue, ^{
        enu = [self.dictionary keyEnumerator];
    });
    return enu;
}

- (id)copy{
    __block id copyInstance;
    dispatch_sync(self.concurrent_queue, ^{
        copyInstance = [self.dictionary copy];
    });
    return copyInstance;
}
@end
