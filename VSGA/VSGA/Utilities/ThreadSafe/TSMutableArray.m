//
//  TSMutableArray.m
//  VStudy
//
//  Created by LAP14011 on 05/03/2023.
//

#import "TSMutableArray.h"


@interface TSMutableArray()

@property (strong, nonatomic) dispatch_queue_t concurrent_queue;

@property (strong, nonatomic) NSMutableArray *array;

@end

@implementation TSMutableArray

- (instancetype)init {
    self = [super init];
    if (self) {
        self.concurrent_queue = dispatch_queue_create("com.domain.tsmutablearray", DISPATCH_QUEUE_CONCURRENT);
        self.array = [[NSMutableArray alloc] init];
    }
    return self;
}

- (instancetype)initWithArray:(NSArray *)array {
    self = [super init];
    if (self) {
        self.concurrent_queue = dispatch_queue_create("com.domain.tsmutablearray", DISPATCH_QUEUE_CONCURRENT);
        self.array = [[NSMutableArray alloc] initWithArray:array];
    }
    return self;
}

- (void)insertObject:(id)anObject
             atIndex:(NSUInteger)index {
    dispatch_barrier_async(self.concurrent_queue, ^{
        [self.array insertObject:anObject atIndex:index];
    });
}

- (void)removeObjectAtIndex:(NSUInteger)index {
    dispatch_barrier_async(self.concurrent_queue, ^{
        if (index < [self.array count]) {
            [self.array removeObjectAtIndex:index];
        }
    });
}

- (void)addObject:(id)anObject {
    dispatch_barrier_async(self.concurrent_queue, ^{
        if (anObject) {
            [self.array addObject:anObject];
        }
    });
}

- (void)removeLastObject {
    dispatch_barrier_async(self.concurrent_queue, ^{
        [self.array removeLastObject];
    });
}

- (void)replaceObjectAtIndex:(NSUInteger)index
                  withObject:(id)anObject {
    dispatch_barrier_async(self.concurrent_queue, ^{
        if (anObject && index < [self.array count]) {
            [self.array replaceObjectAtIndex:index
                                  withObject:anObject];
        }
    });
}

- (void)addObjectsFromArray:(NSArray *)otherArray {
    dispatch_barrier_async(self.concurrent_queue, ^{
        if (otherArray){
            [self.array addObjectsFromArray:otherArray];
        }
    });
}

- (id)objectAtIndex:(NSUInteger)index {
    __block id obj;
    dispatch_sync(self.concurrent_queue, ^{
        if (index < self.array.count) {
            obj = [self.array objectAtIndex:index];
        }
    });
    return obj;
}

- (NSUInteger)count {
    __block NSUInteger count;
    dispatch_sync(self.concurrent_queue, ^{
        count = self.array.count;
    });
    return count;
}

- (NSArray *)getArray {
    __block NSArray *array;
    dispatch_sync(self.concurrent_queue, ^{
        array = [self.array copy];
    });
    return array;
}
@end
