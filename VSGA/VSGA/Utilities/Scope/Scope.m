//
//  Scope.m
//  VStudy
//
//  Created by LAP14011 on 18/02/2023.
//

#import "Scope.h"

void ext_executeCleanupBlock (__strong ext_cleanupBlock_t *block) {
    (*block)();
}
