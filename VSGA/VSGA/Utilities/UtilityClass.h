//
//  UtilityClass.h
//  VStudy
//
//  Created by TamNVM on 10/06/2023.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UtilityClass : NSObject

+ (void)showSuccessToast:(NSString *)message
            centerInView:(UIView *)view
              completion:(void (^)(void))completion;

+ (void)showFailureToast:(NSString *)message
            centerInView:(UIView *)view
              completion:(void (^)(void))completion;

+ (void)showLoadingToastCenterInView:(UIView *)view;

+ (void)hideLoadingToastOfView:(UIView *)view;

+ (CGFloat)getStatusBarHeight;

+ (CGFloat)getSafeAreaBottomHeight;

@end

NS_ASSUME_NONNULL_END
