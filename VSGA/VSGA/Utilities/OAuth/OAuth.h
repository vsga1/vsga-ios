//
//  OAuth.h
//  VStudy
//
//  Created by LAP14011 on 18/02/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, OAuthContentType) {
    OAuthContentTypeUrlEncodedForm,
    OAuthContentTypeJsonObject
};

@interface OAuth : NSObject

+ (NSURLRequest *)URLRequestURL:(NSString *)stringURL
                     parameters:(nullable NSDictionary *)unencodedParameters
                    accessToken:(nullable NSString *)accessToken
                  requestMethod:(NSString *)method
                   dataEncoding:(OAuthContentType)dataEncoding
                   headerValues:(nullable NSDictionary *)headerValues;

@end

NS_ASSUME_NONNULL_END
