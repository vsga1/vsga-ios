//
//  OAuth.m
//  VStudy
//
//  Created by LAP14011 on 18/02/2023.
//

#import "OAuth.h"

#define PCEN(s) \
([[s description] stringByAddingPercentEncodingWithAllowedCharacters:[[NSCharacterSet characterSetWithCharactersInString:@"^!*'();:@&=+$,/?%#[]{}\"`<>\\| "] invertedSet]])

#define Chomp(s) { \
const NSUInteger length = [s length]; \
if (length > 0) \
[s deleteCharactersInRange:NSMakeRange(length - 1, 1)]; \
}

#define OAuthURLRequestTimeout 1000.0

@implementation OAuth {
    NSString *accessToken;
    NSURL *url;
    NSDictionary *params;
    NSString *method;
}

- (id)initWithAccessToken:(NSString *)accessToken {
    accessToken = accessToken;
    return self;
}

- (NSMutableURLRequest *)requestWithHeaderValues:(NSDictionary *)headerValues {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:OAuthURLRequestTimeout];
    if (headerValues) {
        for (NSString* key in headerValues) {
            id value = [headerValues objectForKey:key];
            if ([value isKindOfClass:[NSString class]]) {
                [request setValue:value forHTTPHeaderField:key];
            }
        }
    }
    [request setHTTPMethod:method];
    return request;
}

// unencodedParameters are encoded and assigned to self->params, returns encoded queryString
- (id)setParameters:(NSDictionary *)unencodedParameters {
    NSMutableString *queryString = [NSMutableString string];
    NSMutableDictionary *encodedParameters = [NSMutableDictionary new];
    for (NSString *key in unencodedParameters.allKeys)
    {
        NSString *enkey = PCEN(key);
        id value = unencodedParameters[key];
        if ([value isKindOfClass:[NSArray class]]) {
            NSArray *valueArray = (NSArray *)value;
            NSString *joinedValues = [valueArray componentsJoinedByString:@","];
            NSString *envalue = PCEN(joinedValues);
            encodedParameters[enkey] = envalue;
            
            [queryString appendString:enkey];
            [queryString appendString:@"="];
            [queryString appendString:envalue];
        } else {
            NSString *envalue = PCEN(value);
            encodedParameters[enkey] = envalue;
            
            [queryString appendString:enkey];
            [queryString appendString:@"="];
            [queryString appendString:envalue];
        }
        
        [queryString appendString:@"&"];
//        id envalue = PCEN(unencodedParameters[key]);
//
//        encodedParameters[enkey] = envalue;
//        [queryString appendString:enkey];
//        [queryString appendString:@"="];
//        [queryString appendString:envalue];
       
    }
    Chomp(queryString);
    params = [encodedParameters copy];
    return queryString;
}

//- (NSString *)percentEncodeString:(NSString *)string {
//    NSCharacterSet *allowedCharacters = [NSCharacterSet URLQueryAllowedCharacterSet];
//    NSString *encodedString = [string stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacters];
//    return encodedString;
//}

+ (NSURLRequest *)URLRequestURL:(NSString *)stringURL
                     parameters:(nullable NSDictionary *)unencodedParameters
                    accessToken:(nullable NSString *)accessToken
                  requestMethod:(NSString *)method
                   dataEncoding:(OAuthContentType)dataEncoding
                   headerValues:(nullable NSDictionary *)headerValues {
    if (!stringURL || !method) {
        return nil;
    }
    OAuth *oauth = [[OAuth alloc] initWithAccessToken:accessToken];
    if (!oauth) {
        return nil;
    }
    oauth->method = method;
    oauth->url = [[NSURL alloc] initWithString:stringURL];
    
    NSMutableURLRequest *request;
    if ([method isEqualToString:@"GET"]) {
        if (unencodedParameters) {
            id path = [oauth setParameters:unencodedParameters];
            [path insertString:@"?" atIndex:0];
            oauth->url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",
                                                        stringURL, path]];
        }
        else {
            oauth->url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@",
                                                        stringURL]];
        }
       
        request = [oauth requestWithHeaderValues:headerValues];
    } else {
        if (dataEncoding == OAuthContentTypeUrlEncodedForm) {
            NSMutableString *postbody = [oauth setParameters:unencodedParameters];
            request = [oauth requestWithHeaderValues:headerValues];
            
            if (postbody.length) {
                [request setHTTPBody:[postbody dataUsingEncoding:NSUTF8StringEncoding]];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
                [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)request.HTTPBody.length] forHTTPHeaderField:@"Content-Length"];
            }
        }
        else if (dataEncoding == OAuthContentTypeJsonObject) {
            NSError *error;
            NSData *postbody = [NSJSONSerialization dataWithJSONObject:unencodedParameters options:0 error:&error];
            if (error || !postbody) {
                NSLog(@"Got an error encoding JSON: %@", error);
            } else {
                [oauth setParameters:@{}]; // empty dictionary populates variables without putting data into the signature_base
                request = [oauth requestWithHeaderValues:headerValues];
                
                if (postbody.length) {
                    [request setHTTPBody:postbody];
                    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)request.HTTPBody.length] forHTTPHeaderField:@"Content-Length"];
                }
            }
        }
        // invalid type
        else {
            oauth = nil;
            request = nil;
        }
    }
   
    return request;
}

@end
