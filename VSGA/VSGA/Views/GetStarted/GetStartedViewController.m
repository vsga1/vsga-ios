//
//  GetStartedView.m
//  VSGA
//
//  Created by LAP14011 on 06/02/2023.
//

#import "GetStartedViewController.h"

#import "Macro.h"

#import "UIView+Extension.h"
#import "NSString+Extension.h"

#import "CustomButton.h"

#import "SignInViewController.h"
#import "SignUpViewController.h"

#define kAuthBottomPadding 40
#define buttonSpace 10

@interface GetStartedViewController()

@property (nonatomic, strong) CustomButton *signInButton;

@property (nonatomic, strong) CustomButton *signUpButton;

@end

@implementation GetStartedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createView];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = TRUE;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.signInButton.userInteractionEnabled = YES;
    self.signUpButton.userInteractionEnabled = YES;
}

#pragma mark - CreateView
- (void)createView {
    self.view.backgroundColor = UIColor.whiteColor;
    
    [self createImageView];
    [self createTitleLabel];
    [self createDescriptionLabel];
    
    [self createSignInButton];
    [self createSignUpButton];

}

- (void)createImageView {
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 342, 217)];
    imageView.image = [UIImage imageNamed:@"GetStartedImage"];
    imageView.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/4+30);
    
    [self.view addSubview:imageView];
}

- (void)createTitleLabel {
    NSString *title = @"Welcome to VStudy";
    UIFont *fontSize = kSemiBold32;
    CGFloat textWidth = self.view.width - 20;
    CGSize textSize = [title sizeOfStringWithStyledFont:fontSize withSize:CGSizeMake(textWidth, CGFLOAT_MAX)];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, textSize.width, textSize.height)];
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = fontSize;
    titleLabel.text = title;
    titleLabel.textColor = kPrimaryColor;
    titleLabel.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2+50);
    
    [self.view addSubview:titleLabel];
}

- (void)createDescriptionLabel {
    NSString *title = @"Where you can study, meet and chat with everybody from all over the world";
    UIFont *fontSize = kRegular16;
    CGFloat textWidth = self.view.width - 20;
    CGSize textSize = [title sizeOfStringWithStyledFont:fontSize withSize:CGSizeMake(textWidth, CGFLOAT_MAX)];
    
    UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, textSize.width, textSize.height)];
    descLabel.numberOfLines = 0;
    descLabel.textAlignment = NSTextAlignmentCenter;
    descLabel.font = fontSize;
    descLabel.text = title;
    descLabel.textColor = UIColor.blackColor;
    descLabel.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2 + 100);
}

- (void)createSignInButton {
    CGFloat x = (self.view.width - kButtonWidth) / 2;
    CGFloat y = self.view.height - kButtonHeight * 2 - kAuthBottomPadding - buttonSpace;
    CGRect frame = CGRectMake(x, y, kButtonWidth, kButtonHeight);
    
    UIFont *titleFont = kSemiBold16;
    
    self.signInButton = [[CustomButton alloc] initWithFrame:frame
                                                      title:@"I already have an account"
                                                 titleColor:kPrimaryColor
                                                  titleFont:titleFont backgroundColor:UIColor.whiteColor
                                                borderColor:kPrimaryColor.CGColor];
    [self.signInButton addTarget:self action:@selector(signInButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.signInButton];
}

- (void)createSignUpButton {
    CGFloat x = (self.view.width - kButtonWidth) / 2;
    CGFloat y = self.view.height - kButtonHeight - kAuthBottomPadding;
    CGRect frame = CGRectMake(x, y, kButtonWidth, kButtonHeight);
    
    UIFont *titleFont = kSemiBold16;
    
    self.signUpButton = [[CustomButton alloc] initWithFrame:frame
                                                      title:@"Get Started"
                                                 titleColor:UIColor.whiteColor
                                                  titleFont:titleFont
                                            backgroundColor:kPrimaryColor
                                                borderColor:kPrimaryColor.CGColor];
    [self.signUpButton addTarget:self action:@selector(signUpButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.signUpButton];
}

#pragma mark - OnClicked

- (void)signInButtonClicked:(UIButton *)sender {

    sender.userInteractionEnabled = NO;
    
    SignInViewController *signInVC = [[SignInViewController alloc] init];
    [self.navigationController pushViewController:signInVC animated:YES];

}

- (void)signUpButtonClicked:(UIButton *)sender {

    sender.userInteractionEnabled = NO;
    
    SignUpViewController *signUpVC = [[SignUpViewController alloc] init];
    [self.navigationController pushViewController:signUpVC animated:YES];

}

@end
