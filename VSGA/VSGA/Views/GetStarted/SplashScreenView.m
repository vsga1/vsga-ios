//
//  SplashScreenView.m
//  VSGA
//
//  Created by LAP14011 on 05/02/2023.
//

#import "SplashScreenView.h"
#import "UIView+Extension.h"
#import "NSString+Extension.h"

#import "Macro.h"

@implementation SplashScreenView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createView];
    }
    return self;
}

- (void)createView {
    self.backgroundColor = UIColor.whiteColor;
    UIView *viewContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 190, 60)];
    viewContainer.center = CGPointMake(self.width/2, self.height/2);
    
    [self addSubview:viewContainer];
    
    
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Logo"]];
    logoImageView.contentMode = UIViewContentModeScaleAspectFill;
    logoImageView.backgroundColor = [UIColor clearColor];
    logoImageView.size = CGSizeMake(60, 60);
    
    [viewContainer addSubview:logoImageView];
    
    NSString *title = @"VStudy";
    CGFloat textWidth = self.width - 20;
    UIFont *fontSize = kSemiBold32;
    CGSize textSize = [title sizeOfStringWithStyledFont:fontSize withSize:CGSizeMake(textWidth, CGFLOAT_MAX)];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 10, textSize.width, textSize.height)];
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = fontSize;
    titleLabel.text = title;
    titleLabel.textColor = kPrimaryColor;
    
    [viewContainer addSubview:titleLabel];
}



@end
