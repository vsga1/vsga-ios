//
//  TopicCollectionViewCell.m
//  VStudy
//
//  Created by LAP14011 on 07/05/2023.
//

#import "TopicCollectionViewCell.h"

@implementation TopicCollectionViewCell

+ (NSString *)cellIdentifier {
    return NSStringFromClass(self.class);
}

- (void)setContents {
    // Pill view
    _pillView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.topicItem.itemSize.width, self.topicItem.itemSize.height)];
    _pillView.backgroundColor = self.topicItem.backgroundColor;
    _pillView.layer.cornerRadius = _pillView.frame.size.height / 2 - 5;
    [self.contentView addSubview:_pillView];
    
    // Initialize the title label
    CGFloat x = (self.topicItem.itemSize.width - self.topicItem.labelSize.width) / 2;
    CGFloat y = (self.topicItem.itemSize.height - self.topicItem.labelSize.height) / 2;
    
    _titleLabel = [[CustomLabel alloc] initWithFrame:CGRectMake(x, y, self.topicItem.labelSize.width, self.topicItem.labelSize.height)
                                               title:self.topicItem.name
                                           titleFont:self.topicItem.nameFont
                                          titleColor:self.topicItem.textColor];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:_titleLabel];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    // Remove all subviews from the content view
    for (UIView *subview in self.contentView.subviews) {
        [subview removeFromSuperview];
    }
    _pillView = nil;
    _titleLabel = nil;
}

@end
