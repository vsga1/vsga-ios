//
//  RoomCollectionViewCell.m
//  VStudy
//
//  Created by LAP14011 on 20/02/2023.
//

#import "RoomCollectionViewCell.h"
#import "Macro.h"
#import "TopicCollectionViewCell.h"

static CGFloat kIconSize = 20;
static CGFloat kIconSpacting = 8;
static CGFloat kContentMargin = 10;
static CGFloat kComponentSpacing = 3;
static CGFloat kTopicHeight = 30;
static CGFloat kMemberLabelWidth = 50;

@interface RoomCollectionViewCell () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@end

@implementation RoomCollectionViewCell

- (void)layoutSubviews {
    self.contentView.layer.cornerRadius = 12.0f;
    self.contentView.layer.borderWidth = 1.0f;
    self.contentView.layer.borderColor = [UIColor clearColor].CGColor;
    self.contentView.layer.masksToBounds = YES;
    
    self.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.layer.shadowRadius = 4.0f;
    self.layer.shadowOpacity = 0.5f;
    self.layer.masksToBounds = NO;
    
    self.backgroundColor = UIColor.clearColor;
}

+ (NSString *)cellIdentifier {
    return NSStringFromClass(self.class);
}

- (UIView *)upperHalfView {
    if (_upperHalfView) {
        return _upperHalfView;
    }
    _upperHalfView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.width, 46)];
    _upperHalfView.backgroundColor = kPrimaryColor;
    [self.contentView addSubview:_upperHalfView];
    return _upperHalfView;
}

- (UIView *)lowerHalfView {
    if (_lowerHalfView) {
        return _lowerHalfView;
    }
    _lowerHalfView = [[UIView alloc] initWithFrame:CGRectMake(0, _upperHalfView.bottom, self.width, self.height - _upperHalfView.height)];
    _lowerHalfView.backgroundColor = UIColor.whiteColor;
    [self.contentView addSubview:_lowerHalfView];
    return _lowerHalfView;
}

- (CustomLabel *)roomNameLabel {
    if (_roomNameLabel) {
        return _roomNameLabel;
    }
    NSString *title = @"Name of room";
    UIFont *titleFont = kMedium18;
    
    CGFloat maxWidth = _upperHalfView.width - 2 * kContentMargin;
    CGFloat maxHeight = _upperHalfView.height - 2 * kContentMargin;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(maxWidth, maxHeight)];
    
    CGFloat width = maxWidth;
    CGFloat height = titleSize.height;
    CGFloat x = kContentMargin;
    CGFloat y = (maxHeight - height)/2 + kContentMargin;
    
    _roomNameLabel = [[CustomLabel alloc]
                      initWithFrame:CGRectMake(x,
                                               y,
                                               width,
                                               height)
                      title:title
                      titleFont:titleFont
                      titleColor:UIColor.whiteColor];
    
    [_upperHalfView addSubview:_roomNameLabel];
    return _roomNameLabel;
}

- (UICollectionView *)topicCollectionView {
    if (_topicCollectionView) {
        return _topicCollectionView;
    }
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    CGRect frame = CGRectMake(kContentMargin,
                              kContentMargin,
                              _lowerHalfView.width - 2 * kContentMargin,
                              kTopicHeight);
    _topicCollectionView = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:layout];
    _topicCollectionView.backgroundColor = [UIColor clearColor];
    _topicCollectionView.showsHorizontalScrollIndicator = NO;
    _topicCollectionView.delegate = self;
    _topicCollectionView.dataSource = self;
    
    [_topicCollectionView registerClass:[TopicCollectionViewCell class]
             forCellWithReuseIdentifier:TopicCollectionViewCell.cellIdentifier];
    [_lowerHalfView addSubview:_topicCollectionView];
    return _topicCollectionView;
}

- (UIView *)buttonView {
    if (_buttonView) {
        return _buttonView;
    }
    CGFloat width = (kIconSize + 5) * 2 + kIconSpacting;
    CGFloat height = (kIconSize + 5);
    CGFloat x = kContentMargin;
    //    CGFloat y = _roomDesc.y + _roomDesc.height + kComponentSpacing;
    CGFloat y = _lowerHalfView.height - kContentMargin - height;
    _buttonView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    
    [_lowerHalfView addSubview:_buttonView];
    
    /// Fav  icon
    self.favoriteIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, (kIconSize + 5), (kIconSize + 5))];
    [_buttonView addSubview:self.favoriteIcon];
    
    /// Copy  icon
    UIImageView *copyIcon = [[UIImageView alloc] initWithFrame:CGRectMake((kIconSize + 5) + kIconSpacting, 0, (kIconSize + 5), (kIconSize + 5))];
    copyIcon.image = [UIImage imageNamed:@"CopyIcon"];
    copyIcon.image = [copyIcon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    copyIcon.tintColor = kPrimaryColor;
    [_buttonView addSubview:copyIcon];
    return _buttonView;
    
}

- (UIView *)memberCountView {
    if (_memberCountView) {
        return _memberCountView;
    }
    
    CGFloat width = _lowerHalfView.width - _buttonView.width - 2 * kContentMargin;
    CGFloat height = kIconSize;
    CGFloat x = _lowerHalfView.width - kContentMargin - width;
    //    CGFloat y = _roomDesc.y + _roomDesc.height + kComponentSpacing;
    CGFloat y = _lowerHalfView.height - kContentMargin - height;
    _memberCountView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    
    [_lowerHalfView addSubview:_memberCountView];
    
    /// Member  icon
    UIImageView *memberIcon = [[UIImageView alloc] initWithFrame:CGRectMake(_memberCountView.width - kIconSize, 0, kIconSize, kIconSize)];
    memberIcon.image = [UIImage imageNamed:@"MemberIcon"];
    memberIcon.image = [memberIcon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIColor *lighterBlackColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1.0];
    memberIcon.tintColor = lighterBlackColor;
    
    [_memberCountView addSubview:memberIcon];
    
    return _memberCountView;
}

- (UIImageView *)roomHasPassIcon {
    if (_roomHasPassIcon) {
        return _roomHasPassIcon;
    }
    _roomHasPassIcon = [[UIImageView alloc] initWithFrame:CGRectMake(_memberCountView.width - kIconSize - kMemberLabelWidth - kComponentSpacing - kIconSize - kComponentSpacing, -1, kIconSize - 1, kIconSize - 1)];
    _roomHasPassIcon.image = [UIImage imageNamed:@"LockRoomIcon"];
    _roomHasPassIcon.image = [_roomHasPassIcon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIColor *lighterBlackColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1.0];
    _roomHasPassIcon.tintColor = lighterBlackColor;
    
    [_memberCountView addSubview:_roomHasPassIcon];
    return _roomHasPassIcon;
}

- (UIView *)memberCountLabel {
    if (_memberCountLabel) {
        return _memberCountLabel;
    }
    NSString *title = @"10/10";
    UIFont *titleFont = kRegular14;
    
    CGFloat maxWidth = kMemberLabelWidth;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(maxWidth, _lowerHalfView.height - 2 * kContentMargin)];
    UIColor *lighterBlackColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1.0];
    _memberCountLabel = [[CustomLabel alloc]
                         initWithFrame:CGRectMake(_memberCountView.width - kIconSize - maxWidth - kComponentSpacing,
                                                  (_memberCountView.height - titleSize.height)/2,
                                                  maxWidth,
                                                  titleSize.height)
                         title:title
                         titleFont:titleFont
                         titleColor:lighterBlackColor];
    _memberCountLabel.textAlignment = NSTextAlignmentRight;
    [_memberCountView addSubview:_memberCountLabel];
    return _memberCountLabel;
}

- (UIView *)roomDescContainer {
    if (_roomDescContainer) {
        return _roomDescContainer;
    }
    
    CGFloat maxWidth = _lowerHalfView.width - 2 * kContentMargin;
    
    CGFloat width = maxWidth;
    CGFloat x = kContentMargin;

    CGFloat y = _topicCollectionView.bottom + kContentMargin;
    CGFloat height = _buttonView.top - _topicCollectionView.bottom - 2 * kContentMargin;
    
    _roomDescContainer = [[UIView alloc] initWithFrame:CGRectMake(x,
                                                                  y,
                                                                  width,
                                                                  height)];
    [_lowerHalfView addSubview:_roomDescContainer];
    return _roomDescContainer;
}

- (CustomLabel *)roomDesc {
    if (_roomDesc) {
        return _roomDesc;
    }
    NSString *title = @"This is description. This is description. This is description. This is description.";
    UIFont *titleFont = kRegular16;
    
   
    
    _roomDesc = [[CustomLabel alloc]
                 initWithFrame:CGRectZero
                 title:title
                 titleFont:titleFont
                 titleColor:UIColor.blackColor];
    
    [self.roomDescContainer addSubview:_roomDesc];
    return _roomDesc;
}

- (UIView *)dividerView {
    if (_dividerView) {
        return _dividerView;
    }
    _dividerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.roomDescContainer.height - 0.5, self.roomDescContainer.width, 0.5)];
    _dividerView.backgroundColor = UIColor.lightGrayColor;
    [self.roomDescContainer addSubview:_dividerView];
    return _dividerView;
}
#pragma mark - Set content

- (void)setContents {

    self.upperHalfView.hidden = FALSE;
    
    self.roomNameLabel.hidden = FALSE;
    self.roomNameLabel.text = self.roomItem.name;
    
    self.lowerHalfView.hidden = FALSE;
    
    self.topicCollectionView.hidden = FALSE;
    
    self.buttonView.hidden = FALSE;
    
    self.memberCountView.hidden = FALSE;
    self.memberCountLabel.hidden = FALSE;
    self.memberCountLabel.text = [NSString stringWithFormat:@"%d/%d",self.roomItem.totalMember < 0? 0 : self.roomItem.totalMember ,self.roomItem.capacity];
    
    self.roomDesc.hidden = FALSE;
    CGSize roomDescSize = [self.roomItem.desc sizeOfStringWithStyledFont:self.roomDesc.font withSize:self.roomDescContainer.size];
    self.roomDesc.frame = CGRectMake(0, 0, self.roomDescContainer.size.width, roomDescSize.height);
    
    self.roomDesc.text = self.roomItem.desc;
    self.dividerView.hidden = FALSE;
    
    if (self.roomItem.isProtected) {
        self.roomHasPassIcon.hidden = FALSE;
    }
    
    UIImage *image = [UIImage imageNamed:@"UnFavoriteIcon"];
    if (self.roomItem.isFavored) {
        image = [UIImage imageNamed:@"FavoriteIcon"];
    }
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.favoriteIcon.image = image;
    UIColor *redColor = [UIColor colorWithRed:255.0/255.0 green:59.0/255.0 blue:48.0/255.0 alpha:1.0];
    self.favoriteIcon.tintColor = redColor;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.roomNameLabel.text = nil;
    self.roomDesc.text = nil;
    
    self.memberCountLabel.text = nil;
    
    self.roomHasPassIcon.hidden = TRUE;
    
    // Reset the contents of the topicCollectionView
    [self.topicCollectionView reloadData];
    
}

#pragma mark - Topic Collection View delegate & datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.roomItem.topicItemList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TopicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:TopicCollectionViewCell.cellIdentifier forIndexPath:indexPath];
    TopicItem *item = [self.roomItem.topicItemList objectAtIndex:indexPath.item];
    cell.topicItem = item;
    [cell setContents];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    TopicItem *item = [self.roomItem.topicItemList objectAtIndex:indexPath.item];
    return item.itemSize;
}
@end
