//
//  TopicCollectionViewCell.h
//  VStudy
//
//  Created by LAP14011 on 07/05/2023.
//

#import <Foundation/Foundation.h>
#import "TopicItem.h"

#import "CustomLabel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TopicCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) TopicItem *topicItem;

@property (strong, nonatomic) UIView *pillView;

@property (strong, nonatomic) CustomLabel *titleLabel;

+ (NSString *)cellIdentifier;

- (void)setContents;

@end

NS_ASSUME_NONNULL_END
