//
//  RoomCollectionViewCell.h
//  VStudy
//
//  Created by LAP14011 on 20/02/2023.
//

#import <UIKit/UIKit.h>
#import "RoomItem.h"

#import "Macro.h"

#import "UIView+Extension.h"
#import "NSString+Extension.h"

#import "CustomLabel.h"
#import "RoomItem.h"
#import "TopicItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface RoomCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) RoomItem *roomItem;

@property (strong, nonatomic) UIView *upperHalfView;

@property (strong, nonatomic) UIView *lowerHalfView;

@property (strong, nonatomic) CustomLabel *roomNameLabel;

@property (nonatomic, strong) UICollectionView *topicCollectionView;

@property (strong, nonatomic) UIView *roomDescContainer;

@property (strong, nonatomic) CustomLabel *roomDesc;

@property (strong, nonatomic) UIView *buttonView;

@property (strong, nonatomic) UIImageView *favoriteIcon;

@property (strong, nonatomic) UIView *memberCountView;

@property (strong, nonatomic) CustomLabel *memberCountLabel;

@property (strong, nonatomic) UIView *dividerView;

@property (strong, nonatomic) UIImageView *roomHasPassIcon;

+ (NSString *)cellIdentifier;

- (void)setContents;

@end

NS_ASSUME_NONNULL_END
