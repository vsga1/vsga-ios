//
//  RoomDataSourceProcessor.m
//  VStudy
//
//  Created by LAP14011 on 20/02/2023.
//

#import "RoomDataSourceProcessor.h"

@implementation RoomDataSourceProcessor

- (instancetype)initWithDiffDataSource:(UICollectionViewDiffableDataSource *)dataSource {
    self = [super init];
    if (self) {
        self.dataSource = dataSource;
        [self _addSection];
    }
    return self;
}
    
- (void)_addSection {
    NSDiffableDataSourceSnapshot<Section *, RoomItem *> *snapshot = self.dataSource.snapshot;
    NSArray <Section *> *sections = snapshot.sectionIdentifiers;
    if (sections.count > 0) {
        return;
    }
    Section *section = [[Section alloc] initWithSection:SectionMain];
    sections = [[NSArray alloc] initWithObjects:section, nil];
    [snapshot appendSectionsWithIdentifiers:sections];
    [self.dataSource applySnapshot:snapshot animatingDifferences:NO];
}

- (NSDiffableDataSourceSnapshot *)appendItemArray:(NSArray<RoomItem *> *)itemArray {
    [self _addSection];
    NSDiffableDataSourceSnapshot<Section *, RoomItem *> *snapshot = self.dataSource.snapshot;
    if (itemArray.count > 0) {
        [snapshot appendItemsWithIdentifiers:itemArray];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)appendItem:(RoomItem *)item {
    NSDiffableDataSourceSnapshot<Section *, RoomItem *> *snapshot = self.dataSource.snapshot;
    if (item) {
        [snapshot appendItemsWithIdentifiers:@[item]];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)insertItem:(RoomItem *)item {
    NSDiffableDataSourceSnapshot<Section *, RoomItem *> *snapshot = self.dataSource.snapshot;
    if (item) {
        RoomItem *firstItem = [snapshot.itemIdentifiers firstObject];
        if (firstItem) {
            [snapshot insertItemsWithIdentifiers:@[item] beforeItemWithIdentifier:firstItem];
        }
        else {
            [snapshot appendItemsWithIdentifiers:@[item]];
        }
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)reloadItem:(RoomItem *)item {
    NSDiffableDataSourceSnapshot<Section *, RoomItem *> *snapshot = self.dataSource.snapshot;
    if (item) {
        [snapshot reloadItemsWithIdentifiers:@[item]];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)reloadItemArray:(TSMutableArray<RoomItem *> *)itemArray {
    NSDiffableDataSourceSnapshot<Section *, RoomItem *> *snapshot = self.dataSource.snapshot;
    if (itemArray.count > 0) {
        [snapshot reloadItemsWithIdentifiers:itemArray];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)deleteItem:(RoomItem *)item {
    NSDiffableDataSourceSnapshot<Section *, RoomItem *> *snapshot = self.dataSource.snapshot;
    if (item) {
        [snapshot deleteItemsWithIdentifiers:@[item]];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)deleteItemArray:(TSMutableArray<RoomItem *> *)itemArray {
    NSDiffableDataSourceSnapshot<Section *, RoomItem *> *snapshot = self.dataSource.snapshot;
    if (itemArray.count > 0) {
        [snapshot deleteItemsWithIdentifiers:itemArray];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)deleteAllItems {
    NSDiffableDataSourceSnapshot<Section *, RoomItem *> *snapshot = self.dataSource.snapshot;
    [snapshot deleteAllItems];
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)replaceItem:(RoomItem *)item withNewItem:(RoomItem *)newItem {
    NSDiffableDataSourceSnapshot<Section *, RoomItem *> *snapshot = self.dataSource.snapshot;
    if (item && newItem) {
        [snapshot insertItemsWithIdentifiers:@[newItem] beforeItemWithIdentifier:item];
        [snapshot deleteItemsWithIdentifiers:@[item]];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)replaceWithNewItemArray:(NSArray<RoomItem *> *)itemArray {
    [self _addSection];
    NSDiffableDataSourceSnapshot<Section *, RoomItem *> *snapshot = self.dataSource.snapshot;
    NSArray<RoomItem *> *currentArray = snapshot.itemIdentifiers;
    if (currentArray.firstObject) {
        [snapshot insertItemsWithIdentifiers:itemArray beforeItemWithIdentifier:currentArray.firstObject];
        [snapshot deleteItemsWithIdentifiers:currentArray];
    }
    else {
        [snapshot appendItemsWithIdentifiers:itemArray];
    }
    
    return snapshot;
}

@end

