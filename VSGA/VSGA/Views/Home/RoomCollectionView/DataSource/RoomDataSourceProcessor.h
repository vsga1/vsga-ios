//
//  RoomDataSourceProcessor.h
//  VStudy
//
//  Created by LAP14011 on 20/02/2023.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "TSMutableArray.h"
#import "RoomItem.h"

#import "Section.h"
NS_ASSUME_NONNULL_BEGIN

@interface RoomDataSourceProcessor : NSObject

@property (strong, nonatomic) UICollectionViewDiffableDataSource *dataSource;

- (instancetype)initWithDiffDataSource:(UICollectionViewDiffableDataSource *)dataSource;

// Append Array

- (NSDiffableDataSourceSnapshot *)appendItemArray:(NSArray<RoomItem *> *)itemArray;

// Append Item

- (NSDiffableDataSourceSnapshot *)appendItem:(RoomItem *)item;

// Insert Item

- (NSDiffableDataSourceSnapshot *)insertItem:(RoomItem *)item;

// Reload Item

- (NSDiffableDataSourceSnapshot *)reloadItem:(RoomItem *)item;

// Reload Array

- (NSDiffableDataSourceSnapshot *)reloadItemArray:(TSMutableArray<RoomItem *> *)itemArray;

// Delete Item

- (NSDiffableDataSourceSnapshot *)deleteItem:(RoomItem *)item;

// Delete Array

- (NSDiffableDataSourceSnapshot *)deleteItemArray:(TSMutableArray<RoomItem *> *)itemArray;

// Delete All

- (NSDiffableDataSourceSnapshot *)deleteAllItems;

// Replace Item

- (NSDiffableDataSourceSnapshot *)replaceItem:(RoomItem *)item withNewItem:(RoomItem *)newItem;

// Replace Array

- (NSDiffableDataSourceSnapshot *)replaceWithNewItemArray:(NSArray<RoomItem *> *)itemArray;

@end


NS_ASSUME_NONNULL_END
