//
//  DiscoverViewController.h
//  VStudy
//
//  Created by LAP14011 on 20/02/2023.
//

#import <UIKit/UIKit.h>
#import "NewRoomViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface DiscoverViewController : UIViewController <NewRoomViewControllerDelegate>

@end

NS_ASSUME_NONNULL_END
