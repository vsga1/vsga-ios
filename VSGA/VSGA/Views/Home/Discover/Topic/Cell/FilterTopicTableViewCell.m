//
//  FilterTopicTableViewCell.m
//  VStudy
//
//  Created by TamNVM on 21/05/2023.
//

#import "FilterTopicTableViewCell.h"
#import "Macro.h"
#import "UIView+Extension.h"

static CGFloat checkBoxSize = 20;
static CGFloat padding = 20;

@implementation FilterTopicTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Create checkbox button
        self.checkBoxButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.checkBoxButton.frame = CGRectMake(padding, (self.contentView.height - checkBoxSize)/2, checkBoxSize, checkBoxSize);
        [self.checkBoxButton addTarget:self action:@selector(checkBoxTapped) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.checkBoxButton];
        
        // Create topic label
        self.topicLabel = [[CustomLabel alloc] initWithFrame:CGRectMake(self.checkBoxButton.right + padding, 0, self.contentView.width, self.contentView.frame.size.height)];
        
        
        [self.contentView addSubview:self.topicLabel];
    }
    return self;
}

- (void)setContents {
    [self.topicLabel setTitle:self.item.name
                    titleFont:self.item.nameFont
                   titleColor:UIColor.blackColor];
    
    UIImage *image = self.item.isSelected ? [UIImage imageNamed:@"CheckIcon"] : [UIImage imageNamed:@"UnCheckIcon"];
    
    [UIView transitionWithView:self.contentView
                      duration:0.1
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
        [self.checkBoxButton setImage:image forState:UIControlStateNormal];
    } completion:nil];
    
}

- (void)checkBoxTapped {
    [self.delegate selectItem:self.item];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    self.topicLabel.text = nil;
    [self.checkBoxButton setImage:nil forState:UIControlStateNormal];
}
@end
