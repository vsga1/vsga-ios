//
//  FilterTopicTableViewCell.h
//  VStudy
//
//  Created by TamNVM on 21/05/2023.
//

#import <UIKit/UIKit.h>
#import "FilterTopicItem.h"
#import "CustomLabel.h"
NS_ASSUME_NONNULL_BEGIN

@protocol FilterTopicTableViewCellDelegate <NSObject>

- (void)selectItem:(FilterTopicItem *)item;

@end

@interface FilterTopicTableViewCell : UITableViewCell

@property (nonatomic, weak) id<FilterTopicTableViewCellDelegate> delegate;

@property (nonatomic, strong) FilterTopicItem *item;

@property (nonatomic, strong) CustomLabel *topicLabel;

@property (nonatomic, strong) UIButton *checkBoxButton;

- (void)setContents;

@end

NS_ASSUME_NONNULL_END
