//
//  TopicViewController.m
//  VStudy
//
//  Created by TamNVM on 21/05/2023.
//

#import "TopicViewController.h"
#import "Macro.h"
#import "TopicHandler.h"
#import "TSMutableArray.h"
#import "FilterTopicItem.h"
#import "FilterTopicTableViewCell.h"
#import "UIView+Extension.h"

#import "Section.h"
#import "FilterTopicDataSourceProcessor.h"
static CGFloat kStatusBarHeight = 50;
static CGFloat kBottomButtonHeight = 30;
static CGFloat kBottomButtonWidth = 70;
static CGSize kSize;

@interface TopicViewController () <UITableViewDelegate, UISearchBarDelegate, FilterTopicTableViewCellDelegate>

@property (nonatomic, strong) UITableView *topicsTableView;

@property (nonatomic, strong) UISearchBar *searchTopicBar;

@property (nonatomic, strong) NSTimer *searchTimer;

@property (nonatomic, strong) TSMutableArray <FilterTopicItem *> *allTopicItemList;

@property (nonatomic, strong) TSMutableArray <FilterTopicItem *> *selectedTopicItemList;

@property (nonatomic, strong) TSMutableArray <FilterTopicItem *> *unselectedTopicItemList;

@property (nonatomic, strong) TopicHandler *topicHandler;

@property (nonatomic, strong) UITableViewDiffableDataSource<Section *, FilterTopicItem *> *dataSource;

@property (nonatomic, strong) FilterTopicDataSourceProcessor *dataSourceProcessor;

@end

@implementation TopicViewController

- (instancetype)initWithSize:(CGSize)size
                        type:(TopicViewType)type
              selectedTopics:(nonnull TSMutableArray<FilterTopicItem *> *)selectedTopics
            unSelectedTopics:(nonnull TSMutableArray<FilterTopicItem *> *)unSelectedTopics {
    self = [super init];
    if (self) {
        self.preferredContentSize = size;
        kSize = size;
        if (selectedTopics.count > 0) {
            self.selectedTopicItemList = selectedTopics;
            self.unselectedTopicItemList = unSelectedTopics;
            self.allTopicItemList = [[TSMutableArray alloc] initWithArray:selectedTopics];
            [self.allTopicItemList addObjectsFromArray:unSelectedTopics];
        }
        else {
            self.allTopicItemList = [[TSMutableArray alloc] init];
        }
        if (type == TopicViewTypePopover) {
            self.modalPresentationStyle = UIModalPresentationPopover;
            self.popoverPresentationController.popoverLayoutMargins = UIEdgeInsetsZero;
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViews];
    [self setupValues];
}

- (void)setupViews {
    CGFloat padding = 10;
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Create search bar
    self.searchTopicBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, kSize.width, kStatusBarHeight)];
    self.searchTopicBar.delegate = self;
    self.searchTopicBar.placeholder = @"Search topics";
    [self.view addSubview:self.searchTopicBar];
    
    // Create table view
    self.topicsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                                         kStatusBarHeight,
                                                                         kSize.width,
                                                                         kSize.height - kStatusBarHeight - kBottomButtonHeight - 2 * padding)
                                                        style:UITableViewStylePlain];
    self.topicsTableView.delegate = self;
    self.topicsTableView.dataSource = self.dataSource;
    [self.topicsTableView registerClass:[FilterTopicTableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    [self.view addSubview:self.topicsTableView];
    
    // Create the "Clear" button
    UIButton *clearButton = [UIButton buttonWithType:UIButtonTypeSystem];
    clearButton.frame = CGRectMake(0,
                                   kSize.height - kBottomButtonHeight - padding,
                                   kBottomButtonWidth,
                                   kBottomButtonHeight);
    [clearButton setTitle:@"Clear" forState:UIControlStateNormal];
    [clearButton.titleLabel setFont:kRegular18];
    [clearButton.titleLabel setTintColor:kPrimaryColor];
    
    [clearButton addTarget:self action:@selector(clearButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:clearButton];
    
    
    // Create the "Done" button
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeSystem];
    doneButton.frame = CGRectMake(kSize.width - kBottomButtonWidth,
                                  kSize.height - kBottomButtonHeight - padding,
                                  kBottomButtonWidth,
                                  kBottomButtonHeight);
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton.titleLabel setFont:kRegular18];
    [doneButton.titleLabel setTintColor:kPrimaryColor];
    
    [doneButton addTarget:self action:@selector(doneButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:doneButton];
    
}

- (void)setupValues {
    if (self.allTopicItemList.count > 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor appendItemArray:self.allTopicItemList];
            [self applySnapshot:snapshot animation:YES completion:nil];
        });
        return;
    }
    self.allTopicItemList = [[TSMutableArray alloc] init];
    self.selectedTopicItemList = [[TSMutableArray alloc] init];
    self.unselectedTopicItemList = [[TSMutableArray alloc] init];
    
    [self.topicHandler getSuggestTopicWithCompletionHandler:^(NSArray<FilterTopicItem *> * _Nullable topicItemArray, NSError * _Nullable error) {
        if (error) {
            return;
        }
        [self.allTopicItemList addObjectsFromArray:topicItemArray];
        [self.unselectedTopicItemList addObjectsFromArray:topicItemArray];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor appendItemArray:self.unselectedTopicItemList];
            [self applySnapshot:snapshot animation:YES completion:nil];
        });
    }];
}

#pragma mark - NSDiffableDataSourceSnapshot

- (void)applySnapshot:(NSDiffableDataSourceSnapshot *)snapshot
            animation:(BOOL)animation
           completion:(void (^)(void))completion {
    [self.dataSource applySnapshot:snapshot animatingDifferences:animation completion:completion];
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

#pragma mark - Buton tap

// Action method for the "Done" button
- (void)doneButtonTapped {
    [self.delegate filterWithSelectedTopics:self.selectedTopicItemList
                           unSelectedTopics:self.unselectedTopicItemList];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Action method for the "Clear" button
- (void)clearButtonTapped {
    for (FilterTopicItem *item in self.selectedTopicItemList) {
        item.isSelected = !item.isSelected;
    }
    NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor reloadItemArray:self.selectedTopicItemList];
    [self applySnapshot:snapshot animation:YES completion:nil];
    [self.selectedTopicItemList removeAllObjects];
}

#pragma mark - Search bar delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // Cancel previous search timer
    [self.searchTimer invalidate];
    
    // Start a new search timer with a 1-second delay
    self.searchTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(callAPIWithSearchText:) userInfo:searchText repeats:NO];
}

- (void)callAPIWithSearchText:(NSTimer *)timer {
    NSString *searchText = timer.userInfo;
    
    [self _searchTopicWithSearchText:searchText];
}

- (void)_searchTopicWithSearchText:(NSString *)searchText {
    [self.topicHandler searchTopicWithText:searchText
                         completionHandler:^(NSArray<FilterTopicItem *> * _Nullable searchTopics, NSError * _Nullable error) {
        if (error) {
            return;
        }
        self.allTopicItemList = [[TSMutableArray alloc] initWithArray:self.selectedTopicItemList];
        NSMutableSet *nameSet = [NSMutableSet set];

        for (FilterTopicItem *topicItem in self.selectedTopicItemList) {
            [nameSet addObject:topicItem.name];
        }
        self.unselectedTopicItemList = [[TSMutableArray alloc] init];
        for (FilterTopicItem *topicItem in searchTopics) {
            if (![nameSet containsObject:topicItem.name]) {
                [self.unselectedTopicItemList addObject:topicItem];
            }
        }
        
        [self.allTopicItemList addObjectsFromArray:self.unselectedTopicItemList];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *deleteSnapshot = [self.dataSourceProcessor deleteAllItems];
            [self applySnapshot:deleteSnapshot animation:NO completion:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSDiffableDataSourceSnapshot *appendSnapshot = [self.dataSourceProcessor appendItemArray:self.allTopicItemList];
                    [self applySnapshot:appendSnapshot animation:YES completion:nil];
                });
            }];
        });
        
    }];
}

#pragma mark - FilterTopicTableViewCellDelegate

- (void)selectItem:(FilterTopicItem *)item {
    item.isSelected = !item.isSelected;
    NSDiffableDataSourceSnapshot *moveSnapshot;
    if (item.isSelected) {
        [self.selectedTopicItemList addObject:item];
        [self.unselectedTopicItemList removeObject:item];
        moveSnapshot = [self.dataSourceProcessor moveForwardItem:item];
    }
    else {
        [self.selectedTopicItemList removeObject:item];
        [self.unselectedTopicItemList addObject:item];
        moveSnapshot = [self.dataSourceProcessor moveBackwardItem:item];
    }
    NSDiffableDataSourceSnapshot *reloadSnapshot = [self.dataSourceProcessor reloadItem:item];
    [self applySnapshot:reloadSnapshot animation:YES completion:^{
        [self applySnapshot:moveSnapshot animation:YES completion:nil];
    }];
}

#pragma mark - Lazy Loading

- (TopicHandler *)topicHandler {
    if (_topicHandler) {
        return _topicHandler;
    }
    _topicHandler = [[TopicHandler alloc] init];
    return _topicHandler;
}

- (UITableViewDiffableDataSource<Section *,FilterTopicItem *> *)dataSource {
    if (_dataSource) {
        return _dataSource;
    }
    _dataSource = [[UITableViewDiffableDataSource alloc] initWithTableView:self.topicsTableView cellProvider:^UITableViewCell * _Nullable(UITableView * _Nonnull tableView, NSIndexPath * _Nonnull indexPath, FilterTopicItem   * _Nonnull item) {
        if (item) {
            FilterTopicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
            cell.item = item;
            [cell setContents];
            return cell;
        }
        return [[FilterTopicTableViewCell alloc] init];
    }];
    return _dataSource;
}

- (FilterTopicDataSourceProcessor *)dataSourceProcessor {
    if (_dataSourceProcessor) {
        return _dataSourceProcessor;
    }
    _dataSourceProcessor = [[FilterTopicDataSourceProcessor alloc] initWithDiffDataSource:self.dataSource];
    return _dataSourceProcessor;
}


@end
