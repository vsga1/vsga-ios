//
//  FilterTopicDataSourceProcessor.m
//  VStudy
//
//  Created by TamNVM on 21/05/2023.
//

#import "FilterTopicDataSourceProcessor.h"
#import "Section.h"

@implementation FilterTopicDataSourceProcessor

- (instancetype)initWithDiffDataSource:(UITableViewDiffableDataSource *)dataSource {
    self = [super init];
    if (self) {
        self.dataSource = dataSource;
        [self _addSection];
    }
    return self;
}
    
- (void)_addSection {
    NSDiffableDataSourceSnapshot<Section *, FilterTopicItem *> *snapshot = self.dataSource.snapshot;
    NSArray <Section *> *sections = snapshot.sectionIdentifiers;
    if (sections.count == 0) {
        Section *section = [[Section alloc] initWithSection:SectionMain];
        sections = [[NSArray alloc] initWithObjects:section, nil];
        [snapshot appendSectionsWithIdentifiers:sections];
    }
    [self.dataSource applySnapshot:snapshot animatingDifferences:NO];
}

- (NSDiffableDataSourceSnapshot *)appendItemArray:(TSMutableArray<FilterTopicItem *> *)itemArray {
    [self _addSection];
    NSDiffableDataSourceSnapshot<Section *, FilterTopicItem *> *snapshot = self.dataSource.snapshot;
    if (itemArray.count > 0) {
        [snapshot appendItemsWithIdentifiers:itemArray];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)appendItem:(FilterTopicItem *)item {
    NSDiffableDataSourceSnapshot<Section *, FilterTopicItem *> *snapshot = self.dataSource.snapshot;
    if (item) {
        [snapshot appendItemsWithIdentifiers:@[item]];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)insertItem:(FilterTopicItem *)item {
    NSDiffableDataSourceSnapshot<Section *, FilterTopicItem *> *snapshot = self.dataSource.snapshot;
    if (item) {
        FilterTopicItem *firstItem = [snapshot.itemIdentifiers firstObject];
        if (firstItem) {
            [snapshot insertItemsWithIdentifiers:@[item] beforeItemWithIdentifier:firstItem];
        }
        else {
            [snapshot appendItemsWithIdentifiers:@[item]];
        }
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)reloadItem:(FilterTopicItem *)item {
    NSDiffableDataSourceSnapshot<Section *, FilterTopicItem *> *snapshot = self.dataSource.snapshot;
    if (item) {
        [snapshot reloadItemsWithIdentifiers:@[item]];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)reloadItemArray:(TSMutableArray<FilterTopicItem *> *)itemArray {
    NSDiffableDataSourceSnapshot<Section *, FilterTopicItem *> *snapshot = self.dataSource.snapshot;
    if (itemArray.count > 0) {
        [snapshot reloadItemsWithIdentifiers:itemArray];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)deleteItem:(FilterTopicItem *)item {
    NSDiffableDataSourceSnapshot<Section *, FilterTopicItem *> *snapshot = self.dataSource.snapshot;
    if (item) {
        [snapshot deleteItemsWithIdentifiers:@[item]];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)deleteItemArray:(TSMutableArray<FilterTopicItem *> *)itemArray {
    NSDiffableDataSourceSnapshot<Section *, FilterTopicItem *> *snapshot = self.dataSource.snapshot;
    if (itemArray.count > 0) {
        [snapshot deleteItemsWithIdentifiers:itemArray];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)deleteAllItems {
    NSDiffableDataSourceSnapshot<Section *, FilterTopicItem *> *snapshot = self.dataSource.snapshot;
    [snapshot deleteAllItems];
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)replaceItem:(FilterTopicItem *)item withNewItem:(FilterTopicItem *)newItem {
    NSDiffableDataSourceSnapshot<Section *, FilterTopicItem *> *snapshot = self.dataSource.snapshot;
    if (item && newItem) {
        [snapshot insertItemsWithIdentifiers:@[newItem] beforeItemWithIdentifier:item];
        [snapshot deleteItemsWithIdentifiers:@[item]];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)replaceWithNewItemArray:(NSArray<FilterTopicItem *> *)itemArray {
    NSDiffableDataSourceSnapshot<Section *, FilterTopicItem *> *snapshot = self.dataSource.snapshot;
    NSArray<FilterTopicItem *> *currentArray = snapshot.itemIdentifiers;
    if (currentArray.firstObject) {
        [snapshot insertItemsWithIdentifiers:itemArray beforeItemWithIdentifier:currentArray.firstObject];
        [snapshot deleteItemsWithIdentifiers:currentArray];
    }
    else {
        [snapshot appendItemsWithIdentifiers:itemArray];
    }
    
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)moveForwardItem:(FilterTopicItem *)item {
    NSDiffableDataSourceSnapshot<Section *, FilterTopicItem *> *snapshot = self.dataSource.snapshot;
    if (item) {
        FilterTopicItem *firstItem = [snapshot.itemIdentifiers firstObject];
        if (firstItem && firstItem != item) {
            [snapshot moveItemWithIdentifier:item beforeItemWithIdentifier:firstItem];
        }
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)moveBackwardItem:(FilterTopicItem *)item {
    NSDiffableDataSourceSnapshot<Section *, FilterTopicItem *> *snapshot = self.dataSource.snapshot;
    if (item) {
        FilterTopicItem *lastItem = [snapshot.itemIdentifiers lastObject];
        if (lastItem && lastItem != item) {
            [snapshot moveItemWithIdentifier:item afterItemWithIdentifier:lastItem];
        }
    }
    return snapshot;
}
@end
