//
//  FilterTopicDataSourceProcessor.h
//  VStudy
//
//  Created by TamNVM on 21/05/2023.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TSMutableArray.h"
#import "FilterTopicItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface FilterTopicDataSourceProcessor : NSObject

@property (strong, nonatomic) UITableViewDiffableDataSource *dataSource;

- (instancetype)initWithDiffDataSource:(UITableViewDiffableDataSource *)dataSource;

// Append Array

- (NSDiffableDataSourceSnapshot *)appendItemArray:(TSMutableArray<FilterTopicItem *> *)itemArray;

// Append Item

- (NSDiffableDataSourceSnapshot *)appendItem:(FilterTopicItem *)item;

// Insert Item

- (NSDiffableDataSourceSnapshot *)insertItem:(FilterTopicItem *)item;

// Reload Item

- (NSDiffableDataSourceSnapshot *)reloadItem:(FilterTopicItem *)item;

// Reload Array

- (NSDiffableDataSourceSnapshot *)reloadItemArray:(TSMutableArray<FilterTopicItem *> *)itemArray;

// Delete Item

- (NSDiffableDataSourceSnapshot *)deleteItem:(FilterTopicItem *)item;

// Delete Array

- (NSDiffableDataSourceSnapshot *)deleteItemArray:(TSMutableArray<FilterTopicItem *> *)itemArray;

// Delete All

- (NSDiffableDataSourceSnapshot *)deleteAllItems;

// Replace Item

- (NSDiffableDataSourceSnapshot *)replaceItem:(FilterTopicItem *)item withNewItem:(FilterTopicItem *)newItem;

// Replace Array

- (NSDiffableDataSourceSnapshot *)replaceWithNewItemArray:(NSArray<FilterTopicItem *> *)itemArray;

// Move forward

- (NSDiffableDataSourceSnapshot *)moveForwardItem:(FilterTopicItem *)item;

// Move backward

- (NSDiffableDataSourceSnapshot *)moveBackwardItem:(FilterTopicItem *)item;

@end

NS_ASSUME_NONNULL_END
