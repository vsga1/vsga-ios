//
//  TopicViewController.h
//  VStudy
//
//  Created by TamNVM on 21/05/2023.
//

#import <UIKit/UIKit.h>
#import "TSMutableArray.h"
#import "FilterTopicItem.h"
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, TopicViewType) {
    TopicViewTypePopover
};

@protocol TopicViewControllerDelegate <NSObject>

- (void)filterWithSelectedTopics:(TSMutableArray <FilterTopicItem *> *)selectedTopics
                unSelectedTopics:(TSMutableArray <FilterTopicItem *> *)unSelectedTopics;

@end

@interface TopicViewController : UIViewController

@property (nonatomic, weak) id<TopicViewControllerDelegate> delegate;

- (instancetype)initWithSize:(CGSize)size
                        type:(TopicViewType)type
              selectedTopics:(TSMutableArray <FilterTopicItem *> *)selectedTopics
            unSelectedTopics:(TSMutableArray <FilterTopicItem *> *)unSelectedTopics;

@end

NS_ASSUME_NONNULL_END
