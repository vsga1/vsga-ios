//
//  NewRoomViewController.m
//  VStudy
//
//  Created by LAP14011 on 07/05/2023.
//

#import "NewRoomViewController.h"
#import "Macro.h"
#import "Constant.h"
#import "RoomHandler.h"
#import "RoomManager.h"

#import "CustomTextView.h"
#import "CustomButton.h"
#import "InputTextField.h"
#import "CustomLabel.h"
#import "ErrorLabel.h"
#import "UIView+Extension.h"
#import "UIImage+Extension.h"
#import "RoomDetailViewController.h"
#import "TopicSelectionView.h"
#import "UtilityClass.h"

const CGFloat kNewRoomElementSpace = 15;
const CGFloat kNewRoomInputSpace = 5;
const CGFloat kNewRoomPadding = 20;
const CGFloat kNewRoomTextViewPadding = 10;
@interface NewRoomViewController () <NewRoomDelegate, UITextViewDelegate, TopicSelectionViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) CustomLabel *addNewRoomLabel;

@property (nonatomic, strong) CustomTextView *nameTextView;

@property (nonatomic, strong) CustomTextView *descriptionTextView;

@property (nonatomic, strong) UISegmentedControl *typeSegmentedControl;

@property (nonatomic, strong) CustomTextView *capacityTextView;

@property (nonatomic, strong) CustomTextView *passwordTextView;

@property (nonatomic, strong) TopicSelectionView *topicView;

@property (nonatomic, strong) CustomButton *createRoomButton;

@property (nonatomic, strong) UIView *errorLabelContainer;

@property (nonatomic, strong) ErrorLabel *errorLabel;

@property (nonatomic, strong) RoomHandler *roomHandler;

@property (nonatomic, assign) UIEdgeInsets originalContentInset;

@property (nonatomic, assign) CGFloat contentHeight;

@property (nonatomic, assign) CGFloat selectTopicHeight;

@end

@implementation NewRoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupViews];
    [self setupGestures];
    [self setupDelegates];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = TRUE;
    
    // Register for keyboard notifications
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardWillShow:)
    //                                                 name:UIKeyboardWillShowNotification
    //                                               object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardWillHide:)
    //                                                 name:UIKeyboardWillHideNotification
    //                                               object:nil];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.errorLabel setErrorLabelWithTitle:@""];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - Setup Views

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGFloat scrollViewHeight = self.view.height - self.addNewRoomLabel.bottom - kNewRoomElementSpace;
    CGRect scrollViewFrame = CGRectMake(0,
                                        self.addNewRoomLabel.bottom + kNewRoomElementSpace,
                                        self.view.width,
                                        scrollViewHeight);
    self.scrollView.frame = scrollViewFrame;
    self.scrollView.contentSize = CGSizeMake(scrollViewFrame.size.width, self.contentHeight);
}

- (void)setupViews {
    self.view.backgroundColor = UIColor.whiteColor;
    
    CGFloat maxWidth = self.view.width - 2 * kNewRoomPadding;
    
    /// Create new room label
    CGFloat labelHeight = 40;
    self.addNewRoomLabel = [[CustomLabel alloc] initWithFrame:CGRectMake(kNewRoomPadding,
                                                                         kNewRoomPadding,
                                                                         maxWidth,
                                                                         labelHeight)
                                                        title:@"CREATE NEW ROOM"
                                                    titleFont:kSemiBold32
                                                   titleColor:UIColor.blackColor];
    self.addNewRoomLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.addNewRoomLabel];
    
    CGFloat scrollViewHeight = self.view.height - self.addNewRoomLabel.bottom - kNewRoomElementSpace;
    CGRect scrollViewFrame = CGRectMake(0,
                                        self.addNewRoomLabel.bottom + kNewRoomElementSpace,
                                        self.view.width,
                                        scrollViewHeight);
    [self createScrollViewWithFrame:scrollViewFrame];
}

- (void)createScrollViewWithFrame:(CGRect)frame {
    CGFloat maxWidth = self.view.width - 2 * kNewRoomPadding;
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:frame];
    self.scrollView.contentSize = frame.size;
    [self.view addSubview:self.scrollView];
    
    /// Name
    CustomLabel *nameLabel = [[CustomLabel alloc] initWithFrame:CGRectMake(kNewRoomPadding,
                                                                           0,
                                                                           maxWidth,
                                                                           20)
                                                          title:@"Name:"
                                                      titleFont:kRegular18
                                                     titleColor:UIColor.blackColor];
    [self.scrollView addSubview:nameLabel];
    
    
    self.nameTextView = [[CustomTextView alloc] initWithFont:kRegular16
                                               withTextColor:UIColor.blackColor
                                         withPlaceholderText:@"Enter room's name"
                                    withPlaceholderTextColor:UIColor.lightGrayColor];
    self.nameTextView.frame = CGRectMake(kNewRoomPadding,
                                         nameLabel.bottom + kNewRoomInputSpace,
                                         maxWidth,
                                         80);
    [self.nameTextView placeholderTopLeftPadding:kNewRoomTextViewPadding + 2];
    self.nameTextView.textContainerInset = UIEdgeInsetsMake(kNewRoomTextViewPadding,
                                                            kNewRoomTextViewPadding,
                                                            kNewRoomTextViewPadding,
                                                            kNewRoomTextViewPadding);
    
    self.nameTextView.layer.borderWidth = 1.0;
    self.nameTextView.layer.borderColor = UIColor.lightGrayColor.CGColor;
    self.nameTextView.layer.cornerRadius = 10;
    
    [self.scrollView addSubview:self.nameTextView];
    
    /// Description
    CustomLabel *descLabel = [[CustomLabel alloc] initWithFrame:CGRectMake(kNewRoomPadding,
                                                                           self.nameTextView.bottom + kNewRoomElementSpace,
                                                                           maxWidth,
                                                                           20)
                                                          title:@"Description (Optional):"
                                                      titleFont:kRegular18
                                                     titleColor:UIColor.blackColor];
    [self.scrollView addSubview:descLabel];
    
    self.descriptionTextView = [[CustomTextView alloc] initWithFont:kRegular16
                                                      withTextColor:UIColor.blackColor
                                                withPlaceholderText:@"Enter description"
                                           withPlaceholderTextColor:UIColor.lightGrayColor];
    self.descriptionTextView.frame = CGRectMake(kNewRoomPadding,
                                                descLabel.bottom + kNewRoomInputSpace,
                                                maxWidth,
                                                140);
    [self.descriptionTextView placeholderTopLeftPadding:kNewRoomTextViewPadding + 2];
    self.descriptionTextView.textContainerInset = UIEdgeInsetsMake(kNewRoomTextViewPadding,
                                                                   kNewRoomTextViewPadding,
                                                                   kNewRoomTextViewPadding,
                                                                   kNewRoomTextViewPadding);
    
    self.descriptionTextView.layer.borderWidth = 1.0;
    self.descriptionTextView.layer.borderColor = UIColor.lightGrayColor.CGColor;
    self.descriptionTextView.layer.cornerRadius = 15;
    
    [self.scrollView addSubview:self.descriptionTextView];
    
    /// Type segmented control
    CustomLabel *typeLabel = [[CustomLabel alloc] initWithFrame:CGRectMake(kNewRoomPadding,
                                                                           self.descriptionTextView.bottom + kNewRoomElementSpace,
                                                                           (maxWidth - kNewRoomPadding)/2,
                                                                           20)
                                                          title:@"Type:"
                                                      titleFont:kRegular18
                                                     titleColor:UIColor.blackColor];
    [self.scrollView addSubview:typeLabel];
    
    self.typeSegmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"PUBLIC", @"PRIVATE"]];
    self.typeSegmentedControl.frame = CGRectMake(kNewRoomPadding,
                                                 typeLabel.bottom + kNewRoomInputSpace,
                                                 (maxWidth - kNewRoomPadding)/2,
                                                 40);
    self.typeSegmentedControl.selectedSegmentIndex = 0;
    [self.scrollView addSubview:self.typeSegmentedControl];
    
    /// Capacity
    CustomLabel *capacityLabel = [[CustomLabel alloc] initWithFrame:CGRectMake(typeLabel.right + kNewRoomPadding,
                                                                               self.descriptionTextView.bottom + kNewRoomElementSpace,
                                                                               (maxWidth - kNewRoomPadding)/2,
                                                                               20)
                                                              title:@"Capacity:"
                                                          titleFont:kRegular18
                                                         titleColor:UIColor.blackColor];
    [self.scrollView addSubview:capacityLabel];
    
    self.capacityTextView = [[CustomTextView alloc] initWithFont:kRegular16
                                                   withTextColor:UIColor.blackColor
                                             withPlaceholderText:@""
                                        withPlaceholderTextColor:UIColor.lightGrayColor];
    self.capacityTextView.text = @"10";
    self.capacityTextView.frame = CGRectMake(typeLabel.right + kNewRoomPadding,
                                             capacityLabel.bottom + kNewRoomInputSpace,
                                             (maxWidth - kNewRoomPadding)/2,
                                             40);
    [self.capacityTextView placeholderTopLeftPadding:kNewRoomTextViewPadding + 2];
    self.capacityTextView.textContainerInset = UIEdgeInsetsMake(kNewRoomTextViewPadding,
                                                                kNewRoomTextViewPadding,
                                                                kNewRoomTextViewPadding,
                                                                kNewRoomTextViewPadding);
    
    self.capacityTextView.layer.borderWidth = 1.0;
    self.capacityTextView.layer.borderColor = UIColor.lightGrayColor.CGColor;
    self.capacityTextView.layer.cornerRadius = 7;
    
    self.capacityTextView.keyboardType = UIKeyboardTypeNumberPad;
    [self.scrollView addSubview:self.capacityTextView];
    
    /// Password
    CustomLabel *passwordLabel = [[CustomLabel alloc] initWithFrame:CGRectMake(kNewRoomPadding,
                                                                               self.capacityTextView.bottom + kNewRoomElementSpace,
                                                                               maxWidth,
                                                                               20)
                                                              title:@"Password (Optional):"
                                                          titleFont:kRegular18
                                                         titleColor:UIColor.blackColor];
    [self.scrollView addSubview:passwordLabel];
    
    
    self.passwordTextView = [[CustomTextView alloc] initWithFont:kRegular16
                                                   withTextColor:UIColor.blackColor
                                             withPlaceholderText:@"Enter room's password"
                                        withPlaceholderTextColor:UIColor.lightGrayColor];
    self.passwordTextView.frame = CGRectMake(kNewRoomPadding,
                                             passwordLabel.bottom + kNewRoomInputSpace,
                                             maxWidth,
                                             40);
    [self.passwordTextView placeholderTopLeftPadding:kNewRoomTextViewPadding + 2];
    self.passwordTextView.textContainerInset = UIEdgeInsetsMake(kNewRoomTextViewPadding,
                                                                kNewRoomTextViewPadding,
                                                                kNewRoomTextViewPadding,
                                                                kNewRoomTextViewPadding);
    
    self.passwordTextView.layer.borderWidth = 1.0;
    self.passwordTextView.layer.borderColor = UIColor.lightGrayColor.CGColor;
    self.passwordTextView.layer.cornerRadius = 7;
    
    self.passwordTextView.secureTextEntry = TRUE;
    [self.scrollView addSubview:self.passwordTextView];
    
    /// topic
    CustomLabel *topicLabel = [[CustomLabel alloc] initWithFrame:CGRectMake(kNewRoomPadding,
                                                                            self.passwordTextView.bottom + kNewRoomElementSpace,
                                                                            maxWidth,
                                                                            20)
                                                           title:@"Select topics:"
                                                       titleFont:kRegular18
                                                      titleColor:UIColor.blackColor];
    [self.scrollView addSubview:topicLabel];
    
    self.selectTopicHeight = 40;
    self.topicView = [[TopicSelectionView alloc] initWithFrame:CGRectMake(0,
                                                                          topicLabel.bottom + kNewRoomInputSpace,
                                                                          self.view.width,
                                                                          self.selectTopicHeight)];
    self.topicView.delegate = self;
    [self.scrollView addSubview:self.topicView];
    
    /// Create Button
    CGFloat x = (self.view.width - kButtonWidth) / 2;
    CGFloat y = self.topicView.bottom + kNewRoomElementSpace;
    UIFont *titleFont = kSemiBold16;
    
    self.createRoomButton = [[CustomButton alloc] initWithFrame:CGRectMake(x,
                                                                           y,
                                                                           kButtonWidth,
                                                                           kButtonHeight)
                                                          title:@"Create"
                                                     titleColor:UIColor.whiteColor
                                                      titleFont:titleFont
                                                backgroundColor:kPrimaryColor
                                                    borderColor:kPrimaryColor.CGColor];
    [self.createRoomButton addTarget:self action:@selector(createRoomButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:self.createRoomButton];
    
    self.errorLabelContainer = [[UIView alloc] initWithFrame:CGRectMake(kNewRoomPadding,
                                                                           self.createRoomButton.bottom + kNewRoomElementSpace,
                                                                           maxWidth,
                                                                           35)];
    [self.scrollView addSubview:self.errorLabelContainer];
    
    self.errorLabel = [[ErrorLabel alloc] initWithOrigin:CGPointMake(0,0)
                                                maxWidth:maxWidth
                                               titleFont:kRegular16];
    [self.errorLabelContainer addSubview:self.errorLabel];
    
    self.contentHeight = self.errorLabelContainer.bottom;
}

#pragma mark - Setup Gestures

- (void)setupGestures {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - Setup Delegated

- (void)setupDelegates {
    NSArray *textViews = @[self.nameTextView, self.descriptionTextView, self.capacityTextView, self.passwordTextView];
    
    for (UITextView *textView in textViews) {
        textView.delegate = self;
    }
    
    self.roomHandler.createRoomDelegate = self;
}

#pragma mark - TextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    CustomTextView *customTextView = (CustomTextView *)textView;
    if ([customTextView isEmpty]) {
        [customTextView displayPlaceholder];
    }
    else {
        [customTextView displayContent];
    }
}

#pragma mark - OnClicked

- (void)createRoomButtonClicked:(CustomButton *)sender {
    [sender showLoading];
    
    //TODO: validate
    
    NSString *name = self.nameTextView.text;
    NSString *description = self.descriptionTextView.text;
    NSString *capacity = self.capacityTextView.text;
    NSString *password = self.passwordTextView.text;
    
    if ([name isEqualToString:@""]) {
        [self.errorLabel setErrorLabelWithTitle:@"Please enter room's name"];
        [sender showAndHideLoadingInDuration:0.1f];
        return;
    }
    
    if ([capacity intValue] < 2 || [capacity intValue] > 10) {
        [self.errorLabel setErrorLabelWithTitle:@"Capacity must be greater than 1 and less than 10"];
        [sender showAndHideLoadingInDuration:0.1f];
        return;
    }
    
    NSInteger selectedSegmentIndex = self.typeSegmentedControl.selectedSegmentIndex;
    NSString *selectedType = [self.typeSegmentedControl titleForSegmentAtIndex:selectedSegmentIndex];
    NSArray *topicArray = [self.topicView getSelectedTopicName];
    
    NSDictionary *dict = @{@"name":name,
                           @"description":description,
                           @"capacity":capacity,
                           @"roomType":selectedType,
                           @"password":password,
                           @"topics":topicArray
    };
    [self.roomHandler postCreateNewRoomWithInfo:dict];
}

#pragma mark - TopicSelectionViewDelegate

- (void)isExpandTopicList:(BOOL)isExpand newHeight:(CGFloat)height {
    [UIView animateWithDuration:0.3 animations:^{
        self.topicView.frame = CGRectMake(self.topicView.x,
                                          self.topicView.y,
                                          self.topicView.width,
                                          height);
        self.createRoomButton.frame = CGRectMake(self.createRoomButton.x,
                                                 self.topicView.bottom + kNewRoomElementSpace,
                                                 self.createRoomButton.width,
                                                 self.createRoomButton.height);
        self.errorLabelContainer.frame = CGRectMake(self.errorLabelContainer.x,
                                                    self.createRoomButton.bottom + kNewRoomElementSpace,
                                                    self.errorLabelContainer.width,
                                                    self.errorLabelContainer.height);
    }];
    self.contentHeight = self.errorLabelContainer.bottom;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.width, self.contentHeight);
}

- (void)showReachMaxTopicSelection {
    NSString *message = [NSString stringWithFormat:@"You can only select up to %ld topics.", (long)kTopicViewMaxSelectedCount];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Maximum Topics Reached"
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    // Dismiss the alert after 1 seconds
    NSTimer *dismissTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(dismissAlertController:) userInfo:alertController repeats:NO];
}

- (void)dismissAlertController:(NSTimer *)timer {
    UIAlertController *alertController = timer.userInfo;
    [alertController dismissViewControllerAnimated:YES completion:nil];
    [timer invalidate];
}

#pragma mark - NewRoomDelegate

- (void)onFinishCreateNewRoomWithError:(RoomHandlerErrorCode)errorCode newRoomItem:(RoomItem *)item {
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (errorCode) {
            case RoomHandlerServerError:
            case RoomHandlerInvalidDataError:
                break;
            case RoomHandlerNoError:
                [self navigateToRoomDetail:item];
                break;
        }
    });
}

- (void)navigateToRoomDetail:(RoomItem *)item {
    [self.createRoomButton hideLoading];
    
    [self.delegate NewRoomViewControllerDidDismiss:self newRoomItem:item];
    //
    //
    //    // Dismiss the current view controller
    //    [self dismissViewControllerAnimated:YES completion:nil];
    //
    //    // Present a new view controller after a slight delay
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //        RoomDetailViewController *roomDetailVC = [[RoomDetailViewController alloc] init];
    //        UINavigationController *roomDetailNavVC = [[UINavigationController alloc] initWithRootViewController:roomDetailVC];
    //        roomDetailNavVC.modalPresentationStyle = UIModalPresentationFullScreen;
    //        [self.superclass presentViewController:roomDetailNavVC animated:YES completion:nil];
    //        [[RoomManager sharedManager] joinRoom:item];
    //    });
    //
    
}

- (RoomHandler *)roomHandler {
    if (_roomHandler) {
        return _roomHandler;
    }
    _roomHandler = [[RoomHandler alloc] init];
    return _roomHandler;
}

@end
