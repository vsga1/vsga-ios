//
//  TopicSelectionView.h
//  VStudy
//
//  Created by TamNVM on 21/06/2023.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TopicSelectionViewDelegate <NSObject>

- (void)isExpandTopicList:(BOOL)isExpand newHeight:(CGFloat)height;

- (void)showReachMaxTopicSelection;

@end

@interface TopicSelectionView : UIView

@property (weak, nonatomic) id<TopicSelectionViewDelegate>delegate;

- (NSArray *)getSelectedTopicName;

@end

NS_ASSUME_NONNULL_END

