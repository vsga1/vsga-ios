//
//  TopicSelectionView.m
//  VStudy
//
//  Created by TamNVM on 21/06/2023.
//

#import "TopicSelectionView.h"
#import <Masonry/Masonry.h>
#import "Macro.h"
#import "Constant.h"
#import "TSMutableArray.h"

#import "UIView+Extension.h"
#import "NSString+Extension.h"

#import "TopicHandler.h"
#import "Section.h"
#import "FilterTopicItem.h"
#import "FilterTopicDataSourceProcessor.h"
#import "FilterTopicTableViewCell.h"
#import "TopicSelectionCollectionViewCell.h"

const CGFloat kTopicViewPadding = 20;
const CGFloat kTopicViewTopMargin = 5;
const CGFloat kTopicViewDropDownButtonSize = 30;

@interface TopicSelectionView ()
<UITableViewDelegate,
UISearchBarDelegate,
FilterTopicTableViewCellDelegate,
UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout,
TopicSelectionCollectionViewCellDelegate>

@property (nonatomic, strong) UIView *selectedTopicView;
@property (nonatomic, assign) CGFloat initialSelectedViewHeight;
@property (nonatomic, assign) CGFloat selectedViewHeight;

@property (nonatomic, strong) UIButton *dropdownButton;
@property (nonatomic, assign) BOOL isDropdownOpen;

@property (nonatomic, strong) UIView *containerView;

@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) NSTimer *searchTimer;
@property (nonatomic, assign) CGFloat searchBarHeight;

@property (nonatomic, strong) UITableView *allTopicTableView;
@property (nonatomic, assign) CGFloat allTopicTableViewHeight;
@property (nonatomic, strong) TSMutableArray <FilterTopicItem *> *allTopicItemList;

@property (nonatomic, strong) UICollectionView *selectedTopicCollectionView;
@property (nonatomic, strong) TSMutableArray <FilterTopicItem *> *selectedTopicItemList;

@property (nonatomic, strong) TopicHandler *topicHandler;
@property (nonatomic, strong) UITableViewDiffableDataSource<Section *, FilterTopicItem *> *dataSource;
@property (nonatomic, strong) FilterTopicDataSourceProcessor *dataSourceProcessor;

@end

@implementation TopicSelectionView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
        [self setupValues];
    }
    return self;
}

- (void)setupViews {
    self.searchBarHeight = 44;
    self.allTopicTableViewHeight = 230;
    self.initialSelectedViewHeight = 33;
    self.selectedViewHeight = 33;
    
    [self setupSelectedTopicViews];
    [self setupAllTopicViews];
}

- (void)setupSelectedTopicViews {
    // Selected topic view
    self.selectedTopicView = [[UIView alloc] init];
    [self addSubview:self.selectedTopicView];
    [self.selectedTopicView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(kTopicViewPadding);
        make.right.equalTo(self).offset(-(kTopicViewPadding + kTopicViewDropDownButtonSize));
        make.top.equalTo(self);
        make.height.equalTo(@(self.initialSelectedViewHeight));
    }];
    
    self.selectedTopicView.layer.borderWidth = 1.0;
    self.selectedTopicView.layer.borderColor = UIColor.lightGrayColor.CGColor;
    self.selectedTopicView.layer.cornerRadius = 7;
    
    
    // Selected Collection View
    
    // Configure collection view layout
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.minimumInteritemSpacing = kTopicViewItemSpacing;
    flowLayout.minimumLineSpacing = kTopicViewItemSpacing;
    
    // Create collection view
    self.selectedTopicCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.selectedTopicCollectionView.backgroundColor = [UIColor whiteColor];
    self.selectedTopicCollectionView.delegate = self;
    self.selectedTopicCollectionView.dataSource = self;
    self.selectedTopicCollectionView.contentInset = UIEdgeInsetsMake(kTopicViewContentInset,
                                                                     kTopicViewContentInset,
                                                                     kTopicViewContentInset,
                                                                     kTopicViewContentInset);
    [self.selectedTopicCollectionView registerClass:[TopicSelectionCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    [self.selectedTopicView addSubview:self.selectedTopicCollectionView];
    [self.selectedTopicCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.selectedTopicView);
    }];
    
    
    // Dropdown button
    self.dropdownButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self addSubview:self.dropdownButton];
    [self.dropdownButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.selectedTopicView.mas_right);
        make.right.equalTo(self).offset(-(kTopicViewPadding));
        make.centerY.equalTo(self.selectedTopicView);
        make.height.equalTo(@(kTopicViewDropDownButtonSize));
    }];
    
    [self.dropdownButton setImage:[UIImage imageNamed:@"ExpandArrow"] forState:UIControlStateNormal];
    self.dropdownButton.tintColor = kPrimaryColor;
    [self.dropdownButton addTarget:self action:@selector(dropdownButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)setupAllTopicViews {
    self.containerView = [[UIView alloc] init];
    [self addSubview:self.containerView];
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(kTopicViewPadding);
        make.right.equalTo(self).offset(-(kTopicViewPadding));
        make.top.equalTo(self.selectedTopicView.mas_bottom).offset(kTopicViewTopMargin);
        make.height.equalTo(@(self.searchBarHeight + self.allTopicTableViewHeight));
    }];
    
    self.containerView.hidden = YES;
    
    self.containerView.layer.borderWidth = 1.0;
    self.containerView.layer.borderColor = UIColor.lightGrayColor.CGColor;
    self.containerView.layer.cornerRadius = 10;
    
    // Search bar
    self.searchBar = [[UISearchBar alloc] init];
    [self.containerView addSubview:self.searchBar];
    [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.containerView);
        make.height.equalTo(@(self.searchBarHeight));
    }];
    
    self.searchBar.delegate = self;
    self.searchBar.hidden = YES;
    
    
    // Topic list view
    self.allTopicTableView = [[UITableView alloc] init];
    [self.containerView addSubview:self.allTopicTableView];
    [self.allTopicTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.containerView);
        make.top.equalTo(self.searchBar.mas_bottom);
        make.height.equalTo(@(self.allTopicTableViewHeight));
    }];
    
    self.allTopicTableView.delegate = self;
    self.allTopicTableView.dataSource = self.dataSource;
    
    [self.allTopicTableView registerClass:[FilterTopicTableViewCell class] forCellReuseIdentifier:@"Cell"];
    self.allTopicTableView.hidden = YES;
}

#pragma mark - Setup Values

- (void)setupValues {
    self.allTopicItemList = [[TSMutableArray alloc] init];
    self.selectedTopicItemList = [[TSMutableArray alloc] init];
    
    [self _getSuggestTopic];
}

#pragma mark - Dropdown Button

- (void)dropdownButtonTapped {
    self.isDropdownOpen = !self.isDropdownOpen;
    
    // Toggle the visibility of the search bar and table view
    self.containerView.hidden = !self.isDropdownOpen;
    self.searchBar.hidden = !self.isDropdownOpen;
    self.allTopicTableView.hidden = !self.isDropdownOpen;
    
    // Update the dropdown button title based on the dropdown state
    NSString *dropdownButtonName = self.isDropdownOpen ? @"CollapseArrow" : @"ExpandArrow";
    [self.dropdownButton setImage:[UIImage imageNamed:dropdownButtonName] forState:UIControlStateNormal];
    [self updateHeightDelegate];
}


#pragma mark - NSDiffableDataSourceSnapshot

- (void)applySnapshot:(NSDiffableDataSourceSnapshot *)snapshot
            animation:(BOOL)animation
           completion:(void (^)(void))completion {
    [self.dataSource applySnapshot:snapshot animatingDifferences:animation completion:completion];
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // Cancel previous search timer
    [self.searchTimer invalidate];
    
    // Start a new search timer with a 1-second delay
    self.searchTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(callAPIWithSearchText:) userInfo:searchText repeats:NO];
}

- (void)callAPIWithSearchText:(NSTimer *)timer {
    NSString *searchText = timer.userInfo;
    if ([searchText isEqualToString:@""]) {
        [self _getSuggestTopic];
    }
    else {
        [self _searchTopicWithSearchText:searchText];
    }
}

- (void)_searchTopicWithSearchText:(NSString *)searchText {
    [self.topicHandler searchTopicWithText:searchText
                         completionHandler:^(NSArray<FilterTopicItem *> * _Nullable searchTopics, NSError * _Nullable error) {
        if (error) {
            return;
        }
        
        NSMutableSet *nameSet = [NSMutableSet set];
        for (FilterTopicItem *topicItem in self.selectedTopicItemList) {
            [nameSet addObject:topicItem.name];
        }
        for (FilterTopicItem *topicItem in searchTopics) {
            if ([nameSet containsObject:topicItem.name]) {
                topicItem.isSelected = YES;
            }
        }
        self.allTopicItemList = [[TSMutableArray alloc] initWithArray:searchTopics];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *deleteSnapshot = [self.dataSourceProcessor deleteAllItems];
            [self applySnapshot:deleteSnapshot animation:NO completion:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSDiffableDataSourceSnapshot *appendSnapshot = [self.dataSourceProcessor appendItemArray:self.allTopicItemList];
                    [self applySnapshot:appendSnapshot animation:YES completion:nil];
                });
            }];
        });
        
    }];
}

- (void)_getSuggestTopic {
    [self.topicHandler getSuggestTopicWithCompletionHandler:^(NSArray<FilterTopicItem *> * _Nullable topicItemArray, NSError * _Nullable error) {
        if (error) {
            return;
        }
        NSMutableSet *nameSet = [NSMutableSet set];
        for (FilterTopicItem *topicItem in self.selectedTopicItemList) {
            [nameSet addObject:topicItem.name];
        }
        for (FilterTopicItem *topicItem in topicItemArray) {
            if ([nameSet containsObject:topicItem.name]) {
                topicItem.isSelected = YES;
            }
        }
        self.allTopicItemList = [[TSMutableArray alloc] initWithArray:topicItemArray];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *deleteSnapshot = [self.dataSourceProcessor deleteAllItems];
            [self applySnapshot:deleteSnapshot animation:NO completion:^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSDiffableDataSourceSnapshot *appendSnapshot = [self.dataSourceProcessor appendItemArray:self.allTopicItemList];
                    [self applySnapshot:appendSnapshot animation:YES completion:nil];
                });
            }];
        });
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.selectedTopicItemList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TopicSelectionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.delegate = self;
    cell.item = [self.selectedTopicItemList objectAtIndex:indexPath.item];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    FilterTopicItem *topicItem = self.selectedTopicItemList[indexPath.row];
    NSLog(@"tamnvm>> sizeForItemAtIndexPath %f", topicItem.selectedCellSize.width);
    return topicItem.selectedCellSize;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return kTopicViewItemSpacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return kTopicViewItemSpacing;
}

#pragma mark - Calculate Height

- (CGFloat)getSelectedViewHeight {
    
    CGFloat currentHeight = 0;
    CGFloat currentWidth = 0;
    CGFloat maxWidth = self.selectedTopicCollectionView.width - 2*kTopicViewContentInset;
    CGFloat cellHeight = 0;
    NSLog(@"Tamnvm>> maxWidth %f", maxWidth);
    
    for (int i = 0; i < self.selectedTopicItemList.count; i++) {
        FilterTopicItem *topicItem = self.selectedTopicItemList[i];
        CGSize textSize = [topicItem.name sizeOfStringWithStyledFont:topicItem.nameFont withSize:CGSizeMake(maxWidth,CGFLOAT_MAX)];
        CGSize labelSize = CGSizeMake(textSize.width + 2*kTopicViewItemPadding, textSize.height + 2*kTopicViewItemPadding);
        CGSize cellSize = CGSizeMake(labelSize.width + kTopicViewDeleteButtonSize + 2*kTopicViewDeleteButtonHPadding, labelSize.height);
        cellHeight = cellSize.height;
        
        topicItem.selectedLabelSize = labelSize;
        topicItem.selectedContainerSize = cellSize;
        topicItem.selectedCellSize = cellSize;
        NSLog(@"Tamnvm>> cellSize.width %f - item %d - name %@", cellSize.width, i, topicItem.name);
        
        if (currentWidth + cellSize.width <= maxWidth) {
            currentWidth += cellSize.width;
            currentWidth += kTopicViewItemSpacing;
            NSLog(@"Tamnvm>> currentWidth %f", currentWidth);
        }
        else {
            //Expand before topic
            FilterTopicItem *beforeTopicItem = self.selectedTopicItemList[i-1];
            currentWidth = currentWidth - kTopicViewItemSpacing - beforeTopicItem.selectedCellSize.width;
            beforeTopicItem.selectedCellSize = CGSizeMake(maxWidth - currentWidth, cellHeight);
            NSLog(@"Tamnvm>>update cellSize.width %f - item %d - name %@", maxWidth - currentWidth, i-1, beforeTopicItem.name);
            
            currentWidth = cellSize.width + kTopicViewItemSpacing;
            currentHeight += cellHeight;
            currentHeight += kTopicViewItemSpacing;
            NSLog(@"tamnvm>> current height %f", currentHeight);
        }
    }
    
    if (currentWidth - kTopicViewItemSpacing <= maxWidth) {
        currentHeight += cellHeight;
    }
    else {
        currentHeight -= kTopicViewItemSpacing;
    }
    CGFloat newSelectedViewHeight = currentHeight + 2*kTopicViewContentInset;
    newSelectedViewHeight = newSelectedViewHeight < self.initialSelectedViewHeight ? self.initialSelectedViewHeight : newSelectedViewHeight;
    return newSelectedViewHeight;
}

#pragma mark - Update Height

- (void)updateHeight {
    self.selectedViewHeight = [self getSelectedViewHeight];
    NSLog(@"Tamnvm>> selectItem height %f",   self.selectedViewHeight);
    [self.selectedTopicView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@( self.selectedViewHeight));
    }];
    
    [self updateHeightDelegate];
}

#pragma mark - Delegate

- (void)updateHeightDelegate {
    CGFloat currentHeight = self.selectedViewHeight;
    if (self.isDropdownOpen) {
        currentHeight += self.containerView.height;
    }
    [self.delegate isExpandTopicList:self.isDropdownOpen newHeight:currentHeight];
}

#pragma mark - Find item

- (FilterTopicItem *)findTopicItemWithName:(NSString *)name inList:(TSMutableArray <FilterTopicItem *> *)list {
    for (FilterTopicItem *item in list) {
        if ([item.name isEqualToString:name]) {
            return item;
        }
    }
    return nil;
}

#pragma mark - FilterTopicTableViewCellDelegate

- (void)selectItem:(FilterTopicItem *)item {
    if (!item.isSelected && self.selectedTopicItemList.count == kTopicViewMaxSelectedCount) {
        [self.delegate showReachMaxTopicSelection];
        return;
    }
    item.isSelected = !item.isSelected;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor reloadItem:item];
        [self applySnapshot:snapshot animation:YES completion:nil];
    });
    if (item.isSelected) {
        [self.selectedTopicItemList addObject:item];
    }
    else {
        FilterTopicItem *selectedItem = [self findTopicItemWithName:item.name inList:self.selectedTopicItemList];
        [self.selectedTopicItemList removeObject:selectedItem];
    }
    [self updateHeight];
    [self.selectedTopicCollectionView reloadData];
}

#pragma mark - TopicSelectionCollectionViewCellDelegate

- (void)deleteItem:(FilterTopicItem *)item {
    FilterTopicItem *deleteItem = [self findTopicItemWithName:item.name inList:self.allTopicItemList];
    if (deleteItem) {
        deleteItem.isSelected = !deleteItem.isSelected;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor reloadItem:deleteItem];
            [self applySnapshot:snapshot animation:YES completion:nil];
        });
    }
    [self.selectedTopicItemList removeObject:item];
    [self updateHeight];
    [self.selectedTopicCollectionView reloadData];
}

#pragma mark - Get Selected Topic Name

- (NSArray *)getSelectedTopicName {
    NSMutableArray *topicNameList = [NSMutableArray new];
    for (FilterTopicItem *item in self.selectedTopicItemList) {
        [topicNameList addObject:item.name];
    }
    return topicNameList;
}

#pragma mark - Lazy Loading

- (TopicHandler *)topicHandler {
    if (_topicHandler) {
        return _topicHandler;
    }
    _topicHandler = [[TopicHandler alloc] init];
    return _topicHandler;
}

- (UITableViewDiffableDataSource<Section *,FilterTopicItem *> *)dataSource {
    if (_dataSource) {
        return _dataSource;
    }
    _dataSource = [[UITableViewDiffableDataSource alloc] initWithTableView:self.allTopicTableView cellProvider:^UITableViewCell * _Nullable(UITableView * _Nonnull tableView, NSIndexPath * _Nonnull indexPath, FilterTopicItem   * _Nonnull item) {
        if (item) {
            FilterTopicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
            cell.item = item;
            [cell setContents];
            return cell;
        }
        return [[FilterTopicTableViewCell alloc] init];
    }];
    return _dataSource;
}

- (FilterTopicDataSourceProcessor *)dataSourceProcessor {
    if (_dataSourceProcessor) {
        return _dataSourceProcessor;
    }
    _dataSourceProcessor = [[FilterTopicDataSourceProcessor alloc] initWithDiffDataSource:self.dataSource];
    return _dataSourceProcessor;
}

@end

