//
//  TopicSelectionCollectionViewCell.m
//  VStudy
//
//  Created by TamNVM on 22/06/2023.
//

#import "TopicSelectionCollectionViewCell.h"
#import "Macro.h"
#import "Constant.h"
#import "UIView+Extension.h"



@implementation TopicSelectionCollectionViewCell

#pragma mark - Set item

- (void)setItem:(FilterTopicItem *)item {
    _item = item;
    
    self.containerView.frame = CGRectMake(0,
                                          0,
                                          item.selectedContainerSize.width,
                                          item.selectedContainerSize.height);
    self.containerView.backgroundColor = item.backgroundColor;
    
    self.topicLabel.frame = CGRectMake(self.deleteButton.right + kTopicViewItemPadding,
                                       0,
                                       item.selectedLabelSize.width,
                                       item.selectedLabelSize.height);
    self.topicLabel.text = item.name;
    self.topicLabel.textColor = item.textColor;
    self.deleteButton.tintColor = item.textColor;
    
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.topicLabel.text = nil;
}

#pragma mark - Lazy loading

- (UIView *)containerView {
    if (_containerView) {
        return _containerView;
    }
    _containerView = [[UIView alloc] init];
    _containerView.layer.cornerRadius = 5;
    
    [self.contentView addSubview:_containerView];
    return _containerView;
}

- (UIButton *)deleteButton {
    if (_deleteButton) {
        return _deleteButton;
    }
    _deleteButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _deleteButton.frame = CGRectMake(kTopicViewDeleteButtonHPadding,
                                     (self.height - kTopicViewDeleteButtonSize)/2,
                                     kTopicViewDeleteButtonSize,
                                     kTopicViewDeleteButtonSize);
    [_deleteButton setImage:[UIImage systemImageNamed:@"x.circle.fill"] forState:UIControlStateNormal];
    [_deleteButton addTarget:self action:@selector(deleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.containerView addSubview:_deleteButton];
    return _deleteButton;
}

- (CustomLabel *)topicLabel {
    if (_topicLabel) {
        return _topicLabel;
    }
    _topicLabel = [[CustomLabel alloc] initWithFrame:CGRectZero
                                               title:@""
                                           titleFont:kRegular16
                                          titleColor:UIColor.whiteColor];
    _topicLabel.textAlignment = NSTextAlignmentCenter;
    [self.containerView addSubview:_topicLabel];
    return _topicLabel;
}

- (void)deleteButtonTapped:(UIButton *)sender {
    [self.delegate deleteItem:self.item];
}
@end
