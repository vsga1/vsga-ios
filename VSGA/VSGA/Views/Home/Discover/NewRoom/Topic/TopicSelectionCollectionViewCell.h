//
//  TopicSelectionCollectionViewCell.h
//  VStudy
//
//  Created by TamNVM on 22/06/2023.
//

#import <UIKit/UIKit.h>
#import "FilterTopicItem.h"
#import "CustomLabel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TopicSelectionCollectionViewCellDelegate <NSObject>

- (void)deleteItem:(FilterTopicItem *)item;

@end

@interface TopicSelectionCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id<TopicSelectionCollectionViewCellDelegate> delegate;

@property (nonatomic, strong) FilterTopicItem *item;

@property (nonatomic, strong) UIView *containerView;

@property (nonatomic, strong) CustomLabel *topicLabel;

@property (nonatomic, strong) UIButton *deleteButton;

@end

NS_ASSUME_NONNULL_END
