//
//  NewRoomViewController.h
//  VStudy
//
//  Created by LAP14011 on 07/05/2023.
//

#import <UIKit/UIKit.h>
#import "RoomItem.h"
NS_ASSUME_NONNULL_BEGIN

@class NewRoomViewController;

@protocol NewRoomViewControllerDelegate <NSObject>

- (void)NewRoomViewControllerDidDismiss:(UIViewController *)viewController newRoomItem:(RoomItem *)item;

@end

@interface NewRoomViewController : UIViewController

@property (nonatomic, weak) id<NewRoomViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
