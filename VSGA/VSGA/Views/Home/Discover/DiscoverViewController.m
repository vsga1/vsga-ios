//
//  DiscoverViewController.m
//  VStudy
//
//  Created by LAP14011 on 20/02/2023.
//

#import "DiscoverViewController.h"
#import "TSMutableArray.h"
#import "UIView+Extension.h"
#import "RoomDataSourceProcessor.h"
#import "RoomCollectionViewCell.h"
#import "Section.h"
#import "RoomItem.h"
#import "RoomDetailViewController.h"

#import "AppDelegate.h"
#import "AccountManager.h"
#import "RoomHandler.h"
#import "RoomManager.h"

#import "NewRoomViewController.h"
#import "TopicViewController.h"

const CGFloat kSearchRoomBarHeight = 60;
const CGFloat kSearchTopicBarHeight = 50;
const CGFloat kFilterTopicWidth = 315;
const CGFloat kFilterTopicHeight = 350;

static CGFloat kMargin = 0;
@interface DiscoverViewController () <UICollectionViewDelegate, UISearchBarDelegate, JoinRoomDelegate, UIPopoverPresentationControllerDelegate, TopicViewControllerDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (nonatomic, strong) UICollectionViewFlowLayout *collectionViewLayout;

@property (nonatomic, strong) UICollectionViewDiffableDataSource<Section *, RoomItem *> *dataSource;

@property (nonatomic, strong) RoomDataSourceProcessor *dataSourceProcessor;

@property (nonatomic, strong) TSMutableArray<RoomItem *> *roomItemList;

@property (nonatomic, assign) NSInteger currentLoadPage;

@property (nonatomic, strong) RoomItem *selectedRoomItem;

@property (nonatomic, strong) RoomHandler *roomHandler;

@property (nonatomic, assign) NSInteger totalRoom;

//

@property (nonatomic, strong) UIView *searchRoomBarContainer;

@property (nonatomic, strong) UISearchBar *searchRoomBar;

@property (nonatomic, strong) NSTimer *searchTimer;

//

@property (nonatomic, strong) UIPopoverPresentationController *popoverController;

@property (nonatomic, strong) UIView *filterContainer;

@property (nonatomic, strong) UIButton *filterButton;

@property (nonatomic, strong) TSMutableArray<FilterTopicItem *> *selectedTopicItemList;

@property (nonatomic, strong) TSMutableArray<FilterTopicItem *> *unSelectedTopicItemList;

@end

@implementation DiscoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupViews];
    [self setupValues];
    [self setupDelegates];
}

#pragma mark - Setup Views

- (void)setupViews {
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupNavigationBar];
    [self setupSearchAndFilterBar];
    
    [self setupCollectionView];
    [self setupPullToRefresh];
    [self setupTapGesture];
}

- (void)setupNavigationBar {
    UINavigationBar *navigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(kStatusBarFrame), self.view.width, kNavigationBarHeight)];
    navigationBar.barTintColor = [UIColor whiteColor];
    
    UINavigationItem *navigationItem = [[UINavigationItem alloc] initWithTitle:@"Discover"];
    [navigationBar setItems:@[navigationItem]];
    
    [self.view addSubview:navigationBar];
    
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Logo"]];
    logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UIBarButtonItem *logoBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:logoImageView];
    
    navigationItem.leftBarButtonItem = logoBarButtonItem;
    navigationItem.leftItemsSupplementBackButton = YES;
    
    logoImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [NSLayoutConstraint activateConstraints:@[
        [logoImageView.leadingAnchor constraintEqualToAnchor:logoBarButtonItem.customView.leadingAnchor],
        [logoImageView.topAnchor constraintEqualToAnchor:logoBarButtonItem.customView.topAnchor],
        [logoImageView.bottomAnchor constraintEqualToAnchor:logoBarButtonItem.customView.bottomAnchor],
        [logoImageView.widthAnchor constraintEqualToConstant:32.0],
        [logoImageView.heightAnchor constraintEqualToConstant:32.0]
    ]];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 80, 30);
    button.backgroundColor = kPrimaryColor;
    button.layer.cornerRadius = 8.0;
    [button setTitle:@"Create" forState:UIControlStateNormal];
    [button.titleLabel setFont:kRegular16];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(addNewRoom) forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    navigationItem.rightBarButtonItem = barButtonItem;
    
    kMargin = navigationBar.layoutMargins.right;
}

- (void)setupSearchAndFilterBar {
    UIView *whiteBG = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(kStatusBarFrame) + kNavigationBarHeight, self.view.width, kSearchRoomBarHeight)];
    whiteBG.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:whiteBG];
    
    [self setupSearchRoomBar];
    [self setupFilterButton];
}

- (void)setupSearchRoomBar {
    // Create search bar container
    self.searchRoomBarContainer = [[UIView alloc] initWithFrame:CGRectMake(kMargin, CGRectGetMaxY(kStatusBarFrame) + kNavigationBarHeight, self.view.width - 35 - 2*kMargin, kSearchRoomBarHeight)];
    self.searchRoomBarContainer.backgroundColor = [UIColor whiteColor];
    self.searchRoomBarContainer.layer.cornerRadius = 10.0;
    self.searchRoomBarContainer.layer.maskedCorners = kCALayerMinXMinYCorner | kCALayerMaxXMinYCorner;
    self.searchRoomBarContainer.clipsToBounds = YES;
    
    [self.view addSubview:self.searchRoomBarContainer];
    
    self.searchRoomBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.searchRoomBarContainer.width, self.searchRoomBarContainer.height)];
    self.searchRoomBar.barTintColor = [UIColor clearColor];
    self.searchRoomBar.backgroundColor = [UIColor clearColor];
    self.searchRoomBar.placeholder = @"Search";
    self.searchRoomBar.searchBarStyle = UISearchBarStyleMinimal;
    self.searchRoomBar.delegate = self;
    [self.searchRoomBarContainer addSubview:self.searchRoomBar];
    
    // Add search text on the right side
    UITextField *searchTextField = [self.searchRoomBar valueForKey:@"searchField"];
    searchTextField.textAlignment = NSTextAlignmentLeft;
}

- (void)setupFilterButton {
    CGFloat filterSize = 30;
    self.filterContainer = [[UIView alloc] initWithFrame:CGRectMake(self.searchRoomBarContainer.right, self.searchRoomBarContainer.top, filterSize, self.searchRoomBarContainer.height)];
    self.filterContainer.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.filterContainer];
    
    
    CGFloat y = (self.filterContainer.height - filterSize)/2;
    self.filterButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.filterButton.frame = CGRectMake(0, y, filterSize, filterSize);
    [self.filterButton setImage:[UIImage imageNamed:@"FilterIcon"] forState:UIControlStateNormal];
    [self.filterButton setTintColor:kPrimaryColor];
    [self.filterContainer addSubview:self.filterButton];
    
    [self.filterButton addTarget:self action:@selector(filterButtonTapped) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setupCollectionView {
    self.collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];
    CGFloat itemWidth = self.view.width - 4*kMargin;
    CGFloat itemHeight = kRoomCellHeight;
    self.collectionViewLayout.itemSize = CGSizeMake(itemWidth, itemHeight);
    self.collectionViewLayout.minimumLineSpacing = 30;
    
    CGFloat x = 0;
    CGFloat y = CGRectGetMaxY(self.searchRoomBarContainer.frame);
    CGFloat width = self.view.width;
    CGFloat height = self.view.height - (self.searchRoomBarContainer.y + self.searchRoomBarContainer.height);
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(x,
                                                                             y,
                                                                             width,
                                                                             height)
                                             collectionViewLayout:self.collectionViewLayout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self.dataSource;
    
    self.collectionView.layer.backgroundColor = kBackgroundColor.CGColor;
    self.collectionView.contentInset = UIEdgeInsetsMake(10, 0, 10, 0);
    
    [self.collectionView registerClass:[RoomCollectionViewCell class]
            forCellWithReuseIdentifier:RoomCollectionViewCell.cellIdentifier];
    [self.view addSubview:self.collectionView];
    
}

- (void)setupPullToRefresh {
    // Initialize refresh control
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    
    // Add refresh control to collection view
    self.collectionView.refreshControl = self.refreshControl;
}

- (void)setupTapGesture {
    // Initialize the tap gesture recognizer
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    tapGestureRecognizer.delegate = self;
    [self.view addGestureRecognizer:tapGestureRecognizer];
}

#pragma mark - Setup Values

- (void)setupValues {
    self.roomItemList = [[TSMutableArray alloc] init];
    self.currentLoadPage = 0;
    [self _getRoomItemArrayWithPage:self.currentLoadPage searchText:@"" topics:[NSArray new]];
    
    self.selectedTopicItemList = [[TSMutableArray alloc] init];
    self.unSelectedTopicItemList = [[TSMutableArray alloc] init];
}

- (void)_getRoomItemArrayWithPage:(NSInteger)pageNum searchText:(NSString *)text topics:(NSArray *)topics {
    NSMutableArray *topicNames = [NSMutableArray array];
    
    for (FilterTopicItem *item in topics) {
        NSString *topicName = item.name;
        if (topicName) {
            [topicNames addObject:topicName];
        }
    }
    
    [self.roomHandler getRoomListWithPage:pageNum text:text topics:topicNames completionHandler:^(NSArray<RoomItem *> * _Nullable roomItemArray, NSInteger total, NSError * _Nullable error) {
        if (error) {
            return;
        }
        self.totalRoom = total;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *snapshot;
            if (pageNum > 0) {
                [self.roomItemList addObjectsFromArray:roomItemArray];
                snapshot = [self.dataSourceProcessor appendItemArray:roomItemArray];
            }
            else {
                [self.roomItemList removeAllObjects];
                [self.roomItemList addObjectsFromArray:roomItemArray];
                snapshot = [self.dataSourceProcessor replaceWithNewItemArray:roomItemArray];
            }
            [self applySnapshot:snapshot animation:NO completion:nil];
        });
    }];
}

#pragma mark - Setup Delegate

- (void)setupDelegates {
    self.roomHandler.joinRoomDelegate = self;
}

#pragma mark - NSDiffableDataSourceSnapshot

- (void)applySnapshot:(NSDiffableDataSourceSnapshot *)snapshot
            animation:(BOOL)animation
           completion:(void (^)(void))completion {
    [self.dataSource applySnapshot:snapshot animatingDifferences:animation completion:completion];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    RoomItem *selectedRoom = [self.roomItemList objectAtIndex:indexPath.item];
    self.selectedRoomItem = selectedRoom;
    if (selectedRoom.isProtected) {
        // Create an alert controller with a text field for the password input
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Password Required"
                                                                                 message:@"Please enter the password for the room"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Password";
            textField.secureTextEntry = YES;
        }];
        
        // Create an action for handling the password input
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"Enter" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            UITextField *passwordTextField = alertController.textFields.firstObject;
            NSString *enteredPassword = passwordTextField.text;
            
            [self checkPassword:enteredPassword ofRoom:selectedRoom];
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        
        // Add the action to the alert controller
        [alertController addAction:cancelAction];
        [alertController addAction:confirmAction];
        
        // Present the alert controller modally
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    else {
        self.selectedRoomItem = selectedRoom;
        NSDictionary *dict = @{@"roomCode":selectedRoom.code};
        //Call API join room event
        [self.roomHandler postJoinRoomEventWithInfo:dict];
    }
}

- (void)showErrorAlertWithTitle:(NSString *)title message:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)checkPassword:(NSString *)password ofRoom:(RoomItem *)room {
    NSDictionary *dict = @{@"roomCode":room.code, @"password":password};
    //Call API join room event
    [self.roomHandler postJoinRoomEventWithInfo:dict];
}

#pragma mark - JoinRoomDelegate

- (void)onFinishJoinRoomWithError:(RoomHandlerErrorCode)errorCode {
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (errorCode) {
            case RoomHandlerServerError:
            case RoomHandlerInvalidDataError:
                break;
            case RoomHandlerNoError:
                [self navigateToRoomDetail];
                break;
            case RoomHandlerWrongPassword:
                [self showErrorAlertWithTitle:@"Incorrect Password" message:@"The entered password is incorrect"];
                break;
            case RoomHandlerWaitForHost:
                [self showErrorAlertWithTitle:@"Wait for host" message:@"Please wait the host to start this room"];
                break;
        }
    });
}

- (void)navigateToRoomDetail {
    RoomDetailViewController *roomDetailVC = [[RoomDetailViewController alloc] initWithRoomItem:self.selectedRoomItem];
    UINavigationController *roomDetailNavVC = [[UINavigationController alloc] initWithRootViewController:roomDetailVC];
    roomDetailNavVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:roomDetailNavVC animated:YES completion:nil];
    [[RoomManager sharedManager] joinRoom:self.selectedRoomItem];
}

#pragma mark - Refresh data

- (void)refreshData {
    self.roomItemList = [[TSMutableArray alloc] init];
    self.currentLoadPage = 0;
    
    [self.roomHandler getRoomListWithPage:self.currentLoadPage text:self.searchRoomBar.text topics:self.selectedTopicItemList completionHandler:^(NSArray<RoomItem *> * _Nullable roomItemArray, NSInteger total, NSError * _Nullable error) {
        if (error) {
            return;
        }
        self.totalRoom = total;
        [self.roomItemList addObjectsFromArray:roomItemArray];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Disable scrolling
            [self.collectionView setScrollEnabled:NO];
            
            // Dismiss refresh control
            [self.collectionView.refreshControl endRefreshing];
            
            // Reset content offset
            [self.collectionView setContentOffset:CGPointZero animated:YES];
            
            // Enable scrolling
            [self.collectionView setScrollEnabled:YES];
            
            
            NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor replaceWithNewItemArray:roomItemArray];
            [self applySnapshot:snapshot animation:YES completion:nil];
            
        });
    }];
}

#pragma mark - Add new room

- (void)addNewRoom {
    NewRoomViewController *newRoomVC = [[NewRoomViewController alloc] init];
    newRoomVC.delegate = self;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:newRoomVC];
    
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)NewRoomViewControllerDidDismiss:(UIViewController *)viewController newRoomItem:(RoomItem *)item {
    [viewController dismissViewControllerAnimated:YES completion:^{
        RoomDetailViewController *roomDetailVC = [[RoomDetailViewController alloc] initWithRoomItem:item];
        UINavigationController *roomDetailNavVC = [[UINavigationController alloc] initWithRootViewController:roomDetailVC];
        roomDetailNavVC.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:roomDetailNavVC animated:YES completion:nil];
        [[RoomManager sharedManager] joinRoom:item];
    }];
}

#pragma mark - Search bar delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // Cancel previous search timer
    [self.searchTimer invalidate];
    
    // Start a new search timer with a 1-second delay
    self.searchTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(callAPIWithSearchText:) userInfo:searchText repeats:NO];
}

- (void)callAPIWithSearchText:(NSTimer *)timer {
    NSString *searchText = timer.userInfo;
    
    self.currentLoadPage = 0;
    [self _getRoomItemArrayWithPage:self.currentLoadPage searchText:searchText topics:[self.selectedTopicItemList getArray]];
}

#pragma mark - Filter by topic

- (void)filterButtonTapped {
    
    TopicViewController *controller = [[TopicViewController alloc] initWithSize:CGSizeMake(kFilterTopicWidth,
                                                                                           kFilterTopicHeight)
                                                                           type:TopicViewTypePopover selectedTopics:self.selectedTopicItemList unSelectedTopics:self.unSelectedTopicItemList];
    
    UIPopoverPresentationController *popover = controller.popoverPresentationController;
    popover.delegate = self;
    popover.sourceView = self.filterButton;
    popover.sourceRect = self.filterButton.bounds;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    controller.delegate = self;
    // display the controller in the usual way
    [self presentViewController:controller animated:YES completion:nil];
    
}

#pragma mark - UIPopoverPresentationControllerDelegate

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    // Ensure that the popover is displayed even on devices with small screen sizes
    return UIModalPresentationNone;
}

#pragma mark - TopicViewControllerDelegate

- (void)filterWithSelectedTopics:(TSMutableArray<FilterTopicItem *> *)selectedTopics unSelectedTopics:(TSMutableArray<FilterTopicItem *> *)unSelectedTopics {
    [self.popoverPresentationController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
    self.selectedTopicItemList = selectedTopics;
    self.unSelectedTopicItemList = unSelectedTopics;
    
    self.currentLoadPage = 0;
    [self _getRoomItemArrayWithPage:self.currentLoadPage searchText:self.searchRoomBar.text topics:[self.selectedTopicItemList getArray]];
}


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    // Determine if the touch should be recognized or ignored
    if (touch.view != self.searchRoomBar) {
        [self.searchRoomBar resignFirstResponder]; // Resign the search bar's first responder status
    }
    
    return NO; // Disable the tap gesture recognizer
}

- (void)handleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer {
    // Handle tap gesture if needed
}

#pragma mark - Lazy Loading

- (RoomHandler *)roomHandler {
    if (_roomHandler) {
        return _roomHandler;
    }
    _roomHandler = [[RoomHandler alloc] init];
    return _roomHandler;
}

- (UICollectionViewDiffableDataSource<Section *, RoomItem *> *)dataSource {
    if (_dataSource) {
        return _dataSource;
    }
    _dataSource = [[UICollectionViewDiffableDataSource alloc] initWithCollectionView:self.collectionView cellProvider:^RoomCollectionViewCell *(UICollectionView * collectionView, NSIndexPath *indexPath, RoomItem *roomItem) {
        if (roomItem) {
            RoomCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:RoomCollectionViewCell.cellIdentifier forIndexPath:indexPath];
            
            cell.roomItem = [self.roomItemList objectAtIndex:indexPath.item];
            [cell setContents];
            
            if (indexPath.item == self.roomItemList.count - 5 && self.roomItemList.count <= self.totalRoom) {
                self.currentLoadPage++;
                [self _getRoomItemArrayWithPage:self.currentLoadPage searchText:self.searchRoomBar.text topics:self.selectedTopicItemList];
            }
            return cell;
        }
        return [[RoomCollectionViewCell alloc] init];
    }];
    return _dataSource;
}

- (RoomDataSourceProcessor *)dataSourceProcessor {
    if (_dataSourceProcessor) {
        return _dataSourceProcessor;
    }
    _dataSourceProcessor = [[RoomDataSourceProcessor alloc] initWithDiffDataSource:self.dataSource];
    return _dataSourceProcessor;
}

@end
