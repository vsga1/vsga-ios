//
//  ViewController.m
//  VSGA
//
//  Created by LAP14011 on 31/01/2023.
//

#import "HomeViewController.h"

#import "Macro.h"

#import "DiscoverViewController.h"
#import "RoomListViewController.h"
#import "ProfileViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupTabBar];
}

- (void)setupTabBar {
    
    self.tabBar.tintColor = UIColor.blackColor;
    self.tabBar.backgroundColor = UIColor.whiteColor;
    
    DiscoverViewController *discoverVC = [[DiscoverViewController alloc] init];
    RoomListViewController *roomListVC = [[RoomListViewController alloc] init];
    ProfileViewController *profileVC = [[ProfileViewController alloc] init];
    
    discoverVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Discover" image:[UIImage imageNamed:@"DiscoverIcon"] tag:0];
    roomListVC.tabBarItem =[[UITabBarItem alloc] initWithTitle:@"Rooms" image:[UIImage imageNamed:@"RoomIcon"] tag:1];
    profileVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Profile" image:[UIImage imageNamed:@"ProfileIcon"] tag:2];
    
    
    NSArray *tabViewControllers = @[discoverVC, roomListVC, profileVC];
    [self setViewControllers:tabViewControllers];
    
    self.delegate = self;
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    NSArray *tabViewControllers = tabBarController.viewControllers;
    UIView *fromView = tabBarController.selectedViewController.view;
    UIView *toView = viewController.view;
    if (fromView == toView) {
        return false;
    }
    
    NSUInteger toIndex = [tabViewControllers indexOfObject:viewController];
    
    [UIView transitionFromView:fromView
                        toView:toView
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished) {
        if (finished) {
            tabBarController.selectedIndex = toIndex;
        }
    }];
    return true;
}

@end
