//
//  RoomListViewController.m
//  VStudy
//
//  Created by LAP14011 on 20/02/2023.
//

#import "RoomListViewController.h"
#import <Masonry/Masonry.h>
#import "Macro.h"
#import "TSMutableArray.h"
#import "UIView+Extension.h"

#import "RoomHandler.h"
#import "RoomManager.h"

#import "Section.h"
#import "RoomItem.h"
#import "RoomDetailViewController.h"

#import "TitleAndToggleButton.h"

#import "RoomCollectionViewCell.h"
#import "RoomDataSourceProcessor.h"

static CGFloat kCollectionViewMargin = 0;

@interface RoomListViewController () <UICollectionViewDelegate, JoinRoomDelegate>

@property (nonatomic, strong) TSMutableArray<RoomItem *> *lastVisitedRoomList;

@property (nonatomic, strong) TSMutableArray<RoomItem *> *ownerRoomList;

@property (nonatomic, strong) TSMutableArray<RoomItem *> *favoriteRoomList;

@property (nonatomic, strong) RoomItem *selectedRoomItem;

@property (nonatomic, strong) RoomHandler *roomHandler;

// View
@property (nonatomic, strong) UIView *containerView;

@property (nonatomic, strong) UIView *lastVisitedContainerView;
@property (nonatomic, strong) TitleAndToggleButton *lastVisitedTitleView;
@property (nonatomic, strong) UICollectionView *lastVisitedCollectionView;
@property (nonatomic, strong) UICollectionViewDiffableDataSource<Section *, RoomItem *> *lastVisitedDataSource;
@property (nonatomic, strong) RoomDataSourceProcessor *lastVisitedDataSourceProcessor;

@property (nonatomic, strong) UIView *ownerRoomContainerView;
@property (nonatomic, strong) TitleAndToggleButton *ownerRoomTitleView;
@property (nonatomic, strong) UICollectionView *ownerRoomCollectionView;
@property (nonatomic, strong) UICollectionViewDiffableDataSource<Section *, RoomItem *> *ownerRoomDataSource;
@property (nonatomic, strong) RoomDataSourceProcessor *ownerRoomDataSourceProcessor;

@property (nonatomic, strong) UIView *favoriteRoomContainerView;
@property (nonatomic, strong) TitleAndToggleButton *favoriteRoomTitleView;
@property (nonatomic, strong) UICollectionView *favoriteRoomCollectionView;
@property (nonatomic, strong) UICollectionViewDiffableDataSource<Section *, RoomItem *> *favoriteRoomDataSource;
@property (nonatomic, strong) RoomDataSourceProcessor *favoriteRoomDataSourceProcessor;

// Data
@end

@implementation RoomListViewController {
    RoomSection *_lastVisitedRoomSection;
    RoomSection *_ownerRoomSection;
    RoomSection *_favoriteRoomSection;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupViews];
    [self setupValues];
    [self setupDelegates];
}

#pragma mark - Setup Views

- (void)setupViews {
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupNavigationBar];
    [self setupContainerView];
}

- (void)setupNavigationBar {
    UINavigationBar *navigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(kStatusBarFrame), self.view.width, kNavigationBarHeight)];
    navigationBar.barTintColor = [UIColor whiteColor];
    
    UINavigationItem *navigationItem = [[UINavigationItem alloc] initWithTitle:@"Rooms"];
    [navigationBar setItems:@[navigationItem]];
    
    [self.view addSubview:navigationBar];
    
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Logo"]];
    logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UIBarButtonItem *logoBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:logoImageView];
    
    navigationItem.leftBarButtonItem = logoBarButtonItem;
    navigationItem.leftItemsSupplementBackButton = YES;
    
    logoImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [NSLayoutConstraint activateConstraints:@[
        [logoImageView.leadingAnchor constraintEqualToAnchor:logoBarButtonItem.customView.leadingAnchor],
        [logoImageView.topAnchor constraintEqualToAnchor:logoBarButtonItem.customView.topAnchor],
        [logoImageView.bottomAnchor constraintEqualToAnchor:logoBarButtonItem.customView.bottomAnchor],
        [logoImageView.widthAnchor constraintEqualToConstant:32.0],
        [logoImageView.heightAnchor constraintEqualToConstant:32.0]
    ]];
    
    kCollectionViewMargin = navigationBar.layoutMargins.right;
}

- (void)setupContainerView {
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    CGFloat contentHeight = self.view.height - kStatusBarFrame.size.height - kNavigationBarHeight - tabBarHeight;
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.scrollEnabled = YES;
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view).mas_offset(CGRectGetMaxY(kStatusBarFrame) + kNavigationBarHeight);
        make.height.equalTo(@(contentHeight));
    }];
    self.containerView = [[UIView alloc] init];
    self.containerView.backgroundColor = kBackgroundColor;
    [scrollView addSubview:self.containerView];
    
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(scrollView);
        make.width.equalTo(scrollView);
        make.height.equalTo(@(840));
    }];
    
    [self setupLastVisitedView];
    [self setupOwnerRoomView];
    [self setupFavoriteRoomView];
}

- (void)setupLastVisitedView {
    CGFloat collectionViewHeight = kRoomCellHeight + kRoomCellPadding * 2;
    CGFloat titleViewHeight = 30;
    CGFloat containerHeight = collectionViewHeight + titleViewHeight + kRoomCellPadding * 2;
    
    // Container
    self.lastVisitedContainerView = [[UIView alloc] init];
    self.lastVisitedContainerView.backgroundColor = UIColor.whiteColor;
    [self.containerView addSubview:self.lastVisitedContainerView];
    
    [self.lastVisitedContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.containerView);
        make.top.equalTo(self.containerView);
        make.height.equalTo(@(containerHeight));
    }];
    
    // Title
    self.lastVisitedTitleView = [[TitleAndToggleButton alloc] init];
    [self.lastVisitedContainerView addSubview:self.lastVisitedTitleView];
    
    [self.lastVisitedTitleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.lastVisitedContainerView).offset(kCollectionViewMargin);
        make.right.equalTo(self.lastVisitedContainerView).offset(-kCollectionViewMargin);
        make.top.equalTo(self.lastVisitedContainerView).offset(kRoomCellPadding);
        make.height.equalTo(@(titleViewHeight));
    }];
    _lastVisitedRoomSection = [[RoomSection alloc] initWithSection:RoomSectionLastVisted isExpand:YES];
    [self.lastVisitedTitleView setObjectWithTaskSection:_lastVisitedRoomSection];
    
    //CollectionView
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    CGFloat itemWidth = kRoomCellHeight + 50;
    CGFloat itemHeight = kRoomCellHeight;
    layout.itemSize = CGSizeMake(itemWidth, itemHeight);
    layout.minimumLineSpacing = 15;
    
    self.lastVisitedCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                                        collectionViewLayout:layout];
    self.lastVisitedCollectionView.delegate = self;
    self.lastVisitedCollectionView.dataSource = self.lastVisitedDataSource;
    
    self.lastVisitedCollectionView.backgroundColor = UIColor.whiteColor;
    self.lastVisitedCollectionView.contentInset = UIEdgeInsetsMake(kRoomCellPadding,
                                                                   kRoomCellPadding,
                                                                   kRoomCellPadding,
                                                                   kRoomCellPadding);
    
    [self.lastVisitedCollectionView registerClass:[RoomCollectionViewCell class]
                       forCellWithReuseIdentifier:RoomCollectionViewCell.cellIdentifier];
    [self.lastVisitedContainerView addSubview:self.lastVisitedCollectionView];
    [self.lastVisitedCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.lastVisitedContainerView).offset(kCollectionViewMargin);
        make.right.equalTo(self.lastVisitedContainerView).offset(-kCollectionViewMargin);
        make.height.equalTo(@(collectionViewHeight));
        make.top.equalTo(self.lastVisitedTitleView.mas_bottom);
    }];
}

- (void)setupOwnerRoomView {
    CGFloat collectionViewHeight = kRoomCellHeight + kRoomCellPadding * 2;
    CGFloat titleViewHeight = 30;
    CGFloat containerHeight = collectionViewHeight + titleViewHeight + kRoomCellPadding * 2;
    // Container
    self.ownerRoomContainerView = [[UIView alloc] init];
    self.ownerRoomContainerView.backgroundColor = UIColor.whiteColor;
    [self.containerView addSubview:self.ownerRoomContainerView];
    
    [self.ownerRoomContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.containerView);
        make.top.equalTo(self.lastVisitedContainerView.mas_bottom).offset(15);
        make.height.equalTo(@(containerHeight));
    }];
    // Title
    self.ownerRoomTitleView = [[TitleAndToggleButton alloc] init];
    [self.ownerRoomContainerView addSubview:self.ownerRoomTitleView];
    
    [self.ownerRoomTitleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.ownerRoomContainerView).offset(kCollectionViewMargin);
        make.right.equalTo(self.ownerRoomContainerView).offset(-kCollectionViewMargin);
        make.top.equalTo(self.ownerRoomContainerView).offset(kRoomCellPadding);
        make.height.equalTo(@(titleViewHeight));
    }];
    _ownerRoomSection = [[RoomSection alloc] initWithSection:RoomSectionOwner isExpand:YES];
    [self.ownerRoomTitleView setObjectWithTaskSection:_ownerRoomSection];
    
    //CollectionView
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    CGFloat itemWidth = kRoomCellHeight + 50;
    CGFloat itemHeight = kRoomCellHeight;
    layout.itemSize = CGSizeMake(itemWidth, itemHeight);
    layout.minimumLineSpacing = 15;
    
    self.ownerRoomCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                                      collectionViewLayout:layout];
    self.ownerRoomCollectionView.delegate = self;
    self.ownerRoomCollectionView.dataSource = self.ownerRoomDataSource;
    self.ownerRoomCollectionView.backgroundColor = UIColor.whiteColor;
    self.ownerRoomCollectionView.contentInset = UIEdgeInsetsMake(kRoomCellPadding,
                                                                 kRoomCellPadding,
                                                                 kRoomCellPadding,
                                                                 kRoomCellPadding);
    
    [self.ownerRoomCollectionView registerClass:[RoomCollectionViewCell class]
                     forCellWithReuseIdentifier:RoomCollectionViewCell.cellIdentifier];
    [self.ownerRoomContainerView addSubview:self.ownerRoomCollectionView];
    [self.ownerRoomCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.ownerRoomContainerView).offset(kCollectionViewMargin);
        make.right.equalTo(self.ownerRoomContainerView).offset(-kCollectionViewMargin);
        make.height.equalTo(@(collectionViewHeight));
        make.top.equalTo(self.ownerRoomTitleView.mas_bottom);
    }];
}

- (void)setupFavoriteRoomView {
    CGFloat collectionViewHeight = kRoomCellHeight + kRoomCellPadding * 2;
    CGFloat titleViewHeight = 30;
    CGFloat containerHeight = collectionViewHeight + titleViewHeight + kRoomCellPadding * 2;
    // Container
    self.favoriteRoomContainerView = [[UIView alloc] init];
    self.favoriteRoomContainerView.backgroundColor = UIColor.whiteColor;
    [self.containerView addSubview:self.favoriteRoomContainerView];
    
    [self.favoriteRoomContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.containerView);
        make.top.equalTo(self.ownerRoomContainerView.mas_bottom).offset(15);
        make.height.equalTo(@(containerHeight));
    }];
    // Title
    self.favoriteRoomTitleView = [[TitleAndToggleButton alloc] init];
    [self.favoriteRoomContainerView addSubview:self.favoriteRoomTitleView];
    
    [self.favoriteRoomTitleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.favoriteRoomContainerView).offset(kCollectionViewMargin);
        make.right.equalTo(self.favoriteRoomContainerView).offset(-kCollectionViewMargin);
        make.top.equalTo(self.favoriteRoomContainerView).offset(kRoomCellPadding);
        make.height.equalTo(@(titleViewHeight));
    }];
    _favoriteRoomSection = [[RoomSection alloc] initWithSection:RoomSectionFavorite isExpand:YES];
    [self.favoriteRoomTitleView setObjectWithTaskSection:_favoriteRoomSection];
    
    //CollectionView
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    CGFloat itemWidth = kRoomCellHeight + 50;
    CGFloat itemHeight = kRoomCellHeight;
    layout.itemSize = CGSizeMake(itemWidth, itemHeight);
    layout.minimumLineSpacing = 15;
    
    self.favoriteRoomCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                                         collectionViewLayout:layout];
    self.favoriteRoomCollectionView.delegate = self;
    self.favoriteRoomCollectionView.dataSource = self.favoriteRoomDataSource;
    
    self.favoriteRoomCollectionView.backgroundColor = UIColor.whiteColor;
    self.favoriteRoomCollectionView.contentInset = UIEdgeInsetsMake(kRoomCellPadding,
                                                                    kRoomCellPadding,
                                                                    kRoomCellPadding,
                                                                    kRoomCellPadding);
    
    [self.favoriteRoomCollectionView registerClass:[RoomCollectionViewCell class]
                        forCellWithReuseIdentifier:RoomCollectionViewCell.cellIdentifier];
    [self.favoriteRoomContainerView addSubview:self.favoriteRoomCollectionView];
    [self.favoriteRoomCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.favoriteRoomContainerView).offset(kCollectionViewMargin);
        make.right.equalTo(self.favoriteRoomContainerView).offset(-kCollectionViewMargin);
        make.height.equalTo(@(collectionViewHeight));
        make.top.equalTo(self.favoriteRoomTitleView.mas_bottom);
    }];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    RoomItem *selectedRoom;
    if (collectionView == self.lastVisitedCollectionView) {
        selectedRoom = [self.lastVisitedRoomList objectAtIndex:indexPath.item];
    }
    else if (collectionView == self.ownerRoomCollectionView) {
        selectedRoom = [self.ownerRoomList objectAtIndex:indexPath.item];
    }
    else {
        selectedRoom = [self.favoriteRoomList objectAtIndex:indexPath.item];
    }
    if (selectedRoom.isProtected) {
        // Create an alert controller with a text field for the password input
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Password Required"
                                                                                 message:@"Please enter the password for the room"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Password";
            textField.secureTextEntry = YES;
        }];
        
        // Create an action for handling the password input
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"Enter" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            UITextField *passwordTextField = alertController.textFields.firstObject;
            NSString *enteredPassword = passwordTextField.text;
            
            [self checkPassword:enteredPassword ofRoom:selectedRoom];
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        
        // Add the action to the alert controller
        [alertController addAction:cancelAction];
        [alertController addAction:confirmAction];
        
        // Present the alert controller modally
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    else {
        self.selectedRoomItem = selectedRoom;
        NSDictionary *dict = @{@"roomCode":selectedRoom.code};
        //Call API join room event
        [self.roomHandler postJoinRoomEventWithInfo:dict];
    }
}

- (void)showErrorAlertWithTitle:(NSString *)title message:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)checkPassword:(NSString *)password ofRoom:(RoomItem *)room {
    NSDictionary *dict = @{@"roomCode":room.code, @"password":password};
    //Call API join room event
    [self.roomHandler postJoinRoomEventWithInfo:dict];
}

#pragma mark - JoinRoomDelegate

- (void)onFinishJoinRoomWithError:(RoomHandlerErrorCode)errorCode {
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (errorCode) {
            case RoomHandlerServerError:
            case RoomHandlerInvalidDataError:
                break;
            case RoomHandlerNoError:
                [self navigateToRoomDetail];
                break;
            case RoomHandlerWrongPassword:
                [self showErrorAlertWithTitle:@"Incorrect Password" message:@"The entered password is incorrect"];
                break;
            case RoomHandlerWaitForHost:
                [self showErrorAlertWithTitle:@"Wait for host" message:@"Please wait the host to start this room"];
                break;
        }
    });
}

- (void)navigateToRoomDetail {
    RoomDetailViewController *roomDetailVC = [[RoomDetailViewController alloc] initWithRoomItem:self.selectedRoomItem];
    UINavigationController *roomDetailNavVC = [[UINavigationController alloc] initWithRootViewController:roomDetailVC];
    roomDetailNavVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:roomDetailNavVC animated:YES completion:nil];
    [[RoomManager sharedManager] joinRoom:self.selectedRoomItem];
}

#pragma mark - NSDiffableDataSourceSnapshot

- (void)dataSource:(UICollectionViewDiffableDataSource<Section *, RoomItem *> *)dataSource
     applySnapshot:(NSDiffableDataSourceSnapshot *)snapshot
         animation:(BOOL)animation
        completion:(void (^)(void))completion {
    [dataSource applySnapshot:snapshot animatingDifferences:animation completion:completion];
}

#pragma mark - Setup Values

- (void)setupValues {
    [self setupItems];
}

- (void)setupItems {
    self.lastVisitedRoomList = [[TSMutableArray alloc] init];
    self.ownerRoomList = [[TSMutableArray alloc] init];
    self.favoriteRoomList = [[TSMutableArray alloc] init];
    
    [self _getLastVisitedRoomItemArray];
    [self _getOwnerRoomItemArray];
    [self _getFavoriteRoomItemArray];
}

- (void)_getLastVisitedRoomItemArray {
    [self.roomHandler getLastestRoomListWithCompletionHandler:^(NSArray<RoomItem *> * _Nullable roomItemArray, NSError * _Nullable error) {
        if (error) {
            return;
        }
        [self.lastVisitedRoomList addObjectsFromArray:roomItemArray];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *snapshot = [self.lastVisitedDataSourceProcessor appendItemArray:self.lastVisitedRoomList];
            [self dataSource:self.lastVisitedDataSourceProcessor.dataSource applySnapshot:snapshot animation:NO completion:nil];
        });
    }];
}

- (void)_getOwnerRoomItemArray {
    NSDictionary *searchParams = [[NSDictionary alloc] initWithObjectsAndKeys:@(TRUE), @"is_my_room", nil];
    [self.roomHandler getRoomListWithSearchParams:searchParams completionHandler:^(NSArray<RoomItem *> * _Nullable roomItemArray, NSError * _Nullable error) {
        if (error) {
            return;
        }
        [self.ownerRoomList addObjectsFromArray:roomItemArray];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *snapshot = [self.ownerRoomDataSourceProcessor appendItemArray:self.ownerRoomList];
            [self dataSource:self.ownerRoomDataSourceProcessor.dataSource applySnapshot:snapshot animation:NO completion:nil];
        });
    }];
}

- (void)_getFavoriteRoomItemArray {
    NSDictionary *searchParams = [[NSDictionary alloc] initWithObjectsAndKeys:@(TRUE), @"is_favored", nil];
    [self.roomHandler getRoomListWithSearchParams:searchParams completionHandler:^(NSArray<RoomItem *> * _Nullable roomItemArray, NSError * _Nullable error) {
        if (error) {
            return;
        }
        [self.favoriteRoomList addObjectsFromArray:roomItemArray];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *snapshot = [self.favoriteRoomDataSourceProcessor appendItemArray:self.favoriteRoomList];
            [self dataSource:self.favoriteRoomDataSourceProcessor.dataSource applySnapshot:snapshot animation:NO completion:nil];
        });
    }];
}

#pragma mark - Setup Delegate

- (void)setupDelegates {
    self.roomHandler.joinRoomDelegate = self;
}

#pragma mark - Lazy Loading

- (UICollectionViewDiffableDataSource<Section *,RoomItem *> *)lastVisitedDataSource {
    if (_lastVisitedDataSource) {
        return _lastVisitedDataSource;
    }
    _lastVisitedDataSource = [[UICollectionViewDiffableDataSource alloc] initWithCollectionView:self.lastVisitedCollectionView cellProvider:^UICollectionViewCell * _Nullable(UICollectionView * _Nonnull collectionView, NSIndexPath * _Nonnull indexPath, RoomItem *roomItem) {
        if (roomItem) {
            RoomCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:RoomCollectionViewCell.cellIdentifier forIndexPath:indexPath];
            cell.roomItem = roomItem;
            [cell setContents];
            return cell;
        }
        return [[RoomCollectionViewCell alloc] init];
    }];
    return _lastVisitedDataSource;
}

- (RoomDataSourceProcessor *)lastVisitedDataSourceProcessor {
    if (_lastVisitedDataSourceProcessor) {
        return _lastVisitedDataSourceProcessor;
    }
    _lastVisitedDataSourceProcessor = [[RoomDataSourceProcessor alloc] initWithDiffDataSource:self.lastVisitedDataSource];
    return _lastVisitedDataSourceProcessor;
}

- (UICollectionViewDiffableDataSource<Section *,RoomItem *> *)ownerRoomDataSource {
    if (_ownerRoomDataSource) {
        return _ownerRoomDataSource;
    }
    _ownerRoomDataSource = [[UICollectionViewDiffableDataSource alloc] initWithCollectionView:self.ownerRoomCollectionView cellProvider:^UICollectionViewCell * _Nullable(UICollectionView * _Nonnull collectionView, NSIndexPath * _Nonnull indexPath, RoomItem *roomItem) {
        if (roomItem) {
            RoomCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:RoomCollectionViewCell.cellIdentifier forIndexPath:indexPath];
            cell.roomItem = roomItem;
            [cell setContents];
            return cell;
        }
        return [[RoomCollectionViewCell alloc] init];
    }];
    return _ownerRoomDataSource;
}

- (RoomDataSourceProcessor *)ownerRoomDataSourceProcessor {
    if (_ownerRoomDataSourceProcessor) {
        return _ownerRoomDataSourceProcessor;
    }
    _ownerRoomDataSourceProcessor = [[RoomDataSourceProcessor alloc] initWithDiffDataSource:self.ownerRoomDataSource];
    return _ownerRoomDataSourceProcessor;
}

- (UICollectionViewDiffableDataSource<Section *,RoomItem *> *)favoriteRoomDataSource {
    if (_favoriteRoomDataSource) {
        return _favoriteRoomDataSource;
    }
    _favoriteRoomDataSource = [[UICollectionViewDiffableDataSource alloc] initWithCollectionView:self.favoriteRoomCollectionView cellProvider:^UICollectionViewCell * _Nullable(UICollectionView * _Nonnull collectionView, NSIndexPath * _Nonnull indexPath, RoomItem *roomItem) {
        if (roomItem) {
            RoomCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:RoomCollectionViewCell.cellIdentifier forIndexPath:indexPath];
            cell.roomItem = roomItem;
            [cell setContents];
            return cell;
        }
        return [[RoomCollectionViewCell alloc] init];
    }];
    return _favoriteRoomDataSource;
}

- (RoomDataSourceProcessor *)favoriteRoomDataSourceProcessor {
    if (_favoriteRoomDataSourceProcessor) {
        return _favoriteRoomDataSourceProcessor;
    }
    _favoriteRoomDataSourceProcessor = [[RoomDataSourceProcessor alloc] initWithDiffDataSource:self.favoriteRoomDataSource];
    return _favoriteRoomDataSourceProcessor;
}

- (RoomHandler *)roomHandler {
    if (_roomHandler) {
        return _roomHandler;
    }
    _roomHandler = [[RoomHandler alloc] init];
    return _roomHandler;
}

@end
