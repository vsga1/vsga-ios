//
//  ProfileViewController.m
//  VStudy
//
//  Created by LAP14011 on 20/02/2023.
//

#import "ProfileViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Macro.h"

#import "CustomLabel.h"
#import "CustomButton.h"

#import "UIView+Extension.h"
#import "UtilityClass.h"

#import "AppDelegate.h"
#import "AccountManager.h"
#import "UserHandler.h"

const CGFloat kavatarSize = 70;
const CGFloat kContentPadding = 20;

@interface ProfileViewController ()
<UICollectionViewDataSource,
UICollectionViewDelegate>

@property (strong, nonatomic) UIView *containerView;
@property (strong, nonatomic) UIImageView *avatarImageView;
@property (strong, nonatomic) CustomLabel *usernameLabel;
@property (strong, nonatomic) CustomLabel *emailLabel;
@property (strong, nonatomic) UserHandler *userHandler;
@end

@implementation ProfileViewController {
    NSArray *accountItems;
    NSArray *generalItems;
    NSArray *sectionTitles;
    NSArray *sectionData;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupViews];
    [self setupValues];
    
}

#pragma mark - Setup Views

- (void)setupViews {
    [self setupNavigationBar];
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    CGFloat containerHeight = self.view.height - kStatusBarFrame.size.height - kNavigationBarHeight - tabBarHeight;
    
    self.containerView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                  CGRectGetMaxY(kStatusBarFrame) + kNavigationBarHeight,
                                                                  self.view.width,
                                                                  containerHeight)];
    self.containerView.backgroundColor = kBackgroundColor;
    [self.view addSubview:self.containerView];
    
    CGFloat padding = 20;
    CGFloat infoCardHeight = roundf((containerHeight - 2*padding)*1/5);
    CGFloat collectionViewHeight = (containerHeight - 2*padding) - infoCardHeight;
    
    CGRect infoCardFrame = CGRectMake(padding,
                                      padding,
                                      self.view.width - 2*padding,
                                      infoCardHeight);
    [self createInfoCardWithFrame:infoCardFrame];
    
    CGRect collectionViewFrame = CGRectMake(padding,
                                            infoCardFrame.origin.y + infoCardFrame.size.height,
                                            self.view.width - 2*padding,
                                            collectionViewHeight);
    [self createCollectionViewWithFrame:collectionViewFrame];
}

#pragma mark - Navigation bar

- (void)setupNavigationBar {
    UINavigationBar *navigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(kStatusBarFrame), self.view.width, kNavigationBarHeight)];
    navigationBar.barTintColor = [UIColor whiteColor];
    
    UINavigationItem *navigationItem = [[UINavigationItem alloc] initWithTitle:@"Profile"];
    [navigationBar setItems:@[navigationItem]];
    
    [self.view addSubview:navigationBar];
    
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Logo"]];
    logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UIBarButtonItem *logoBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:logoImageView];
    
    navigationItem.leftBarButtonItem = logoBarButtonItem;
    navigationItem.leftItemsSupplementBackButton = YES;
    
    logoImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [NSLayoutConstraint activateConstraints:@[
        [logoImageView.leadingAnchor constraintEqualToAnchor:logoBarButtonItem.customView.leadingAnchor],
        [logoImageView.topAnchor constraintEqualToAnchor:logoBarButtonItem.customView.topAnchor],
        [logoImageView.bottomAnchor constraintEqualToAnchor:logoBarButtonItem.customView.bottomAnchor],
        [logoImageView.widthAnchor constraintEqualToConstant:32.0],
        [logoImageView.heightAnchor constraintEqualToConstant:32.0]
    ]];
}

#pragma mark - Info card

- (void)createInfoCardWithFrame:(CGRect)frame {
    UIView *infoContainerView = [[UIView alloc] initWithFrame:frame];
    infoContainerView.backgroundColor = UIColor.whiteColor;
    infoContainerView.layer.cornerRadius = 15;
    [self.containerView addSubview:infoContainerView];
    
    /// Avatar
    UIView *avatarContainerView = [[UIView alloc] initWithFrame:CGRectMake(kContentPadding,
                                                                           (frame.size.height - kavatarSize)/2,
                                                                           kavatarSize,
                                                                           kavatarSize)];
    
    avatarContainerView.layer.cornerRadius = avatarContainerView.frame.size.width/2;
    avatarContainerView.layer.shadowColor = [UIColor grayColor].CGColor;
    avatarContainerView.layer.shadowRadius = 5;
    avatarContainerView.layer.shadowOpacity = 1;
    avatarContainerView.layer.shadowOffset = CGSizeMake(0, 0);
    avatarContainerView.layer.borderColor = UIColor.whiteColor.CGColor;
    avatarContainerView.layer.borderWidth = 3;
    [infoContainerView addSubview:avatarContainerView];
    
    // Create the image view with the user's avatar
    self.avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                         0,
                                                                         kavatarSize,
                                                                         kavatarSize)];
    self.avatarImageView.center = CGPointMake(avatarContainerView.bounds.size.width/2,
                                              avatarContainerView.bounds.size.height/2);
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.width/2;
    self.avatarImageView.layer.masksToBounds = YES;
//    self.avatarImageView.image = [UIImage imageNamed:@"Avatar"];
    [avatarContainerView addSubview:self.avatarImageView];
    
    /// Full name, username, email
    CGFloat usernameHeight = 30;
    CGFloat emailHeight = 25;
    CGFloat titleWidth = self.view.width - 3*kContentPadding - kavatarSize;
    
    CGFloat usernameY = (frame.size.height - (usernameHeight + emailHeight))/2;
    CGFloat titleX = CGRectGetMaxX(avatarContainerView.frame) + kContentPadding;
    self.usernameLabel = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(titleX,
                                                   usernameY,
                                                   titleWidth,
                                                   usernameHeight)
                          title:@"Username"
                          titleFont:kMedium18
                          titleColor:UIColor.blackColor];
    [infoContainerView addSubview:self.usernameLabel];
    
    self.emailLabel = [[CustomLabel alloc]
                       initWithFrame:CGRectMake(titleX,
                                                self.usernameLabel.bottom,
                                                titleWidth,
                                                emailHeight)
                       title:@"Email"
                       titleFont:kRegular16
                       titleColor:UIColor.grayColor];
    [infoContainerView addSubview:self.emailLabel];
}

#pragma mark - UICollectionView

- (void)createCollectionViewWithFrame:(CGRect)frame {
    UIView *collectionViewContainerView = [[UIView alloc] initWithFrame:frame];
    [self.containerView addSubview:collectionViewContainerView];
    
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,
                                                                                          0,
                                                                                          frame.size.width,
                                                                                          frame.size.height) collectionViewLayout:layout];
    
    UIEdgeInsets sectionInset = UIEdgeInsetsMake(10, 0, 10, 0);
    UIEdgeInsets contentInset = UIEdgeInsetsMake(10, 0, 10, 0);
    
    CGFloat itemWidth = frame.size.width - (sectionInset.left + sectionInset.right) - (contentInset.left + contentInset.right);
    
    CGSize itemSize = CGSizeMake(itemWidth, 55);
    
    CGSize headerSize = CGSizeMake(itemWidth, 30);
    
    layout.sectionInset = sectionInset;
    layout.itemSize = itemSize;
    layout.headerReferenceSize = headerSize;
    layout.minimumLineSpacing = 0;
    collectionView.contentInset = contentInset;
    
    collectionView.backgroundColor = kBackgroundColor;
    
    collectionView.dataSource = self;
    collectionView.delegate = self;
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    [collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Header"];
    [collectionViewContainerView addSubview:collectionView];
    
    // Define the data for the collection view
    accountItems = @[@"Language", @"Notification Settings", @"Edit Profile"];
    generalItems = @[@"Support", @"Terms of Service", @"Sign out"];
    sectionTitles = @[@"Account", @"General"];
    sectionData = @[accountItems, generalItems];
    
}

#pragma mark - Setup Values

- (void)setupValues {
    [self.userHandler getProfileAndUpdateCurrentUserInfoWithCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            return;
        }
        User *currentUser = [[AccountManager sharedManager] getCurrentUser];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.usernameLabel.text = currentUser.username;
            self.emailLabel.text = currentUser.email;
        });
        [self.avatarImageView sd_setImageWithURL:currentUser.avatarURL
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image) {
                self.avatarImageView.image = image;
            } else {
                // Error occurred while loading the image
                NSLog(@"Error loading image: %@", error);
            }
        }];
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return sectionData.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [sectionData[section] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.backgroundColor = UIColor.whiteColor;
    
    // Title
    NSString *title = sectionData[indexPath.section][indexPath.row];
    UIFont *titleFont = kRegular16;
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(kContentPadding,
                                                   0,
                                                   cell.contentView.frame.size.width - 2*kContentPadding - 20,
                                                   cell.contentView.frame.size.height - 0.2)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    [cell.contentView addSubview:label];
    
    // Divider
    UIView *divider = [[UIView alloc] initWithFrame:CGRectMake(kContentPadding,
                                                               label.bottom,
                                                               cell.contentView.frame.size.width - kContentPadding,
                                                               0.2)];
    divider.backgroundColor = UIColor.lightGrayColor;
    [cell.contentView addSubview:divider];
    
    NSInteger numberOfItemsInSection = [collectionView numberOfItemsInSection:indexPath.section];
    if (indexPath.item == numberOfItemsInSection - 1) {
        divider.hidden = TRUE;
    }
    else {
        divider.hidden = FALSE;
    }
    
    // Arrow Icon
    UIImageView *arrowIcon = [[UIImageView alloc] initWithFrame:CGRectMake(label.right,
                                                                           (cell.contentView.height - 20)/2,
                                                                           20,
                                                                           20)];
    arrowIcon.image = [UIImage imageNamed:@"RightArrowIcon"];
    arrowIcon.tintColor = UIColor.lightGrayColor;
    [cell.contentView addSubview:arrowIcon];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Header" forIndexPath:indexPath];
    
    NSString *title = sectionTitles[indexPath.section];
    UIFont *titleFont = kMedium16;
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(kContentPadding,
                                                   0,
                                                   headerView.frame.size.width - 2 * kContentPadding,
                                                   headerView.frame.size.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    [headerView addSubview:label];
    
    return headerView;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger numberOfItemsInSection = [collectionView numberOfItemsInSection:indexPath.section];
    
    // Set the corner radius for the first cell of each section
    if (indexPath.item == 0) {
        CGFloat cornerRadius = 15;
        
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:cell.bounds
                                               byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
                                                     cornerRadii:CGSizeMake(cornerRadius, cornerRadius)].CGPath;
        cell.layer.mask = maskLayer;
    }
    
    // Set the corner radius for the last cell of each section
    if (indexPath.item == numberOfItemsInSection - 1) {
        CGFloat cornerRadius = 15;
        
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:cell.bounds
                                               byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight
                                                     cornerRadii:CGSizeMake(cornerRadius, cornerRadius)].CGPath;
        cell.layer.mask = maskLayer;
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    // Handle cell selection here
    if (indexPath.item == 2 && indexPath.section == 1) {
        [UtilityClass showLoadingToastCenterInView:self.view];
        [[AccountManager sharedManager] removeAccountInfoWithCompletion:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [UtilityClass hideLoadingToastOfView:self.view];
                [AppDelegate.shared updateView];
            });
        }];
        
    }
}

#pragma mark - Lazy loading

- (UserHandler *)userHandler {
    if (_userHandler) {
        return _userHandler;
    }
    _userHandler = [[UserHandler alloc] init];
    return _userHandler;
}

@end
