//
//  TabBarCollectionViewCell.h
//  VStudy
//
//  Created by LAP14011 on 09/03/2023.
//

#import <UIKit/UIKit.h>
#import "CustomLabel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TabBarCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) UIImageView *imageView;

@property (strong, nonatomic) CustomLabel *label;

+ (NSString *)cellIdentifier;

@end

NS_ASSUME_NONNULL_END
