//
//  TabBarCollectionViewCell.m
//  VStudy
//
//  Created by LAP14011 on 09/03/2023.
//

#import "TabBarCollectionViewCell.h"
#import "Macro.h"
#import "UIView+Extension.h"

const CGFloat kIconSize = 30;
const CGFloat kSpacing = 5;

@implementation TabBarCollectionViewCell 

+ (NSString *)cellIdentifier {
    return NSStringFromClass(self.class);
}

- (UIImageView *)imageView {
    if (_imageView) {
        return _imageView;
    }
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.width - kIconSize)/2, kSpacing, kIconSize, kIconSize)];
    [self.contentView addSubview:_imageView];
    return _imageView;
}

- (CustomLabel *)label {
    if (_label) {
        return _label;
    }
    UIFont *titleFont = kRegular16;
    _label = [[CustomLabel alloc] initWithFrame:CGRectMake(0, _imageView.y + _imageView.height + kSpacing, self.width, 20) title:@"" titleFont:titleFont titleColor:UIColor.blackColor];
    _label.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:_label];
    return _label;
}



@end
