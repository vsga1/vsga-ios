//
//  RoomTaskCollectionViewCell.h
//  VStudy
//
//  Created by LAP14011 on 02/04/2023.
//

#import <UIKit/UIKit.h>
#import "RoomTaskItem.h"
#import "CustomTextView.h"
NS_ASSUME_NONNULL_BEGIN

@class RoomTaskCollectionViewCell;

@protocol RoomTaskCollectionViewCellDelegate <NSObject>

- (void)roomTaskUpdateStatusItem:(RoomTaskItem *)item;

@end


@interface RoomTaskCollectionViewCell : UICollectionViewCell <UITextViewDelegate>

@property (nonatomic, weak) id<RoomTaskCollectionViewCellDelegate> delegate;

@property (strong, nonatomic) RoomTaskItem *taskItem;

@property (nonatomic, strong) UIView *circleView;

@property (strong, nonatomic) CustomTextView *descLabel;

@property (strong, nonatomic) UIView *upperView;

@property (strong, nonatomic) UIView *lowerView;

@property (strong, nonatomic) UICollectionView *avatarCollectionView;

+ (NSString *)cellIdentifier;

// MARK: SetContent

- (void)setContents;

- (void)beginSetContentWithItem:(RoomTaskItem *)item;

- (void)endSetContentWithItem:(RoomTaskItem *)item;

// MARK: Layout

- (void)beginLayoutWithItem:(RoomTaskItem *)item;

- (void)endLayoutWithItem:(RoomTaskItem *)item;

@end

NS_ASSUME_NONNULL_END
