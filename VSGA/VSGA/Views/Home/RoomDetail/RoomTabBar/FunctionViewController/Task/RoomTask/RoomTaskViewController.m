//
//  RoomTaskViewController.m
//  VStudy
//
//  Created by LAP14011 on 29/03/2023.
//

#import "RoomTaskViewController.h"
#import <Masonry/Masonry.h>
#import "Scope.h"

#import "Macro.h"
#import "AccountManager.h"
#import "TaskHandler.h"
#import "RoomManager.h"
#import "MySocketManager.h"

#import "UIView+Extension.h"

#import "TaskHeaderCollectionReusableView.h"
#import "RoomTaskCollectionViewCell.h"

#import "TaskSection.h"
#import "RoomTaskItem.h"
#import "RoomTaskDataSourceProcessor.h"
#import "TSMutableArray.h"

static CGSize kRoomTaskTargetSize;

@interface RoomTaskViewController () <UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, RoomTaskCollectionViewCellDelegate>

@property (nonatomic, strong) UICollectionView *taskCollectionView;

@property (nonatomic, strong) UICollectionViewDiffableDataSource<TaskSection *, RoomTaskItem *> *dataSource;

@property (nonatomic, strong) RoomTaskDataSourceProcessor *dataSourceProcessor;

@property (nonatomic, strong) TSMutableArray<RoomTaskItem *> *undoneTaskList;

@property (nonatomic, strong) TSMutableArray<RoomTaskItem *> *doneTaskList;

@property (nonatomic, strong) TSMutableArray<TaskSection *> *sectionList;

@property (nonatomic, strong) TaskHandler *taskHandler;

@end

@implementation RoomTaskViewController {
    TaskSection *_doneTaskSection;
    TaskSection *_undoneTaskSection;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViews];
    [self setupValues];
    [self setupNewTaskSocket];
    [self setupDoneTaskSocket];
}

#pragma mark - Setup Views

- (void)setupViews {
    self.view.backgroundColor = kBackgroundColor;
    [self setupTaskCollectionView];
}

- (void)setupTaskCollectionView {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.headerReferenceSize = CGSizeMake(self.view.width, 50);
    layout.sectionHeadersPinToVisibleBounds = TRUE;
    
    self.taskCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                                     collectionViewLayout:layout];
    
    self.taskCollectionView.dataSource = self.dataSource;
    self.taskCollectionView.delegate = self;
    
    [self.taskCollectionView registerClass:[RoomTaskCollectionViewCell class] forCellWithReuseIdentifier:RoomTaskCollectionViewCell.cellIdentifier];
    [self.taskCollectionView registerClass:[TaskHeaderCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Header"];
    
    [self.view addSubview:self.taskCollectionView];
    
    [self.taskCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop).with.offset(50);
        make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).with.offset(30);
        make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).with.offset(-30);
        make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).with.offset(-30);
    }];
    
    self.taskCollectionView.backgroundColor = kBackgroundColor;
}

- (void)toggleSection:(TaskSection *)section {
    section.isExpand = !section.isExpand;
    
    NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor reloadSection:section];
    [self applySnapshot:snapshot animation:NO completion:nil];
    
    TSMutableArray *itemArray = nil;
    switch (section.sectionType) {
        case TaskSectionDone:
            itemArray = self.doneTaskList;
            break;
        case TaskSectionUndone:
            itemArray= self.undoneTaskList;
            break;
        default:
            break;
    }
    if (!section.isExpand) {
        snapshot = [self.dataSourceProcessor deleteItemArray:itemArray];
    }
    else {
        snapshot = [self.dataSourceProcessor appendItemArray:itemArray intoSection:section];
    }
    [self applySnapshot:snapshot animation:YES completion:nil];
}

- (void)toggleSectionAtIndexPath:(NSIndexPath *)indexPath {
    TaskSection *section = [self.sectionList objectAtIndex:indexPath.section];
    [self toggleSection:section];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    kRoomTaskTargetSize = CGSizeMake(self.taskCollectionView.width, 55);
}


#pragma mark - CollectionView

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        RoomTaskItem *item = [self.undoneTaskList objectAtIndex:indexPath.item];
        if (item.doneUserArray.count == 0) {
            return kRoomTaskTargetSize;
        }
        else {
            return CGSizeMake(kRoomTaskTargetSize.width, kRoomTaskTargetSize.height + 40);
        }
    }
    RoomTaskItem *item = [self.doneTaskList objectAtIndex:indexPath.item];
    if (item.doneUserArray.count == 0) {
        return kRoomTaskTargetSize;
    }
    else {
        return CGSizeMake(kRoomTaskTargetSize.width, kRoomTaskTargetSize.height + 40);
    }
}

#pragma mark - Setup Socket

- (void)setupDoneTaskSocket {
    @weakify(self)
    [[MySocketManager sharedInstance] listenForDoneRoomTaskWithCompletion:^(NSString * _Nonnull code, NSString * _Nonnull scheme, User * _Nonnull user) {
        @strongify(self)
        TaskSection *section = self->_undoneTaskSection;
        RoomTaskItem *item = [self didList:self.undoneTaskList containTaskWithScheme:scheme];
        if (!item) {
            item = [self didList:self.doneTaskList containTaskWithScheme:scheme];
            section = self->_doneTaskSection;
        }
        if (!item) {
            return;
        }
        
        [item addMoreDoneUser:user];
        if (!section.isExpand) {
            [self toggleSection:section];
        }
        
        NSString *currentUsername = [[AccountManager sharedManager] getCurrentUsername];
        if ([user.username isEqualToString:currentUsername]) {
            [self.undoneTaskList removeObject:item];
            [self.doneTaskList insertObject:item atIndex:0];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor deleteItem:item];
                [self applySnapshot:snapshot animation:YES completion:^{
                    if (!self->_doneTaskSection.isExpand) {
                        return;
                    }
                    NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor insertItem:item intoSection:self->_doneTaskSection];
                    [self applySnapshot:snapshot animation:YES completion:nil];
                }];
            });
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor reloadItem:item];
            [self applySnapshot:snapshot animation:YES completion:nil];
        });
        NSLog(@"tamnvm>> item %@", item);
        
    }];
}

- (void)setupNewTaskSocket {
    @weakify(self)
    [[MySocketManager sharedInstance] listenForGetNewRoomTaskWithCompletion:^(RoomTaskItem * _Nonnull item) {
        @strongify(self)
        if (!self->_undoneTaskSection.isExpand) {
            [self toggleSection:self->_undoneTaskSection];
        }
        
        [self.undoneTaskList insertObject:item atIndex:0];
        
        [item setupDisplayInView];
       
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor insertItem:item intoSection:self->_undoneTaskSection];
            [self applySnapshot:snapshot animation:YES completion:nil];
        });
        NSLog(@"tamnvm>> item %@", item);
        
    }];
}

- (RoomTaskItem *)didList:(TSMutableArray *)list containTaskWithScheme:(NSString *)scheme {
    for (RoomTaskItem *item in list) {
        if ([item.scheme isEqualToString:scheme]) {
            return item;
        }
    }
    return nil;
}

#pragma mark - Room Cell Delegate

- (void)roomTaskUpdateStatusItem:(RoomTaskItem *)item {
    [self.taskHandler updateTask:item completionHandler:^(NSError * _Nullable error) {\
        if (error) {
            return;
        }
        User *currentUser = [[AccountManager sharedManager] getCurrentUser];
        NSString *roomCode = [[RoomManager sharedManager] getRoomCode];
        NSDictionary *dataSocket = @{@"roomCode": roomCode, @"scheme": item.scheme, @"code": item.code, @"username":currentUser.username};
        [[MySocketManager sharedInstance] emit:kDoneRoomTask with:dataSocket completion:^(BOOL success) {
            if (!success) {
                return;
            }
        }];
    }];
    
    
}

#pragma mark - Setup Values

- (void)setupValues {
    [self setupSections];
    [self setupItems];
}

- (void)setupSections {
    self.sectionList = [[TSMutableArray alloc] init];
    
    _undoneTaskSection = [[TaskSection alloc] initWithSection:TaskSectionUndone isExpand:YES];
    _doneTaskSection = [[TaskSection alloc] initWithSection:TaskSectionDone isExpand:YES];
    
    [self.sectionList addObject:_undoneTaskSection];
    [self.sectionList addObject:_doneTaskSection];
    
    NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor appendSections:self.sectionList];
    [self applySnapshot:snapshot animation:NO completion:nil];
}

- (void)setupItems {
    self.undoneTaskList = [[TSMutableArray alloc] init];
    self.doneTaskList = [[TSMutableArray alloc] init];
   
    [self _getUndoneTaskItemArray];
    [self _getDoneTaskItemArray];
}

- (void)_getUndoneTaskItemArray {
    NSString *roomCode = [[RoomManager sharedManager] getRoomCode];
    [self.taskHandler getTaskWithRoomCode:roomCode
                                     type:TaskTypeRoom
                                   status:TaskStatusUndone
                        completionHandler:^(NSArray<TaskItem *> * _Nullable taskItemArray, NSError * _Nullable error) {
        if (error) {
            return;
        }
        dispatch_group_t group = dispatch_group_create();
        for (TaskItem *item in taskItemArray) {
            dispatch_group_enter(group);
            [self.taskHandler postRoomTaskDoneWithItem:item completionHandler:^(RoomTaskItem * _Nullable roomItem, NSError * _Nullable error) {
                if (error) {
                    dispatch_group_leave(group);
                    return;
                }
                [roomItem setupDisplayInView];
                [self.undoneTaskList insertObject:roomItem atIndex:0];
                dispatch_group_leave(group);
            }];
        }
        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor appendItemArray:self.undoneTaskList intoSection:self->_undoneTaskSection];
            [self applySnapshot:snapshot animation:NO completion:nil];
        });
    }];
}

- (void)_getDoneTaskItemArray {
    NSString *roomCode = [[RoomManager sharedManager] getRoomCode];
    [self.taskHandler getTaskWithRoomCode:roomCode
                                     type:TaskTypeRoom
                                   status:TaskStatusDone
                        completionHandler:^(NSArray<TaskItem *> * _Nullable taskItemArray, NSError * _Nullable error) {
        if (error) {
            return;
        }
        dispatch_group_t group = dispatch_group_create();
        for (TaskItem *item in taskItemArray) {
            dispatch_group_enter(group);
            [self.taskHandler postRoomTaskDoneWithItem:item completionHandler:^(RoomTaskItem * _Nullable roomItem, NSError * _Nullable error) {
                if (error) {
                    dispatch_group_leave(group);
                    return;
                }
                [roomItem setupDisplayInView];
                [self.doneTaskList insertObject:roomItem atIndex:0];
                dispatch_group_leave(group);
            }];
        }
        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor appendItemArray:self.doneTaskList intoSection:self->_doneTaskSection];
            [self applySnapshot:snapshot animation:NO completion:nil];
        });
    }];
}

- (NSArray<RoomTaskItem *> *)_updateTaskInfoAndConvertToRoomTaskArray:(NSArray<TaskItem *> *)taskItemArray {
    NSMutableArray *roomTaskItemArray = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < taskItemArray.count; i++) {
        TaskItem *item = [taskItemArray objectAtIndex:i];
        [item setupDisplayInView];
        RoomTaskItem *roomTaskItem = [[RoomTaskItem alloc] initWithTaskItem:item doneUserArray:nil];
        [roomTaskItemArray addObject:roomTaskItem];
    }
    return roomTaskItemArray;
}

#pragma mark - NSDiffableDataSourceSnapshot

- (void)applySnapshot:(NSDiffableDataSourceSnapshot *)snapshot
            animation:(BOOL)animation
           completion:(void (^)(void))completion {
    [self.dataSource applySnapshot:snapshot animatingDifferences:animation completion:completion];
}

#pragma mark - Add new task

- (void)handleAddNewPersonalTaskUI {
    
}

#pragma mark - Lazy Loading

- (UICollectionViewDiffableDataSource<TaskSection *,RoomTaskItem *> *)dataSource {
    if (_dataSource) {
        return _dataSource;
    }
    __weak typeof(self) weakSelf1 = self;
    _dataSource = [[UICollectionViewDiffableDataSource alloc] initWithCollectionView:self.taskCollectionView cellProvider:^UICollectionViewCell * _Nullable(UICollectionView * _Nonnull collectionView, NSIndexPath * _Nonnull indexPath, RoomTaskItem *taskItem) {
        __strong typeof(weakSelf1) strongSelf = weakSelf1;
        if (taskItem) {
            RoomTaskCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[[RoomTaskCollectionViewCell class] cellIdentifier] forIndexPath:indexPath];
            cell.taskItem = taskItem;
            cell.delegate = strongSelf;
            [cell setContents];
            return cell;
        }
        return [[RoomTaskCollectionViewCell alloc] init];
    }];
    __weak typeof(self) weakSelf2 = self;
    _dataSource.supplementaryViewProvider = ^UICollectionReusableView *(UICollectionView *collectionView, NSString *elementKind, NSIndexPath *indexPath) {
        __strong typeof(weakSelf2) strongSelf = weakSelf2;
        if ([elementKind isEqualToString:UICollectionElementKindSectionHeader]) {
            TaskSection *section = [strongSelf.sectionList objectAtIndex:indexPath.section];
            TaskHeaderCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:elementKind withReuseIdentifier:@"Header" forIndexPath:indexPath];
            [headerView setObjectWithTaskSection:section];
            headerView.toggleHandler = ^{
                [strongSelf toggleSectionAtIndexPath:indexPath];
            };
            return headerView;
        } else {
            return nil;
        }
        
    };
    return _dataSource;
}

- (RoomTaskDataSourceProcessor *)dataSourceProcessor {
    if (_dataSourceProcessor) {
        return _dataSourceProcessor;
    }
    _dataSourceProcessor = [[RoomTaskDataSourceProcessor alloc] initWithDiffDataSource:self.dataSource];
    return _dataSourceProcessor;
}

- (TaskHandler *)taskHandler {
    if (_taskHandler) {
        return _taskHandler;
    }
    _taskHandler = [[TaskHandler alloc] init];
    return _taskHandler;
}

@end
