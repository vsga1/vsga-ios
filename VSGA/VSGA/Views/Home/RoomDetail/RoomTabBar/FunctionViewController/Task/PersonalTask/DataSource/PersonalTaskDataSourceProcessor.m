//
//  PersonalTaskDataSourceProcessor.m
//  VStudy
//
//  Created by LAP14011 on 30/03/2023.
//

#import "PersonalTaskDataSourceProcessor.h"

@implementation PersonalTaskDataSourceProcessor

- (instancetype)initWithDiffDataSource:(UICollectionViewDiffableDataSource *)dataSource {
    self = [super init];
    if (self) {
        self.dataSource = dataSource;
    }
    return self;
}

- (NSDiffableDataSourceSnapshot *)appendSections:(TSMutableArray<TaskSection *> *)sections {
    NSDiffableDataSourceSnapshot<TaskSection *, TaskItem *> *snapshot = [[NSDiffableDataSourceSnapshot alloc] init];
    if (sections.count > 0) {
        [snapshot appendSectionsWithIdentifiers:sections];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)reloadSection:(TaskSection *)section {
    NSDiffableDataSourceSnapshot<TaskSection *, TaskItem *> *snapshot = self.dataSource.snapshot;
    [snapshot reloadSectionsWithIdentifiers:@[section]];
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)appendItemArray:(TSMutableArray<TaskItem *> *)taskItemArray intoSection:(TaskSection *)section {
    NSDiffableDataSourceSnapshot<TaskSection *, TaskItem *> *snapshot = self.dataSource.snapshot;
    if (taskItemArray.count > 0) {
        [snapshot appendItemsWithIdentifiers:taskItemArray intoSectionWithIdentifier:section];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)appendItem:(TaskItem *)taskItem intoSection:(TaskSection *)section {
    NSDiffableDataSourceSnapshot<TaskSection *, TaskItem *> *snapshot = self.dataSource.snapshot;
    if (taskItem) {
        [snapshot appendItemsWithIdentifiers:@[taskItem] intoSectionWithIdentifier:section];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)insertItem:(TaskItem *)taskItem intoSection:(TaskSection *)section {
    NSDiffableDataSourceSnapshot<TaskSection *, TaskItem *> *snapshot = self.dataSource.snapshot;
    TaskItem *firstItem = [[snapshot itemIdentifiersInSectionWithIdentifier:section] firstObject];
    if (firstItem) {
        [snapshot insertItemsWithIdentifiers:@[taskItem] beforeItemWithIdentifier:firstItem];
    }
    else {
        [snapshot appendItemsWithIdentifiers:@[taskItem] intoSectionWithIdentifier:section];
    }
    return snapshot;
}
- (NSDiffableDataSourceSnapshot *)reloadItem:(TaskItem *)taskItem {
    NSDiffableDataSourceSnapshot<TaskSection *, TaskItem *> *snapshot = self.dataSource.snapshot;
    [snapshot reloadItemsWithIdentifiers:@[taskItem]];
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)reloadItemArray:(TSMutableArray<TaskItem *> *)taskItemArray {
    NSDiffableDataSourceSnapshot<TaskSection *, TaskItem *> *snapshot = self.dataSource.snapshot;
    [snapshot reloadItemsWithIdentifiers:taskItemArray];
//    NSLog(@"reloaded item array %@",snapshot.reloadedItemIdentifiers);
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)deleteItem:(TaskItem *)taskItem {
    NSDiffableDataSourceSnapshot<TaskSection *, TaskItem *> *snapshot = self.dataSource.snapshot;
    [snapshot deleteItemsWithIdentifiers:@[taskItem]];
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)deleteItemArray:(TSMutableArray<TaskItem *> *)taskItemArray {
    NSDiffableDataSourceSnapshot<TaskSection *, TaskItem *> *snapshot = self.dataSource.snapshot;
    [snapshot deleteItemsWithIdentifiers:taskItemArray];
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)replaceItem:(TaskItem *)taskItem withNewItem:(TaskItem *)newTaskItem {
    NSDiffableDataSourceSnapshot<TaskSection *, TaskItem *> *snapshot = self.dataSource.snapshot;
    [snapshot insertItemsWithIdentifiers:@[newTaskItem] beforeItemWithIdentifier:taskItem];
    [snapshot deleteItemsWithIdentifiers:@[taskItem]];
    return snapshot;
}
@end
