//
//  RoomTaskDataSourceProcessor.m
//  VStudy
//
//  Created by LAP14011 on 02/04/2023.
//

#import "RoomTaskDataSourceProcessor.h"

@implementation RoomTaskDataSourceProcessor

- (instancetype)initWithDiffDataSource:(UICollectionViewDiffableDataSource *)dataSource {
    self = [super init];
    if (self) {
        self.dataSource = dataSource;
    }
    return self;
}

- (NSDiffableDataSourceSnapshot *)appendSections:(TSMutableArray<TaskSection *> *)sections {
    NSDiffableDataSourceSnapshot<TaskSection *, RoomTaskItem *> *snapshot = [[NSDiffableDataSourceSnapshot alloc] init];
    if (sections.count > 0) {
        [snapshot appendSectionsWithIdentifiers:sections];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)reloadSection:(TaskSection *)section {
    NSDiffableDataSourceSnapshot<TaskSection *, RoomTaskItem *> *snapshot = self.dataSource.snapshot;
    [snapshot reloadSectionsWithIdentifiers:@[section]];
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)appendItemArray:(TSMutableArray<RoomTaskItem *> *)taskItemArray intoSection:(TaskSection *)section {
    NSDiffableDataSourceSnapshot<TaskSection *, RoomTaskItem *> *snapshot = self.dataSource.snapshot;
    if (taskItemArray.count > 0) {
        [snapshot appendItemsWithIdentifiers:taskItemArray intoSectionWithIdentifier:section];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)appendItem:(RoomTaskItem *)taskItem intoSection:(TaskSection *)section {
    NSDiffableDataSourceSnapshot<TaskSection *, RoomTaskItem *> *snapshot = self.dataSource.snapshot;
    if (taskItem) {
        [snapshot appendItemsWithIdentifiers:@[taskItem] intoSectionWithIdentifier:section];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)insertItem:(RoomTaskItem *)taskItem intoSection:(TaskSection *)section {
    NSDiffableDataSourceSnapshot<TaskSection *, RoomTaskItem *> *snapshot = self.dataSource.snapshot;
    RoomTaskItem *firstItem = [[snapshot itemIdentifiersInSectionWithIdentifier:section] firstObject];
    if (firstItem) {
        [snapshot insertItemsWithIdentifiers:@[taskItem] beforeItemWithIdentifier:firstItem];
    }
    else {
        [snapshot appendItemsWithIdentifiers:@[taskItem] intoSectionWithIdentifier:section];
    }
    return snapshot;
}
- (NSDiffableDataSourceSnapshot *)reloadItem:(RoomTaskItem *)taskItem {
    NSDiffableDataSourceSnapshot<TaskSection *, RoomTaskItem *> *snapshot = self.dataSource.snapshot;
    [snapshot reloadItemsWithIdentifiers:@[taskItem]];
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)reloadItemArray:(TSMutableArray<RoomTaskItem *> *)taskItemArray {
    NSDiffableDataSourceSnapshot<TaskSection *, RoomTaskItem *> *snapshot = self.dataSource.snapshot;
    [snapshot reloadItemsWithIdentifiers:taskItemArray];
//    NSLog(@"reloaded item array %@",snapshot.reloadedItemIdentifiers);
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)deleteItem:(RoomTaskItem *)taskItem {
    NSDiffableDataSourceSnapshot<TaskSection *, RoomTaskItem *> *snapshot = self.dataSource.snapshot;
    [snapshot deleteItemsWithIdentifiers:@[taskItem]];
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)deleteItemArray:(TSMutableArray<RoomTaskItem *> *)taskItemArray {
    NSDiffableDataSourceSnapshot<TaskSection *, RoomTaskItem *> *snapshot = self.dataSource.snapshot;
    [snapshot deleteItemsWithIdentifiers:taskItemArray];
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)replaceItem:(RoomTaskItem *)taskItem withNewItem:(RoomTaskItem *)newTaskItem {
    NSDiffableDataSourceSnapshot<TaskSection *, RoomTaskItem *> *snapshot = self.dataSource.snapshot;
    [snapshot insertItemsWithIdentifiers:@[newTaskItem] beforeItemWithIdentifier:taskItem];
    [snapshot deleteItemsWithIdentifiers:@[taskItem]];
    return snapshot;
}
@end
