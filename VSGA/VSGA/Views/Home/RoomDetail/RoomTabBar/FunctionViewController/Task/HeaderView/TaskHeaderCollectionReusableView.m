//
//  TaskHeaderCollectionReusableView.m
//  VStudy
//
//  Created by LAP14011 on 01/04/2023.
//

#import "TaskHeaderCollectionReusableView.h"
#import "Macro.h"
#import "UIView+Extension.h"

@implementation TaskHeaderCollectionReusableView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialize label
        self.titleLabel = [[CustomLabel alloc] initWithFrame:CGRectZero];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.font = kRegular16;
        [self addSubview:self.titleLabel];
        
        // Initialize disclosure button
        self.disclosureButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [self.disclosureButton addTarget:self action:@selector(handleDisclosureButtonTap) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.disclosureButton];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    // Position label and button
    CGFloat padding = 10.0;
    CGFloat buttonSize = 20.0;
    self.titleLabel.frame = CGRectMake(padding,
                                       0,
                                       self.bounds.size.width - 2 * padding - buttonSize,
                                       self.bounds.size.height);
    self.disclosureButton.frame = CGRectMake(self.bounds.size.width - buttonSize - padding,
                                             (self.bounds.size.height - buttonSize) / 2,
                                             buttonSize,
                                             buttonSize);
}

- (void)handleDisclosureButtonTap {
    if (self.toggleHandler) {
        self.toggleHandler();
    }
}

- (void)setObjectWithTaskSection:(TaskSection *)section {
    switch (section.sectionType) {
        case TaskSectionUndone:
            self.titleLabel.text = @"Undone Tasks";
            break;
        case TaskSectionDone:
            self.titleLabel.text = @"Done Tasks";
            break;
        default:
            break;
    }
    
    UIImage *image;
    if (section.isExpand) {
        image = [UIImage imageNamed:@"ExpandArrow"];
    }
    else {
        image = [UIImage imageNamed:@"CollapseArrow"];
    }
    [self.disclosureButton setImage:image forState:UIControlStateNormal];
}

@end
