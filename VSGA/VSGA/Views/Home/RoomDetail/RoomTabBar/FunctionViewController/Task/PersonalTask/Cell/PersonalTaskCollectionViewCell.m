//
//  PersonalTaskCollectionViewCell.m
//  VStudy
//
//  Created by LAP14011 on 30/03/2023.
//

#import "PersonalTaskCollectionViewCell.h"
#import "Macro.h"
#import "UIView+Extension.h"
#import "NSString+Extension.h"
#import "RoomManager.h"
const CGFloat kTaskCellMargin = 10;
const CGFloat kCircleViewSize = 20;

@implementation PersonalTaskCollectionViewCell

+ (NSString *)cellIdentifier {
    return NSStringFromClass(self.class);
}

#pragma mark - Lazy loadig

- (UIView *)circleView {
    if (_circleView) {
        return _circleView;
    }
    _circleView = [[UIView alloc] init];
    _circleView.layer.cornerRadius = 10;
    _circleView.layer.borderWidth = 1;
    _circleView.layer.borderColor = [UIColor grayColor].CGColor;
    [self.contentView addSubview:_circleView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(circleViewTapped:)];
    [_circleView addGestureRecognizer:tapGesture];
    return _circleView;
}

- (CustomTextView *)descLabel {
    if (_descLabel) {
        return _descLabel;
    }
    _descLabel = [[CustomTextView alloc] init];
    _descLabel.textContainerInset = UIEdgeInsetsMake(5, 5, 5, 5);
    [self.contentView addSubview:_descLabel];
    
    return _descLabel;
}

#pragma mark - PrepareForReuse

- (void)prepareForReuse {
    [super prepareForReuse];
    
    if (_descLabel) {
        _descLabel.hidden = TRUE;
    }
    
    if (_circleView) {
        _circleView.hidden = TRUE;
    }
}

#pragma mark - Layout

- (void)layoutSubviews {
    [super layoutSubviews];
    [self beginLayoutWithItem:self.taskItem];
    
    [self _layoutCircleViewWithItem:self.taskItem];
    [self _layoutDescWithItem:self.taskItem];
    
    [self endLayoutWithItem:self.taskItem];
}

- (void)beginLayoutWithItem:(TaskItem *)item {
    self.layer.cornerRadius = 10.0f;
    self.contentView.layer.borderWidth = 1.0f;
    self.contentView.layer.borderColor = [UIColor clearColor].CGColor;
    self.contentView.layer.masksToBounds = YES;
    
    self.contentView.layoutMargins = UIEdgeInsetsMake(kTaskCellMargin, kTaskCellMargin, kTaskCellMargin, kTaskCellMargin);
    
    self.backgroundColor = UIColor.whiteColor;
}

- (void)endLayoutWithItem:(TaskItem *)item {
    
}

- (void)_layoutCircleViewWithItem:(TaskItem *)item {
    self.circleView.hidden = FALSE;
    CGFloat y = (self.contentView.height - 2*kTaskCellMargin - kCircleViewSize)/2 + kTaskCellMargin;
    
    self.circleView.frame = CGRectMake(kTaskCellMargin,
                                       y,
                                       kCircleViewSize,
                                       kCircleViewSize);
}

- (void)_layoutDescWithItem:(TaskItem *)item {
    self.descLabel.hidden = FALSE;
    // Calculate the height required for the text view
    CGSize containerSize = CGSizeMake(self.contentView.width - self.circleView.width - 5 - 2 * kTaskCellMargin - self.descLabel.textContainerInset.left - self.descLabel.textContainerInset.right, CGFLOAT_MAX);
    CGSize size = [item.desc sizeOfStringWithStyledFont:item.descFont withSize:containerSize];

    // Add the container inset to the size
    size.width += self.descLabel.textContainerInset.left + self.descLabel.textContainerInset.right;
    size.height += self.descLabel.textContainerInset.top + self.descLabel.textContainerInset.bottom;
    
    CGFloat containerHeight = self.contentView.height - 2*kTaskCellMargin;
    CGFloat y = 0;
    CGFloat height = containerHeight;
    if (containerHeight - size.height > 0) {
        y = (containerHeight - size.height)/2;
        height = size.height;
    }
    y += kTaskCellMargin;
    
    self.descLabel.frame = CGRectMake(self.circleView.right + 5,
                                      y,
                                      containerSize.width,
                                      height);
    [self.descLabel placeholderTopLeftPadding:7];
    
    CGSize sizeThatFits = [self.descLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, self.descLabel.frame.size.height)];
    self.descLabel.contentSize = CGSizeMake(sizeThatFits.width, self.descLabel.frame.size.height);
}

#pragma mark - Set content

- (void)setContents {
    [self beginSetContentWithItem:self.taskItem];
    
    [self _setContentCircleViewWithItem:self.taskItem];
    [self _setContentDescWithItem:self.taskItem];
    
    [self endSetContentWithItem:self.taskItem];
}

- (void)beginSetContentWithItem:(TaskItem *)item {
    
}

- (void)endSetContentWithItem:(TaskItem *)item {
    
}

- (void)_setContentCircleViewWithItem:(TaskItem *)item {
    if (item.status == TaskStatusDone) {
        self.circleView.backgroundColor = kGreenColor;
    }
    else {
        self.circleView.backgroundColor = UIColor.clearColor;
    }
}

- (void)_setContentDescWithItem:(TaskItem *)item {
    [self.descLabel setFont:item.descFont textColor:UIColor.blackColor];
    self.descLabel.text = item.desc;
    self.descLabel.delegate = self;
    
    self.descLabel.scrollEnabled = YES;
    self.descLabel.showsHorizontalScrollIndicator = YES;
    self.descLabel.textContainer.maximumNumberOfLines = 1;
    self.descLabel.textContainer.lineBreakMode = NSLineBreakByTruncatingTail;

}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [self.descLabel endEditing:TRUE];
        return NO;
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (self.taskItem.isTempItem) {
        if ([textView.text isEqualToString:@""]) {
            [self.delegate personalTaskRemoveItem:self.taskItem];
            return;
        }
        else {
            NSString *roomCode = [[RoomManager sharedManager] getRoomCode];
            [self.taskItem updateTempItemWithRoomCode:roomCode
                                               scheme:nil
                                                 desc:textView.text];
            [self.delegate personalTaskAddNewItem:self.taskItem];
            return;
        }
    }
    else {
        if ([textView.text isEqualToString:self.taskItem.desc]) {
            return;
        }
        else {
            self.taskItem.desc = textView.text;
            [self.delegate personalTaskUpdateItem:self.taskItem];
            return;
        }
    }
}

- (void)circleViewTapped:(UITapGestureRecognizer *)gestureRecognizer {
    if (self.taskItem.status == TaskStatusDone) {
        return;
    }
    if (self.taskItem.isTempItem) {
        return;
    }
    self.taskItem.status = TaskStatusDone;
    [self.delegate personalTaskUpdateStatusItem:self.taskItem];
}
@end
