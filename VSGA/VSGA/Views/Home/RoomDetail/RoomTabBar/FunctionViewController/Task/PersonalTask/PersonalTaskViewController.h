//
//  PersonalTaskViewController.h
//  VStudy
//
//  Created by LAP14011 on 29/03/2023.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PersonalTaskViewController : UIViewController

- (void)handleAddNewPersonalTaskUI;

@end

NS_ASSUME_NONNULL_END
