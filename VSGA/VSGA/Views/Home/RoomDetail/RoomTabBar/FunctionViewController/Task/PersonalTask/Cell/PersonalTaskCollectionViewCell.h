//
//  PersonalTaskCollectionViewCell.h
//  VStudy
//
//  Created by LAP14011 on 30/03/2023.
//

#import <UIKit/UIKit.h>
#import "TaskItem.h"

#import "CustomTextView.h"
NS_ASSUME_NONNULL_BEGIN
@class PersonalTaskCollectionViewCell;

@protocol PersonalTaskCollectionViewCellDelegate <NSObject>

- (void)personalTaskUpdateItem:(TaskItem *)item;

- (void)personalTaskAddNewItem:(TaskItem *)item;

- (void)personalTaskRemoveItem:(TaskItem *)item;

- (void)personalTaskUpdateStatusItem:(TaskItem *)item;

@end

@interface PersonalTaskCollectionViewCell : UICollectionViewCell <UITextViewDelegate>

@property (nonatomic, weak) id<PersonalTaskCollectionViewCellDelegate> delegate;

@property (strong, nonatomic) TaskItem *taskItem;

@property (nonatomic, strong) UIView *circleView;

@property (strong, nonatomic) CustomTextView *descLabel;

+ (NSString *)cellIdentifier;

// MARK: SetContent

- (void)setContents;

- (void)beginSetContentWithItem:(TaskItem *)item;

- (void)endSetContentWithItem:(TaskItem *)item;

// MARK: Layout

- (void)beginLayoutWithItem:(TaskItem *)item;

- (void)endLayoutWithItem:(TaskItem *)item;

@end

NS_ASSUME_NONNULL_END
