//
//  RoomTaskDataSourceProcessor.h
//  VStudy
//
//  Created by LAP14011 on 02/04/2023.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TSMutableArray.h"
#import "TaskSection.h"
#import "RoomTaskItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface RoomTaskDataSourceProcessor : NSObject

@property (strong, nonatomic) UICollectionViewDiffableDataSource *dataSource;

- (instancetype)initWithDiffDataSource:(UICollectionViewDiffableDataSource *)dataSource;

// Append Section

- (NSDiffableDataSourceSnapshot *)appendSections:(TSMutableArray<TaskSection *> *)sections;

// Reload Section

- (NSDiffableDataSourceSnapshot *)reloadSection:(TaskSection *)section;

// Append Array

- (NSDiffableDataSourceSnapshot *)appendItemArray:(TSMutableArray<RoomTaskItem *> *)taskItemArray
                                      intoSection:(TaskSection *)section;

// Append Item

- (NSDiffableDataSourceSnapshot *)appendItem:(RoomTaskItem *)taskItem
                                 intoSection:(TaskSection *)section;

// Insert Item

- (NSDiffableDataSourceSnapshot *)insertItem:(RoomTaskItem *)taskItem
                                 intoSection:(TaskSection *)section;

// Reload Item

- (NSDiffableDataSourceSnapshot *)reloadItem:(RoomTaskItem *)taskItem;

// Reload Array

- (NSDiffableDataSourceSnapshot *)reloadItemArray:(TSMutableArray<TaskItem *> *)taskItemArray;

// Delete Item

- (NSDiffableDataSourceSnapshot *)deleteItem:(TaskItem *)taskItem;

// Delete Array

- (NSDiffableDataSourceSnapshot *)deleteItemArray:(TSMutableArray<TaskItem *> *)taskItemArray;

// Replace Item

- (NSDiffableDataSourceSnapshot *)replaceItem:(TaskItem *)taskItem withNewItem:(TaskItem *)newTaskItem;

@end

NS_ASSUME_NONNULL_END
