//
//  PersonalTaskDataSourceProcessor.h
//  VStudy
//
//  Created by LAP14011 on 30/03/2023.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TSMutableArray.h"
#import "TaskSection.h"
#import "TaskItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface PersonalTaskDataSourceProcessor : NSObject

@property (strong, nonatomic) UICollectionViewDiffableDataSource *dataSource;

- (instancetype)initWithDiffDataSource:(UICollectionViewDiffableDataSource *)dataSource;

// Append Section

- (NSDiffableDataSourceSnapshot *)appendSections:(TSMutableArray<TaskSection *> *)sections;

// Reload Section

- (NSDiffableDataSourceSnapshot *)reloadSection:(TaskSection *)section;

// Append Array

- (NSDiffableDataSourceSnapshot *)appendItemArray:(TSMutableArray<TaskItem *> *)taskItemArray
                                      intoSection:(TaskSection *)section;

// Append Item

- (NSDiffableDataSourceSnapshot *)appendItem:(TaskItem *)taskItem
                                 intoSection:(TaskSection *)section;

// Insert Item

- (NSDiffableDataSourceSnapshot *)insertItem:(TaskItem *)taskItem
                                 intoSection:(TaskSection *)section;

// Reload Item

- (NSDiffableDataSourceSnapshot *)reloadItem:(TaskItem *)taskItem;

// Reload Array

- (NSDiffableDataSourceSnapshot *)reloadItemArray:(TSMutableArray<TaskItem *> *)taskItemArray;

// Delete Item

- (NSDiffableDataSourceSnapshot *)deleteItem:(TaskItem *)taskItem;

// Delete Array

- (NSDiffableDataSourceSnapshot *)deleteItemArray:(TSMutableArray<TaskItem *> *)taskItemArray;

// Replace Item

- (NSDiffableDataSourceSnapshot *)replaceItem:(TaskItem *)taskItem withNewItem:(TaskItem *)newTaskItem;

@end

NS_ASSUME_NONNULL_END
