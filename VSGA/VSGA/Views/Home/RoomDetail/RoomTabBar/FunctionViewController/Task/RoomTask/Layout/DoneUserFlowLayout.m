//
//  DoneUserFlowLayout.m
//  VStudy
//
//  Created by LAP14011 on 03/04/2023.
//

#import "DoneUserFlowLayout.h"

@implementation DoneUserFlowLayout

- (BOOL)flipsHorizontallyInOppositeLayoutDirection {
    return TRUE;
}

- (UIUserInterfaceLayoutDirection)developmentLayoutDirection {
    return UIUserInterfaceLayoutDirectionRightToLeft;
}

@end
