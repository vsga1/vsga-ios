//
//  PersonalTaskViewController.m
//  VStudy
//
//  Created by LAP14011 on 29/03/2023.
//

#import "PersonalTaskViewController.h"
#import <Masonry/Masonry.h>
#import "Macro.h"
#import "TaskHandler.h"
#import "RoomManager.h"
#import "TSMutableArray.h"
#import "TaskSection.h"
#import "TaskItem.h"

#import "PersonalTaskDataSourceProcessor.h"
#import "PersonalTaskCollectionViewCell.h"
#import "TaskHeaderCollectionReusableView.h"

#import "UIView+Extension.h"
static CGSize kTaskTargetSize;

@interface PersonalTaskViewController () <UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, PersonalTaskCollectionViewCellDelegate>

@property (nonatomic, strong) UICollectionView *taskCollectionView;

@property (nonatomic, strong) UICollectionViewDiffableDataSource<TaskSection *, TaskItem *> *dataSource;

@property (nonatomic, strong) PersonalTaskDataSourceProcessor *dataSourceProcessor;

@property (nonatomic, strong) TSMutableArray<TaskItem *> *undoneTaskList;

@property (nonatomic, strong) TSMutableArray<TaskItem *> *doneTaskList;

@property (nonatomic, strong) TSMutableArray<TaskSection *> *sectionList;

@property (nonatomic, strong) TaskHandler *taskHandler;

@end

@implementation PersonalTaskViewController {
    TaskSection *_doneTaskSection;
    TaskSection *_undoneTaskSection;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViews];
    [self setupValues];
}

#pragma mark - Setup Views

- (void)setupViews {
    self.view.backgroundColor = kBackgroundColor;
    [self setupTaskCollectionView];
}

- (void)setupTaskCollectionView {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.headerReferenceSize = CGSizeMake(self.view.width, 50);
    layout.sectionHeadersPinToVisibleBounds = TRUE;
    
    self.taskCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                                     collectionViewLayout:layout];
    
    self.taskCollectionView.dataSource = self.dataSource;
    self.taskCollectionView.delegate = self;
    
    [self.taskCollectionView registerClass:[PersonalTaskCollectionViewCell class] forCellWithReuseIdentifier:PersonalTaskCollectionViewCell.cellIdentifier];
    [self.taskCollectionView registerClass:[TaskHeaderCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Header"];
    
    [self.view addSubview:self.taskCollectionView];
    
    [self.taskCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop).with.offset(50);
        make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).with.offset(30);
        make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).with.offset(-30);
        make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).with.offset(-30);
    }];
    
    self.taskCollectionView.backgroundColor = kBackgroundColor;
}

- (void)toggleSection:(TaskSection *)section {
    section.isExpand = !section.isExpand;
    
    NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor reloadSection:section];
    [self applySnapshot:snapshot animation:NO completion:nil];
    
    TSMutableArray *itemArray = nil;
    switch (section.sectionType) {
        case TaskSectionDone:
            itemArray = self.doneTaskList;
            break;
        case TaskSectionUndone:
            itemArray= self.undoneTaskList;
            break;
        default:
            break;
    }
    if (!section.isExpand) {
        snapshot = [self.dataSourceProcessor deleteItemArray:itemArray];
    }
    else {
        snapshot = [self.dataSourceProcessor appendItemArray:itemArray intoSection:section];
    }
    [self applySnapshot:snapshot animation:YES completion:nil];
}

- (void)toggleSectionAtIndexPath:(NSIndexPath *)indexPath {
    TaskSection *section = [self.sectionList objectAtIndex:indexPath.section];
    [self toggleSection:section];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    kTaskTargetSize = CGSizeMake(self.taskCollectionView.width, 55);
}


#pragma mark - CollectionView

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return kTaskTargetSize;
}

#pragma mark - Description Text View

- (void)personalTaskUpdateItem:(TaskItem *)item {
    //Call api
    __weak typeof(self) weakSelf = self;
    [self.taskHandler updateTask:item completionHandler:^(NSError * _Nullable error) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (error) {
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *snapshot = [strongSelf.dataSourceProcessor reloadItem:item];
            [strongSelf applySnapshot:snapshot animation:NO completion:nil];
        });
    }];
}

- (void)personalTaskAddNewItem:(TaskItem *)item {
    //Call api
    __weak typeof(self) weakSelf = self;
    [self.taskHandler addNewTask:item completionHandler:^(TaskItem * _Nullable newItem, NSError * _Nullable error) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (error) {
            return;
        }
        if (newItem && item) {
            [newItem setupDisplayInView];
            NSUInteger index = [strongSelf.undoneTaskList indexOfObject:item];
            [strongSelf.undoneTaskList replaceObjectAtIndex:index withObject:newItem];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDiffableDataSourceSnapshot *snapshot = [strongSelf.dataSourceProcessor replaceItem:item withNewItem:newItem];
                [strongSelf applySnapshot:snapshot animation:NO completion:nil];
            });
        }
    }];
}

- (void)personalTaskRemoveItem:(TaskItem *)item {
    [self.undoneTaskList removeObject:item];
    NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor deleteItem:item];
    [self applySnapshot:snapshot animation:YES completion:nil];
}

- (void)personalTaskUpdateStatusItem:(TaskItem *)item {
    [self.taskHandler updateTask:item completionHandler:^(NSError * _Nullable error) {\
        if (error) {
            return;
        }
        
        [self.undoneTaskList removeObject:item];
        [self.doneTaskList insertObject:item atIndex:0];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor deleteItem:item];
            [self applySnapshot:snapshot animation:YES completion:^{
                if (!self->_doneTaskSection.isExpand) {
                    return;
                }
                NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor insertItem:item intoSection:self->_doneTaskSection];
                [self applySnapshot:snapshot animation:YES completion:nil];
            }];
        });
    }];
    
}
#pragma mark - Setup Values

- (void)setupValues {
    [self setupSections];
    [self setupItems];
}

- (void)setupSections {
    self.sectionList = [[TSMutableArray alloc] init];
    
    _undoneTaskSection = [[TaskSection alloc] initWithSection:TaskSectionUndone isExpand:YES];
    _doneTaskSection = [[TaskSection alloc] initWithSection:TaskSectionDone isExpand:NO];
    
    [self.sectionList addObject:_undoneTaskSection];
    [self.sectionList addObject:_doneTaskSection];
    
    NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor appendSections:self.sectionList];
    [self applySnapshot:snapshot animation:NO completion:nil];
}

- (void)setupItems {
    self.undoneTaskList = [[TSMutableArray alloc] init];
    self.doneTaskList = [[TSMutableArray alloc] init];
   
    [self _getUndoneTaskItemArray];
    [self _getDoneTaskItemArray];
}

- (void)_getUndoneTaskItemArray {
    NSString *roomCode = [[RoomManager sharedManager] getRoomCode];
    [self.taskHandler getTaskWithRoomCode:roomCode
                                     type:TaskTypePersonal
                                   status:TaskStatusUndone
                        completionHandler:^(NSArray<TaskItem *> * _Nullable taskItemArray, NSError * _Nullable error) {
        if (error) {
            return;
        }
        [self _updateTaskInfo:taskItemArray];
        taskItemArray = [[taskItemArray reverseObjectEnumerator] allObjects];
        [self.undoneTaskList addObjectsFromArray:taskItemArray];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor appendItemArray:self.undoneTaskList intoSection:self->_undoneTaskSection];
            [self applySnapshot:snapshot animation:NO completion:nil];
        });
    }];
}

- (void)_getDoneTaskItemArray {
    NSString *roomCode = [[RoomManager sharedManager] getRoomCode];
    [self.taskHandler getTaskWithRoomCode:roomCode
                                     type:TaskTypePersonal
                                   status:TaskStatusDone
                        completionHandler:^(NSArray<TaskItem *> * _Nullable taskItemArray, NSError * _Nullable error) {
        if (error) {
            return;
        }
        [self _updateTaskInfo:taskItemArray];
        taskItemArray = [[taskItemArray reverseObjectEnumerator] allObjects];
        [self.doneTaskList addObjectsFromArray:taskItemArray];
        
    }];
}

- (void)_updateTaskInfo:(NSArray<TaskItem *> *)taskItemArray {
    for (NSInteger i = 0; i < taskItemArray.count; i++) {
        TaskItem *item = [taskItemArray objectAtIndex:i];
        [item setupDisplayInView];
    }
}

#pragma mark - NSDiffableDataSourceSnapshot

- (void)applySnapshot:(NSDiffableDataSourceSnapshot *)snapshot
            animation:(BOOL)animation
           completion:(void (^)(void))completion {
    [self.dataSource applySnapshot:snapshot animatingDifferences:animation completion:completion];
}

#pragma mark - Add new task

- (void)handleAddNewPersonalTaskUI {
    if (!_undoneTaskSection.isExpand) {
        [self toggleSection:_undoneTaskSection];
    }
    
    TaskItem *newItem = [[TaskItem alloc] initTempItemWithType:TaskTypePersonal];
    [self.undoneTaskList insertObject:newItem atIndex:0];
    [newItem setupDisplayInView];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor insertItem:newItem intoSection:self->_undoneTaskSection];
        [self applySnapshot:snapshot animation:YES completion:^{
            NSIndexPath *newItemIndexPath = [NSIndexPath indexPathForItem:0 inSection:TaskSectionUndone];
            
            [self.taskCollectionView scrollToItemAtIndexPath:newItemIndexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
            PersonalTaskCollectionViewCell *cell = (PersonalTaskCollectionViewCell *)[self.taskCollectionView cellForItemAtIndexPath:newItemIndexPath];
            [cell.descLabel becomeFirstResponder];
        }];
    });
}

#pragma mark - Lazy Loading

- (UICollectionViewDiffableDataSource<TaskSection *,TaskItem *> *)dataSource {
    if (_dataSource) {
        return _dataSource;
    }
    __weak typeof(self) weakSelf1 = self;
    _dataSource = [[UICollectionViewDiffableDataSource alloc] initWithCollectionView:self.taskCollectionView cellProvider:^UICollectionViewCell * _Nullable(UICollectionView * _Nonnull collectionView, NSIndexPath * _Nonnull indexPath, TaskItem *taskItem) {
        __strong typeof(weakSelf1) strongSelf = weakSelf1;
        if (taskItem) {
            PersonalTaskCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[[PersonalTaskCollectionViewCell class] cellIdentifier] forIndexPath:indexPath];
            cell.taskItem = taskItem;
            cell.delegate = strongSelf;
            [cell setContents];
            return cell;
        }
        return [[PersonalTaskCollectionViewCell alloc] init];
    }];
    __weak typeof(self) weakSelf2 = self;
    _dataSource.supplementaryViewProvider = ^UICollectionReusableView *(UICollectionView *collectionView, NSString *elementKind, NSIndexPath *indexPath) {
        __strong typeof(weakSelf2) strongSelf = weakSelf2;
        if ([elementKind isEqualToString:UICollectionElementKindSectionHeader]) {
            TaskSection *section = [strongSelf.sectionList objectAtIndex:indexPath.section];
            TaskHeaderCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:elementKind withReuseIdentifier:@"Header" forIndexPath:indexPath];
            [headerView setObjectWithTaskSection:section];
            headerView.toggleHandler = ^{
                [strongSelf toggleSectionAtIndexPath:indexPath];
            };
            return headerView;
        } else {
            return nil;
        }
        
    };
    return _dataSource;
}

- (PersonalTaskDataSourceProcessor *)dataSourceProcessor {
    if (_dataSourceProcessor) {
        return _dataSourceProcessor;
    }
    _dataSourceProcessor = [[PersonalTaskDataSourceProcessor alloc] initWithDiffDataSource:self.dataSource];
    return _dataSourceProcessor;
}

- (TaskHandler *)taskHandler {
    if (_taskHandler) {
        return _taskHandler;
    }
    _taskHandler = [[TaskHandler alloc] init];
    return _taskHandler;
}

@end
