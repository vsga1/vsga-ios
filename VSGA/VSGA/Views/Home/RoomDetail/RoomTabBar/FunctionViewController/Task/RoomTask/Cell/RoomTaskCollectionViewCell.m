//
//  RoomTaskCollectionViewCell.m
//  VStudy
//
//  Created by LAP14011 on 02/04/2023.
//

#import "RoomTaskCollectionViewCell.h"
#import "Macro.h"
#import "UIView+Extension.h"
#import "NSString+Extension.h"
#import "RoomManager.h"
#import "DoneUserFlowLayout.h"

const CGFloat kRoomTaskCellMargin = 10;
const CGFloat kRoomCircleViewSize = 20;
static CGFloat kContentMargin = 8;

const CGFloat kViewSpacing = 5;
const CGFloat kUpperViewHeight = 35;
const CGFloat kLowerViewHeight = 40;
const CGFloat kMinDescHeight = 33;


@interface RoomTaskCollectionViewCell() <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@end

@implementation RoomTaskCollectionViewCell
+ (NSString *)cellIdentifier {
    return NSStringFromClass(self.class);
}

#pragma mark - Lazy loadig

- (UIView *)upperView {
    if (_upperView) {
        return _upperView;
    }
    _upperView = [[UIView alloc] initWithFrame:CGRectMake(kRoomTaskCellMargin,
                                                          kRoomTaskCellMargin,
                                                          self.width - 2 * kRoomTaskCellMargin,
                                                          kUpperViewHeight)];
    [self.contentView addSubview:_upperView];
    return _upperView;
}

- (UIView *)circleView {
    if (_circleView) {
        return _circleView;
    }
    _circleView = [[UIView alloc] init];
    _circleView.layer.cornerRadius = 10;
    _circleView.layer.borderWidth = 1;
    _circleView.layer.borderColor = [UIColor grayColor].CGColor;
    [_upperView addSubview:_circleView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(circleViewTapped:)];
    [_circleView addGestureRecognizer:tapGesture];
    return _circleView;
}

- (CustomTextView *)descLabel {
    if (_descLabel) {
        return _descLabel;
    }
    _descLabel = [[CustomTextView alloc] init];
    _descLabel.textContainerInset = UIEdgeInsetsMake(5, 5, 5, 5);
    [_upperView addSubview:_descLabel];
    
    return _descLabel;
}

- (UIView *)lowerView{
    if (_lowerView) {
        return _lowerView;
    }
    _lowerView = [[UIView alloc] initWithFrame:CGRectMake(kRoomTaskCellMargin,
                                                          _upperView.bottom + kViewSpacing,
                                                          self.width - 2 * kRoomTaskCellMargin,
                                                          kLowerViewHeight)];
    [self.contentView addSubview:_lowerView];
    return _lowerView;
}

- (UICollectionView *)avatarCollectionView {
    if (_avatarCollectionView) {
        return _avatarCollectionView;
    }
    DoneUserFlowLayout *layout = [[DoneUserFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    CGRect frame = CGRectMake(0,
                              0,
                              _lowerView.width,
                              _lowerView.height);
    _avatarCollectionView = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:layout];
    _avatarCollectionView.backgroundColor = [UIColor clearColor];
    _avatarCollectionView.hidden = TRUE;
    
    [_avatarCollectionView registerClass:[UICollectionViewCell class]
             forCellWithReuseIdentifier:@"AvatarCollectionViewCell"];
    [_lowerView addSubview:_avatarCollectionView];
    return _avatarCollectionView;
}

#pragma mark - PrepareForReuse

- (void)prepareForReuse {
    [super prepareForReuse];
    
    if (_upperView) {
        _upperView.hidden = TRUE;
    }
    
    if (_descLabel) {
        _descLabel.hidden = TRUE;
    }
    
    if (_circleView) {
        _circleView.hidden = TRUE;
    }
    
    if (_lowerView) {
        _lowerView.hidden = TRUE;
    }
    
    if (_avatarCollectionView) {
        _avatarCollectionView.hidden = TRUE;
    }
}

#pragma mark - Layout

- (void)layoutSubviews {
    [super layoutSubviews];
    [self beginLayoutWithItem:self.taskItem];
    
    [self _layoutCircleViewWithItem:self.taskItem];
    [self _layoutDescWithItem:self.taskItem];
    
    [self endLayoutWithItem:self.taskItem];
}

- (void)beginLayoutWithItem:(RoomTaskItem *)item {
    self.layer.cornerRadius = 10.0f;
    self.contentView.layer.borderWidth = 1.0f;
    self.contentView.layer.borderColor = [UIColor clearColor].CGColor;
    self.contentView.layer.masksToBounds = YES;
    
    self.contentView.layoutMargins = UIEdgeInsetsMake(kRoomTaskCellMargin, kRoomTaskCellMargin, kRoomTaskCellMargin, kRoomTaskCellMargin);
    
    self.backgroundColor = UIColor.whiteColor;
}

- (void)endLayoutWithItem:(RoomTaskItem *)item {
    
}

- (void)_layoutCircleViewWithItem:(RoomTaskItem *)item {
    self.circleView.hidden = FALSE;
    CGFloat y = (self.upperView.height - kRoomCircleViewSize)/2;
    
    self.circleView.frame = CGRectMake(0,
                                       y,
                                       kRoomCircleViewSize,
                                       kRoomCircleViewSize);
}

- (void)_layoutDescWithItem:(RoomTaskItem *)item {
    self.descLabel.hidden = FALSE;
    
    // Calculate the height required for the text view
    CGSize containerSize = CGSizeMake(self.upperView.width - self.circleView.width - 5 - self.descLabel.textContainerInset.left - self.descLabel.textContainerInset.right, CGFLOAT_MAX);
    CGSize size = [item.desc sizeOfStringWithStyledFont:item.descFont withSize:containerSize];

    // Add the container inset to the size
    size.width += self.descLabel.textContainerInset.left + self.descLabel.textContainerInset.right;
    size.height += self.descLabel.textContainerInset.top + self.descLabel.textContainerInset.bottom;
    
    CGFloat height = size.height;
    CGFloat y = (self.upperView.height - size.height)/2;
    if (y < 0) {
        height = kMinDescHeight;
        y = (self.upperView.height - kMinDescHeight)/2;
    }
    
    self.descLabel.frame = CGRectMake(self.circleView.right + 5,
                                      y,
                                      containerSize.width,
                                      height);
}

#pragma mark - Set content

- (void)setContents {
    [self beginSetContentWithItem:self.taskItem];
    
    self.upperView.hidden = FALSE;
    
    [self _setContentCircleViewWithItem:self.taskItem];
    [self _setContentDescWithItem:self.taskItem];
    
    if (self.taskItem.doneUserArray.count > 0) {
        self.lowerView.hidden = FALSE;
        self.avatarCollectionView.delegate = self;
        self.avatarCollectionView.dataSource = self;
        self.avatarCollectionView.hidden = FALSE;
    }
   
    
    [self endSetContentWithItem:self.taskItem];
}

- (void)beginSetContentWithItem:(RoomTaskItem *)item {
    
}

- (void)endSetContentWithItem:(RoomTaskItem *)item {
    
}

- (void)_setContentCircleViewWithItem:(RoomTaskItem *)item {
    if (item.status == TaskStatusDone) {
        self.circleView.backgroundColor = kGreenColor;
    }
    else {
        self.circleView.backgroundColor = UIColor.clearColor;
    }
}

- (void)_setContentDescWithItem:(RoomTaskItem *)item {
    [self.descLabel setFont:item.descFont textColor:UIColor.blackColor];
    self.descLabel.text = item.desc;
    self.descLabel.delegate = self;
    
    self.descLabel.scrollEnabled = YES;
    self.descLabel.showsHorizontalScrollIndicator = YES;
    self.descLabel.textContainer.maximumNumberOfLines = 1;
    self.descLabel.textContainer.lineBreakMode = NSLineBreakByTruncatingTail;
    
    self.descLabel.editable = FALSE;

}

#pragma mark - Topic Collection View delegate & datasource
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 3;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.taskItem.doneUserArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AvatarCollectionViewCell" forIndexPath:indexPath];
//    User *user = [self.taskItem.doneUserArray objectAtIndex:indexPath.item];
    
    UIImageView *avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    avatarImageView.layer.cornerRadius = avatarImageView.frame.size.height / 2;
    avatarImageView.layer.borderColor = UIColor.grayColor.CGColor;
    avatarImageView.layer.borderWidth = 0.5f;
    avatarImageView.layer.masksToBounds = YES;
    // Construct the name of the image
    NSString *imageName = [NSString stringWithFormat:@"Avatar%ld", indexPath.item % 4];
    avatarImageView.image = [UIImage imageNamed:imageName];
    
    [cell.contentView addSubview:avatarImageView];

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(30, 30);
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [self.descLabel endEditing:TRUE];
        return NO;
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (self.taskItem.isTempItem) {
        if ([textView.text isEqualToString:@""]) {
//            [self.delegate personalTaskRemoveItem:self.taskItem];
            return;
        }
        else {
            NSString *roomCode = [[RoomManager sharedManager] getRoomCode];
            [self.taskItem updateTempItemWithRoomCode:roomCode
                                               scheme:nil
                                                 desc:textView.text];
//            [self.delegate personalTaskAddNewItem:self.taskItem];
            return;
        }
    }
    else {
        if ([textView.text isEqualToString:self.taskItem.desc]) {
            return;
        }
        else {
            self.taskItem.desc = textView.text;
//            [self.delegate personalTaskUpdateItem:self.taskItem];
            return;
        }
    }
}

- (void)circleViewTapped:(UITapGestureRecognizer *)gestureRecognizer {
    if (self.taskItem.status == TaskStatusDone) {
        return;
    }
    if (self.taskItem.isTempItem) {
        return;
    }
    self.taskItem.status = TaskStatusDone;
    [self.delegate roomTaskUpdateStatusItem:self.taskItem];
}
@end
