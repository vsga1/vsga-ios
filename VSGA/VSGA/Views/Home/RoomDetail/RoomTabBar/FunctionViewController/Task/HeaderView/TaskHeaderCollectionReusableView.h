//
//  TaskHeaderCollectionReusableView.h
//  VStudy
//
//  Created by LAP14011 on 01/04/2023.
//

#import <UIKit/UIKit.h>
#import "TaskSection.h"
#import "CustomLabel.h"

NS_ASSUME_NONNULL_BEGIN
@class TaskHeaderCollectionReusableView;

@interface TaskHeaderCollectionReusableView : UICollectionReusableView

@property (nonatomic, strong) CustomLabel *titleLabel;

@property (nonatomic, strong) UIButton *disclosureButton;

@property (nonatomic, copy) void (^toggleHandler)(void);

- (void)setObjectWithTaskSection:(TaskSection *)section;

@end

NS_ASSUME_NONNULL_END
