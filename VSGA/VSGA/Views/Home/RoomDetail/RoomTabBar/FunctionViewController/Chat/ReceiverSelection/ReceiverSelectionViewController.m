//
//  ReceiverSelectionViewController.m
//  VStudy
//
//  Created by LAP14011 on 08/04/2023.
//

#import "ReceiverSelectionViewController.h"
#import "Macro.h"
#import "UIView+Extension.h"
#import "CustomLabel.h"
@interface ReceiverSelectionViewController () <UITableViewDataSource, UITableViewDelegate>



@end

@implementation ReceiverSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Initialize the table view and set up its constraints
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ReceiverCell"];
    [self.view addSubview:self.tableView];
    
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [NSLayoutConstraint activateConstraints:@[
        [self.tableView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor],
        [self.tableView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor],
        [self.tableView.topAnchor constraintEqualToAnchor:self.view.topAnchor],
        [self.tableView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor]
    ]];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 46;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.userArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReceiverCell" forIndexPath:indexPath];
    
    User *user = [self.userArray objectAtIndex:indexPath.item];
    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(15, 5, self.view.width - 30, 36)];
    [cell.contentView addSubview:containerView];
    
    containerView.layer.cornerRadius = 10.0f;
    containerView.layer.borderWidth = 1.0f;
    containerView.layer.borderColor = [UIColor grayColor].CGColor;
    containerView.layer.masksToBounds = YES;
    
    UIImageView *receiverAvatar = [[UIImageView alloc] initWithFrame:CGRectMake(kReceiverContainerPadding, 5, 26, 26)];
    receiverAvatar.layer.cornerRadius = 26 / 2;
    receiverAvatar.layer.borderColor = UIColor.grayColor.CGColor;
    receiverAvatar.layer.borderWidth = 0.5f;
    receiverAvatar.layer.masksToBounds = YES;
    
    if (user.userType == UserTypeFakeEveryone) {
        receiverAvatar.hidden = TRUE;
    }
    [containerView addSubview:receiverAvatar];
    
    CustomLabel *label = [[CustomLabel alloc] initWithFrame:CGRectMake(receiverAvatar.right + kReceiverContainerSpacing, 5, 200, 26)];
    [containerView addSubview:label];
    label.text = user.username;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    User *user = self.userArray[indexPath.row];
    [self.delegate didSelectReceiver:user];
}

@end
