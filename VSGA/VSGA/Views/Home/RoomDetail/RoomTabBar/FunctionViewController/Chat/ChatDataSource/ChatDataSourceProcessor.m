//
//  ChatDataSourceProcessor.m
//  VStudy
//
//  Created by LAP14011 on 05/03/2023.
//

#import "ChatDataSourceProcessor.h"

@implementation ChatDataSourceProcessor

- (instancetype)initWithDiffDataSource:(UICollectionViewDiffableDataSource *)dataSource {
    self = [super init];
    if (self) {
        self.dataSource = dataSource;
    }
    return self;
}

- (NSDiffableDataSourceSnapshot *)appendItemArray:(NSArray<MessageItem *> *)messageItemArray {
    NSDiffableDataSourceSnapshot<Section *, MessageItem *> *snapshot = [[NSDiffableDataSourceSnapshot alloc] init];
    NSArray <Section *> *sections = self.dataSource.snapshot.sectionIdentifiers;
    if (sections.count == 0) {
        Section *section = [[Section alloc] initWithSection:SectionMain];
        sections = [[NSArray alloc] initWithObjects:section, nil];
    }
    [snapshot appendSectionsWithIdentifiers:sections];
    if (messageItemArray.count > 0) {
        [snapshot appendItemsWithIdentifiers:messageItemArray intoSectionWithIdentifier:sections[SectionMain]];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)appendItem:(MessageItem *)messageItem {
    NSDiffableDataSourceSnapshot<Section *, MessageItem *> *snapshot = self.dataSource.snapshot;
    [snapshot appendItemsWithIdentifiers:@[messageItem]];
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)reloadItem:(MessageItem *)messageItem {
    NSDiffableDataSourceSnapshot<Section *, MessageItem *> *snapshot = self.dataSource.snapshot;
    [snapshot reloadItemsWithIdentifiers:@[messageItem]];
//    NSLog(@"reloaded item %@",snapshot.reloadedItemIdentifiers);
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)reloadItemArray:(NSArray<MessageItem *> *)messageItemArray {
    NSDiffableDataSourceSnapshot<Section *, MessageItem *> *snapshot = self.dataSource.snapshot;
    [snapshot reloadItemsWithIdentifiers:messageItemArray];
//    NSLog(@"reloaded item array %@",snapshot.reloadedItemIdentifiers);
    return snapshot;
}

@end
