//
//  ReceiverSelectionViewController.h
//  VStudy
//
//  Created by LAP14011 on 08/04/2023.
//

#import <UIKit/UIKit.h>
#import "User.h"
NS_ASSUME_NONNULL_BEGIN

@protocol ReceiverSelectionDelegate <NSObject>

- (void)didSelectReceiver:(User *)user;

@end

@interface ReceiverSelectionViewController : UIViewController

@property (nonatomic, weak) id<ReceiverSelectionDelegate> delegate;

@property (nonatomic, strong) NSArray *userArray;

@property (nonatomic, strong) UITableView *tableView;
@end

NS_ASSUME_NONNULL_END
