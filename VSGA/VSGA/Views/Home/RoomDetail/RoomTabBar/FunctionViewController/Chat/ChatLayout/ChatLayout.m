//
//  ChatLayout.m
//  VStudy
//
//  Created by LAP14011 on 04/03/2023.
//

#import "ChatLayout.h"

@implementation ChatLayout

- (NSArray<__kindof UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray *original = [super layoutAttributesForElementsInRect:rect];
    NSArray *attributesInRect = [[NSArray alloc] initWithArray:original copyItems:YES];
    for (UICollectionViewLayoutAttributes *cellAttributes in attributesInRect) {
        [self modifyLayoutAttributes:cellAttributes];
    }
    return attributesInRect;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewLayoutAttributes *attributes = [[super layoutAttributesForItemAtIndexPath:indexPath] copy];
    [self modifyLayoutAttributes:attributes];
    return attributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewLayoutAttributes *attributes = [[super layoutAttributesForDecorationViewOfKind:elementKind atIndexPath:indexPath] copy];
    [self modifyLayoutAttributes:attributes];
    return attributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewLayoutAttributes *attributes = [[super layoutAttributesForDecorationViewOfKind: elementKind atIndexPath:indexPath] copy];
    [self modifyLayoutAttributes:attributes];
    return attributes;
}

- (void)modifyLayoutAttributes:(UICollectionViewLayoutAttributes *)attributes {
    attributes.transform = CGAffineTransformMakeScale(1.0, -1.0);
}

@end
