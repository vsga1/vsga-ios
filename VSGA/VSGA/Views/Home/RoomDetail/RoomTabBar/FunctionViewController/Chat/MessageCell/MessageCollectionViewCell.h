//
//  MessageCollectionViewCell.h
//  VStudy
//
//  Created by LAP14011 on 05/03/2023.
//

#import <UIKit/UIKit.h>

#import "MessageItem.h"
#import "TimestampView.h"
#import "CustomLabel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MessageCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) MessageItem *messageItem;

@property (strong, nonatomic) UIImageView *avatarImageView;

@property (strong, nonatomic) UIView *messageContentView;

@property (strong, nonatomic) CustomLabel *nameLabel;

@property (strong, nonatomic) CustomLabel *messageTextLabel;

@property (strong, nonatomic) TimestampView *timestampView;

+ (NSString *)cellIdentifier;

// MARK: SetContent

- (void)setContents;

- (void)beginSetContentWithItem:(MessageItem *)item;

- (void)endSetContentWithItem:(MessageItem *)item;

// MARK: Layout

- (void)beginLayoutWithItem:(MessageItem *)item;

- (void)endLayoutWithItem:(MessageItem *)item;

@end

NS_ASSUME_NONNULL_END
