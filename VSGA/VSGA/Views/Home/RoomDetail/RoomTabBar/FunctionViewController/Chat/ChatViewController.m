//
//  ChatViewController.m
//  VStudy
//
//  Created by LAP14011 on 04/03/2023.
//

#import <Masonry/Masonry.h>
#import "ChatViewController.h"

#import "Macro.h"

#import "TSMutableArray.h"

#import "UIView+Extension.h"

#import "CustomLabel.h"
#import "CustomTextView.h"
#import "ImageViewContainer.h"

#import "ChatDataSourceProcessor.h"
#import "ChatLayout.h"
#import "MessageCollectionViewCell.h"

#import "Section.h"
#import "MessageItem.h"

#import "AccountManager.h"
#import "MySocketManager.h"
#import "ChatManager.h"
#import "RoomManager.h"

#import "ReceiverSelectionViewController.h"

static CGSize kTargetSize;
static CGFloat kMaxBubbleCellWidth;
static CGFloat kKeyboardHeight = 0;
const CGFloat kReceiverHeight = 36;
const CGFloat kReceiverSelectionHeight = 150;

@interface ChatViewController () <UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UITextViewDelegate, ReceiverSelectionDelegate>

/// Message Collection View

@property (nonatomic, strong) UICollectionView *messageCollectionView;

@property (nonatomic, strong) ChatLayout *messageCollectionViewLayout;

@property (nonatomic, strong) UICollectionViewDiffableDataSource<Section *, MessageItem *> *messageDataSource;

@property (nonatomic, strong) ChatDataSourceProcessor *messageDataSourceProcessor;

/// Chat Bar View

@property (nonatomic, strong) UIView *chatBarContainerView;

@property (nonatomic, strong) UIView *receiverContainerView;

@property (nonatomic, strong) CustomLabel *receiverLabel;

@property (nonatomic, strong) UIButton *receiverButton;

@property (nonatomic, strong) UIImageView *receiverAvatar;

@property (nonatomic, strong) UIView *inputContainerView;

@property (nonatomic, strong) CustomTextView *chatBarTextView;

@property (nonatomic, strong) ImageViewContainer *sendIconView;

@property (nonatomic, assign) CGFloat currentTextViewHeight;

@property (nonatomic, assign) CGFloat maxTextViewHeight;

@property (nonatomic, assign) BOOL hasShownScrollBar;

@property (nonatomic, assign) BOOL didExpand;

/// Receiver View Controller

@property (nonatomic, strong) ReceiverSelectionViewController *receiverSelectionViewController;

/// Chat Manager

@property (nonatomic, strong) ChatManager *chatManager;

@end

@implementation ChatViewController {
    User *_selectedReceiver;
    User *_fakeReceiver;
}

#pragma mark - Life Cycle

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = kBackgroundColor;
    
    [self setupViews];
    [self setupValues];
    [self registerKeyboardNotifications];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupSocketListener];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    
    [[MySocketManager sharedInstance] off:kReceiveMessageEvent];
    [[MySocketManager sharedInstance] off:kSendMessageEvent];
}

#pragma mark - Setup Views

- (void)setupViews {
    [self setupNavigationBar];
    [self setupChatBarContainer];
    [self setupReceiverSelectionView];
    [self setupMessageCollectionView];
}

#pragma mark - Setup Navigation Bar

- (void)setupNavigationBar {
    self.navigationItem.title = @"Chat";
    [self createBackButton];
}

- (void)createBackButton {
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] init];
    self.navigationController.navigationBar.topItem.backBarButtonItem = backButton;
    self.navigationItem.leftItemsSupplementBackButton = true;
}

#pragma mark - Setup Chat Bar Container

- (void)setupChatBarContainer {
    /// Container view
    self.chatBarContainerView = [[UIView alloc] init];
    self.chatBarContainerView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:self.chatBarContainerView];
    
    [self.chatBarContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view.mas_bottom);
        make.trailing.equalTo(self.view.mas_trailing);
        make.leading.equalTo(self.view.mas_leading);
    }];
    
    kKeyboardHeight = 0;
    
    //set up receiver view
    [self setupReceiverContainerView];
    //set up input view
    [self setupInputContainerView];
}

#pragma mark - Setup Receiver Container View

- (void)setupReceiverContainerView {
    self.receiverContainerView = [[UIView alloc] init];
    self.receiverContainerView.backgroundColor = UIColor.whiteColor;
    [self.chatBarContainerView addSubview:self.receiverContainerView];
    
    [self.receiverContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.chatBarContainerView.mas_top).with.offset(kChatBarContainerPadding);
        make.trailing.equalTo(self.chatBarContainerView.mas_trailing).with.offset(-kChatBarContainerPadding);
        make.leading.equalTo(self.chatBarContainerView.mas_leading).with.offset(kChatBarContainerPadding);
        make.height.mas_equalTo(kReceiverHeight);
    }];
    
    self.receiverContainerView.layer.cornerRadius = 10.0f;
    self.receiverContainerView.layer.borderWidth = 1.0f;
    self.receiverContainerView.layer.borderColor = [UIColor grayColor].CGColor;
    self.receiverContainerView.layer.masksToBounds = YES;
    
    [self setupReceiverAvatar];
    [self setupReceiverButton];
    [self setupReceiverLabel];
}

- (void)setupReceiverAvatar {
    self.receiverAvatar = [[UIImageView alloc] init];
    self.receiverAvatar.layer.cornerRadius = 26 / 2;
    self.receiverAvatar.layer.borderColor = UIColor.grayColor.CGColor;
    self.receiverAvatar.layer.borderWidth = 0.5f;
    self.receiverAvatar.layer.masksToBounds = YES;
    
    self.receiverAvatar.hidden = TRUE;
    
    [self.receiverContainerView addSubview:self.receiverAvatar];
    
    [self.receiverAvatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.receiverContainerView.mas_centerY);
        make.leading.equalTo(self.receiverContainerView.mas_leading).with.offset(kReceiverContainerPadding);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(26);
    }];
}

- (void)setupReceiverLabel {
    self.receiverLabel = [[CustomLabel alloc] init];
    [self.receiverContainerView addSubview:self.receiverLabel];
    [self.receiverLabel setTitle:@"everyone" titleFont:kRegular16 titleColor:UIColor.blackColor];
    
    [self.receiverContainerView addSubview:self.receiverLabel];
    
    [self.receiverLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.receiverContainerView.mas_top);
        make.trailing.equalTo(self.receiverButton.mas_leading);
        make.leading.equalTo(self.receiverAvatar.mas_trailing).with.offset(kReceiverContainerSpacing);
        make.height.mas_equalTo(kReceiverHeight);
    }];
    
}

- (void)setupReceiverButton {
    self.receiverButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.receiverButton addTarget:self action:@selector(handleReceiverButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.receiverContainerView addSubview:self.receiverButton];
    
    [self.receiverButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.receiverContainerView.mas_centerY);
        make.trailing.equalTo(self.receiverContainerView.mas_trailing).with.offset(-kReceiverContainerPadding);
        make.height.mas_equalTo(26);
        make.width.mas_equalTo(26);
    }];
    UIImage *image;
    if (self.didExpand) {
        image = [UIImage imageNamed:@"ExpandArrow"];
    }
    else {
        image = [UIImage imageNamed:@"CollapseArrow"];
    }
    [self.receiverButton setImage:image forState:UIControlStateNormal];
}

#pragma mark - Setup Input Container View

- (void)setupInputContainerView {
    
    self.inputContainerView = [[UIView alloc] init];
    self.inputContainerView.backgroundColor = UIColor.whiteColor;
    [self.chatBarContainerView addSubview:self.inputContainerView];
    
    [self.inputContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.receiverContainerView.mas_bottom).with.offset(kChatBarContainerSpacing);
        make.trailing.equalTo(self.chatBarContainerView.mas_trailing).with.offset(-kChatBarContainerPadding);
        make.leading.equalTo(self.chatBarContainerView.mas_leading).with.offset(kChatBarContainerPadding);
    }];
    
    //icon view
    [self setupInputIconView];
    //text view
    [self setupChatBarTextView];
    
    //update constraints
    CGFloat maxWidth = self.inputContainerView.width - self.sendIconView.width;
    self.currentTextViewHeight = [self.chatBarTextView sizeThatFits:CGSizeMake(maxWidth, MAXFLOAT)].height;
    self.maxTextViewHeight = self.currentTextViewHeight * 3;
    
    [self updateChatBarConstraints];
    
}

- (void)setupInputIconView {
    CGFloat iconSize = 35;
    /// Send icon view
    self.sendIconView = [[ImageViewContainer alloc] initImageViewCenterToViewWithNormalTintColor:nil
                                                                           withSelectedTintColor:nil];
    self.sendIconView.imageView.image = [UIImage imageNamed:@"SendIcon"];
    self.sendIconView.hidden = FALSE;
    
    [self.inputContainerView addSubview:self.sendIconView];
   
    [self.sendIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.inputContainerView.mas_top);
        make.trailing.equalTo(self.inputContainerView.mas_trailing);
        make.width.mas_equalTo(iconSize);
        make.height.mas_equalTo(iconSize);
    }];
    
    [self registerSendIconTabRecognizer];
}

#pragma mark - Chat Bar Text View

- (void)setupChatBarTextView {
    self.chatBarTextView = [[CustomTextView alloc] initWithFont:kRegular18
                                                   withTextColor:UIColor.blackColor
                                             withPlaceholderText:@"Tin nhắn"
                                        withPlaceholderTextColor:UIColor.lightGrayColor];
    [self.chatBarTextView placeholderTopLeftPadding:kChatBarTextViewPadding + 2];
    
    self.chatBarTextView.textContainerInset = UIEdgeInsetsMake(kChatBarTextViewPadding,
                                                               kChatBarTextViewPadding,
                                                               kChatBarTextViewPadding,
                                                               kChatBarTextViewPadding);
    
    self.chatBarTextView.delegate = self;
    self.chatBarTextView.scrollEnabled = FALSE;
    
    [self.inputContainerView addSubview:self.chatBarTextView];
    [self.chatBarTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.inputContainerView.mas_top);
        make.leading.equalTo(self.inputContainerView.mas_leading);
        make.trailing.equalTo(self.sendIconView.mas_leading);
    }];
    
    [self registerChatBarTextViewTabRecognizer];
}

#pragma mark - Update Constraints

- (void)updateChatBarConstraints {
    [self _updateChatBarTextViewConstraints];
    [self _updateInputViewConstraints];
    [self _updateChatBarContainerViewConstraints];
}

- (void)_updateChatBarTextViewConstraints {
    [self.chatBarTextView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(self.currentTextViewHeight);
    }];
}

- (void)_updateInputViewConstraints {
    [self.inputContainerView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(self.currentTextViewHeight);
    }];
}

- (void)_updateChatBarContainerViewConstraints {
    CGFloat chatBarContainerHeight = self.currentTextViewHeight + 2*kChatBarContainerPadding + kChatBarContainerSpacing + kReceiverHeight;
    chatBarContainerHeight += kKeyboardHeight;
    if (!kKeyboardHeight) {
        chatBarContainerHeight += [self calculateHomeIndicatorHeight];
    }
    
    [self.chatBarContainerView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(chatBarContainerHeight);
    }];
}

- (CGFloat)calculateHomeIndicatorHeight {
    CGFloat homeIndicatorHeight = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.windows.firstObject;
        UIEdgeInsets safeAreaInsets = window.safeAreaInsets;
        homeIndicatorHeight = safeAreaInsets.bottom;
    }
    return homeIndicatorHeight;
}

#pragma mark - Receiver Selection View Controller

- (void)setupReceiverSelectionView {
    self.receiverSelectionViewController = [[ReceiverSelectionViewController alloc] init];
    self.receiverSelectionViewController.delegate = self;
    [self addChildViewController:self.receiverSelectionViewController];
    [self.receiverSelectionViewController didMoveToParentViewController:self];
    
    [self.view addSubview:self.receiverSelectionViewController.view];
    
    self.receiverSelectionViewController.view.backgroundColor = UIColor.purpleColor;
    [self.receiverSelectionViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.chatBarContainerView.mas_top);
        make.trailing.equalTo(self.view.mas_trailing);
        make.leading.equalTo(self.view.mas_leading);
    }];
    [self updateReceiverSelectionViewConstraints];
}

- (void)updateReceiverSelectionViewConstraints {
    CGFloat height = self.didExpand ? kReceiverSelectionHeight : 0;
    [self.receiverSelectionViewController.view mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(height);
    }];
}

#pragma mark - Setup Message Collection View

- (void)setupMessageCollectionView {
    
    self.messageCollectionViewLayout = [[ChatLayout alloc] init];
    self.messageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                                    collectionViewLayout:self.messageCollectionViewLayout];
    kMaxBubbleCellWidth = self.messageCollectionView.width - kBubbleMinBlankSpace - kChatAvatarSize - 2 * kChatAvatarMargin;
    kTargetSize = CGSizeMake(kMaxBubbleCellWidth, CGFLOAT_MAX);
    
    self.messageCollectionView.backgroundColor = kBackgroundColor;
    
    self.messageCollectionView.dataSource = self.messageDataSource;
    self.messageCollectionView.delegate = self;
    self.messageCollectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    self.messageCollectionView.transform = CGAffineTransformMakeScale(1.0, -1.0);
    
    [self updateCollectionViewInsets];
    
    self.messageCollectionView.showsHorizontalScrollIndicator = NO;
    
    
    [self.messageCollectionView registerClass:[MessageCollectionViewCell class] forCellWithReuseIdentifier:MessageCollectionViewCell.cellIdentifier];
    
    [self.view addSubview:self.messageCollectionView];
    
    [self.messageCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.receiverSelectionViewController.view.mas_top);
        make.leading.equalTo(self.view.mas_leading);
        make.trailing.equalTo(self.view.mas_trailing);
        make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
    }];
    
    
}

- (void)updateCollectionViewInsets {
    self.messageCollectionView.contentInset = UIEdgeInsetsMake(kMessageCollectionViewInset,
                                                               0,
                                                               kMessageCollectionViewInset,
                                                               0);
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    MessageItem *messageItem = [[self.chatManager getMessages] objectAtIndex:indexPath.item];
    return CGSizeMake(collectionView.width,messageItem.cellHeight);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self didTapMessageCollectionViewAtIndexPath:indexPath];
    
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self didTapMessageCollectionViewAtIndexPath:indexPath];
}

- (void)didTapMessageCollectionViewAtIndexPath:(NSIndexPath *)indexPath {
    //hide keyboard
    if (self.chatBarTextView.isFirstResponder) {
        [self.chatBarTextView resignFirstResponder];
    }
    [self updateLayoutWithAnimation];
}

#pragma mark - TextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    CustomTextView *chatBarTextView = (CustomTextView *)textView;
    if ([chatBarTextView isEmpty]) {
        [chatBarTextView displayPlaceholder];
    }
    else {
        [chatBarTextView displayContent];
    }
    [self calculateTextViewHeight];
}

- (BOOL)isBlankTextView {
    NSString *text = [self.chatBarTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    return [text isEqualToString:@""];
}

- (void)calculateTextViewHeight {
    CGFloat newHeight = [self.chatBarTextView sizeThatFits:CGSizeMake(self.chatBarTextView.width, MAXFLOAT)].height;
    if (newHeight < self.maxTextViewHeight) {
        if (newHeight != self.currentTextViewHeight) {
            self.currentTextViewHeight = newHeight;
            [self updateLayoutWithAnimation];
        }
        self.chatBarTextView.scrollEnabled = FALSE;
        self.hasShownScrollBar = FALSE;
    }
    else {
        self.chatBarTextView.scrollEnabled = TRUE;
        if (!self.hasShownScrollBar) {
            [self.chatBarTextView flashScrollIndicators];
            self.hasShownScrollBar = TRUE;
        }
    }
}

#pragma mark - Update Layout

- (void)updateLayoutWithAnimation {
    [UIView animateWithDuration:0.5 animations:^{
        [self updateChatBarConstraints];
        [self updateReceiverSelectionViewConstraints];
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5 animations:^{
            [self updateCollectionViewInsets];
        }];
    }];
}

#pragma mark - Receiver Delegate

- (void)didSelectReceiver:(User *)user {
    _selectedReceiver = user;
    self.receiverLabel.text = user.username;
    if (user.userType == UserTypeFakeEveryone) {
        self.receiverAvatar.hidden = TRUE;
    }
    else {
        self.receiverAvatar.hidden = FALSE;
    }
    [self handleReceiverButtonTap];
}

#pragma mark - Tab Recognizer

- (void)registerChatBarTextViewTabRecognizer {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(chatBarTextViewTapped:)];
    [self.chatBarTextView addGestureRecognizer:tapRecognizer];
}

- (void)chatBarTextViewTapped:(UITapGestureRecognizer *)tabRecognizer {
    [self.chatBarTextView becomeFirstResponder];
}

- (void)registerSendIconTabRecognizer {
    UITapGestureRecognizer *sendIconTapped = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(sendIconTapped:)];
    [self.sendIconView setUserInteractionEnabled:YES];
    [self.sendIconView addGestureRecognizer:sendIconTapped];
}

- (void)sendIconTapped:(UITapGestureRecognizer *)tabRecognizer {
    //text msg
    if (![self.chatBarTextView isEmpty]) {
        if ([self.chatBarTextView isDisplayPlaceholder]) {
            return;
        }
        
        if ([self isBlankTextView]) {
            [self.chatBarTextView displayPlaceholder];
            [self calculateTextViewHeight];
            return;
        }
        
        [self addNewMessageItem];
        [self updateUIAfterAddMessage];
    }
}

#pragma mark - Keyboard

- (void)registerKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChange:)
                                                 name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    kKeyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    [self updateLayoutWithAnimation];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    kKeyboardHeight = 0;
    [self updateLayoutWithAnimation];
}

- (void)keyboardWillChange:(NSNotification *)notification {
    kKeyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    [self updateLayoutWithAnimation];
}

#pragma mark - Receiver tap

- (void)handleReceiverButtonTap {
    [self loadReceiverArray];
    self.didExpand = !self.didExpand;
    
    UIImage *image;
    if (self.didExpand) {
        image = [UIImage imageNamed:@"ExpandArrow"];
    }
    else {
        image = [UIImage imageNamed:@"CollapseArrow"];
    }
    [self.receiverButton setImage:image forState:UIControlStateNormal];
    
    [self updateLayoutWithAnimation];
}

#pragma mark - Setup Values

- (void)setupValues {
    self.chatManager = [ChatManager sharedManager];
    
    [self loadMessageItemArray];
    _fakeReceiver = [[User alloc] initFakeUserEveryone];
    _selectedReceiver = _fakeReceiver;
    [self loadReceiverArray];
}

- (void)setupSocketListener {
    __weak typeof(self) weakSelf = self;
    [[MySocketManager sharedInstance] listenForReceiveMessageEventWithCompletion:^(MessageItem *messageItem) {
        [weakSelf handleNewComingMessage:messageItem];
    }];
}

#pragma mark - NSDiffableDataSourceSnapshot

- (void)applySnapshotWithAnimation:(BOOL)animation withCompletion:(void (^)(void))completion {
    NSDiffableDataSourceSnapshot *snapshot = [self.messageDataSourceProcessor appendItemArray:[self.chatManager getMessages]];
    [self.messageDataSource applySnapshot:snapshot animatingDifferences:animation completion:completion];
}

#pragma mark - Load MSG

- (void)loadMessageItemArray {
    NSArray *array = [self.chatManager getMessages];
    for (MessageItem *item in array) {
        [item calculateSizeAndInfoWithTargetSize:kTargetSize];
    }
    [self applySnapshotWithAnimation:FALSE withCompletion:nil];
}

#pragma mark - Add new MSG

- (void)updateUIAfterAddMessage {
    // scroll to lastest msg
    [self.messageCollectionView setContentOffset:CGPointMake(0, -kMessageCollectionViewInset) animated:TRUE];
    
    // display placeholder in chat bar
    [self.chatBarTextView displayPlaceholder];
    [self calculateTextViewHeight];
    
}

- (void)addNewMessageItem {
    NSString *type;
    NSDictionary *data;
    if (_selectedReceiver.userType == UserTypeFakeEveryone) {
        type = @"ALL";
        data = @{@"text": self.chatBarTextView.text};
    }
    else {
        type = @"DIRECTIVE";
        data = @{@"text": self.chatBarTextView.text, @"receiver": _selectedReceiver.username};
    }
    [[MySocketManager sharedInstance] emit:kSendMessageEvent with:data completion:^(BOOL success) {
        
    }];
}

- (void)handleNewComingMessage:(MessageItem *)messageItem {
    
    [self.chatManager insertAtFirst:messageItem];
    [messageItem calculateSizeAndInfoWithTargetSize:kTargetSize];
    [self applySnapshotWithAnimation:TRUE withCompletion:nil];
    [self.messageCollectionView setContentOffset:CGPointMake(0, -kMessageCollectionViewInset) animated:TRUE];
}

#pragma mark - Receiver Selection Array

- (void)loadReceiverArray {
    NSMutableArray *receiverArray = [[NSMutableArray alloc] initWithArray:[[RoomManager sharedManager] getMemberArray]];
    //Remove current user and add everyone user
    NSString *currentUsername = [[AccountManager sharedManager] getCurrentUsername];
    for (User *user in receiverArray) {
        if ([user.username isEqualToString:currentUsername]) {
            [receiverArray removeObject:user];
            break;
        }
    }

    [receiverArray insertObject:_fakeReceiver atIndex:0];
    self.receiverSelectionViewController.userArray = [NSArray arrayWithArray:receiverArray];
    [self.receiverSelectionViewController.tableView reloadData];
}

#pragma mark - Lazy Loading

- (UICollectionViewDiffableDataSource<Section *, MessageItem *> *)messageDataSource {
    if (_messageDataSource) {
        return _messageDataSource;
    }
    _messageDataSource = [[UICollectionViewDiffableDataSource alloc] initWithCollectionView:self.messageCollectionView cellProvider:^MessageCollectionViewCell *(UICollectionView *collectionView, NSIndexPath *indexPath, MessageItem *messageItem) {
        
        if (messageItem) {
            Class cellClass = [messageItem getCellClass];
            MessageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[cellClass cellIdentifier] forIndexPath:indexPath];
            [cell setMessageItem:messageItem];
            [cell setContents];
            
            return cell;
        }
        return [[MessageCollectionViewCell alloc] init];
    }];
    return _messageDataSource;
}

- (ChatDataSourceProcessor *)messageDataSourceProcessor {
    if (_messageDataSourceProcessor) {
        return _messageDataSourceProcessor;
    }
    _messageDataSourceProcessor = [[ChatDataSourceProcessor alloc] initWithDiffDataSource:self.messageDataSource];
    return _messageDataSourceProcessor;
}

@end
