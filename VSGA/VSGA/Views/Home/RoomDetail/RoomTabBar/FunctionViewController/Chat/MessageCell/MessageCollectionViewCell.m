//
//  MessageCollectionViewCell.m
//  VStudy
//
//  Created by LAP14011 on 05/03/2023.
//

#import "MessageCollectionViewCell.h"

#import "Macro.h"

#import "UIView+Extension.h"

#import "LayoutInfo.h"
@implementation MessageCollectionViewCell

+ (NSString *)cellIdentifier {
    return NSStringFromClass(self.class);
}

#pragma mark - Lazy loading

- (UIImageView *)avatarImageView {
    if (_avatarImageView) {
        return _avatarImageView;
    }
    _avatarImageView = [[UIImageView alloc] init];
    _avatarImageView.layer.cornerRadius = kChatAvatarSize/2;
    _avatarImageView.clipsToBounds = true;
    _avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    _avatarImageView.hidden = TRUE;
    [self.contentView addSubview:_avatarImageView];
    
    return _avatarImageView;
}

- (CustomLabel *)nameLabel {
    if (_nameLabel) {
        return _nameLabel;
    }
    _nameLabel = [[CustomLabel alloc] init];
    [self.contentView addSubview:_nameLabel];
    
    return _nameLabel;
}

- (UIView *)messageContentView {
    if (_messageContentView) {
        return _messageContentView;
    }
    _messageContentView = [[UIView alloc] init];
    [self.contentView addSubview:_messageContentView];
    //draw shape and border
    [_messageContentView roundedWithCornerRadius:kBubbleCornerRadius];
    [_messageContentView borderWithColor:kBubbleBorderColor
                         withBorderWidth:kBubbleBorderWidth];
    return _messageContentView;
}

- (CustomLabel *)messageTextLabel {
    if (_messageTextLabel) {
        return _messageTextLabel;
    }
    _messageTextLabel = [[CustomLabel alloc] init];
    [self.messageContentView addSubview:_messageTextLabel];
    
    return _messageTextLabel;
}

- (TimestampView *)timestampView {
    if (_timestampView) {
        return _timestampView;
    }
    _timestampView = [[TimestampView alloc] init];
    [self.contentView addSubview:_timestampView];
    return _timestampView;
}

#pragma mark - PrepareForReuse

- (void)prepareForReuse {
    [super prepareForReuse];
    
    if (_avatarImageView) {
        _avatarImageView.image = nil;
        _avatarImageView.hidden = TRUE;
    }
    
    if (_messageContentView) {
        _messageContentView.hidden = TRUE;
    }
    
    if (_nameLabel) {
        _nameLabel.hidden = TRUE;
    }
    
    if (_messageTextLabel) {
        _messageTextLabel.text = nil;
        _messageTextLabel.hidden = TRUE;
    }
    
    if (_timestampView) {
        _timestampView.hidden = TRUE;
    }
}

#pragma mark - LayoutSubviews

- (void)layoutSubviews {
    [super layoutSubviews];
    [self beginLayoutWithItem:self.messageItem];
    
    MessageItem *item = self.messageItem;
    
    [self layoutAvatarWithItem:item];
    [self layoutMessageNameWithItem:item];
    [self layoutMessageContentViewWithItem:item];
    
    [self layoutTextWithItem:item];
    [self layoutTimestampWithItem:item];
    
    [self endLayoutWithItem:self.messageItem];
}

- (void)beginLayoutWithItem:(MessageItem *)item {
    
}

- (void)endLayoutWithItem:(MessageItem *)item {
    
}

- (void)layoutAvatarWithItem:(MessageItem *)item {
    if (item.isSendMessage) {
        return;
    }
    self.avatarImageView.hidden = FALSE;
    self.avatarImageView.frame = [LayoutInfo findFrameLayoutIn:self.messageItem.layoutInfo forKey:kAvatarLayout];
    self.avatarImageView.layer.borderWidth = 0.5;
}

- (void)layoutMessageNameWithItem:(MessageItem *)item {
    self.nameLabel.hidden = FALSE;
    self.nameLabel.frame = [LayoutInfo findFrameLayoutIn:self.messageItem.layoutInfo forKey:kMessageNameLayout];
}

- (void)layoutMessageContentViewWithItem:(MessageItem *)item {
    self.messageContentView.hidden = FALSE;
    self.messageContentView.frame = [LayoutInfo findFrameLayoutIn:self.messageItem.layoutInfo forKey:kBubbleLayout];
}

- (void)layoutTextWithItem:(MessageItem *)item {
    self.messageTextLabel.hidden = FALSE;
    self.messageTextLabel.frame = [LayoutInfo findFrameLayoutIn:self.messageItem.layoutInfo forKey:kMainContentLayout];
}

- (void)layoutTimestampWithItem:(MessageItem *)item {
    self.timestampView.hidden = FALSE;
    self.timestampView.frame = [LayoutInfo findFrameLayoutIn:self.messageItem.layoutInfo forKey:kTimestampLayout];
}

#pragma mark - SetContent

- (void)setContents {
    [self beginSetContentWithItem:self.messageItem];
    
    MessageItem *item = self.messageItem;
    
    [self setContentSenderAvatarWithItem:item];
    [self setContentMessageNameWithItem:item];
    [self setContentMessageContentViewWithItem:item];
    [self setContentTextWithItem:item];
    [self setContentTimestampWithItem:item];
    
    [self endSetContentWithItem:self.messageItem];
    
}

- (void)beginSetContentWithItem:(MessageItem *)item {
    
}

- (void)endSetContentWithItem:(MessageItem *)item {
    
}

- (void)setContentSenderAvatarWithItem:(MessageItem *)item {
    if (!item.isShowAvatar) {
        return;
    }
}

- (void)setContentMessageNameWithItem:(MessageItem *)item {
    [self.nameLabel setTitle:item.messageName
                   titleFont:item.messageNameFont
                  titleColor:item.messageNameTextColor];
}

- (void)setContentMessageContentViewWithItem:(MessageItem *)item {
    self.messageContentView.backgroundColor = item.isSendMessage ? kPrimaryColor : UIColor.whiteColor;
}

- (void)setContentTextWithItem:(MessageItem *)item {
    [self.messageTextLabel setTitle:item.text
                   titleFont:item.textFont
                  titleColor:UIColor.blackColor];
}

- (void)setContentTimestampWithItem:(MessageItem *)item {
    //BG and rounded corner
    self.timestampView.backgroundColor = kTimestampBGColor;
    [self.timestampView roundedWithCornerRadius:kOutsideTimestampCornerRadius];
    //text
    [self.timestampView displayLabelWithText:item.timestampString
                                    withFont:item.timestampFont
                               withTextColor:UIColor.whiteColor];
}
@end
