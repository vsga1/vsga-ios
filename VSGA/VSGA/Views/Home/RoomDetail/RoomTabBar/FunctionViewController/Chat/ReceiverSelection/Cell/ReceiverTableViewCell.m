//
//  ReceiverTableViewCell.m
//  VStudy
//
//  Created by LAP14011 on 09/04/2023.
//

#import "ReceiverTableViewCell.h"

@implementation ReceiverTableViewCell

- (UIView *)containerView {
    if (_containerView) {
        return _containerView;
    }
    return nil;
}

@end
