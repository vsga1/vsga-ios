//
//  ReceiverTableViewCell.h
//  VStudy
//
//  Created by LAP14011 on 09/04/2023.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "CustomLabel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ReceiverTableViewCell : UITableViewCell

@property (nonatomic, strong) User *user;

@property (strong, nonatomic) UIView *containerView;

@property (strong, nonatomic) UIImageView *receiverAvatar;

@property (strong, nonatomic) CustomLabel *label;

@end

NS_ASSUME_NONNULL_END
