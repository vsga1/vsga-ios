//
//  ChatDataSourceProcessor.h
//  VStudy
//
//  Created by LAP14011 on 05/03/2023.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "TSMutableArray.h"
#import "Section.h"
#import "MessageItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface ChatDataSourceProcessor : NSObject

@property (strong, nonatomic) UICollectionViewDiffableDataSource *dataSource;

- (instancetype)initWithDiffDataSource:(UICollectionViewDiffableDataSource *)dataSource;

// Append Array

- (NSDiffableDataSourceSnapshot *)appendItemArray:(NSArray<MessageItem *> *)messageItemArray;

// Append Item

- (NSDiffableDataSourceSnapshot *)appendItem:(MessageItem *)messageItem;

// Reload Item

- (NSDiffableDataSourceSnapshot *)reloadItem:(MessageItem *)messageItem;

// Reload Array

- (NSDiffableDataSourceSnapshot *)reloadItemArray:(NSArray<MessageItem *> *)messageItemArray;

@end

NS_ASSUME_NONNULL_END
