//
//  MemberDataSourceProcessor.m
//  VStudy
//
//  Created by LAP14011 on 15/04/2023.
//

#import "MemberDataSourceProcessor.h"
#import "Section.h"

@implementation MemberDataSourceProcessor

- (instancetype)initWithDiffDataSource:(UICollectionViewDiffableDataSource *)dataSource {
    self = [super init];
    if (self) {
        self.dataSource = dataSource;
        [self _addSection];
    }
    return self;
}
    
- (void)_addSection {
    NSDiffableDataSourceSnapshot<Section *, User *> *snapshot = self.dataSource.snapshot;
    NSArray <Section *> *sections = snapshot.sectionIdentifiers;
    if (sections.count == 0) {
        Section *section = [[Section alloc] initWithSection:SectionMain];
        sections = [[NSArray alloc] initWithObjects:section, nil];
    }
    [snapshot appendSectionsWithIdentifiers:sections];
    [self.dataSource applySnapshot:snapshot animatingDifferences:NO];
}

- (NSDiffableDataSourceSnapshot *)appendItemArray:(TSMutableArray<User *> *)itemArray {
    NSDiffableDataSourceSnapshot<Section *, User *> *snapshot = self.dataSource.snapshot;
    if (itemArray.count > 0) {
        [snapshot appendItemsWithIdentifiers:itemArray];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)appendItem:(User *)item {
    NSDiffableDataSourceSnapshot<Section *, User *> *snapshot = self.dataSource.snapshot;
    if (item) {
        [snapshot appendItemsWithIdentifiers:@[item]];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)insertItem:(User *)item {
    NSDiffableDataSourceSnapshot<Section *, User *> *snapshot = self.dataSource.snapshot;
    if (item) {
        User *firstItem = [snapshot.itemIdentifiers firstObject];
        if (firstItem) {
            [snapshot insertItemsWithIdentifiers:@[item] beforeItemWithIdentifier:firstItem];
        }
        else {
            [snapshot appendItemsWithIdentifiers:@[item]];
        }
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)reloadItem:(User *)item {
    NSDiffableDataSourceSnapshot<Section *, User *> *snapshot = self.dataSource.snapshot;
    if (item) {
        [snapshot reloadItemsWithIdentifiers:@[item]];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)reloadItemArray:(TSMutableArray<User *> *)itemArray {
    NSDiffableDataSourceSnapshot<Section *, User *> *snapshot = self.dataSource.snapshot;
    if (itemArray.count > 0) {
        [snapshot reloadItemsWithIdentifiers:itemArray];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)deleteItem:(User *)item {
    NSDiffableDataSourceSnapshot<Section *, User *> *snapshot = self.dataSource.snapshot;
    if (item) {
        [snapshot deleteItemsWithIdentifiers:@[item]];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)deleteItemArray:(TSMutableArray<User *> *)itemArray {
    NSDiffableDataSourceSnapshot<Section *, User *> *snapshot = self.dataSource.snapshot;
    if (itemArray.count > 0) {
        [snapshot deleteItemsWithIdentifiers:itemArray];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)replaceItem:(User *)item withNewItem:(User *)newItem {
    NSDiffableDataSourceSnapshot<Section *, User *> *snapshot = self.dataSource.snapshot;
    if (item && newItem) {
        [snapshot insertItemsWithIdentifiers:@[newItem] beforeItemWithIdentifier:item];
        [snapshot deleteItemsWithIdentifiers:@[item]];
    }
    return snapshot;
}



@end
