//
//  MemberCollectionViewCell.m
//  VStudy
//
//  Created by LAP14011 on 15/04/2023.
//

#import "MemberCollectionViewCell.h"
#import "Macro.h"
#import "AccountManager.h"
#import "UIView+Extension.h"

#define kCellContentMargin 10

@implementation MemberCollectionViewCell

+ (NSString *)cellIdentifier {
    return NSStringFromClass(self.class);
}

#pragma mark - Lazy loadig

- (UIImageView *)avatarImageView {
    if (_avatarImageView) {
        return _avatarImageView;
    }
    _avatarImageView = [[UIImageView alloc] init];
    _avatarImageView.layer.cornerRadius = kMemberAvatarSize/2;
    _avatarImageView.clipsToBounds = true;
    _avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    _avatarImageView.hidden = FALSE;
    [self.contentView addSubview:_avatarImageView];
    
    return _avatarImageView;
}

- (CustomLabel *)usernameLabel {
    if (_usernameLabel) {
        return _usernameLabel;
    }
    _usernameLabel = [[CustomLabel alloc] init];
    [self.contentView addSubview:_usernameLabel];
    
    return _usernameLabel;
}

- (CustomLabel *)roleLabel {
    if (_roleLabel) {
        return _roleLabel;
    }
    _roleLabel = [[CustomLabel alloc] init];
    [self.contentView addSubview:_roleLabel];
    
    return _roleLabel;
}

- (UIButton *)menuButton {
    if (_menuButton) {
        return _menuButton;
    }
    _menuButton = [[UIButton alloc] init];
    UIImage *menuImage = [UIImage imageNamed:@"MenuButtonIcon"];
    menuImage = [menuImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [_menuButton setImage:menuImage forState:UIControlStateNormal];
    [_menuButton addTarget:self action:@selector(menuButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

    [self.contentView addSubview:_menuButton];
    
    return _menuButton;
}

#pragma mark - PrepareForReuse

- (void)prepareForReuse {
    [super prepareForReuse];
    
    if (_avatarImageView) {
        _avatarImageView.image = nil;
    }
    
    if (_usernameLabel) {
        _usernameLabel.text = nil;
    }
    
    if (_roleLabel) {
        _roleLabel.text = nil;
    }
    
    if (_menuButton) {
        _menuButton.hidden = TRUE;
    }
}

#pragma mark - Layout

- (void)layoutSubviews {
    [super layoutSubviews];
    [self beginLayoutWithItem:self.memberItem];
    
    [self _layoutAvatarImageViewWithItem:self.memberItem];
    [self _layoutUsernameLabelWithItem:self.memberItem];
    [self _layoutRoleLabelWithItem:self.memberItem];
    [self _layoutMenuButtonWithItem:self.memberItem];
    
    [self endLayoutWithItem:self.memberItem];
}

- (void)beginLayoutWithItem:(User *)item {
    
}

- (void)endLayoutWithItem:(User *)item {
    
}

- (void)_layoutAvatarImageViewWithItem:(User *)item {
    CGFloat y = (self.contentView.height - kMemberAvatarSize - 2 * kCellContentMargin)/2 + kCellContentMargin;
    
    self.avatarImageView.frame = CGRectMake(kCellContentMargin,
                                            y,
                                            kMemberAvatarSize,
                                            kMemberAvatarSize);
}

- (void)_layoutUsernameLabelWithItem:(User *)item {
    CGFloat width = self.contentView.width - self.avatarImageView.right - kCellContentMargin - kMenuButtonSize - kCellContentMargin;
    
    self.usernameLabel.frame = CGRectMake(self.avatarImageView.right + kCellContentMargin,
                                          kCellContentMargin,
                                          width,
                                          30);
}

- (void)_layoutRoleLabelWithItem:(User *)item {
    CGFloat width = self.contentView.width - self.avatarImageView.right - kCellContentMargin - kMenuButtonSize - kCellContentMargin;
    
    self.roleLabel.frame = CGRectMake(self.avatarImageView.right + kCellContentMargin,
                                      self.usernameLabel.bottom,
                                      width,
                                      20);
}

- (void)_layoutMenuButtonWithItem:(User *)item {
    CGFloat x = self.contentView.width - kCellContentMargin - kMenuButtonSize;
    CGFloat y = (self.contentView.height - kMenuButtonSize - 2 * kCellContentMargin)/2 + kCellContentMargin;
    self.menuButton.frame = CGRectMake(x,
                                       y,
                                       kMenuButtonSize,
                                       kMenuButtonSize);
}
#pragma mark - Set content

- (void)setContents {
    [self beginSetContentWithItem:self.memberItem];
    
    [self _setContentAvatarImageViewWithItem:self.memberItem];
    [self _setContentUsernameLabelWithItem:self.memberItem];
    [self _setContentRoleLabelWithItem:self.memberItem];
    [self _setContentMenuButtonWithItem:self.memberItem];
    
    [self endSetContentWithItem:self.memberItem];
}

- (void)beginSetContentWithItem:(User *)item {
    
}

- (void)endSetContentWithItem:(User *)item {
    
}

- (void)_setContentAvatarImageViewWithItem:(User *)item {
    NSString *imageName = [NSString stringWithFormat:@"Avatar%u", arc4random_uniform(4)];
    self.avatarImageView.image = [UIImage imageNamed:imageName];
}

- (void)_setContentUsernameLabelWithItem:(User *)item {
    [self.usernameLabel setTitle:item.username
                       titleFont:kRegular18
                      titleColor:UIColor.blackColor];
}

- (void)_setContentRoleLabelWithItem:(User *)item {
    [self.roleLabel setTitle:item.role
                       titleFont:kRegular14
                      titleColor:UIColor.grayColor];
}

- (void)_setContentMenuButtonWithItem:(User *)item {
    User *currentUser = [[AccountManager sharedManager] getCurrentUser];
    if (currentUser.roleType == UserRoleHost && item.roleType != UserRoleHost) {
        self.menuButton.hidden = FALSE;
    }
    else {
        self.menuButton.hidden = TRUE;
    }
}
#pragma mark - Menu Button

- (void)menuButtonTapped:(UIButton *)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *transitionAction;
    if (self.memberItem.roleType == UserRoleCoHost) {
        transitionAction = [UIAlertAction actionWithTitle:@"Demote" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.delegate item:self.memberItem didTapAction:MemberActionDemote];
        }];
    }
    else if (self.memberItem.roleType == UserRoleMember) {
        transitionAction = [UIAlertAction actionWithTitle:@"Promote" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.delegate item:self.memberItem didTapAction:MemberActionPromote];
        }];
    }
   
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"Kick" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // Handle action 2
    }];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        // Handle cancel
    }];

    [alertController addAction:transitionAction];
    [alertController addAction:deleteAction];
    [alertController addAction:cancelAction];
    
    [self.delegate showAlertController:alertController];
}


@end
