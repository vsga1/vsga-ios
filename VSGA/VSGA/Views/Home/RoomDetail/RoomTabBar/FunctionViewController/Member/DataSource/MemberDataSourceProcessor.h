//
//  MemberDataSourceProcessor.h
//  VStudy
//
//  Created by LAP14011 on 15/04/2023.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TSMutableArray.h"
#import "User.h"
NS_ASSUME_NONNULL_BEGIN

@interface MemberDataSourceProcessor : NSObject

@property (strong, nonatomic) UICollectionViewDiffableDataSource *dataSource;

- (instancetype)initWithDiffDataSource:(UICollectionViewDiffableDataSource *)dataSource;

// Append Array

- (NSDiffableDataSourceSnapshot *)appendItemArray:(TSMutableArray<User *> *)itemArray;

// Append Item

- (NSDiffableDataSourceSnapshot *)appendItem:(User *)item;

// Insert Item

- (NSDiffableDataSourceSnapshot *)insertItem:(User *)item;

// Reload Item

- (NSDiffableDataSourceSnapshot *)reloadItem:(User *)item;

// Reload Array

- (NSDiffableDataSourceSnapshot *)reloadItemArray:(TSMutableArray<User *> *)itemArray;

// Delete Item

- (NSDiffableDataSourceSnapshot *)deleteItem:(User *)item;

// Delete Array

- (NSDiffableDataSourceSnapshot *)deleteItemArray:(TSMutableArray<User *> *)itemArray;

// Replace Item

- (NSDiffableDataSourceSnapshot *)replaceItem:(User *)item withNewItem:(User *)newItem;

@end

NS_ASSUME_NONNULL_END
