//
//  MemberViewController.m
//  VStudy
//
//  Created by LAP14011 on 15/04/2023.
//

#import "MemberViewController.h"
#import <Masonry/Masonry.h>
#import "UIView+Extension.h"
#import "Macro.h"
#import "User.h"
#import "Section.h"
#import "TSMutableArray.h"
#import "MemberDataSourceProcessor.h"
#import "MemberCollectionViewCell.h"

#import "RoomManager.h"
#import "UserHandler.h"
#import "MySocketManager.h"

@interface MemberViewController () <UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MemberCollectionViewCellDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) UICollectionViewDiffableDataSource<Section *, User *> *dataSource;

@property (nonatomic, strong) MemberDataSourceProcessor *dataSourceProcessor;

@property (nonatomic, strong) TSMutableArray<User *> *memberList;

@property (nonatomic, strong) UserHandler *userHandler;

@end

@implementation MemberViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViews];
    [self setupValues];
    [self setupSocket];
}

#pragma mark - Setup Views

- (void)setupViews {
    self.view.backgroundColor = kBackgroundColor;
    self.navigationItem.title = @"Member";
    [self setupCollectionView];
}

- (void)setupCollectionView {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                                     collectionViewLayout:layout];
    
    self.collectionView.dataSource = self.dataSource;
    self.collectionView.delegate = self;
    
    [self.collectionView registerClass:[MemberCollectionViewCell class] forCellWithReuseIdentifier:MemberCollectionViewCell.cellIdentifier];
    
    [self.view addSubview:self.collectionView];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop).with.offset(RoomDetailViewSpacing);
        make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).with.offset(RoomDetailViewSpacing);
        make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).with.offset(-RoomDetailViewSpacing);
        make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).with.offset(-RoomDetailViewSpacing);
    }];
    
    self.collectionView.layer.cornerRadius = 10.0f;
    self.collectionView.layer.borderWidth = 1.0f;
    self.collectionView.layer.borderColor = [UIColor clearColor].CGColor;
    self.collectionView.layer.masksToBounds = YES;
    
    self.collectionView.backgroundColor = UIColor.whiteColor;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.collectionView.width, 70);
}

#pragma mark - Setup Values

- (void)setupValues {
    self.memberList = [[TSMutableArray alloc] initWithArray:[[RoomManager sharedManager] getMemberArray]];
    
    NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor appendItemArray:self.memberList];
    [self applySnapshot:snapshot animation:NO completion:nil];
}

#pragma mark - NSDiffableDataSourceSnapshot

- (void)applySnapshot:(NSDiffableDataSourceSnapshot *)snapshot
            animation:(BOOL)animation
           completion:(void (^)(void))completion {
    [self.dataSource applySnapshot:snapshot animatingDifferences:animation completion:completion];
}

#pragma mark - Member Cell Delegate

- (void)showAlertController:(UIAlertController *)alertController {
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)item:(User *)item didTapAction:(MemberAction)action {
    if (action == MemberActionPromote) {
        [self _handlePromoteMemberAPI:item];
        return;
    }
    if (action == MemberActionDemote) {
        [self _handleDemoteMember];
        return;
    }
    if (action == MemberActionKick) {
        [self _handleKickMember];
        return;
    }
}

#pragma mark - Handle Member

- (void)_handlePromoteMemberAPI:(User *)member {
    NSString *roomCode = [[RoomManager sharedManager] getRoomCode];
    [self.userHandler promoteMember:member
                               inRoom:roomCode
                    completionHandler:^(NSError * _Nullable error) {
        if (error) {
            return;
        }
        [self _handlePromoteMemberSocket:member];
    }];
}

- (void)_handleDemoteMember {
    
}

- (void)_handleKickMember {
    
}

#pragma mark - Socket Emit

- (void)_handlePromoteMemberSocket:(User *)member {
    NSString *roomCode = [[RoomManager sharedManager] getRoomCode];
    NSDictionary *dataSocket = @{@"roomCode": roomCode, @"username": member.username, @"role": @"CO_HOST"};
    [[MySocketManager sharedInstance] emit:kPromoteMember with:dataSocket completion:^(BOOL success) {
        if (success) {
            return;
        }
    }];
}

#pragma mark - Socket Listener

- (void)setupSocket {
    [[MySocketManager sharedInstance] listenForRoomDataEventWithCompletion:^(NSArray<User *> * memberArray) {
        [[RoomManager sharedManager] createMemberArrayWithArray:memberArray];
        self.memberList = [[TSMutableArray alloc] initWithArray:memberArray];
        
    }];
}

#pragma mark - Lazy Loading

- (UICollectionViewDiffableDataSource<Section *,User *> *)dataSource {
    if (_dataSource) {
        return _dataSource;
    }
    _dataSource = [[UICollectionViewDiffableDataSource alloc] initWithCollectionView:self.collectionView
                                                                        cellProvider:^UICollectionViewCell * _Nullable(UICollectionView * _Nonnull collectionView, NSIndexPath * _Nonnull indexPath, User *item) {
        if (item) {
            MemberCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MemberCollectionViewCell.cellIdentifier forIndexPath:indexPath];
            cell.memberItem = item;
            cell.delegate = self;
            [cell setContents];
            return cell;
        }
        return [[MemberCollectionViewCell alloc] init];
    }];
    return _dataSource;
}

- (MemberDataSourceProcessor *)dataSourceProcessor {
    if (_dataSourceProcessor) {
        return _dataSourceProcessor;
    }
    _dataSourceProcessor = [[MemberDataSourceProcessor alloc] initWithDiffDataSource:self.dataSource];
    return _dataSourceProcessor;
}

- (UserHandler *)userHandler {
    if (_userHandler) {
        return _userHandler;
    }
    _userHandler = [[UserHandler alloc] init];
    return _userHandler;
}

@end
