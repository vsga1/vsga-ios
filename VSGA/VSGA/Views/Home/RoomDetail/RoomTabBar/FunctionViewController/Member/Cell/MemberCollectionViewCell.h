//
//  MemberCollectionViewCell.h
//  VStudy
//
//  Created by LAP14011 on 15/04/2023.
//

#import <UIKit/UIKit.h>
#import "User.h"

#import "CustomLabel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, MemberAction) {
    MemberActionPromote,
    MemberActionDemote,
    MemberActionKick
};

@protocol MemberCollectionViewCellDelegate <NSObject>

- (void)item:(User *)item didTapAction:(MemberAction)action;

- (void)showAlertController:(UIAlertController *)alertController;

@end

@interface MemberCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id<MemberCollectionViewCellDelegate> delegate;

@property (nonatomic, strong) User *memberItem;

@property (nonatomic, strong) UIImageView *avatarImageView;

@property (nonatomic, strong) CustomLabel *usernameLabel;

@property (nonatomic, strong) CustomLabel *roleLabel;

@property (nonatomic, strong) UIButton *menuButton;

+ (NSString *)cellIdentifier;

// MARK: SetContent

- (void)setContents;

- (void)beginSetContentWithItem:(User *)item;

- (void)endSetContentWithItem:(User *)item;

// MARK: Layout

- (void)beginLayoutWithItem:(User *)item;

- (void)endLayoutWithItem:(User *)item;


@end

NS_ASSUME_NONNULL_END
