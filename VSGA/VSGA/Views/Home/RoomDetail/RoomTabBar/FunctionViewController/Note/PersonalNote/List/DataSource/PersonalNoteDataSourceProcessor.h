//
//  PersonalNoteDataSourceProcessor.h
//  VStudy
//
//  Created by LAP14011 on 18/03/2023.
//

#import <Foundation/Foundation.h>
#import "TSMutableArray.h"
#import "Section.h"
#import "PersonalNoteItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface PersonalNoteDataSourceProcessor : NSObject

@property (strong, nonatomic) UICollectionViewDiffableDataSource *dataSource;

- (instancetype)initWithDiffDataSource:(UICollectionViewDiffableDataSource *)dataSource;

// Append Array

- (NSDiffableDataSourceSnapshot *)appendItemArray:(TSMutableArray<PersonalNoteItem *> *)noteItemArray;

// Append Item

- (NSDiffableDataSourceSnapshot *)appendItem:(PersonalNoteItem *)noteItem;

// Insert Item

- (NSDiffableDataSourceSnapshot *)insertItem:(PersonalNoteItem *)noteItem;

// Reload Item

- (NSDiffableDataSourceSnapshot *)reloadItem:(PersonalNoteItem *)noteItem;

// Reload Array

- (NSDiffableDataSourceSnapshot *)reloadItemArray:(TSMutableArray<PersonalNoteItem *> *)noteItemArray;

@end

NS_ASSUME_NONNULL_END
