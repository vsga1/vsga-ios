//
//  NoteViewController.m
//  VStudy
//
//  Created by LAP14011 on 15/03/2023.
//

#import "NoteViewController.h"

#import <Masonry/Masonry.h>

#import "RoomNoteViewController.h"
#import "PersonalNoteViewController.h"
@interface NoteViewController ()

@property (strong, nonatomic) UISegmentedControl *segmentedControl;

@property (nonatomic, strong) UIView *childSegmentedView;

@property (strong, nonatomic) RoomNoteViewController *roomNoteVC;

@property (strong, nonatomic) PersonalNoteViewController *personalNoteVC;
@end

@implementation NoteViewController {
    UIBarButtonItem *_addNoteButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    [self createView];
}

#pragma mark - Create View

- (void)createView {
    self.navigationItem.title = @"Note";
    [self createAddNewNoteButton];
    [self createSegmentedControl];
    
    [self createChildViewControllers];
    [self createSegmentedViews];
}

- (void)createAddNewNoteButton {
    _addNoteButton  = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"AddNewIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(addNewPersonalNote)];
}

- (void)createChildViewControllers {
    self.roomNoteVC = [[RoomNoteViewController alloc] init];
    self.personalNoteVC = [[PersonalNoteViewController alloc] init];
}

- (void)createSegmentedControl {
    NSArray *itemArray = [NSArray arrayWithObjects: @"Room Note", @"Personal Note", nil];
    self.segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
    [self.segmentedControl addTarget:self action:@selector(segmentValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.segmentedControl];
    
    [self.segmentedControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop);
        make.leading.mas_equalTo(self.view.mas_leading);
        make.trailing.mas_equalTo(self.view.mas_trailing);
        make.height.mas_equalTo(40);
    }];
    [self.segmentedControl addTarget:self
                              action:@selector(segmentValueChanged:)
                    forControlEvents:UIControlEventValueChanged];
    self.segmentedControl.selectedSegmentIndex = 0;
}

- (void)createSegmentedViews {
    self.childSegmentedView = [[UIView alloc] init];
    [self.view addSubview:self.childSegmentedView];
    
    [self.childSegmentedView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.segmentedControl.mas_bottom);
        make.leading.mas_equalTo(self.view.mas_leading);
        make.trailing.mas_equalTo(self.view.mas_trailing);
        make.bottom.mas_equalTo(self.view.mas_bottom);
    }];
    
    ///Default is room note
    [self addSegmentChildViewController:self.roomNoteVC];
}

- (void)addSegmentChildViewController:(UIViewController *)childController {
    [self addChildViewController:childController];
    [self.childSegmentedView addSubview:childController.view];
    [childController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.childSegmentedView);
    }];
    [childController didMoveToParentViewController:self];
}

- (void)removeSegmentChildViewController:(UIViewController *)childController {
    [childController willMoveToParentViewController:nil];
    [childController.view removeFromSuperview];
    [childController removeFromParentViewController];
}

- (void)segmentValueChanged:(UISegmentedControl *)sender {
    [self changeViewWithSelectedSegmentIndex:sender.selectedSegmentIndex];
}

- (void)changeViewWithSelectedSegmentIndex:(NSInteger)selectedSegmentIndex {
    if (selectedSegmentIndex == 0) {
        [self removeSegmentChildViewController:self.personalNoteVC];
        [self addSegmentChildViewController:self.roomNoteVC];
        // Hide the UIBarButtonItem
        self.navigationItem.rightBarButtonItem = nil;
        
    }
    if (selectedSegmentIndex == 1) {
        [self removeSegmentChildViewController:self.roomNoteVC];
        [self addSegmentChildViewController:self.personalNoteVC];
        // Show the UIBarButtonItem
        self.navigationItem.rightBarButtonItem = _addNoteButton;
    }
}

#pragma mark - Add Note button

- (void)addNewPersonalNote {
    [self.personalNoteVC handleAddNewNote];
}

@end
