//
//  RoomNoteViewController.m
//  VStudy
//
//  Created by LAP14011 on 15/03/2023.
//

#import "RoomNoteViewController.h"
#import <Masonry/Masonry.h>
#import "Scope.h"
#import "Macro.h"

#import "UIView+Extension.h"
#import "NSString+Extension.h"

#import "CustomTextView.h"
#import "CustomLabel.h"
#import "CustomButton.h"

#import "RoomManager.h"
#import "MySocketManager.h"
#import "NoteHandler.h"

const CGFloat kStatusHeight = 30;
const CGFloat kSaveButtonHeight = 30;
const CGFloat kRoomSeparatorWidth = 300;
const CGFloat kRoomSeparatorHeight = 2;

const CGFloat kStatusMargin = 10;

const NSInteger kRoomHeaderTag = 0;
const NSInteger kRoomDescTag = 1;

@interface RoomNoteViewController () <UITextViewDelegate, RoomNoteHandlerDelegate, RoomNoteHandlerDelegate>

@property (nonatomic, strong) UIView *statusContainerView;

@property (nonatomic, strong) UIView *noteContainerView;

@property (nonatomic, strong) CustomTextView *headerTextView;

@property (nonatomic, strong) CustomTextView *descTextView;

@property (nonatomic, strong) UIView *separatorView;

@property (nonatomic, strong) UIImageView *statusIcon;

@property (nonatomic, strong) CustomLabel *statusTitle;

@property (nonatomic, strong) CustomButton *saveButton;

@property (nonatomic, strong) NoteHandler *noteHandler;

@property (nonatomic, strong) RoomNoteItem *noteItem;

@property (nonatomic, assign) BOOL isEditing;

@end

@implementation RoomNoteViewController {
    CGFloat kNoteHeight;

    CGFloat kRoomCurrentHeaderHeight;
    CGFloat kRoomCurrentDescHeight;

    CGFloat kRoomMaxHeaderHeight;
    CGFloat kRoomMaxDescHeight;
    
    CGFloat kKeyboardHeight;
}

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupDelegates];
    [self setupViews];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    kKeyboardHeight = 0;
    [self registerKeyboardNotifications];
    
    [self registerAppTerminateOrAppear];
    
    [self setupValues];
    [self setupOpenEditNoteSocket];
    [self setupCloseEditNoteSocket];
    
    self.isEditing = FALSE;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    
    [[MySocketManager sharedInstance] off:kOpenEditNote];
    [[MySocketManager sharedInstance] off:kCloseEditNote];
    
    if (!self.isEditing) {
        return;
    }
    
    self.noteItem.statusType = RoomNoteStatusOpen;
    [self updateNoteAPIAndSocket];
    
    [self.headerTextView endEditing:TRUE];
    [self.descTextView endEditing:TRUE];
    self.isEditing = FALSE;
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self updateNoteConstraints];
}

#pragma mark - Setup Views

- (void)setupViews {
    self.view.backgroundColor = kBackgroundColor;
    [self setupStatusView];
    [self setupNoteView];
}

#pragma mark - Status View

- (void)setupStatusView {
    
    self.statusContainerView = [[UIView alloc] init];
    [self.view addSubview:self.statusContainerView];
    
    [self.statusContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop).with.offset(kStatusMargin);
        make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).with.offset(30);
        make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).with.offset(-30);
        make.height.mas_equalTo(kStatusHeight);
    }];
    
    self.statusIcon = [[UIImageView alloc] init];
    [self.statusContainerView addSubview:self.statusIcon];
    
    [self.statusIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.statusContainerView.mas_left);
        make.centerY.equalTo(self.statusContainerView.mas_centerY);
        make.width.mas_equalTo(20);
        make.height.mas_equalTo(20);
    }];
    
    self.saveButton = [[CustomButton alloc] initWithFrame:CGRectZero
                                                    title:@"Save"
                                               titleColor:UIColor.whiteColor
                                                titleFont:kRegular16
                                          backgroundColor:kPrimaryColor
                                              borderColor:kPrimaryColor.CGColor];
    [self.saveButton addTarget:self action:@selector(saveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.statusContainerView addSubview:self.saveButton];
    
    [self.saveButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.statusContainerView.mas_right);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(30);
    }];
    
    self.statusTitle = [[CustomLabel alloc] init];
    [self.statusContainerView addSubview:self.statusTitle];
    [self.statusTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.statusIcon.mas_right).with.offset(5);
        make.right.equalTo(self.saveButton.mas_left).with.offset(-5);
        make.centerY.equalTo(self.statusContainerView.mas_centerY);
        make.height.mas_equalTo(20);
    }];
}

#pragma mark - Note View

- (void)setupNoteView {
    self.noteContainerView = [[UIView alloc] init];
    [self.view addSubview:self.noteContainerView];
    
    self.noteContainerView.layer.cornerRadius = 10.0f;
    self.noteContainerView.layer.borderWidth = 1.0f;
    self.noteContainerView.layer.borderColor = [UIColor clearColor].CGColor;
    self.noteContainerView.layer.masksToBounds = YES;
    
    [self createHeaderView];
    [self createSeparatorView];
    [self createDescView];
    
    [self setupNoteLayout];
}

- (void)createHeaderView {
    UIFont *headerFont = kSemiBold18;
    self.headerTextView = [[CustomTextView alloc] initWithFont:headerFont
                                                 withTextColor:UIColor.blackColor
                                           withPlaceholderText:@"Header"
                                      withPlaceholderTextColor:UIColor.lightGrayColor];
    self.headerTextView.tag = kRoomHeaderTag;
    [self.headerTextView placeholderTopLeftPadding:12];
    self.headerTextView.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10);
    
    self.headerTextView.delegate = self;
    self.headerTextView.scrollEnabled = FALSE;
    
    [self.noteContainerView addSubview:self.headerTextView];
}

- (void)createSeparatorView {
    self.separatorView = [[UIView alloc] init];
    self.separatorView.backgroundColor = [UIColor lightGrayColor];
    
    [self.noteContainerView addSubview:self.separatorView];
}

- (void)createDescView {
    UIFont *descFont = kRegular18;
    self.descTextView = [[CustomTextView alloc] initWithFont:descFont
                                               withTextColor:UIColor.blackColor
                                         withPlaceholderText:@"Description"
                                    withPlaceholderTextColor:UIColor.lightGrayColor];
    self.descTextView.tag = kRoomDescTag;
    [self.descTextView placeholderTopLeftPadding:12];
    self.descTextView.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10);
    
    self.descTextView.delegate = self;
    self.descTextView.scrollEnabled = TRUE;
    
    [self.noteContainerView addSubview:self.descTextView];
}

- (void)setupNoteLayout {
    
    CGFloat maxViewHeight = self.view.safeAreaLayoutGuide.layoutFrame.size.height;
    kNoteHeight = maxViewHeight - 3 * kStatusMargin - kStatusHeight;
    
    kNoteHeight -= kKeyboardHeight;
    
    kRoomCurrentHeaderHeight = [self.headerTextView sizeThatFits:CGSizeMake(self.headerTextView.width, MAXFLOAT)].height;
    kRoomMaxHeaderHeight = kRoomCurrentHeaderHeight * 2;
    
    kRoomCurrentDescHeight = [self.descTextView sizeThatFits:CGSizeMake(self.descTextView.width, MAXFLOAT)].height;
    kRoomMaxDescHeight = kNoteHeight - kRoomMaxHeaderHeight;
    
    [self.noteContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.statusContainerView.mas_bottom).with.offset(kStatusMargin);
        make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).with.offset(30);
        make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).with.offset(-30);
        make.height.mas_equalTo(kNoteHeight);
    }];
    
    [self.headerTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.noteContainerView.mas_top);
        make.left.equalTo(self.noteContainerView.mas_left);
        make.right.equalTo(self.noteContainerView.mas_right);
        make.height.mas_equalTo(kRoomCurrentHeaderHeight);
    }];
    
    [self.separatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(kRoomSeparatorHeight);
        make.width.mas_equalTo(kRoomSeparatorWidth);
        make.centerX.equalTo(self.headerTextView.mas_centerX);
        make.top.equalTo(self.headerTextView.mas_bottom).offset(-kRoomSeparatorHeight);
    }];
    
    [self.descTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerTextView.mas_bottom);
        make.left.equalTo(self.noteContainerView.mas_left);
        make.right.equalTo(self.noteContainerView.mas_right);
        make.bottom.equalTo(self.noteContainerView.mas_bottom);
    }];
    
}

#pragma mark - Layout

- (void)updateNoteConstraints {
    CGFloat maxViewHeight = self.view.safeAreaLayoutGuide.layoutFrame.size.height;
    kNoteHeight = maxViewHeight - 3 * kStatusMargin - kStatusHeight;
    
    kNoteHeight -= kKeyboardHeight;
    [self.noteContainerView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(kNoteHeight);
    }];
    [self.headerTextView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(kRoomCurrentHeaderHeight);
    }];
    [self scrollToCursor];
}

- (void)scrollToCursor {
    NSRange range = self.descTextView.selectedRange;
    [self.descTextView scrollRangeToVisible:range];
}

- (void)updateLayoutWithAnimation {
    [UIView animateWithDuration:0.5 animations:^{
        [self updateNoteConstraints];
    }];
}

#pragma mark - Update UI

- (void)updateNoteUI {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateNoteView];
        [self updateStatusView];
    });
}

- (void)updateStatusView {
    if (self.noteItem.statusType == RoomNoteStatusOpen) {
        self.statusIcon.image = [UIImage imageNamed:@"DoneIcon"];
        [self.statusTitle setTitle:@"Able to edit"
                         titleFont:kRegular14
                        titleColor:UIColor.blackColor];
        self.saveButton.enabled = TRUE;
    }
    else if (self.noteItem.statusType == RoomNoteStatusEdit) {
        self.statusIcon.image = [UIImage imageNamed:@"CloseIcon"];
        [self.statusTitle setTitle:@"Someone is editing"
                         titleFont:kRegular14
                        titleColor:UIColor.blackColor];
        self.saveButton.enabled = FALSE;
    }
}

- (void)updateNoteView {
    self.headerTextView.text = self.noteItem.header;
    self.descTextView.text = self.noteItem.desc;
    [self.headerTextView displayContentOrPlaceholder];
    [self.descTextView displayContentOrPlaceholder];
    
    if (self.noteItem.statusType == RoomNoteStatusOpen) {
        self.headerTextView.editable = TRUE;
        self.descTextView.editable = TRUE;
    }
    else if (self.noteItem.statusType == RoomNoteStatusEdit) {
        self.headerTextView.editable = FALSE;
        self.descTextView.editable = FALSE;
    }
}

#pragma mark - Setup Values

- (void)setupValues {
    NSString *roomCode = [[RoomManager sharedManager] getRoomCode];
    @weakify(self)
    [self.noteHandler getRoomNoteWithRoomCode:roomCode
                            completionHandler:^(RoomNoteItem * item, NSError * error) {
        @strongify(self)
        if (error) {
            return;
        }
        self.noteItem = item;
        [self updateNoteUI];
    }];
}

#pragma mark - Setup Socket

- (void)setupOpenEditNoteSocket {
    @weakify(self)
    [[MySocketManager sharedInstance] listenForOpenEditNoteWithCompletion:^(RoomNoteItem *item) {
        @strongify(self)
        self.noteItem = item;
        [self updateNoteUI];
    }];
}

- (void)setupCloseEditNoteSocket {
    @weakify(self)
    [[MySocketManager sharedInstance] listenForCloseEditNoteWithCompletion:^(BOOL success) {
        @strongify(self)
        if (success) {
            self.noteItem.statusType = RoomNoteStatusEdit;
            [self updateNoteUI];
        }
    }];
}

#pragma mark - Setup Delegate

- (void)setupDelegates {
    self.noteHandler.roomNoteDelegate = self;
}

#pragma mark - Save Note

- (void)updateNoteAPIAndSocket {
    
    NSString *roomCode = [[RoomManager sharedManager] getRoomCode];
    NSString *header = self.headerTextView.text;
    NSString *desc = self.descTextView.text;
    
    NSDictionary *dataSocket = [NSDictionary new];
    NSDictionary *info;
    
    switch (self.noteItem.statusType) {
        case RoomNoteStatusOpen:
            dataSocket = @{@"roomCode": roomCode, @"header": header, @"desc": desc};
            info = @{@"roomCode": roomCode, @"header": header, @"desc": desc, @"status": @"OPEN"};
            [[MySocketManager sharedInstance] emit:kOpenEditNote with:dataSocket completion:nil];
            break;
        case RoomNoteStatusEdit:
            dataSocket = @{@"roomCode": roomCode};
            info = @{@"roomCode": roomCode, @"status": @"EDIT"};
            [[MySocketManager sharedInstance] emit:kCloseEditNote with:dataSocket completion:nil];
            break;
        default:
            break;
    }
    
    [self.noteHandler updateRoomNoteWithInfo:info];
}

- (void)saveButtonClicked:(CustomButton *)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [sender showLoading];
    });
    
    self.noteItem.statusType = RoomNoteStatusOpen;
    [self updateNoteAPIAndSocket];
    
    [self.headerTextView endEditing:TRUE];
    [self.descTextView endEditing:TRUE];
    self.isEditing = FALSE;
}

#pragma mark - NoteHandlerDelegate

- (void)onFinishUpdateNote:(RoomNoteItem *)note error:(NoteHandlerErrorCode)errorCode {
    self.noteItem = note;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.saveButton hideLoading];
    });
    if (!self.isEditing) {
        [self updateNoteUI];
    }
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if (self.isEditing) {
        return;
    }
    self.isEditing = TRUE;
    self.noteItem.statusType = RoomNoteStatusEdit;
    [self updateNoteAPIAndSocket];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
//    if (!self.isEditing) {
//        return;
//    }
//    self.isEditing = FALSE;
//    self.noteItem.statusType = RoomNoteStatusOpen;
//    [self updateNoteAPIAndSocket];
}

- (void)textViewDidChange:(UITextView *)textView {
    CustomTextView *customTextView = (CustomTextView *)textView;
    [customTextView displayContentOrPlaceholder];
    [self calculateTextViewHeight:customTextView];
}

- (void)calculateTextViewHeight:(CustomTextView *)textView {
    if (textView.tag == kRoomHeaderTag) {
        CGFloat newHeight = [self.headerTextView sizeThatFits:CGSizeMake(self.headerTextView.width, MAXFLOAT)].height;
        if (newHeight >= kRoomMaxHeaderHeight) {
            self.headerTextView.scrollEnabled = TRUE;
        }
        else {
            if (newHeight != kRoomCurrentHeaderHeight) {
                kRoomCurrentHeaderHeight = newHeight;
                [self updateLayoutWithAnimation];
            }
            self.headerTextView.scrollEnabled = FALSE;
        }
    }
    else if (textView.tag == kRoomDescTag) {
        kRoomMaxDescHeight = kNoteHeight - kRoomMaxHeaderHeight;
        CGFloat newHeight = [self.descTextView sizeThatFits:CGSizeMake(self.descTextView.width, MAXFLOAT)].height;
        if (newHeight >= kRoomMaxDescHeight) {
            self.descTextView.scrollEnabled = TRUE;
        }
        else {
            if (newHeight != kRoomCurrentDescHeight) {
                kRoomCurrentDescHeight = newHeight;
                [self updateLayoutWithAnimation];
            }
            self.descTextView.scrollEnabled = FALSE;
        }
    }
}

#pragma mark - Keyboard

- (void)registerKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChange:)
                                                 name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    kKeyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    [self updateLayoutWithAnimation];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    kKeyboardHeight = 0;
    [self updateLayoutWithAnimation];
}

- (void)keyboardWillChange:(NSNotification *)notification {
    kKeyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    [self updateLayoutWithAnimation];
}

#pragma mark - App terminate

- (void)registerAppTerminateOrAppear {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appWillTerminate:)
                                                 name:UIApplicationWillTerminateNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appWillTerminate:)
                                                 name:UIApplicationWillResignActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appWillEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)appWillTerminate:(NSNotification *)notification {
    if (!self.isEditing) {
        return;
    }
    self.noteItem.statusType = RoomNoteStatusOpen;
    [self updateNoteAPIAndSocket];
    
    [self.headerTextView endEditing:TRUE];
    [self.descTextView endEditing:TRUE];
    self.isEditing = FALSE;
}

- (void)appWillEnterForeground:(NSNotification *)notification {
    [self viewWillAppear:TRUE];
}
#pragma mark - Lazy loading

- (NoteHandler *)noteHandler {
    if (_noteHandler) {
        return _noteHandler;
    }
    _noteHandler = [[NoteHandler alloc] init];
    return _noteHandler;
}

@end
