//
//  NoteListCollectionViewCell.m
//  VStudy
//
//  Created by LAP14011 on 15/03/2023.
//

#import "NoteListCollectionViewCell.h"
#import "UIView+Extension.h"
#import "NSString+Extension.h"

const CGFloat kCellMargin = 10;

@implementation NoteListCollectionViewCell

+ (NSString *)cellIdentifier {
    return NSStringFromClass(self.class);
}

#pragma mark - Lazy loadig

- (CustomLabel *)headerLabel {
    if (_headerLabel) {
        return _headerLabel;
    }
    _headerLabel = [[CustomLabel alloc] init];
    [self.contentView addSubview:_headerLabel];
    
    return _headerLabel;
}

- (CustomLabel *)descLabel {
    if (_descLabel) {
        return _descLabel;
    }
    _descLabel = [[CustomLabel alloc] init];
    [self.contentView addSubview:_descLabel];
    
    return _descLabel;
}

#pragma mark - PrepareForReuse

- (void)prepareForReuse {
    [super prepareForReuse];
    
    if (_descLabel) {
        _descLabel.hidden = TRUE;
    }
    
    if (_headerLabel) {
        _headerLabel.hidden = TRUE;
    }
}

#pragma mark - Layout

- (void)layoutSubviews {
    [super layoutSubviews];
    [self beginLayoutWithItem:self.noteItem];
    
    [self _layoutHeaderWithItem:self.noteItem];
    [self _layoutDescWithItem:self.noteItem];
    
    [self endLayoutWithItem:self.noteItem];
}

- (void)beginLayoutWithItem:(PersonalNoteItem *)item {
    self.layer.cornerRadius = 10.0f;
    self.contentView.layer.borderWidth = 1.0f;
    self.contentView.layer.borderColor = [UIColor clearColor].CGColor;
    self.contentView.layer.masksToBounds = YES;
    
    self.contentView.layoutMargins = UIEdgeInsetsMake(kCellMargin, kCellMargin, kCellMargin, kCellMargin);
    
    self.backgroundColor = UIColor.whiteColor;
}

- (void)endLayoutWithItem:(PersonalNoteItem *)item {
    
}

- (void)_layoutHeaderWithItem:(PersonalNoteItem *)item {
    self.headerLabel.hidden = FALSE;
    
    CGFloat cellHeight = (self.contentView.height - 2 * kCellMargin);
    CGFloat cellWidth = (self.contentView.width - 2 * kCellMargin);
    CGFloat itemHeight = [item.header sizeOfStringWithStyledFont:item.headerFont withSize:CGSizeMake(MAXFLOAT, 0)].height;
    CGFloat headerHeight = MAX(cellHeight * 1/3, itemHeight);
    
    self.headerLabel.frame = CGRectMake(kCellMargin,
                                        kCellMargin,
                                        cellWidth,
                                        headerHeight);
}

- (void)_layoutDescWithItem:(PersonalNoteItem *)item {
    self.descLabel.hidden = FALSE;
    
    CGFloat cellHeight = (self.contentView.height - 2 * kCellMargin);
    CGFloat cellWidth = (self.contentView.width - 2 * kCellMargin);
    CGFloat descHeight = cellHeight - (self.headerLabel.height + self.headerLabel.y) - 10;
    
    self.descLabel.frame = CGRectMake(kCellMargin,
                                      self.headerLabel.height + self.headerLabel.y + 10,
                                      cellWidth,
                                      descHeight);
}

#pragma mark - Set content

- (void)setContents {
    [self beginSetContentWithItem:self.noteItem];
    
    [self _setContentHeaderWithItem:self.noteItem];
    [self _setContentDescWithItem:self.noteItem];
    
    [self endSetContentWithItem:self.noteItem];
}

- (void)beginSetContentWithItem:(PersonalNoteItem *)item {
    
}

- (void)endSetContentWithItem:(PersonalNoteItem *)item {
    
}

- (void)_setContentHeaderWithItem:(PersonalNoteItem *)item {
    [self.headerLabel setTitle:item.header
                     titleFont:item.headerFont
                    titleColor:UIColor.blackColor];
    self.headerLabel.numberOfLines = 1;
}

- (void)_setContentDescWithItem:(PersonalNoteItem *)item {
    [self.descLabel setTitle:item.desc
                   titleFont:item.descFont
                  titleColor:UIColor.blackColor];
    self.descLabel.numberOfLines = 1;
}

@end
