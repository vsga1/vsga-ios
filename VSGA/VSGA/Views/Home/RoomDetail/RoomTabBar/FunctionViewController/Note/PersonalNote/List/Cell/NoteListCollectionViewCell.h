//
//  NoteListCollectionViewCell.h
//  VStudy
//
//  Created by LAP14011 on 15/03/2023.
//

#import <UIKit/UIKit.h>
#import "PersonalNoteItem.h"
#import "CustomLabel.h"
NS_ASSUME_NONNULL_BEGIN

@interface NoteListCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) PersonalNoteItem *noteItem;

@property (strong, nonatomic) CustomLabel *headerLabel;

@property (strong, nonatomic) CustomLabel *descLabel;

+ (NSString *)cellIdentifier;

// MARK: SetContent

- (void)setContents;

- (void)beginSetContentWithItem:(PersonalNoteItem *)item;

- (void)endSetContentWithItem:(PersonalNoteItem *)item;

// MARK: Layout

- (void)beginLayoutWithItem:(PersonalNoteItem *)item;

- (void)endLayoutWithItem:(PersonalNoteItem *)item;

@end

NS_ASSUME_NONNULL_END
