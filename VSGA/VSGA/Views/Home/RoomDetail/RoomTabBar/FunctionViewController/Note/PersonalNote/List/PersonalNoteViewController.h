//
//  PersonalNoteViewController.h
//  VStudy
//
//  Created by LAP14011 on 15/03/2023.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PersonalNoteViewController : UIViewController

- (void)handleAddNewNote;

@end

NS_ASSUME_NONNULL_END
