//
//  PersonalNoteViewController.m
//  VStudy
//
//  Created by LAP14011 on 15/03/2023.
//

#import "PersonalNoteViewController.h"
#import <Masonry/Masonry.h>
#import "Macro.h"
#import "UIView+Extension.h"
#import "TSMutableArray.h"
#import "Section.h"
#import "PersonalNoteItem.h"
#import "NoteListCollectionViewCell.h"
#import "PersonalNoteDataSourceProcessor.h"
#import "NoteHandler.h"
#import "RoomManager.h"

#import "NoteDetailViewController.h"

static CGSize kNoteTargetSize;

@interface PersonalNoteViewController () <UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, NoteDetailViewControllerDelegate>

@property (nonatomic, strong) UICollectionView *noteListCollectionView;

@property (nonatomic, strong) UICollectionViewDiffableDataSource<Section *, PersonalNoteItem *> *dataSource;

@property (nonatomic, strong) PersonalNoteDataSourceProcessor *dataSourceProcessor;

@property (nonatomic, strong) TSMutableArray<PersonalNoteItem *> *noteItemList;

@property (nonatomic, strong) NoteHandler *noteHandler;

@end

@implementation PersonalNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupViews];
    [self setupValues];
}

#pragma mark - Setup Views

- (void)setupViews {
    self.view.backgroundColor = kBackgroundColor;
    [self setupNoteListCollectionView];
}

- (void)setupNoteListCollectionView {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    self.noteListCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                                     collectionViewLayout:layout];
    
    self.noteListCollectionView.dataSource = self.dataSource;
    self.noteListCollectionView.delegate = self;
    
    [self.noteListCollectionView registerClass:[NoteListCollectionViewCell class] forCellWithReuseIdentifier:NoteListCollectionViewCell.cellIdentifier];
    
    [self.view addSubview:self.noteListCollectionView];
    
    [self.noteListCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop).with.offset(50);
        make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).with.offset(30);
        make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).with.offset(-30);
        make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).with.offset(-30);
    }];
    
    self.noteListCollectionView.backgroundColor = kBackgroundColor;
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    kNoteTargetSize = CGSizeMake(self.noteListCollectionView.width, 80);
}

#pragma mark - CollectionView

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return kNoteTargetSize;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NoteDetailViewController *noteDetailVC = [[NoteDetailViewController alloc] init];
    noteDetailVC.item = [self.noteItemList objectAtIndex:indexPath.item];
    noteDetailVC.delegate = self;
    [self.navigationController pushViewController:noteDetailVC animated:YES];
}

#pragma mark - Setup Values

- (void)setupValues {
    self.noteItemList = [[TSMutableArray alloc] init];
    
    [self _getPersonalNoteItemArray];
}

- (void)_getPersonalNoteItemArray {
    NSString *roomCode = [[RoomManager sharedManager] getRoomCode];
   [self.noteHandler getPersonalNoteListWithRoomCode:roomCode
                           completionHandler:^(NSArray<PersonalNoteItem *> * _Nullable noteItemArray, NSError * _Nullable error) {
       if (error) {
           return;
       }
       [self _updateNoteInfo:noteItemArray];
       noteItemArray = [[noteItemArray reverseObjectEnumerator] allObjects];
       [self.noteItemList addObjectsFromArray:noteItemArray];
       
       dispatch_async(dispatch_get_main_queue(), ^{
           NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor appendItemArray:self.noteItemList];
           [self applySnapshot:snapshot animation:NO completion:nil];
       });
   }];
}

- (void)_updateNoteInfo:(NSArray<PersonalNoteItem *> *)noteItemArray {
    for (NSInteger i = 0; i < noteItemArray.count; i++) {
        PersonalNoteItem *item = [noteItemArray objectAtIndex:i];
        [item setupDisplayInListView];
    }
}

#pragma mark - NSDiffableDataSourceSnapshot

- (void)applySnapshot:(NSDiffableDataSourceSnapshot *)snapshot
            animation:(BOOL)animation
           completion:(void (^)(void))completion {
    [self.dataSource applySnapshot:snapshot animatingDifferences:animation completion:completion];
}

#pragma mark - Navigate to NoteDetailViewController

- (void)handleAddNewNote {
    NoteDetailViewController *noteDetailVC = [[NoteDetailViewController alloc] init];
    noteDetailVC.delegate = self;
    [self.navigationController pushViewController:noteDetailVC animated:TRUE];
}

#pragma mark - NoteDetailViewControllerDelegate

- (void)controller:(NoteDetailViewController *)controller addNewNote:(PersonalNoteItem *)newItem {
    [self.noteItemList insertObject:newItem atIndex:0];
    [newItem setupDisplayInListView];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor insertItem:newItem];
        [self applySnapshot:snapshot animation:NO completion:nil];
    });
}

- (void)controller:(NoteDetailViewController *)controller updateNote:(PersonalNoteItem *)newItem {
    for (__strong PersonalNoteItem *item in self.noteItemList) {
        if ([item.code isEqualToString:newItem.code]) {
            item.header = newItem.header;
            item.desc = newItem.desc;
            [newItem setupDisplayInListView];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDiffableDataSourceSnapshot *snapshot = [self.dataSourceProcessor reloadItem:item];
                [self applySnapshot:snapshot animation:NO completion:nil];
            });
            break;
        }
    }
}

#pragma mark - Lazy Loading

- (UICollectionViewDiffableDataSource<Section *, PersonalNoteItem *> *)dataSource {
    if (_dataSource) {
        return _dataSource;
    }
    _dataSource = [[UICollectionViewDiffableDataSource alloc] initWithCollectionView:self.noteListCollectionView cellProvider:^NoteListCollectionViewCell *(UICollectionView *collectionView, NSIndexPath *indexPath, PersonalNoteItem *noteItem) {
        
        if (noteItem) {
            Class cellClass = [noteItem getCellClass];
            NoteListCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[cellClass cellIdentifier] forIndexPath:indexPath];
            
            [cell setNoteItem:noteItem];
            [cell setContents];
            return cell;
        }
        return [[NoteListCollectionViewCell alloc] init];
    }];
    return _dataSource;
}

- (PersonalNoteDataSourceProcessor *)dataSourceProcessor {
    if (_dataSourceProcessor) {
        return _dataSourceProcessor;
    }
    _dataSourceProcessor = [[PersonalNoteDataSourceProcessor alloc] initWithDiffDataSource:self.dataSource];
    return _dataSourceProcessor;
}

- (NoteHandler *)noteHandler {
    if (_noteHandler) {
        return _noteHandler;
    }
    _noteHandler = [[NoteHandler alloc] init];
    return _noteHandler;
}

@end
