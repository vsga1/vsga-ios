//
//  NoteDetailViewController.m
//  VStudy
//
//  Created by LAP14011 on 18/03/2023.
//

#import "NoteDetailViewController.h"
#import <Masonry/Masonry.h>
#import "Macro.h"
#import "NoteHandler.h"
#import "RoomManager.h"

#import "UIView+Extension.h"
#import "CustomTextView.h"

const NSInteger kHeaderTag = 0;
const NSInteger kDescTag = 1;

const CGFloat kSeparatorWidth = 300;
const CGFloat kSeparatorHeight = 2;

@interface NoteDetailViewController () <UITextViewDelegate, PersonalNoteHandlerDelegate>

@property (nonatomic, strong) UIView *noteContainerView;

@property (nonatomic, strong) CustomTextView *headerTextView;

@property (nonatomic, strong) CustomTextView *descTextView;

@property (nonatomic, strong) UIView *separatorView;

@property (nonatomic, strong) NoteHandler *noteHandler;

@end

@implementation NoteDetailViewController {
    UIBarButtonItem *_saveButton;
    
    CGFloat kNoteHeight;
    CGFloat kCurrentHeaderHeight;
    CGFloat kCurrentDescHeight;

    CGFloat kMaxHeaderHeight;
    CGFloat kMaxDescHeight;
    
    CGFloat kKeyboardHeight;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViews];
    [self setupValues];
    [self setupDelegates];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    kKeyboardHeight = 0;
    [self registerKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    
    [self updateNoteConstraints];
    
    // Center the separator view horizontally
    [self.separatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.headerTextView.mas_centerX);
    }];
    
    // Position the separator view below the text view
    [self.separatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerTextView.mas_bottom).with.offset(-self.separatorView.height);
    }];
}

#pragma mark - Setup Views

- (void)setupViews {
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupNavigationBar];
    [self setupNoteView];
}

- (void)setupNavigationBar {
    _saveButton = [[UIBarButtonItem alloc]
                   initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                   target:self
                   action:@selector(saveNote)];
    self.navigationItem.rightBarButtonItem = _saveButton;
    _saveButton.enabled = FALSE;
}

- (void)setupNoteView {
    self.noteContainerView = [[UIView alloc] init];
    [self.view addSubview:self.noteContainerView];
    
    [self createHeaderView];
    [self createSeparatorView];
    [self createDescView];
    
    [self setupNoteLayout];
}

- (void)createHeaderView {
    UIFont *headerFont = kSemiBold18;
    self.headerTextView = [[CustomTextView alloc] initWithFont:headerFont
                                                 withTextColor:UIColor.blackColor
                                           withPlaceholderText:@"Header"
                                      withPlaceholderTextColor:UIColor.lightGrayColor];
    self.headerTextView.tag = kHeaderTag;
    [self.headerTextView placeholderTopLeftPadding:12];
    self.headerTextView.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10);
    
    self.headerTextView.delegate = self;
    self.headerTextView.scrollEnabled = FALSE;
    
    [self.noteContainerView addSubview:self.headerTextView];
}

- (void)createSeparatorView {
    self.separatorView = [[UIView alloc] init];
    self.separatorView.backgroundColor = [UIColor lightGrayColor];
    
    [self.noteContainerView addSubview:self.separatorView];
}

- (void)createDescView {
    UIFont *descFont = kRegular18;
    self.descTextView = [[CustomTextView alloc] initWithFont:descFont
                                               withTextColor:UIColor.blackColor
                                         withPlaceholderText:@"Description"
                                    withPlaceholderTextColor:UIColor.lightGrayColor];
    self.descTextView.tag = kDescTag;
    [self.descTextView placeholderTopLeftPadding:12];
    self.descTextView.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10);
    
    self.descTextView.delegate = self;
    self.descTextView.scrollEnabled = TRUE;
    
    [self.noteContainerView addSubview:self.descTextView];
}

- (void)setupNoteLayout {
    
    CGFloat maxViewHeight = self.view.safeAreaLayoutGuide.layoutFrame.size.height;
    kNoteHeight = maxViewHeight;
    
    kNoteHeight -= kKeyboardHeight;
    
    kCurrentHeaderHeight = [self.headerTextView sizeThatFits:CGSizeMake(self.headerTextView.width, MAXFLOAT)].height;
    kMaxHeaderHeight = kCurrentHeaderHeight * 2;
    
    kCurrentDescHeight = [self.descTextView sizeThatFits:CGSizeMake(self.descTextView.width, MAXFLOAT)].height;
    kMaxDescHeight = kNoteHeight - kMaxHeaderHeight;
    
    [self.noteContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
        make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).with.offset(10);
        make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).with.offset(-10);
        make.height.mas_equalTo(kNoteHeight);
    }];
    
    [self.headerTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.noteContainerView.mas_top);
        make.left.equalTo(self.noteContainerView.mas_left);
        make.right.equalTo(self.noteContainerView.mas_right);
        make.height.mas_equalTo(kCurrentHeaderHeight);
    }];
    
    [self.separatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(kSeparatorHeight);
        make.width.mas_equalTo(kSeparatorWidth);
        make.centerX.equalTo(self.headerTextView.mas_centerX);
        make.top.equalTo(self.headerTextView.mas_bottom).offset(-kSeparatorHeight);
    }];
    
    [self.descTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerTextView.mas_bottom);
        make.left.equalTo(self.noteContainerView.mas_left);
        make.right.equalTo(self.noteContainerView.mas_right);
        make.bottom.equalTo(self.noteContainerView.mas_bottom);
    }];
    
}

#pragma mark - Layout

- (void)updateNoteConstraints {
    CGFloat maxViewHeight = self.view.safeAreaLayoutGuide.layoutFrame.size.height;
    kNoteHeight = maxViewHeight;
    
    kNoteHeight -= kKeyboardHeight;
    [self.noteContainerView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(kNoteHeight);
    }];
    [self.headerTextView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(kCurrentHeaderHeight);
    }];
    [self scrollToCursor];
}

- (void)scrollToCursor {
    NSRange range = self.descTextView.selectedRange;
    [self.descTextView scrollRangeToVisible:range];
}

- (void)updateLayoutWithAnimation {
    [UIView animateWithDuration:0.5 animations:^{
        [self updateNoteConstraints];
    }];
}

#pragma mark - SetupValues

- (void)setupValues {
    if (_item) {
        self.headerTextView.text = _item.header;
        [self.headerTextView displayContentOrPlaceholder];
        
        self.descTextView.text = _item.desc;
        [self.descTextView displayContentOrPlaceholder];
    }
}

#pragma mark - SetupDelegates

- (void)setupDelegates {
    self.noteHandler.personalNoteDelegate = self;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    CustomTextView *customTextView = (CustomTextView *)textView;
    if (self.headerTextView.text.length > 0 || self.descTextView.text.length > 0) {
        _saveButton.enabled = YES;
    } else {
        _saveButton.enabled = NO;
    }
    [customTextView displayContentOrPlaceholder];
    [self calculateTextViewHeight:customTextView];
}

- (void)calculateTextViewHeight:(CustomTextView *)textView {
    if (textView.tag == kHeaderTag) {
        CGFloat newHeight = [self.headerTextView sizeThatFits:CGSizeMake(self.headerTextView.width, MAXFLOAT)].height;
        if (newHeight >= kMaxHeaderHeight) {
            self.headerTextView.scrollEnabled = TRUE;
        }
        else {
            if (newHeight != kCurrentHeaderHeight) {
                kCurrentHeaderHeight = newHeight;
                [self updateLayoutWithAnimation];
            }
            self.headerTextView.scrollEnabled = FALSE;
        }
    }
    else if (textView.tag == kDescTag) {
        kMaxDescHeight = kNoteHeight - kMaxHeaderHeight;
        CGFloat newHeight = [self.descTextView sizeThatFits:CGSizeMake(self.descTextView.width, MAXFLOAT)].height;
        if (newHeight >= kMaxDescHeight) {
            self.descTextView.scrollEnabled = TRUE;
        }
        else {
            if (newHeight != kCurrentDescHeight) {
                kCurrentDescHeight = newHeight;
                [self updateLayoutWithAnimation];
            }
            self.descTextView.scrollEnabled = FALSE;
        }
    }
}

#pragma mark - Keyboard

- (void)registerKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChange:)
                                                 name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    kKeyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    [self updateLayoutWithAnimation];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    kKeyboardHeight = 0;
    [self updateLayoutWithAnimation];
}

- (void)keyboardWillChange:(NSNotification *)notification {
    kKeyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    [self updateLayoutWithAnimation];
}

#pragma mark - Handle save note

- (void)saveNote {
    [self.view endEditing:YES];
    [self handleSavingNote];
}

- (void)handleSavingNote {
    if ([self.headerTextView isEmpty] && [self.descTextView isEmpty]) {
        return;
    }
    NSString *header = self.headerTextView.text;
    NSString *desc = self.descTextView.text;
    NSString *roomCode = [[RoomManager sharedManager] getRoomCode];
    if (!_item) {
        NSDictionary *dict = @{@"roomCode":roomCode,
                               @"header": header ?: [NSNull null],
                               @"desc": desc ?: [NSNull null]};
        NSMutableDictionary *cleanDict = [NSMutableDictionary dictionaryWithDictionary:dict];
        [cleanDict removeObjectsForKeys:[dict allKeysForObject:[NSNull null]]];
        
        [self.noteHandler addPersonalNoteWithInfo:[NSDictionary dictionaryWithDictionary:cleanDict]];
    }
    else {
        NSString *code = _item.code;
        NSDictionary *dict = @{@"roomCode":roomCode,
                               @"code":code,
                               @"header": header ?: [NSNull null],
                               @"desc": desc ?: [NSNull null],
                               @"status":@"ACTIVE"};
        NSMutableDictionary *cleanDict = [NSMutableDictionary dictionaryWithDictionary:dict];
        [cleanDict removeObjectsForKeys:[dict allKeysForObject:[NSNull null]]];
        
        [self.noteHandler updatePersonalNoteWithInfo:[NSDictionary dictionaryWithDictionary:cleanDict]];
    }
}

#pragma mark - NoteHandlerDelegate

- (void)onFinishAddNewNote:(PersonalNoteItem *)newNote error:(NoteHandlerErrorCode)errorCode {
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (errorCode) {
            case NoteHandlerError:
                
                break;
            default:
                self.item = newNote;
                [self _processAfterAddNewNoteSuccess];
                break;
        }
    });
}

- (void)onFinishUpdateNote:(PersonalNoteItem *)note error:(NoteHandlerErrorCode)errorCode {
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (errorCode) {
            case NoteHandlerError:
                
                break;
            default:
                self.item = note;
                [self _processAfterUpdateNoteSuccess];
                break;
        }
    });
}

- (void)_processAfterAddNewNoteSuccess {
    [self.delegate controller:self addNewNote:_item];
    UINavigationController *navigationController = self.navigationController;
    [navigationController popViewControllerAnimated:YES];
}

- (void)_processAfterUpdateNoteSuccess {
    [self.delegate controller:self updateNote:_item];
    UINavigationController *navigationController = self.navigationController;
    [navigationController popViewControllerAnimated:YES];
}

#pragma mark - Lazy loading

- (NoteHandler *)noteHandler {
    if (_noteHandler) {
        return _noteHandler;
    }
    _noteHandler = [[NoteHandler alloc] init];
    return _noteHandler;
}
@end
