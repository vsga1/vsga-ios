//
//  NoteDetailViewController.h
//  VStudy
//
//  Created by LAP14011 on 18/03/2023.
//

#import <UIKit/UIKit.h>
#import "PersonalNoteItem.h"

NS_ASSUME_NONNULL_BEGIN

@class NoteDetailViewController;

@protocol NoteDetailViewControllerDelegate <NSObject>

- (void)controller:(NoteDetailViewController *)controller addNewNote:(PersonalNoteItem *)newItem;

- (void)controller:(NoteDetailViewController *)controller updateNote:(PersonalNoteItem *)newItem;

@end

@interface NoteDetailViewController : UIViewController

@property (nonatomic, weak) id <NoteDetailViewControllerDelegate> delegate;

@property (strong, nonatomic) PersonalNoteItem *item;

@end

NS_ASSUME_NONNULL_END
