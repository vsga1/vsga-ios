//
//  PersonalNoteDataSourceProcessor.m
//  VStudy
//
//  Created by LAP14011 on 18/03/2023.
//

#import "PersonalNoteDataSourceProcessor.h"

@implementation PersonalNoteDataSourceProcessor

- (instancetype)initWithDiffDataSource:(UICollectionViewDiffableDataSource *)dataSource {
    self = [super init];
    if (self) {
        self.dataSource = dataSource;
    }
    return self;
}

- (NSDiffableDataSourceSnapshot *)appendItemArray:(TSMutableArray<PersonalNoteItem *> *)noteItemArray {
    NSDiffableDataSourceSnapshot<Section *, PersonalNoteItem *> *snapshot = [[NSDiffableDataSourceSnapshot alloc] init];
    NSArray <Section *> *sections = self.dataSource.snapshot.sectionIdentifiers;
    if (sections.count == 0) {
        Section *section = [[Section alloc] initWithSection:SectionMain];
        sections = [[NSArray alloc] initWithObjects:section, nil];
    }
    [snapshot appendSectionsWithIdentifiers:sections];
    if (noteItemArray.count > 0) {
        [snapshot appendItemsWithIdentifiers:noteItemArray intoSectionWithIdentifier:sections[SectionMain]];
    }
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)appendItem:(PersonalNoteItem *)noteItem {
    NSDiffableDataSourceSnapshot<Section *, PersonalNoteItem *> *snapshot = self.dataSource.snapshot;
    [snapshot appendItemsWithIdentifiers:@[noteItem]];
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)insertItem:(PersonalNoteItem *)noteItem {
    NSDiffableDataSourceSnapshot<Section *, PersonalNoteItem *> *snapshot = self.dataSource.snapshot;
    PersonalNoteItem *firstItem = snapshot.itemIdentifiers.firstObject;
    if (firstItem) {
        [snapshot insertItemsWithIdentifiers:@[noteItem] beforeItemWithIdentifier:firstItem];
    }
    else {
        [snapshot appendItemsWithIdentifiers:@[noteItem]];
    }
    return snapshot;
}
- (NSDiffableDataSourceSnapshot *)reloadItem:(PersonalNoteItem *)noteItem {
    NSDiffableDataSourceSnapshot<Section *, PersonalNoteItem *> *snapshot = self.dataSource.snapshot;
    [snapshot reloadItemsWithIdentifiers:@[noteItem]];
//    NSLog(@"reloaded item %@",snapshot.reloadedItemIdentifiers);
    return snapshot;
}

- (NSDiffableDataSourceSnapshot *)reloadItemArray:(TSMutableArray<PersonalNoteItem *> *)noteItemArray {
    NSDiffableDataSourceSnapshot<Section *, PersonalNoteItem *> *snapshot = self.dataSource.snapshot;
    [snapshot reloadItemsWithIdentifiers:noteItemArray];
//    NSLog(@"reloaded item array %@",snapshot.reloadedItemIdentifiers);
    return snapshot;
}

@end
