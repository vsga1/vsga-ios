//
//  RoomDetailViewController.m
//  VSGA
//
//  Created by LAP14011 on 31/01/2023.
//


#import "RoomDetailViewController.h"
#import <AgoraRtcKit/AgoraRtcKit.h>
#import <AVFoundation/AVFoundation.h>
#import <JavaScriptCore/JavaScriptCore.h>
#import <HMSegmentedControl/HMSegmentedControl.h>
#import <Masonry/Masonry.h>

#import "Macro.h"
#import "UIView+Extension.h"

#import "ChatManager.h"
#import "MySocketManager.h"
#import "RoomManager.h"
#import "AccountManager.h"

#import "EmbedYoutubeViewController.h"
#import "CameraViewController.h"

#import "ChatViewController.h"
#import "NoteViewController.h"
#import "TaskViewController.h"
#import "MemberViewController.h"

#import "TabBarCollectionViewCell.h"

static CGFloat kTabBarHeight;

@interface RoomDetailViewController () <UICollectionViewDelegate, UICollectionViewDataSource, AgoraRtcEngineDelegate>

@property (nonatomic, strong) UICollectionView *tabBarCollectionView;

@property (nonatomic, strong) UIView *videoCallView;

@property (nonatomic, strong) UIView *tabBarView;

@property (nonatomic, assign) BOOL isMuteMic;

@property (nonatomic, assign) BOOL isOffCamera;

@property (nonatomic, strong) AgoraRtcEngineKit *agoraEngine;

@property (nonatomic, strong) RoomItem *roomItem;

@property (nonatomic, strong) UIView *childSegmentedView;

@property (nonatomic, strong) HMSegmentedControl *segmentedControl;

@property (nonatomic, strong) EmbedYoutubeViewController *embedVC;

@property (nonatomic, strong) CameraViewController *cameraVC;

@property (nonatomic, strong) MySocketManager *socketManager;

@end

@implementation RoomDetailViewController {
    NSArray *_tabBarTitle;
    NSArray *_tabBarIcon;
}

- (instancetype)initWithRoomItem:(RoomItem *)roomItem {
    self = [super init];
    if (self) {
        self.roomItem = roomItem;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self setupViews];
    [self setupValues];
    [self setupSocket];
    [self setupVoiceCall];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupReceiveMessageSocket];
    [self updateChatTabIcon];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    // Layout tab bar collection view
    CGFloat tabBarHeight = 100.0;
    self.tabBarCollectionView.frame = CGRectMake(0, self.view.height - tabBarHeight, self.view.width, tabBarHeight);

}

#pragma mark - Setup Views

- (void)setupViews {
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupNavigationBar];
    [self setupTabbar];
    [self setupSegmentedControl];
}

- (void)setupNavigationBar {
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    navigationBar.barTintColor = [UIColor whiteColor];

    // Create a label for the title
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = self.roomItem.name;
    titleLabel.font = kSemiBold24;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.numberOfLines = 2;
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;

    // Create a container view for the title label
    UIView *titleContainerView = [[UIView alloc] init];
    [titleContainerView addSubview:titleLabel];

    // Add constraints to limit the width of the title view
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [titleLabel.leadingAnchor constraintEqualToAnchor:titleContainerView.leadingAnchor].active = YES;
    [titleLabel.trailingAnchor constraintEqualToAnchor:titleContainerView.trailingAnchor].active = YES;
    [titleLabel.topAnchor constraintEqualToAnchor:titleContainerView.topAnchor].active = YES;
    [titleLabel.bottomAnchor constraintEqualToAnchor:titleContainerView.bottomAnchor].active = YES;
    [titleLabel.widthAnchor constraintLessThanOrEqualToConstant:250].active = YES;

    // Set the title view of the navigation item
    self.navigationItem.titleView = titleContainerView;

    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Logo"]];
    logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UIBarButtonItem *logoBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:logoImageView];
    
    self.navigationItem.leftBarButtonItem = logoBarButtonItem;
    self.navigationItem.leftItemsSupplementBackButton = YES;
    
    logoImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [NSLayoutConstraint activateConstraints:@[
        [logoImageView.leadingAnchor constraintEqualToAnchor:logoBarButtonItem.customView.leadingAnchor],
        [logoImageView.topAnchor constraintEqualToAnchor:logoBarButtonItem.customView.topAnchor],
        [logoImageView.bottomAnchor constraintEqualToAnchor:logoBarButtonItem.customView.bottomAnchor],
        [logoImageView.widthAnchor constraintEqualToConstant:32.0],
        [logoImageView.heightAnchor constraintEqualToConstant:32.0]
    ]];
    
    UIBarButtonItem *leaveRoomButton = [[UIBarButtonItem alloc] initWithTitle:@"Leave" style:UIBarButtonItemStylePlain target:self action:@selector(leaveRoom)];
    NSDictionary *attributes = @{
        NSForegroundColorAttributeName: kPrimaryColor,
        NSFontAttributeName: kSemiBold18
    };
    [leaveRoomButton setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [leaveRoomButton setTitleTextAttributes:attributes forState:UIControlStateHighlighted];
    self.navigationItem.rightBarButtonItem = leaveRoomButton;
}

- (void)setupTabbar {
    // Create the tab bar collection view
    UICollectionViewFlowLayout *tabBarLayout = [[UICollectionViewFlowLayout alloc] init];
    tabBarLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    tabBarLayout.minimumInteritemSpacing = 20;
    tabBarLayout.minimumLineSpacing = 20;
    tabBarLayout.itemSize = CGSizeMake(70, 70);
    
    self.tabBarCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:tabBarLayout];
    [self.tabBarCollectionView registerClass:[TabBarCollectionViewCell class] forCellWithReuseIdentifier:TabBarCollectionViewCell.cellIdentifier];
    self.tabBarCollectionView.dataSource = self;
    self.tabBarCollectionView.delegate = self;
    self.tabBarCollectionView.contentInset = UIEdgeInsetsMake(20, 20, 20, 20);
    self.tabBarCollectionView.backgroundColor = UIColor.clearColor;
    [self.view addSubview:self.tabBarCollectionView];
}

- (void)setupSegmentedControl {
    [self createSegmentedControl];
    
//    [self createChildViewControllers];
    [self createSegmentedViews];
}

#pragma mark - Segmented Control

- (void)createSegmentedControl {
    NSArray *itemArray = [NSArray arrayWithObjects: @"Camera Call", @"Youtube Video", nil];
    self.segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:itemArray];
    self.segmentedControl.selectionIndicatorHeight = 4.0;
    self.segmentedControl.selectionIndicatorColor = kPrimaryColor; // Set the color of the selection indicator
    self.segmentedControl.segmentEdgeInset = UIEdgeInsetsMake(0, 5, 0, 5); // Set the inset for the segments
    self.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationBottom;

    self.segmentedControl.titleTextAttributes = @{
        NSFontAttributeName: kRegular18, // Set the font size
        NSForegroundColorAttributeName: [UIColor blackColor] // Set the text color
    };
    self.segmentedControl.selectedTitleTextAttributes = @{
        NSForegroundColorAttributeName: kPrimaryColor // Set the text color for the selected segment
    };
    
    [self.segmentedControl addTarget:self action:@selector(segmentValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.segmentedControl];
    
    [self.segmentedControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop).offset(10);
        make.leading.mas_equalTo(self.view.mas_leading);
        make.trailing.mas_equalTo(self.view.mas_trailing);
        make.height.mas_equalTo(50);
    }];
    [self.segmentedControl addTarget:self
                              action:@selector(segmentValueChanged:)
                    forControlEvents:UIControlEventValueChanged];
    self.segmentedControl.selectedSegmentIndex = 0;
}

//- (void)createChildViewControllers {
//    self.embedVC = [[EmbedYoutubeViewController alloc] init];
//    self.cameraVC = [[CameraViewController alloc] init];
//}

#pragma mark - Lazy loading
- (EmbedYoutubeViewController *)embedVC {
    if (_embedVC) {
        return _embedVC;
    }
    _embedVC = [[EmbedYoutubeViewController alloc] init];
    [self addSegmentChildViewController:_embedVC];
    return _embedVC;
}

- (CameraViewController *)cameraVC {
    if (_cameraVC) {
        return _cameraVC;
    }
    _cameraVC = [[CameraViewController alloc] init];
    [self addSegmentChildViewController:_cameraVC];
    return _cameraVC;
}

- (void)segmentValueChanged:(UISegmentedControl *)sender {
    [self changeViewWithSelectedSegmentIndex:sender.selectedSegmentIndex];
}

- (void)changeViewWithSelectedSegmentIndex:(NSInteger)selectedSegmentIndex {
    if (selectedSegmentIndex == 0) {
        [self.cameraVC.view setHidden:NO];
        [self.embedVC.view setHidden:YES];
    } else if (selectedSegmentIndex == 1) {
        [self.cameraVC.view setHidden:YES];
        [self.embedVC.view setHidden:NO];
    }
}

- (void)createSegmentedViews {
    self.childSegmentedView = [[UIView alloc] init];
    [self.view addSubview:self.childSegmentedView];
    
    [self.childSegmentedView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.segmentedControl.mas_bottom);
        make.leading.mas_equalTo(self.view.mas_leading);
        make.trailing.mas_equalTo(self.view.mas_trailing);
        make.bottom.mas_equalTo(self.tabBarCollectionView.mas_top);
    }];
    
    ///Default is camera
//    [self addSegmentChildViewController:self.cameraVC];
    [self.cameraVC.view setHidden:NO];
}

- (void)addSegmentChildViewController:(UIViewController *)childController {
    [self addChildViewController:childController];
    [self.childSegmentedView addSubview:childController.view];
    [childController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.childSegmentedView);
    }];
    [childController didMoveToParentViewController:self];
}

- (void)removeSegmentChildViewController:(UIViewController *)childController {
    [childController willMoveToParentViewController:nil];
    [childController.view removeFromSuperview];
    [childController removeFromParentViewController];
}

#pragma mark - Setup Values

- (void)setupValues {
    [self setUpTabBarValues];
}

- (void)setUpTabBarValues {
    _tabBarTitle = @[@"Micro", @"Camera", @"Chat", @"Member", @"Task", @"Note", @"Setting"];
    _tabBarIcon = @[[UIImage imageNamed:@"NoMicTab"],[UIImage imageNamed:@"NoCameraTab"],[UIImage imageNamed:@"ChatTab"],[UIImage imageNamed:@"MemberTab"],[UIImage imageNamed:@"TaskTab"],[UIImage imageNamed:@"NoteTab"],[UIImage imageNamed:@"SettingTab"]];
    self.isMuteMic = TRUE;
    self.isOffCamera = TRUE;
}

#pragma mark - Setup Socket

- (void)setupSocket {
    self.socketManager = [MySocketManager sharedInstance];
    [self.socketManager connect];
    
    [self setupJoinRoomSocket];
    [self setupRoomDataSocket];
}

- (void)setupJoinRoomSocket {
    NSString *roomCode = [[RoomManager sharedManager] getRoomCode];
    User *user = [[AccountManager sharedManager] getCurrentUser];

    // Emit the 'join room' event with the room code and username
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:@{@"roomCode": roomCode,
                                                                                  @"username": user.username,
                                                                                  @"role":user.role}];
    if (user.avatarURLString) {
        [data setObject:user.avatarURLString forKey: @"avatar_img_url"];
    }
    
    [self.socketManager emit:kJoinRoomEvent with:data completion:^(BOOL success) {
    
    }];
}

- (void)setupRoomDataSocket {
    [self.socketManager listenForRoomDataEventWithCompletion:^(NSArray<User *> * memberArray) {
        [[RoomManager sharedManager] createMemberArrayWithArray:memberArray];
        
        //TODO: call to update in table view
    }];
}

- (void)setupReceiveMessageSocket {
    __weak typeof(self) weakSelf = self;
    [[MySocketManager sharedInstance] listenForReceiveMessageEventWithCompletion:^(MessageItem *messageItem) {
        [weakSelf handleNewComingMessage:messageItem];
    }];
}

#pragma mark - Handle Coming Message

- (void)handleNewComingMessage:(MessageItem *)messageItem {
    [[ChatManager sharedManager] insertAtFirst:messageItem];
    [[ChatManager sharedManager] increaseUnreadCount:1];
    [self updateChatTabIcon];
}

- (void)updateChatTabIcon {
    UIImage *newImage = nil;
    if ([[ChatManager sharedManager] getUnreadCount] != 0) {
        newImage = [UIImage imageNamed:@"NewMessageTab"];
    }
    else {
        newImage = [UIImage imageNamed:@"ChatTab"];
    }
    TabBarCollectionViewCell *cell = (TabBarCollectionViewCell *) [self.tabBarCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:2 inSection:0]];
    [self changeTabBarCell:cell newImage:newImage];
}

- (void)resetUnreadCount {
    [[ChatManager sharedManager] resetUnreadCount];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 7;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TabBarCollectionViewCell *cell = (TabBarCollectionViewCell *) [collectionView dequeueReusableCellWithReuseIdentifier:TabBarCollectionViewCell.cellIdentifier forIndexPath:indexPath];
    
    cell.imageView.image = [_tabBarIcon objectAtIndex:indexPath.item];
    cell.label.text = [_tabBarTitle objectAtIndex:indexPath.item];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.tabBarCollectionView) {
        if (indexPath.item == 0) {
            self.isMuteMic = !self.isMuteMic;
            [self.agoraEngine muteLocalAudioStream:self.isMuteMic];
//            if (result == 0) {
//                int i = 0;
//            }
            UIImage *newImage;
            if (self.isMuteMic) {
                newImage = [UIImage imageNamed:@"NoMicTab"];
            }
            else {
                newImage = [UIImage imageNamed:@"MicTab"];
            }
            TabBarCollectionViewCell *cell = (TabBarCollectionViewCell *) [collectionView cellForItemAtIndexPath:indexPath];
            [self changeTabBarCell:cell newImage:newImage];
        }
        if (indexPath.item == 1) {
            self.isOffCamera = !self.isOffCamera;
            UIImage *newImage;
            if (self.isOffCamera) {
                newImage = [UIImage imageNamed:@"NoCameraTab"];
            }
            else {
                newImage = [UIImage imageNamed:@"CameraTab"];
            }
            TabBarCollectionViewCell *cell = (TabBarCollectionViewCell *) [collectionView cellForItemAtIndexPath:indexPath];
            [self changeTabBarCell:cell newImage:newImage];
        }
        if (indexPath.item == 2) {
            ChatViewController *chatVC = [[ChatViewController alloc] init];
            [self resetUnreadCount];
            [[MySocketManager sharedInstance] off:kReceiveMessageEvent];
            [self.navigationController pushViewController:chatVC animated:YES];
        }
        if (indexPath.item == 3) {
            MemberViewController *memberVC = [[MemberViewController alloc] init];
            [self.navigationController pushViewController:memberVC animated:YES];
        }
        if (indexPath.item == 4) {
            TaskViewController *taskVC = [[TaskViewController alloc] init];
            [self.navigationController pushViewController:taskVC animated:YES];
        }
        if (indexPath.item == 5) {
            NoteViewController *noteVC = [[NoteViewController alloc] init];
            [self.navigationController pushViewController:noteVC animated:YES];
        }
    }
}

#pragma mark - Change tab bar icon

- (void)changeTabBarCell:(TabBarCollectionViewCell *)cell newImage:(UIImage *)newImage {
    [UIView transitionWithView:self.view
                      duration:0.1
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
        cell.imageView.image = newImage;
    } completion:nil];
}

#pragma mark - Leave Room

- (void)leaveRoom {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self leaveChannel];
    [[ChatManager sharedManager] resetMessages];
    [[RoomManager sharedManager] leaveRoom];
    [[MySocketManager sharedInstance] off:kJoinRoomEvent];
    [[MySocketManager sharedInstance] off:kReceiveMessageEvent];
    [[MySocketManager sharedInstance] emit:kOutRoomEvent with:[NSDictionary new] completion:^(BOOL success) {
        [[MySocketManager sharedInstance] disconnect];
    }];
}

#pragma mark - Voice

- (BOOL)checkForMicPermission {
    BOOL hasPermissions = [self avAuthorizationWithMediaType:AVMediaTypeAudio];
    return hasPermissions;
}

- (BOOL)avAuthorizationWithMediaType:(AVMediaType)mediaType {
    AVAuthorizationStatus mediaAuthorizationStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    switch (mediaAuthorizationStatus) {
        case AVAuthorizationStatusDenied:
        case AVAuthorizationStatusRestricted:
            return NO;
        case AVAuthorizationStatusAuthorized:
            return YES;
        case AVAuthorizationStatusNotDetermined:
            return [self requestAccessForMediaType:mediaType];
        default:
            return NO;
    }
}

- (BOOL)requestAccessForMediaType:(AVMediaType)mediaType {
    __block BOOL granted = NO;
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL result) {
        granted = result;
        dispatch_semaphore_signal(sema);
    }];
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    return granted;
}

- (void)showMessageWithTitle:(NSString *)title text:(NSString *)text delay:(NSInteger)delay {
    dispatch_time_t deadlineTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC));
    dispatch_after(deadlineTime, dispatch_get_main_queue(), ^{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:text preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:nil];
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
}

- (void)generateAgoraTokenWithChannelName:(NSString *)channelName
                                    role:(NSString *)role
                               tokenType:(NSString *)tokenType
                                     uid:(NSString *)uid
                                 expiry:(NSInteger)expiry
                       completionHandler:(void (^)(NSString *, NSError *))completionHandler {
    // Construct the URL with the provided parameters
    NSString *urlString = [NSString stringWithFormat:@"https://agora-token-service-production-7d1c.up.railway.app/rtc/%@/%@/%@/%@/?expiry=%ld",
                           channelName, role, tokenType, uid, (long)expiry];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            completionHandler(nil, error);
            return;
        }
        
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSString *token = responseDict[@"rtcToken"];
        completionHandler(token, nil);
    }];
    
    [dataTask resume];
}

- (void)joinChannel {
    BOOL hasPermissions = [self checkForMicPermission];
    if (!hasPermissions) {
        [self showMessageWithTitle:@"Error" text:@"Permissions were not granted" delay:1];
        return;
    }
    
    AgoraRtcChannelMediaOptions *option = [AgoraRtcChannelMediaOptions new];
    option.clientRoleType = AgoraClientRoleBroadcaster;
    option.channelProfile = AgoraChannelProfileLiveBroadcasting;
    
    
    
    NSString *channelName = [[RoomManager sharedManager] getRoomCode];
   
    NSString *role = @"1";
    NSString *tokenType = @"uid";
    NSString *uid = @"0";
    NSInteger expiry = 60 * 60 * 24 * 180;

    [self generateAgoraTokenWithChannelName:channelName
                                       role:role
                                  tokenType:tokenType
                                        uid:uid
                                    expiry:expiry
                          completionHandler:^(NSString *token, NSError *error) {
        if (error) {
            NSLog(@"Token generation failed with error: %@", error);
        } else {
            NSLog(@"Generated token: %@", token);
            // Use the generated token as needed
            
            
            int result = [self.agoraEngine
                          joinChannelByToken:token
                          channelId:channelName
                          uid:0
                          mediaOptions:option
                          joinSuccess:^(NSString * _Nonnull channel, NSUInteger uid, NSInteger elapsed) {
                NSLog(@"Tamnvm>>join success");
            }];
            NSLog(@"Tamnvm>>joinChannelByToken result %d",result);
            if (result == 0) {
        //        self.joined = YES;
                NSLog(@"Tamnvm>>Successfully joined the channel");
        //        [self showMessageWithTitle:@"Success" text:@"Successfully joined the channel" delay:1];
            }
        }
    }];
    
    
    BOOL enableAudio = YES; // Whether to enable audio publishing
    BOOL enableVideo = YES; // Whether to enable video publishing
    
    [self.agoraEngine setLocalPublishFallbackOption:AgoraStreamFallbackOptionAudioOnly];
    [self.agoraEngine setRemoteSubscribeFallbackOption:AgoraStreamFallbackOptionAudioOnly];
    [self.agoraEngine enableLocalAudio:enableAudio];
    [self.agoraEngine enableLocalVideo:enableVideo];
    [self.agoraEngine muteLocalAudioStream:YES];
    [self.agoraEngine muteLocalVideoStream:NO];
    [self.agoraEngine muteAllRemoteAudioStreams:NO];
    [self.agoraEngine muteAllRemoteVideoStreams:NO];
}

- (void)leaveChannel {
    int result = [self.agoraEngine leaveChannel:nil];
    // Check if leaving the channel was successful and set joined Bool accordingly
    if (result == 0) {
//        self.joined = NO;
    }
}

- (void)setupVoiceCall {
    NSString *appID = kVoiceCallAppID;
    AgoraRtcEngineConfig *config = [AgoraRtcEngineConfig new];
    config.appId = appID;
    
    self.agoraEngine = [AgoraRtcEngineKit sharedEngineWithConfig:config delegate:self];
    [self joinChannel];
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine didJoinChannel:(NSString *)channel withUid:(NSUInteger)uid elapsed:(NSInteger)elapsed {
    
}
@end
