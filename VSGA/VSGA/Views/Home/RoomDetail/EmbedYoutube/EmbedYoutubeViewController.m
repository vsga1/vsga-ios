//
//  EmbedYoutubeViewController.m
//  VStudy
//
//  Created by TamNVM on 24/05/2023.
//
@import YouTubeiOSPlayerHelper;

#import "EmbedYoutubeViewController.h"
#import <WebKit/WebKit.h>
#import <Masonry/Masonry.h>
#import "Macro.h"
#import "UIView+Extension.h"
#import "InputTextField.h"
#import "CustomButton.h"

const CGFloat padding = 10;
const CGFloat inputPadding = 10;
const CGFloat buttonWidth = 180;
const CGFloat inputHeight = 40;
const NSString *kYTPlayerKey = @"AIzaSyAKPH5EZYu_RDrKExo1Mf5aiCo7jIZbfE0";
@interface EmbedYoutubeViewController () <YTPlayerViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) NSString *currentVideoId;

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIButton *clearButton;
@property (nonatomic, strong) CustomButton *playButton;
@property (nonatomic, strong) YTPlayerView *playerView;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;

@end

@implementation EmbedYoutubeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    self.currentVideoId = @"0QLt-ZI1r6o";
    
    [self setupViews];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(backgroundTapped:)];
    [self.view addGestureRecognizer:tapRecognizer];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGFloat clearIconSize = 40;
    CGFloat textFieldWidth = self.view.width - 2 * padding - clearIconSize;
    self.textField.frame = CGRectMake(padding,
                                      padding,
                                      textFieldWidth,
                                      inputHeight);
    self.clearButton.frame = CGRectMake(self.textField.right, padding, clearIconSize, clearIconSize);
    CGRect frame = CGRectMake((self.view.width - buttonWidth)/2,
                              self.textField.bottom + inputPadding,
                              buttonWidth,
                              inputHeight);
    self.playButton.frame = frame;
    CGFloat ratio = 16/9;
    CGFloat videoWidth = self.view.width;
    CGFloat videoHeight = videoWidth * ratio;
    self.playerView.frame = CGRectMake(0,
                                       self.playButton.bottom + padding,
                                       videoWidth,
                                       videoHeight);
    
}
- (void)setupViews {
    [self setupTextField];
    [self setupPlayButton];
    [self setupPlayerView];
}

- (void)setupPlayerView {
    CGFloat ratio = 16/9;
    CGFloat videoWidth = self.view.width;
    CGFloat videoHeight = videoWidth * ratio;
    
    self.playerView = [[YTPlayerView alloc] initWithFrame:CGRectMake(0,
                                                                     self.playButton.bottom + padding,
                                                                     videoWidth,
                                                                     videoHeight)];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        BOOL result = [self.playerView loadWithVideoId:self.currentVideoId playerVars:@{@"key": kYTPlayerKey}];
        NSLog(@"tamnvm>>YTPlayerView>> result %@", @(result));
    });
    
    self.playerView.delegate = self;
    [self.view addSubview:self.playerView];
    
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleMedium];
    self.spinner.center = self.playerView.center;
    self.spinner.hidesWhenStopped = YES;
    [self.view addSubview:self.spinner];
    [self.spinner startAnimating];
}

- (void)setupTextField {
    CGFloat clearIconSize = 40;
    
    CGRect frame = CGRectMake(padding,
                              padding,
                              self.view.width - 2 * padding - clearIconSize,
                              inputHeight);
    self.textField = [[UITextField alloc] initWithFrame:frame];
    self.textField.placeholder = @"Enter YouTube Link";
    self.textField.text = @"https://www.youtube.com/watch?v=0QLt-ZI1r6o";
    self.textField.font = kRegular16;
    self.textField.borderStyle = UITextBorderStyleRoundedRect;
    self.textField.delegate = self;
    [self.view addSubview:self.textField];
    
    // Create and configure the clear button with a clear icon image
    self.clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.clearButton.frame = CGRectMake(self.textField.right, padding, clearIconSize, clearIconSize); // Adjust the frame as needed
    UIImage *clearIconImage = [UIImage systemImageNamed:@"xmark.circle.fill"];
    clearIconImage = [clearIconImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.clearButton setImage:clearIconImage forState:UIControlStateNormal];
    [self.clearButton setTintColor:kPrimaryColor];
    
    [self.clearButton addTarget:self action:@selector(clearButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.clearButton];
}

- (void)clearButtonTapped {
    self.textField.text = @"";
}

- (void)setupPlayButton {
    CGRect frame = CGRectMake((self.view.width - buttonWidth)/2,
                              self.textField.bottom + inputPadding,
                              buttonWidth,
                              inputHeight);
    self.playButton = [[CustomButton alloc] initWithFrame:frame
                                                    title:@"Play"
                                               titleColor:UIColor.whiteColor
                                                titleFont:kRegular16
                                          backgroundColor:kPrimaryColor
                                              borderColor:kPrimaryColor.CGColor];
    [self.playButton addTarget:self action:@selector(playButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.playButton];
}

- (void)playButtonTapped {
    NSString *youtubeLink = self.textField.text;
    NSString *videoId = [self extractVideoIdFromLink:youtubeLink];
    
    if (videoId) {
        if ([videoId isEqualToString:self.currentVideoId]) {
            return;
        }
        self.currentVideoId = videoId;
        [self.spinner startAnimating];
        // Valid YouTube link, play the video
        [self.playerView loadWithVideoId:videoId];
        [self.playerView playVideo];
    } else {
        // Invalid YouTube link, show an error message
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Invalid Link" message:@"Please enter a valid YouTube link" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (NSString *)extractVideoIdFromLink:(NSString *)link {
    // Regular expression pattern to match YouTube video IDs
    NSString *pattern = @"(?:youtu\\.be\\/|youtube\\.com\\/watch\\?.*v=|youtube\\.com\\/embed\\/|youtube\\.com\\/v\\/|youtube\\.com\\/user\\/[^#]*#([^\\/]*\\/){1,4}?v=|youtube\\.com\\/s[^#]*#([^\\/]*\\/){1,4}?v=)\\/?([^#\\&\\?\\n]*)";
    
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    
    if (error) {
        NSLog(@"Regex error: %@", error.localizedDescription);
        return nil;
    }
    
    NSTextCheckingResult *match = [regex firstMatchInString:link options:0 range:NSMakeRange(0, link.length)];
    
    if (match) {
        NSRange videoIDRange = [match rangeAtIndex:match.numberOfRanges - 1];
        NSString *videoID = [link substringWithRange:videoIDRange];
        return videoID;
    }
    
    return nil;
}

- (void)backgroundTapped:(UITapGestureRecognizer *)tabRecognizer {
    if ([self.textField isFirstResponder]) {
        [self.textField resignFirstResponder];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView {
    [self.spinner stopAnimating];
}

- (void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state {
    NSLog(@"tamnvm>>YTPlayerView>> didChangeToState %ld", (long)state);
}

- (void)playerView:(YTPlayerView *)playerView receivedError:(YTPlayerError)error {
    NSLog(@"tamnvm>>YTPlayerView>> receivedError %ld", (long)error);
}

@end
