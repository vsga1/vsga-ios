//
//  EmbedYoutubeViewController.h
//  VStudy
//
//  Created by TamNVM on 24/05/2023.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EmbedYoutubeViewController : UIViewController

@property (nonatomic, assign) CGSize viewSize;

@end

NS_ASSUME_NONNULL_END
