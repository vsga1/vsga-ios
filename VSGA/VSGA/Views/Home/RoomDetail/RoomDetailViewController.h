//
//  RoomDetailViewController.h
//  VStudy
//
//  Created by LAP14011 on 04/03/2023.
//

#import <UIKit/UIKit.h>

#import "RoomItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface RoomDetailViewController : UIViewController

- (instancetype)initWithRoomItem:(RoomItem *)roomItem;

@end

NS_ASSUME_NONNULL_END
