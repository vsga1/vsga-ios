//
//  CameraViewController.m
//  VStudy
//
//  Created by TamNVM on 24/05/2023.
//

#import "CameraViewController.h"
#import "Macro.h"
#import "UIView+Extension.h"
#import "User.h"
#import "Section.h"
#import "TSMutableArray.h"
#import "RoomManager.h"
#import "CameraDataSourceProcessor.h"
#import "CameraCollectionViewCell.h"

const CGFloat kLayoutSpacing = 5;
const NSInteger kMaxMemberInRoom = 10;

@interface CameraViewController () <RoomManagerDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) UICollectionView *emptyCollectionView;

@property (nonatomic, strong) UICollectionViewDiffableDataSource<Section *, User *> *dataSource;

@property (nonatomic, strong) CameraDataSourceProcessor *dataSourceProcessor;

@property (nonatomic, strong) TSMutableArray<User *> *memberList;

@property (nonatomic, strong) TSMutableArray<User *> *imageList;

@property (nonatomic, strong) RoomManager *roomManager;

@property (nonatomic, strong) dispatch_queue_t serialQueue;

@end

@implementation CameraViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViews];
    [self setupValues];
    
    self.serialQueue = dispatch_queue_create("com.example.snapshotQueue", DISPATCH_QUEUE_SERIAL);
    NSLog(@"Tamnvm>>frame viewDidLoad %f %f",self.view.width, self.view.height);
//    self.emptyCollectionView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"Tamnvm>>frame viewWillAppear %f %f",self.view.width, self.view.height);
}
#pragma mark - Layout Subviews

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    NSLog(@"Tamnvm>>frame viewWillLayoutSubviews %f %f",self.view.width, self.view.height);
    self.collectionView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
    self.emptyCollectionView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    NSLog(@"Tamnvm>>frame viewDidLayoutSubviews %f %f",self.view.width, self.view.height);
}

#pragma mark - Setup Views

- (void)setupViews {
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupCollectionView];
    [self setupEmptyCollectionView];
}

- (void)setupCollectionView {
    CGFloat itemSize = ceil((self.view.width - 3*kLayoutSpacing - 2)/2);
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = kLayoutSpacing;
    layout.minimumLineSpacing = kLayoutSpacing;
    layout.itemSize = CGSizeMake(itemSize, itemSize);
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    self.collectionView = [[UICollectionView alloc]
                           initWithFrame:CGRectZero
                           collectionViewLayout:layout];
    self.collectionView.contentInset = UIEdgeInsetsMake(kLayoutSpacing,
                                                        kLayoutSpacing,
                                                        kLayoutSpacing,
                                                        kLayoutSpacing);
    self.collectionView.backgroundColor = kBackgroundColor;
    [self.collectionView registerClass:[CameraCollectionViewCell class] forCellWithReuseIdentifier:CameraCollectionViewCell.cellIdentifier];
    
    self.collectionView.hidden = TRUE;
    [self.view addSubview:self.collectionView];
}

- (void)setupEmptyCollectionView {
    CGFloat itemSize = ceil((self.view.width - 3*kLayoutSpacing - 2)/2);
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumInteritemSpacing = kLayoutSpacing;
    layout.minimumLineSpacing = kLayoutSpacing;
    layout.itemSize = CGSizeMake(itemSize, itemSize);
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    self.emptyCollectionView = [[UICollectionView alloc]
                           initWithFrame:CGRectZero
                           collectionViewLayout:layout];
    self.emptyCollectionView.contentInset = UIEdgeInsetsMake(kLayoutSpacing,
                                                        kLayoutSpacing,
                                                        kLayoutSpacing,
                                                        kLayoutSpacing);
    self.emptyCollectionView.backgroundColor = kBackgroundColor;
    [self.emptyCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"EmptyCell"];
    
    self.emptyCollectionView.hidden = FALSE;
    self.emptyCollectionView.dataSource = self;
    [self.view addSubview:self.emptyCollectionView];
}
#pragma mark - Setup Values

- (void)setupValues {
    [self.emptyCollectionView reloadData];
    self.roomManager = [RoomManager sharedManager];
    self.roomManager.delegate = self;
    [self setupImageList];
}

#pragma mark - Setup Image List

- (void)setupImageList {
    self.imageList = [[TSMutableArray alloc] init];
    for (NSInteger i = 0; i < 10; i++) {
        NSString *imageName = [NSString stringWithFormat:@"CameraImage%ld", (long)i];
        User *user = [[User alloc] initFakeUserImageWithImageName:imageName];
        [self.imageList addObject:user];
    }
}

#pragma mark - NSDiffableDataSourceSnapshot

- (void)applySnapshot:(NSDiffableDataSourceSnapshot *)snapshot
            animation:(BOOL)animation
           completion:(void (^)(void))completion {
    [self.dataSource applySnapshot:snapshot animatingDifferences:animation completion:completion];
}

#pragma mark - RoomManagerDelegate
- (void)didUpdateMemberArray:(TSMutableArray<User *> *)memberArray {
    self.memberList = [[TSMutableArray alloc] initWithArray:memberArray];
    NSInteger count = self.memberList.count;
    for (int i = 0; i < kMaxMemberInRoom - count; i++) {
        User *fakeUserImage = [self.imageList objectAtIndex:i];
        [self.memberList addObject:fakeUserImage];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDiffableDataSourceSnapshot *deleteSnapshot = [self.dataSourceProcessor deleteAllItems];
        [self applySnapshot:deleteSnapshot animation:NO completion:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDiffableDataSourceSnapshot *appendSnapshot = [self.dataSourceProcessor appendItemArray:self.memberList];
                [self applySnapshot:appendSnapshot animation:YES completion:^{
                    [self updateUI];
                }];
            });
        }];
    });
}

- (void)updateUI {
    self.emptyCollectionView.hidden = YES;
    self.collectionView.hidden = NO;
}

#pragma mark - Empty CollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return kMaxMemberInRoom;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EmptyCell" forIndexPath:indexPath];
    cell.backgroundColor = UIColor.blackColor;
    cell.layer.cornerRadius = 5.0;
    return cell;
}

#pragma mark - Lazy Loading

- (UICollectionViewDiffableDataSource<Section *,User *> *)dataSource {
    if (_dataSource) {
        return _dataSource;
    }
    _dataSource = [[UICollectionViewDiffableDataSource alloc] initWithCollectionView:self.collectionView
                                                                        cellProvider:^UICollectionViewCell * _Nullable(UICollectionView * _Nonnull collectionView, NSIndexPath * _Nonnull indexPath, User *item) {
        if (item) {
            CameraCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CameraCollectionViewCell.cellIdentifier forIndexPath:indexPath];
            cell.backgroundColor = UIColor.blackColor;
            cell.layer.cornerRadius = 5.0;
            cell.layer.borderColor = UIColor.lightGrayColor.CGColor;
            cell.layer.borderWidth = 1.0;
            cell.memberItem = item;
            return cell;
        }
        return [[CameraCollectionViewCell alloc] init];
    }];
    return _dataSource;
}

- (CameraDataSourceProcessor *)dataSourceProcessor {
    if (_dataSourceProcessor) {
        return _dataSourceProcessor;
    }
    _dataSourceProcessor = [[CameraDataSourceProcessor alloc] initWithDiffDataSource:self.dataSource];
    return _dataSourceProcessor;
}

@end
