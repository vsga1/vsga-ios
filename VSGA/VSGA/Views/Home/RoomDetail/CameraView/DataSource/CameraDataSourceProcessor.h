//
//  CameraDataSourceProcessor.h
//  VStudy
//
//  Created by TamNVM on 11/06/2023.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TSMutableArray.h"
#import "User.h"
NS_ASSUME_NONNULL_BEGIN

@interface CameraDataSourceProcessor : NSObject

@property (strong, nonatomic) UICollectionViewDiffableDataSource *dataSource;

- (instancetype)initWithDiffDataSource:(UICollectionViewDiffableDataSource *)dataSource;

// Append Array

- (NSDiffableDataSourceSnapshot *)appendItemArray:(TSMutableArray<User *> *)itemArray;

// Append Item

- (NSDiffableDataSourceSnapshot *)appendItem:(User *)item;

// Insert Item

- (NSDiffableDataSourceSnapshot *)insertItem:(User *)item;

// Reload Item

- (NSDiffableDataSourceSnapshot *)reloadItem:(User *)item;

// Reload Array

- (NSDiffableDataSourceSnapshot *)reloadItemArray:(TSMutableArray<User *> *)itemArray;

// Delete Item

- (NSDiffableDataSourceSnapshot *)deleteItem:(User *)item;

// Delete Array

- (NSDiffableDataSourceSnapshot *)deleteItemArray:(TSMutableArray<User *> *)itemArray;

// Delete All Item

- (NSDiffableDataSourceSnapshot *)deleteAllItems;

// Replace Item

- (NSDiffableDataSourceSnapshot *)replaceItem:(User *)item withNewItem:(User *)newItem;

@end

NS_ASSUME_NONNULL_END
