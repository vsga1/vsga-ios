//
//  CameraCollectionViewCell.m
//  VStudy
//
//  Created by TamNVM on 11/06/2023.
//

#import "CameraCollectionViewCell.h"
#import "Macro.h"
#import "UIView+Extension.h"
#import "CustomLabel.h"
#import <SDWebImage/SDWebImage.h>


const CGFloat kCameraSpacing = 4.0;
const CGFloat kCameraAvatarSize = 70.0;
const CGFloat kCameraUsernameSize = 28.0;

@interface CameraCollectionViewCell ()

@property (nonatomic, strong) UIImageView *avatarImageView;

@property (nonatomic, strong) CustomLabel *usernameLabel;

@property (nonatomic, strong) UIImageView *defaultImageView;

@end

@implementation CameraCollectionViewCell

+ (NSString *)cellIdentifier {
    return NSStringFromClass(self.class);
}

- (void)setMemberItem:(User *)memberItem {
    _memberItem = memberItem;
    if (memberItem.userType == UserTypeReal) {
        self.avatarImageView.hidden = FALSE;
        self.usernameLabel.hidden = FALSE;
        
        CGSize labelSize = [memberItem.username sizeOfStringWithStyledFont:kMedium20 withSize:CGSizeMake(self.contentView.width, (self.contentView.height / 2) - kCameraSpacing)];
        self.usernameLabel.frame = CGRectMake(self.usernameLabel.x,
                                              self.usernameLabel.y,
                                              self.usernameLabel.width,
                                              labelSize.height);
        self.usernameLabel.text = memberItem.username;
        
        if (memberItem.avatarURL) {
            self.avatarImageView.image = [UIImage imageNamed:@"DefaultAvatar"];
            [self.avatarImageView sd_setImageWithURL:memberItem.avatarURL completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                if (error) {
                    // Handle the error, such as displaying a placeholder image
                    NSLog(@"Error loading image: %@", error.localizedDescription);
                    return;
                }
                self.avatarImageView.image = image;
            }];
        }
        else {
            self.avatarImageView.image = [UIImage imageNamed:@"DefaultAvatar"];
        }
    }
    else {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *originalImage = [UIImage imageNamed:memberItem.avatarURLString];
            
            // Set the desired width and height for the resized image
            CGFloat targetWidth = 200; // Specify your desired width
            CGFloat targetHeight = 200; // Specify your desired height
            
            // Calculate the scale factor for resizing
            CGFloat scaleFactor = 0.0;
            CGSize imageSize = originalImage.size;
            CGFloat widthRatio = targetWidth / imageSize.width;
            CGFloat heightRatio = targetHeight / imageSize.height;
            
            // Determine the scale factor based on the smaller ratio to maintain aspect ratio
            if (widthRatio > heightRatio) {
                scaleFactor = heightRatio;
            } else {
                scaleFactor = widthRatio;
            }
            
            // Calculate the new size for the resized image
            CGSize newSize = CGSizeMake(imageSize.width * scaleFactor, imageSize.height * scaleFactor);
            
            // Create a graphics context with the new size
            UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
            
            // Draw the original image into the context with the new size
            [originalImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
            
            // Get the resized image from the context
            UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
            
            // Clean up the graphics context
            UIGraphicsEndImageContext();
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.defaultImageView.hidden = FALSE;
                self.defaultImageView.image = resizedImage;
            });
        });
    }
}

- (void)loadImageFromURL:(NSURL *)url completion:(void (^)(UIImage *image))completion {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        UIImage *image = [UIImage imageWithData:imageData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(image);
        });
    });
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.avatarImageView.hidden = TRUE;
    self.avatarImageView.image = nil;
    
    self.usernameLabel.hidden = TRUE;
    self.usernameLabel.text = nil;
    
    self.defaultImageView.hidden = TRUE;
}
#pragma mark - Lazy loading

- (UIImageView *)avatarImageView {
    if (_avatarImageView) {
        return _avatarImageView;
    }
    _avatarImageView = [[UIImageView alloc] initWithFrame:
                        CGRectMake((self.contentView.width - kCameraAvatarSize) / 2,
                                   (self.contentView.height - kCameraAvatarSize - kCameraSpacing - kCameraUsernameSize) / 2,
                                   kCameraAvatarSize,
                                   kCameraAvatarSize)];
    _avatarImageView.contentMode = UIViewContentModeScaleAspectFit;
    _avatarImageView.layer.cornerRadius = CGRectGetWidth(_avatarImageView.bounds) / 2.0;
    _avatarImageView.layer.masksToBounds = YES;
    [self.contentView addSubview:_avatarImageView];
    return _avatarImageView;
}

- (CustomLabel *)usernameLabel {
    if (_usernameLabel) {
        return _usernameLabel;
    }
    CGRect frame = CGRectMake(0,
                              _avatarImageView.bottom + kCameraSpacing,
                              self.contentView.width,
                              (self.contentView.height / 2) - kCameraSpacing);
    _usernameLabel = [[CustomLabel alloc] initWithFrame:frame
                                                  title:@""
                                              titleFont:kMedium20
                                             titleColor:UIColor.whiteColor];
    _usernameLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:_usernameLabel];
    return _usernameLabel;
}

- (UIImageView *)defaultImageView {
    if (_defaultImageView) {
        return _defaultImageView;
    }
    _defaultImageView = [[UIImageView alloc] initWithFrame:self.contentView.frame];
    _defaultImageView.contentMode = UIViewContentModeScaleAspectFit;
    _defaultImageView.layer.masksToBounds = YES;
 
    [self.contentView addSubview:_defaultImageView];
    return _defaultImageView;
}
@end
