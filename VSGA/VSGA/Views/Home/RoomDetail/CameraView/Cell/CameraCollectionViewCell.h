//
//  CameraCollectionViewCell.h
//  VStudy
//
//  Created by TamNVM on 11/06/2023.
//

#import <UIKit/UIKit.h>
#import "User.h"
NS_ASSUME_NONNULL_BEGIN

@interface CameraCollectionViewCell : UICollectionViewCell

+ (NSString *)cellIdentifier;

@property (nonatomic, strong) User *memberItem;

@end

NS_ASSUME_NONNULL_END
