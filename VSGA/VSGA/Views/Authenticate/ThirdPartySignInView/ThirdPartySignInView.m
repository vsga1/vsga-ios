//
//  ThirdPartySignInView.m
//  VStudy
//
//  Created by LAP14011 on 25/02/2023.
//

#import "ThirdPartySignInView.h"

@import GoogleSignIn;
@import FBSDKLoginKit;

#import "Macro.h"

#import "UIView+Extension.h"

#import "CustomButton.h"

#import "AccountManager.h"

@implementation ThirdPartySignInView {
    CGFloat _buttonWidth;
    CGFloat _buttonHeight;
}

- (instancetype)initWithFrame:(CGRect)frame
                  buttonWidth:(CGFloat)buttonWidth
                 buttonHeight:(CGFloat)buttonHeight
           withViewController:(UIViewController *)viewController {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.viewController = viewController;
        
        _buttonWidth = buttonWidth;
        _buttonHeight = buttonHeight;
        
        [self createOrLabel];
        
//        [self createFacebookSignInButton];
        [self createGoogleSignInButton];
    }
    return self;
}

- (void)createOrLabel {
    CGFloat x = (self.width - kAuthOrLabelWidth) / 2;
    CGFloat y = self.height - _buttonHeight - kAuthOrLabelHeight - kAuthBottomPadding - kAuthComponentSpace;
    CGRect frame = CGRectMake(x, y, kAuthOrLabelWidth, kAuthOrLabelHeight);
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.text = @"- or -";
    label.font = kRegular16;
    label.textColor = UIColor.blackColor;
    
    label.numberOfLines = 0;
    label.textAlignment = NSTextAlignmentCenter;
    
    [self addSubview:label];
    
}

- (void)createFacebookSignInButton {
    CGFloat x = (self.width - _buttonWidth) / 2;
    CGFloat y = self.height - _buttonHeight - kAuthBottomPadding - kAuthComponentSpace;
    CGRect frame = CGRectMake(x, y, _buttonWidth, _buttonHeight);
    
    UIFont *titleFont = kSemiBold16;
    
    CustomButton *facebookButton = [[CustomButton alloc] initWithFrame:frame
                                                                 title:@"Continue with Facebook"
                                                            titleColor:UIColor.blackColor
                                                             titleFont:titleFont backgroundColor:UIColor.whiteColor
                                                           borderColor:UIColor.blackColor.CGColor];
    [facebookButton addTarget:self action:@selector(facebookSignInButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:facebookButton];
}

- (void)createGoogleSignInButton {
    CGFloat x = (self.width - _buttonWidth) / 2;
    CGFloat y = self.height - _buttonHeight - kAuthBottomPadding;
    CGRect frame = CGRectMake(x, y, _buttonWidth, _buttonHeight);
    
    UIFont *titleFont = kSemiBold16;
    
    CustomButton *googleButton = [[CustomButton alloc] initWithFrame:frame
                                                               title:@"Continue with Google"
                                                          titleColor:UIColor.blackColor
                                                           titleFont:titleFont
                                                     backgroundColor:UIColor.whiteColor
                                                         borderColor:UIColor.blackColor.CGColor];
    [googleButton addTarget:self action:@selector(googleSignInButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:googleButton];
    
}

- (void)facebookSignInButtonClicked:(UIButton *)sender {
    
    NSLog(@"facebook sign in");
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logInWithPermissions:@[@"public_profile", @"email"]
                    fromViewController:self.viewController
                               handler:^(FBSDKLoginManagerLoginResult * _Nullable result, NSError * _Nullable error) {
        if (error) {
            NSLog(@"Process error");
            [self.delegate onFinishThirdPartySignInWithErrorCode:ThirdPartySignInErrorFail];
            return;
        }
        if (result.isCancelled) {
            [self.delegate onFinishThirdPartySignInWithErrorCode:ThirdPartySignInCancel];
            return;
        }
        if (![FBSDKAccessToken currentAccessToken]) {
            [self.delegate onFinishThirdPartySignInWithErrorCode:ThirdPartySignInErrorFail];
            return;
        }
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken] tokenString]);
        [[AccountManager sharedManager] setAccountInfoWithUsername:[[FBSDKAccessToken currentAccessToken] userID]
                                                   userAccessToken:[[FBSDKAccessToken currentAccessToken] tokenString]
                                                        isRemember:NO
                                                        completion:^{
            [self.delegate onFinishThirdPartySignInWithErrorCode:ThirdPartySignInHandlerNoError];
        }];
    }];
    
}

- (void)googleSignInButtonClicked:(UIButton *)sender {
    
    [GIDSignIn.sharedInstance signInWithPresentingViewController:self.viewController
                                                      completion:^(GIDSignInResult * _Nullable signInResult,
                                                                   NSError * _Nullable error) {
        if (error) {
            if ([error.userInfo[@"NSLocalizedDescription"] isEqualToString:@"The user canceled the sign-in flow."]) {
                [self.delegate onFinishThirdPartySignInWithErrorCode:ThirdPartySignInCancel];
                return;
            }
            [self.delegate onFinishThirdPartySignInWithErrorCode:ThirdPartySignInErrorFail];
            return;
        }
      
        NSLog(@"signInResult %@", signInResult);
        NSLog(@"_email %@", signInResult.user.profile.email);
        NSLog(@"_accessToken %@", signInResult.user.accessToken.tokenString);
        
        [[AccountManager sharedManager] setAccountInfoWithUsername:signInResult.user.profile.email
                                                   userAccessToken:signInResult.user.accessToken.tokenString
                                                        isRemember:NO
                                                        completion:^{
            
            [self.delegate onFinishThirdPartySignInWithErrorCode:ThirdPartySignInHandlerNoError];
        }];
        
        // If sign in succeeded, display the app's main content View.
    }];
    
}
@end
