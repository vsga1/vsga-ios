//
//  ThirdPartySignInView.h
//  VStudy
//
//  Created by LAP14011 on 25/02/2023.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ThirdPartySignInHandlerErrorCode) {
    ThirdPartySignInErrorFail,
    ThirdPartySignInCancel,
    ThirdPartySignInHandlerNoError
};

@protocol ThirdPartySignInDelegate <NSObject>

- (void)onFinishThirdPartySignInWithErrorCode:(ThirdPartySignInHandlerErrorCode)errorCode;

@end

@interface ThirdPartySignInView : UIView

@property (nonatomic, weak) id<ThirdPartySignInDelegate> delegate;

@property (nonatomic, weak) UIViewController *viewController;

- (instancetype)initWithFrame:(CGRect)frame
                  buttonWidth:(CGFloat)buttonWidth
                 buttonHeight:(CGFloat)buttonHeight
           withViewController:(UIViewController *)viewController;

@end

NS_ASSUME_NONNULL_END
