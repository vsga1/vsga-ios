//
//  SignInViewController.m
//  VSGA
//
//  Created by LAP14011 on 07/02/2023.
//

#import "SignInViewController.h"

#import "AppDelegate.h"
#import "Scope.h"
#import "Macro.h"

#import "UIView+Extension.h"
#import "NSString+Extension.h"
#import "UIImage+Extension.h"

#import "InputTextField.h"
#import "CustomButton.h"
#import "CustomLabel.h"
#import "ErrorLabel.h"

#import "ThirdPartySignInView.h"

#import "ForgotPasswordViewController.h"
#import "HomeViewController.h"

#import "SignInHandler.h"
#import "AccountManager.h"

#import "UtilityClass.h"
@interface SignInViewController() <SignInHandlerDelegate, ThirdPartySignInDelegate, UITextFieldDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) InputTextField *usernameTextField;

@property (nonatomic, strong) InputTextField *passwordTextField;

@property (nonatomic, strong) UITextView *forgotPassword;

@property (nonatomic, strong) CustomButton *signInButton;

@property (nonatomic, strong) SignInHandler *signInHandler;

@property (nonatomic, strong) ErrorLabel *errorLabel;

@property (nonatomic, strong) UIImageView *passwordIcon;

@property (nonatomic, strong) UIImageView *rememberMeIcon;

@property (nonatomic) BOOL isCheckRememberMe;

@property (nonatomic) BOOL isShowPassword;

@property (nonatomic, strong) ThirdPartySignInView *thirdPartySignInView;

@property (nonatomic, assign) UIEdgeInsets originalContentInset;

@property (nonatomic, strong) InputTextField *activeTextField;

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createView];
    [self setupGestures];
    [self setupDelegates];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = FALSE;
    
    // Register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.errorLabel setErrorLabelWithTitle:@""];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Create View

- (void)createView {
    self.view.backgroundColor = UIColor.whiteColor;
    
    ///Sign in form
    CGFloat statusBarHeight = [UtilityClass getStatusBarHeight];
    CGFloat navigationBarHeight = self.navigationController.navigationBar.frame.size.height;
    CGFloat totalBarHeight = statusBarHeight + navigationBarHeight;
    CGFloat safeAreaBottomHeight = [UtilityClass getSafeAreaBottomHeight];
    
    CGFloat scrollViewHeight = self.view.height - totalBarHeight - safeAreaBottomHeight;
    CGRect signInFormFrame = CGRectMake(0,
                                        totalBarHeight,
                                        self.view.width,
                                        scrollViewHeight);
    [self createSignInFormWithFrame:signInFormFrame];
}

- (void)createSignInFormWithFrame:(CGRect)frame {
    CGFloat x = 20;
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:frame];
    self.scrollView.contentSize = frame.size;
    [self.view addSubview:self.scrollView];
    
    CGFloat yTitle;
    if (frame.size.height < 600) {
        yTitle = 0;
    }
    else {
        yTitle = frame.size.height/10;
    }
    
    CustomLabel *titleLabel = [self createTitleLabelWithOrigin:CGPointMake(x, yTitle)];
    [self.scrollView addSubview:titleLabel];
    
    CustomLabel *descLabel = [self createDescriptionLabelWithOrigin:CGPointMake(x, titleLabel.y + titleLabel.height + kAuthComponentSpace)];
    [self.scrollView addSubview:descLabel];
    
    CustomLabel *usernameLabel = [self createUsernameLabelWithOrigin:CGPointMake(x, descLabel.y + descLabel.height + kAuthComponentSpace * 2)];
    [self.scrollView addSubview:usernameLabel];
    
    CGFloat inputWidth = kButtonWidth;
    CGFloat inputHeight = 40;
    
    CGRect usernameInputFrame = CGRectMake(x,
                                           usernameLabel.y + usernameLabel.height + kAuthComponentSpace,
                                           inputWidth,
                                           inputHeight);
    [self createUsernameInputWithFrame:usernameInputFrame];
    [self.scrollView addSubview:self.usernameTextField];
    
    CustomLabel *passwordLabel = [self createPasswordLabelWithOrigin:CGPointMake(x, self.usernameTextField.y + self.usernameTextField.height + kAuthComponentSpace)];
    [self.scrollView addSubview:passwordLabel];
    
    CGRect passwordInputFrame = CGRectMake(x,
                                           passwordLabel.y + passwordLabel.height + kAuthComponentSpace,
                                           inputWidth,
                                           inputHeight);
    [self createPasswordInputWithFrame:passwordInputFrame];
    [self.scrollView addSubview:self.passwordTextField];
    
    UIView *rememberMeView = [self createRememberMeLabelWithOrigin:CGPointMake(x, self.passwordTextField.y + self.passwordTextField.height + kAuthComponentSpace * 2)];
    [self.scrollView addSubview:rememberMeView];
    
    [self createErrorLabelWithOrigin:CGPointMake(x,rememberMeView.y + rememberMeView.height)];
    [self.scrollView addSubview:self.errorLabel];
    
    
    [self createSignInButtonWithOrigin:CGPointMake((self.view.width - kButtonWidth) / 2,self.errorLabel.y + self.errorLabel.height + kAuthComponentSpace * 2)];
    [self.scrollView addSubview:self.signInButton];
    
    
    [self createForgotPasswordWithOrigin:CGPointMake(x,self.signInButton.y + self.signInButton.height + kAuthComponentSpace)];
    [self.scrollView addSubview:self.forgotPassword];
    
    CGFloat thirdPartyHeight = kButtonHeight + kAuthOrLabelHeight + kAuthBottomPadding + kAuthComponentSpace;
    CGRect thirdPartyFrame = CGRectMake(0,
                                        self.forgotPassword.bottom + kAuthComponentSpace,
                                        self.view.width,
                                        thirdPartyHeight);
    self.thirdPartySignInView = [[ThirdPartySignInView alloc] initWithFrame:thirdPartyFrame
                                                                buttonWidth:kButtonWidth
                                                               buttonHeight:kButtonHeight
                                                         withViewController:self];
    [self.scrollView addSubview:self.thirdPartySignInView];
    
    CGFloat blankSpace = self.scrollView.height - self.thirdPartySignInView.bottom;
    self.scrollView.height = self.scrollView.height - blankSpace;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, self.scrollView.height);
    self.originalContentInset = self.scrollView.contentInset;
    
}

- (CustomLabel *)createTitleLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"Sign in";
    UIFont *titleFont = kSemiBold32;
    
    CGFloat titleWidth = self.view.width - 2*origin.x;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:kPrimaryColor];
    
    return label;
}

- (CustomLabel *)createDescriptionLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"Sign in to VStudy with your username and password.";
    UIFont *titleFont = kRegular18;
    
    CGFloat titleWidth = self.view.width - 2*origin.x;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    
    return label;
}

- (CustomLabel *)createUsernameLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"";
    UIFont *titleFont = kSemiBold16;
    
    CGFloat titleWidth = self.view.width - 2*origin.x;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    
    return label;
}

- (void)createUsernameInputWithFrame:(CGRect)frame {
    self.usernameTextField = [[InputTextField alloc] initWithFrame:frame];
    self.usernameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.usernameTextField.placeholder = @"Username";
    NSString *rememberUsername = [[AccountManager sharedManager] getRememberUsername];
    if (!rememberUsername) {
        rememberUsername = @"";
    }
    self.usernameTextField.text = rememberUsername;
    self.usernameTextField.textColor = UIColor.blackColor;
    self.usernameTextField.font = kRegular16;
}

- (CustomLabel *)createPasswordLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"";
    UIFont *titleFont = kSemiBold16;
    
    CGFloat titleWidth = self.view.width - 2*origin.x;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    
    return label;
}

- (void)createPasswordInputWithFrame:(CGRect)frame {
    self.passwordTextField = [[InputTextField alloc] initWithFrame:frame];
    self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.passwordTextField.placeholder = @"Password";
    self.passwordTextField.text = @"";
    self.passwordTextField.textColor = UIColor.blackColor;
    self.passwordTextField.font = kRegular16;
    
    CGFloat iconSize = 20;
    CGFloat iconMargin = 10;
    // Create a container view for the right view
    UIView *rightViewContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, iconSize + iconMargin, iconSize)];
    
    self.passwordIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, iconSize, iconSize)];
    UIImage *image = kHidePassIcon;
    UIImage *resizedImage = [image resizeWithTargetSize:self.passwordIcon.size];
    self.passwordIcon.image = resizedImage;
    self.passwordIcon.contentMode = UIViewContentModeScaleAspectFit;
    self.passwordTextField.secureTextEntry = TRUE;
    self.isShowPassword = FALSE;
    
    [self.passwordIcon setUserInteractionEnabled:YES];
    
    [rightViewContainer addSubview:self.passwordIcon];
    
    /// tap
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(passwordIconClicked:)];
    [self.passwordIcon addGestureRecognizer:tapRecognizer];
    tapRecognizer.cancelsTouchesInView = NO;
    
    self.passwordTextField.rightView = rightViewContainer;
    
}

- (UIView *)createRememberMeLabelWithOrigin:(CGPoint)origin {
    
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(origin.x, origin.y, 250, 50)];
    
    UIView *iconContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    
    CGFloat iconSize = 25;
    self.rememberMeIcon = [[UIImageView alloc] initWithFrame:CGRectMake((iconContainer.width - iconSize)/2,
                                                                        (iconContainer.height - iconSize)/2,
                                                                        iconSize,
                                                                        iconSize)];
    self.rememberMeIcon.image = kCheckIcon;
    self.isCheckRememberMe = TRUE;
    
    [iconContainer addSubview:self.rememberMeIcon];
    [container addSubview:iconContainer];
    
    /// tap
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rememberMeIconClicked:)];
    [iconContainer addGestureRecognizer:tapRecognizer];
    tapRecognizer.cancelsTouchesInView = NO;
    
    NSString *title = @"Remember me";
    UIFont *titleFont = kRegular16;
    
    CGFloat titleWidth = self.view.width - 2*origin.x;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(self.rememberMeIcon.right + 10,
                                                   self.rememberMeIcon.center.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    CGPoint position = CGPointMake(label.center.x, self.rememberMeIcon.center.y);
    label.center = position;
    
    [container addSubview:label];
    return container;
    
}

- (void)createSignInButtonWithOrigin:(CGPoint)origin {
    CGRect frame = CGRectMake(origin.x, origin.y, kButtonWidth, kButtonHeight);
    
    UIFont *titleFont = kSemiBold18;
    
    self.signInButton = [[CustomButton alloc] initWithFrame:frame
                                                      title:@"Sign In"
                                                 titleColor:UIColor.whiteColor
                                                  titleFont:titleFont backgroundColor:kPrimaryColor
                                                borderColor:kPrimaryColor.CGColor];
    [self.signInButton addTarget:self action:@selector(signInButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)createErrorLabelWithOrigin:(CGPoint)origin {
    UIFont *titleFont = kRegular16;
    
    self.errorLabel = [[ErrorLabel alloc] initWithOrigin:origin
                                                maxWidth:kMaxErrorWidth
                                               titleFont:titleFont];
}

- (void)createForgotPasswordWithOrigin:(CGPoint)origin {
    NSString *title = @"Forgot password?";
    UIFont *titleFont = kRegular16;
    
    CGFloat titleWidth = self.view.width - 2*origin.x;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    titleSize.width += 10;
    titleSize.height += 10;
    
    CGRect frame = CGRectMake((self.view.width - titleSize.width)/2, origin.y, titleSize.width, titleSize.height);
    
    self.forgotPassword = [[UITextView alloc] initWithFrame:frame];
    
    self.forgotPassword.text = title;
    
    ///underline
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    self.forgotPassword.attributedText = [[NSAttributedString alloc] initWithString:self.forgotPassword.text
                                                                         attributes:underlineAttribute];
    
    self.forgotPassword.font = titleFont;
    self.forgotPassword.textColor = kPrimaryColor;
    self.forgotPassword.textAlignment = NSTextAlignmentCenter;
    self.forgotPassword.editable = NO;
    self.forgotPassword.scrollEnabled = FALSE;
    
    /// tap
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(forgotPasswordClicked:)];
    [self.forgotPassword addGestureRecognizer:tapRecognizer];
    tapRecognizer.cancelsTouchesInView = NO;
    
    [self.forgotPassword sizeToFit];
    
    [self.view addSubview:self.forgotPassword];
}
#pragma mark - Setup Gestures

- (void)setupGestures {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
    NSArray *textFields = @[self.usernameTextField, self.passwordTextField];
    
    for (UITextField *textField in textFields) {
        textField.delegate = self;
    }
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - Setup Delegated

- (void)setupDelegates {
    self.usernameTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    self.signInHandler.delegate = self;
    self.thirdPartySignInView.delegate = self;
}

#pragma mark - Keyboard

- (void)keyboardWillShow:(NSNotification *)notification {
    // Get the keyboard frame
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    // Calculate the blank space between the scroll view and the sign-up button
    CGFloat blankSpace = CGRectGetMaxY(self.scrollView.frame) - CGRectGetMaxY(self.thirdPartySignInView.frame);
    
    // Calculate the new content inset
    UIEdgeInsets contentInset = self.originalContentInset;
    contentInset.bottom = CGRectGetHeight(keyboardFrame) - blankSpace;
    
    // Update the scroll view's content inset
    self.scrollView.contentInset = contentInset;
    self.scrollView.scrollIndicatorInsets = contentInset;
    
    // Scroll to the active text field
    if (self.activeTextField) {
        CGRect visibleRect = self.activeTextField.frame;
        visibleRect.size.height += 16; // Add some extra space if needed
        [self.scrollView scrollRectToVisible:visibleRect animated:YES];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification {
    self.scrollView.contentInset = self.originalContentInset;
    self.scrollView.scrollIndicatorInsets = self.originalContentInset;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    UITapGestureRecognizer *tapGesture = (UITapGestureRecognizer *)self.view.gestureRecognizers.firstObject;
    tapGesture.enabled = YES;
    self.activeTextField = (InputTextField *)textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    UITapGestureRecognizer *tapGesture = (UITapGestureRecognizer *)self.view.gestureRecognizers.firstObject;
    tapGesture.enabled = [self isAnyTextFieldEditing];
    self.activeTextField = nil;
}

- (BOOL)isAnyTextFieldEditing {
    NSArray *textFields = @[self.usernameTextField, self.passwordTextField];
    
    for (UITextField *textField in textFields) {
        if (textField.isEditing) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.usernameTextField) {
        [self.passwordTextField becomeFirstResponder];
    } else if (textField == self.passwordTextField) {
        // The last text field, perform any desired action
        [textField resignFirstResponder]; // Dismiss the keyboard
        [self signInButtonClicked:nil];
    }
    
    return YES;
}


#pragma mark - On clicked

- (void)passwordIconClicked:(UITapGestureRecognizer *)tapRecognizer {
    UIImage *newImage;
    self.isShowPassword = !self.isShowPassword;
    if (self.isShowPassword) {
        newImage = kShowPassIcon;
        self.passwordTextField.secureTextEntry = FALSE;
    }
    else {
        newImage = kHidePassIcon;
        self.passwordTextField.secureTextEntry = TRUE;
    }
    UIImage *resizedImage = [newImage resizeWithTargetSize:self.passwordIcon.size];
    [UIView transitionWithView:self.view
                      duration:0.1
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
        self.passwordIcon.image = resizedImage;
    } completion:nil];
}

- (void)signInButtonClicked:(CustomButton *)sender {
    
    NSString *username = [self.usernameTextField.text lowercaseString];
    NSString *password = self.passwordTextField.text;
    
    if ([username isEqualToString:@""] || [password isEqualToString:@""]) {
        [self.errorLabel setErrorLabelWithTitle:@"Please input username and password"];
        [sender showAndHideLoadingInDuration:0.1f];
        return;
    }
    [self.errorLabel setErrorLabelWithTitle:@""];
    [sender showLoading];
    
    NSDictionary *info = [[NSDictionary alloc] initWithObjects:@[username,password,@(self.isCheckRememberMe)] forKeys:@[@"username",@"password",@"isRemember"]];
    
    [self.signInHandler startAuthenticationProcessWithInfo:info];
    
}

- (void)rememberMeIconClicked:(UITapGestureRecognizer *)tapRecognizer {
    UIImage *newImage;
    self.isCheckRememberMe = !self.isCheckRememberMe;
    if (self.isCheckRememberMe) {
        newImage = kCheckIcon;
    }
    else {
        newImage = kUnCheckIcon;
    }
    
    [UIView transitionWithView:self.view
                      duration:0.1
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
        self.rememberMeIcon.image = newImage;
    } completion:nil];
}

- (void)forgotPasswordClicked:(UITapGestureRecognizer *)tapRecognizer {
    ForgotPasswordViewController *forgotPasswordVC = [[ForgotPasswordViewController alloc] init];
    [self.navigationController pushViewController:forgotPasswordVC animated:YES];
}

#pragma mark - SignInHandlerDelegate

- (void)onFinishGettingAccessTokenWithErrorCode:(SignInHandlerErrorCode)errorCode {
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (errorCode) {
            case SignInHandlerErrorNotValidData:
                // Toast not valid data
                NSLog(@"[ERROR] %s: NOT VALID DATA", __func__);
                [self.signInButton hideLoading];
                [self.errorLabel setErrorLabelWithTitle:@"Incorrect username or password"];
                break;
            case SignInHandlerErrorServerError:
                // Toast server error
                NSLog(@"[ERROR] %s: SERVER ERROR", __func__);
                [self.signInButton hideLoading];
                [self.errorLabel setErrorLabelWithTitle:@"Something went wrong. Please try again"];
                break;
            case SignInHandlerErrorNetworkError:
                // Toast network error
                NSLog(@"[ERROR] %s: NETWORK ERROR", __func__);
                [self.signInButton hideLoading];
                [self.errorLabel setErrorLabelWithTitle:@"Something went wrong. Please try again"];
                break;
            case SignInHandlerErrorInvalidURL:
                // Toast invalid URL error
                NSLog(@"[ERROR] %s: INVALID URL ERROR", __func__);
                [self.signInButton hideLoading];
                [self.errorLabel setErrorLabelWithTitle:@"Something went wrong. Please try again"];
                break;
            default:
                NSLog(@"[INFO] %s: GOT THE TOKEN", __func__);
                break;
        }
    });
}

- (void)onFinishSavingUserInfo:(SignInHandlerErrorCode)errorCode {
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (errorCode) {
            case SignInHandlerErrorNotValidData: {
                // Toast not valid data
                NSLog(@"[ERROR] %s: NOT VALID DATA", __func__);
                @weakify(self)
                [UtilityClass showFailureToast:@"Error! Try again"
                                  centerInView:self.view
                                    completion:^{
                    @strongify(self)
                    [self.signInButton hideLoading];
                }];
            }
                break;
            default: {
                // Switch to home view
                NSLog(@"[INFO] %s: SWITCH TO HOMEVIEW", __func__);
                [AppDelegate.shared updateView];
            }
                break;
        }
    });
}

#pragma mark - ThirdPartySignInDelegate

- (void)onFinishThirdPartySignInWithErrorCode:(ThirdPartySignInHandlerErrorCode)errorCode {
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (errorCode) {
            case ThirdPartySignInErrorFail:
                // Toast not valid data
                NSLog(@"[ERROR] %s: FAILED", __func__);
                [self.signInButton hideLoading];
                [self.errorLabel setErrorLabelWithTitle:@"Something went wrong. Please try again"];
                break;
            case ThirdPartySignInCancel:
                [self.signInButton hideLoading];
                break;
            default:
                // Switch to home view
                NSLog(@"[INFO] %s: SWITCH TO HOMEVIEW", __func__);
                
                [AppDelegate.shared updateView];
                break;
        }
    });
}

#pragma mark - Custom Accessors

- (SignInHandler *)signInHandler {
    if (_signInHandler) {
        return _signInHandler;
    }
    _signInHandler = [[SignInHandler alloc] init];
    return _signInHandler;
}



@end
