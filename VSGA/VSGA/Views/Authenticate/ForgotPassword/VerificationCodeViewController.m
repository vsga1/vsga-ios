//
//  VerificationCodeViewController.m
//  VSGA
//
//  Created by LAP14011 on 07/02/2023.
//

#import "VerificationCodeViewController.h"

#import "Macro.h"

#import "UIView+Extension.h"
#import "NSString+Extension.h"

#import "InputTextField.h"
#import "CustomButton.h"
#import "CustomLabel.h"
#import "ErrorLabel.h"


#import "ResetPasswordViewController.h"

@interface VerificationCodeViewController ()

@property (nonatomic, strong) InputTextField *codeTextField;

@property (nonatomic, strong) CustomButton *continueButton;

@property (nonatomic, strong) ErrorLabel *errorLabel;

@property (nonatomic, strong) UITextView *resendLink;

@end

@implementation VerificationCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createView];

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.errorLabel setErrorLabelWithTitle:@""];
}

#pragma mark - Create View

- (void)createView {
    self.view.backgroundColor = UIColor.whiteColor;
    
    CGFloat x = 20;
    CGFloat y = self.navigationController.navigationBar.height + 20;
    CGFloat height = self.view.height - kButtonHeight * 2 - kAuthBottomPadding - kAuthComponentSpace * 2;
    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, y, self.view.width, height)];
    [self.view addSubview:containerView];
    
    CGFloat yTitle;
    if (height < 600) {
        yTitle = 0;
    }
    else {
        yTitle = height/10;
    }
    
    CustomLabel *titleLabel = [self createTitleLabelWithOrigin:CGPointMake(x, yTitle)];
    [containerView addSubview:titleLabel];
    
    CustomLabel *descLabel = [self createDescriptionLabelWithOrigin:CGPointMake(x, titleLabel.y + titleLabel.height + kAuthComponentSpace)];
    [containerView addSubview:descLabel];
    
    CustomLabel *codeLabel = [self createCodeLabelWithOrigin:CGPointMake(x, descLabel.y + descLabel.height + kAuthComponentSpace * 2)];
    [containerView addSubview:codeLabel];
    
    CGFloat inputWidth = kButtonWidth;
    CGFloat inputHeight = 40;
    
    CGRect codeInputFrame = CGRectMake(x,
                                        codeLabel.y + codeLabel.height + kAuthComponentSpace,
                                        inputWidth,
                                        inputHeight);
    [self createCodeInputWithFrame:codeInputFrame];
    [containerView addSubview:self.codeTextField];
    
    [self createErrorLabelWithOrigin:CGPointMake(x,self.codeTextField.y + self.codeTextField.height + kAuthComponentSpace * 2)];
    [containerView addSubview:self.errorLabel];
    
    [self createContinueButton];
    
}

- (CustomLabel *)createTitleLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"Verification Code";
    UIFont *titleFont = kSemiBold32;
    
    CGFloat titleWidth = self.view.width - 20;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:kPrimaryColor];
    
    return label;
}

- (CustomLabel *)createDescriptionLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"An OTP code has been sent to example@gmail.com. Check your email and enter the code below.";
    UIFont *titleFont = kRegular18;
    
    CGFloat titleWidth = self.view.width - 20;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    
    return label;
}

- (CustomLabel *)createCodeLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"";
    UIFont *titleFont = kSemiBold16;
    
    CGFloat titleWidth = self.view.width - 20;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    
    return label;
}

- (void)createCodeInputWithFrame:(CGRect)frame {
    self.codeTextField = [[InputTextField alloc] initWithFrame:frame];
    
    self.codeTextField.placeholder = @"CODE";
    
    self.codeTextField.text = @"";
    self.codeTextField.textColor = UIColor.blackColor;
}

- (void)createErrorLabelWithOrigin:(CGPoint)origin {
    UIFont *titleFont = kRegular16;
    
    self.errorLabel = [[ErrorLabel alloc] initWithOrigin:origin
                                                maxWidth:kMaxErrorWidth
                                               titleFont:titleFont];
}

- (void)createContinueButton {
    CGFloat x = (self.view.width - kButtonWidth) / 2;
    CGFloat y = self.view.height - kButtonHeight - kAuthBottomPadding;
    CGRect frame = CGRectMake(x, y, kButtonWidth, kButtonHeight);
    
    UIFont *titleFont = kSemiBold20;
    
    self.continueButton = [[CustomButton alloc] initWithFrame:frame
                                                                 title:@"Continue"
                                                            titleColor:UIColor.whiteColor
                                                             titleFont:titleFont
                                                       backgroundColor:kPrimaryColor
                                                           borderColor:kPrimaryColor.CGColor];
    [self.continueButton addTarget:self action:@selector(continueButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.continueButton];
}

#pragma mark - On Clicked

- (void)continueButtonClicked:(UIButton *)sender {

    ResetPasswordViewController *resetPasswordVC = [[ResetPasswordViewController alloc] initWithVerificationCode:self.codeTextField.text];
    [self.navigationController pushViewController:resetPasswordVC animated:YES];

}

#pragma mark - Delegate


@end
