//
//  ResetPasswordViewController.m
//  VSGA
//
//  Created by LAP14011 on 07/02/2023.
//

#import "ResetPasswordViewController.h"

#import "AppDelegate.h"

#import "Macro.h"

#import "UIView+Extension.h"
#import "NSString+Extension.h"

#import "InputTextField.h"
#import "CustomButton.h"
#import "CustomLabel.h"
#import "ErrorLabel.h"

#import "ResetPasswordHandler.h"

@interface ResetPasswordViewController () <ResetPasswordHandlerDelegate>

@property (nonatomic, strong) InputTextField *passwordTextField;

@property (nonatomic, strong) InputTextField *confirmPasswordTextField;

@property (nonatomic, strong) CustomButton *continueButton;

@property (nonatomic, strong) ErrorLabel *errorLabel;

@property (nonatomic, strong) UIImageView *passwordIcon;

@property (nonatomic) BOOL isShowPassword;

@property (nonatomic, strong) UIImageView *confirmPasswordIcon;

@property (nonatomic) BOOL isShowConfirmPassword;

@property (nonatomic, strong) ResetPasswordHandler *resetPasswordHandler;

@end

@implementation ResetPasswordViewController {
    NSString *_code;
}

- (instancetype)initWithVerificationCode:(NSString *)verificationCode {
    self = [super init];
    if (self) {
        _code = verificationCode;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createView];
    
    self.resetPasswordHandler.delegate = self;
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.errorLabel setErrorLabelWithTitle:@""];
}

#pragma mark - Create View

- (void)createView {
    self.view.backgroundColor = UIColor.whiteColor;
    
    [self createResetPasswordForm];
    
    [self createContinueButton];
}

- (void)createResetPasswordForm {
    CGFloat x = 20;
    CGFloat y = self.navigationController.navigationBar.height + 20;
    CGFloat height = self.view.height - kButtonHeight * 2 - kAuthBottomPadding - kAuthComponentSpace * 2;
    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, y, self.view.width, height)];
    [self.view addSubview:containerView];
    
    CGFloat yTitle;
    if (height < 600) {
        yTitle = 0;
    }
    else {
        yTitle = height/10;
    }
    
    CustomLabel *titleLabel = [self createTitleLabelWithOrigin:CGPointMake(x, yTitle)];
    [containerView addSubview:titleLabel];
    
    CustomLabel *descLabel = [self createDescriptionLabelWithOrigin:CGPointMake(x, titleLabel.y + titleLabel.height + kAuthComponentSpace)];
    [containerView addSubview:descLabel];
    

    CustomLabel *passwordLabel = [self createPasswordLabelWithOrigin:CGPointMake(x, descLabel.y + descLabel.height + kAuthComponentSpace)];
    [containerView addSubview:passwordLabel];
    
    CGFloat inputWidth = kButtonWidth;
    CGFloat inputHeight = 40;
    
    CGRect passwordInputFrame = CGRectMake(x,
                                           passwordLabel.y + passwordLabel.height + kAuthComponentSpace,
                                           inputWidth,
                                           inputHeight);
    [self createPasswordInputWithFrame:passwordInputFrame];
    [containerView addSubview:self.passwordTextField];
    
    CustomLabel *confirmPasswordLabel = [self createConfirmPasswordLabelWithOrigin:CGPointMake(x, self.passwordTextField.y + self.passwordTextField.height + kAuthComponentSpace)];
    [containerView addSubview:confirmPasswordLabel];
    
    CGRect confirmPasswordInputFrame = CGRectMake(x,
                                                  confirmPasswordLabel.y + confirmPasswordLabel.height + kAuthComponentSpace,
                                           inputWidth,
                                           inputHeight);
    [self createConfirmPasswordInputWithFrame:confirmPasswordInputFrame];
    [containerView addSubview:self.confirmPasswordTextField];
    
    [self createErrorLabelWithOrigin:CGPointMake(x,self.confirmPasswordTextField.y + self.confirmPasswordTextField.height + kAuthComponentSpace * 2)];
    [containerView addSubview:self.errorLabel];
    
}

- (CustomLabel *)createTitleLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"Reset Password";
    UIFont *titleFont = kSemiBold32;
    
    CGFloat titleWidth = self.view.width - 20;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:kPrimaryColor];
    
    return label;
}

- (CustomLabel *)createDescriptionLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"Enter your new password.";
    UIFont *titleFont = kRegular18;
    
    CGFloat titleWidth = self.view.width - 20;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    
    return label;
}

- (CustomLabel *)createPasswordLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"";
    UIFont *titleFont = kSemiBold16;
    
    CGFloat titleWidth = self.view.width - 20;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    
    return label;
}

- (void)createPasswordInputWithFrame:(CGRect)frame {
    self.passwordTextField = [[InputTextField alloc] initWithFrame:frame];
    
    self.passwordTextField.placeholder = @"PASSWORD";
    self.passwordTextField.text = @"";
    self.passwordTextField.textColor = UIColor.blackColor;
    
    
    self.passwordTextField.rightViewMode = UITextFieldViewModeAlways;
    self.passwordIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    
    self.passwordIcon.image = kHidePassIcon;
    self.passwordTextField.secureTextEntry = TRUE;
    self.isShowPassword = FALSE;
    
    [self.passwordIcon setUserInteractionEnabled:YES];
    
    /// tap
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(passwordIconClicked:)];
    [self.passwordIcon addGestureRecognizer:tapRecognizer];
    tapRecognizer.cancelsTouchesInView = NO;
    
    self.passwordTextField.rightView = self.passwordIcon;
    
}

- (CustomLabel *)createConfirmPasswordLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"";
    UIFont *titleFont = kSemiBold16;
    
    CGFloat titleWidth = self.view.width - 20;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    
    return label;
}

- (void)createConfirmPasswordInputWithFrame:(CGRect)frame {
    self.confirmPasswordTextField = [[InputTextField alloc] initWithFrame:frame];
    
    self.confirmPasswordTextField.placeholder = @"CONFIRM PASSWORD";
    self.confirmPasswordTextField.text = @"";
    self.confirmPasswordTextField.textColor = UIColor.blackColor;
    
    
    self.confirmPasswordTextField.rightViewMode = UITextFieldViewModeAlways;
    self.confirmPasswordIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    
    self.confirmPasswordIcon.image = kHidePassIcon;
    self.confirmPasswordTextField.secureTextEntry = TRUE;
    self.isShowConfirmPassword = FALSE;
    
    [self.confirmPasswordIcon setUserInteractionEnabled:YES];
    
    /// tap
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(confirmPasswordIconClicked:)];
    [self.confirmPasswordIcon addGestureRecognizer:tapRecognizer];
    tapRecognizer.cancelsTouchesInView = NO;
    
    self.confirmPasswordTextField.rightView = self.confirmPasswordIcon;
    
}

- (void)createContinueButton {
    CGFloat x = (self.view.width - kButtonWidth) / 2;
    CGFloat y = self.view.height - kButtonHeight - kAuthBottomPadding;
    CGRect frame = CGRectMake(x, y, kButtonWidth, kButtonHeight);
    
    UIFont *titleFont = kSemiBold16;
    
    self.continueButton = [[CustomButton alloc] initWithFrame:frame
                                                                 title:@"Continue"
                                                            titleColor:UIColor.whiteColor
                                                             titleFont:titleFont
                                                       backgroundColor:kPrimaryColor
                                                           borderColor:kPrimaryColor.CGColor];
    [self.continueButton addTarget:self action:@selector(continueButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.continueButton];
}

- (void)createErrorLabelWithOrigin:(CGPoint)origin {
    UIFont *titleFont = kRegular16;
    
    self.errorLabel = [[ErrorLabel alloc] initWithOrigin:origin
                                                maxWidth:kMaxErrorWidth
                                                titleFont:titleFont];
    
}

#pragma mark - OnClicked

- (void)passwordIconClicked:(UITapGestureRecognizer *)tapRecognizer {
    UIImage *newImage;
    self.isShowPassword = !self.isShowPassword;
    if (self.isShowPassword) {
        newImage = kShowPassIcon;
        self.passwordTextField.secureTextEntry = FALSE;
    }
    else {
        newImage = kHidePassIcon;
        self.passwordTextField.secureTextEntry = TRUE;
    }
    
    [UIView transitionWithView:self.view
                      duration:0.1
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
        self.passwordIcon.image = newImage;
    } completion:nil];
}

- (void)confirmPasswordIconClicked:(UITapGestureRecognizer *)tapRecognizer {
    UIImage *newImage;
    self.isShowConfirmPassword = !self.isShowConfirmPassword;
    if (self.isShowConfirmPassword) {
        newImage = kShowPassIcon;
        self.confirmPasswordTextField.secureTextEntry = FALSE;
    }
    else {
        newImage = kHidePassIcon;
        self.confirmPasswordTextField.secureTextEntry = TRUE;
    }
    
    [UIView transitionWithView:self.view
                      duration:0.1
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
        self.confirmPasswordIcon.image = newImage;
    } completion:nil];
}

- (void)continueButtonClicked:(CustomButton *)sender {

    if ([self isEmptyInput]) {
        [self.errorLabel setErrorLabelWithTitle:@"Please input password and confirm password"];
        [sender showAndHideLoadingInDuration:0.1f];
        return;
    }
    
    if ([self isNotSamePassword]) {
        [self.errorLabel setErrorLabelWithTitle:@"password and confirm password not match"];
        [sender showAndHideLoadingInDuration:0.1f];
        return;
    }
    
    [self.errorLabel setErrorLabelWithTitle:@""];
    [sender showLoading];
    
    NSString *password = self.passwordTextField.text;
    
    NSDictionary *info = [[NSDictionary alloc] initWithObjects:@[password,_code] forKeys:@[@"password",@"token"]];
    
    [self.resetPasswordHandler resetPasswordWithInfo:info];

}

#pragma mark -

- (BOOL)isNotSamePassword {
    NSString *password = self.passwordTextField.text;
    NSString *confirmPassword = self.confirmPasswordTextField.text;
    if (![password isEqualToString:confirmPassword]) {
        return TRUE;
    }
    return FALSE;
}

- (BOOL)isEmptyInput {
    NSString *password = self.passwordTextField.text;
    NSString *confirmPassword = self.confirmPasswordTextField.text;
    if ([password isEqualToString:@""] || [confirmPassword isEqualToString:@""]) {
        return TRUE;
    }
    return FALSE;
}

#pragma mark - Delegate
- (void)onFinishResetPasswordWithErrorCode:(ResetPasswordHandlerErrorCode)errorCode {
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (errorCode) {
            case ResetPasswordHandlerErrorNotValidData:
                // Toast not valid data
                NSLog(@"[ERROR] %s: NOT VALID DATA", __func__);
                [self.continueButton hideLoading];
                [self.errorLabel setErrorLabelWithTitle:@"Invalid password"];
                break;
            case ResetPasswordHandlerErrorServerError:
                // Toast server error
                NSLog(@"[ERROR] %s: SERVER ERROR", __func__);
                [self.continueButton hideLoading];
                [self.errorLabel setErrorLabelWithTitle:@"Something went wrong. Please try again"];
                break;
            case ResetPasswordHandlerErrorNetworkError:
                // Toast network error
                NSLog(@"[ERROR] %s: NETWORK ERROR", __func__);
                [self.continueButton hideLoading];
                [self.errorLabel setErrorLabelWithTitle:@"Something went wrong. Please try again"];
                break;
            case ResetPasswordHandlerErrorInvalidURL:
                // Toast invalid URL error
                NSLog(@"[ERROR] %s: INVALID URL ERROR", __func__);
                [self.continueButton hideLoading];
                [self.errorLabel setErrorLabelWithTitle:@"Something went wrong. Please try again"];
                break;
            default:
                NSLog(@"[INFO] %s: GOT THE TOKEN", __func__);
                [self.continueButton hideLoading];
                [self dismissViewControllerAnimated:NO completion:nil];
                [self.navigationController popToRootViewControllerAnimated:YES];
                break;
        }
    });
}
#pragma mark -
- (ResetPasswordHandler *)resetPasswordHandler {
    if (_resetPasswordHandler) {
        return _resetPasswordHandler;
    }
    _resetPasswordHandler = [[ResetPasswordHandler alloc] init];
    return _resetPasswordHandler;
}
@end
