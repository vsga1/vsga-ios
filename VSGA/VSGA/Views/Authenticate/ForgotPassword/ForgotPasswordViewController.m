//
//  ForgotPasswordViewController.m
//  VSGA
//
//  Created by LAP14011 on 07/02/2023.
//

#import "ForgotPasswordViewController.h"
#import "Macro.h"

#import "UIView+Extension.h"
#import "NSString+Extension.h"

#import "InputTextField.h"
#import "CustomButton.h"
#import "CustomLabel.h"
#import "ErrorLabel.h"

#import "ForgotPasswordHandler.h"

#import "VerificationCodeViewController.h"

@interface ForgotPasswordViewController () <ForgotPasswordHandlerDelegate>

@property (nonatomic, strong) InputTextField *emailTextField;

@property (nonatomic, strong) ErrorLabel *errorLabel;

@property (nonatomic, strong) CustomButton *continueButton;

@property (nonatomic, strong) ForgotPasswordHandler *forgotPasswordHandler;

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createView];
    
    self.forgotPasswordHandler.delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.errorLabel setErrorLabelWithTitle:@""];
}

#pragma mark - Create View

- (void)createView {
    self.view.backgroundColor = UIColor.whiteColor;
    
    CGFloat x = 20;
    CGFloat y = self.navigationController.navigationBar.height + 20;
//    CGFloat height = self.view.height - kButtonHeight * 2 - kAuthBottomPadding - kAuthComponentSpace * 2;
    CGFloat height = self.view.height;
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, y, self.view.width, height)];
    [self.view addSubview:containerView];
    
    CGFloat yTitle;
    if (height < 600) {
        yTitle = 0;
    }
    else {
        yTitle = height/10;
    }
    
    CustomLabel *titleLabel = [self createTitleLabelWithOrigin:CGPointMake(x, yTitle)];
    [containerView addSubview:titleLabel];
    
    CustomLabel *descLabel = [self createDescriptionLabelWithOrigin:CGPointMake(x, titleLabel.y + titleLabel.height + kAuthComponentSpace)];
    [containerView addSubview:descLabel];
    
    CustomLabel *emailLabel = [self createEmailLabelWithOrigin:CGPointMake(x, descLabel.y + descLabel.height + kAuthComponentSpace * 2)];
    [containerView addSubview:emailLabel];
    
    CGFloat inputWidth = kButtonWidth;
    CGFloat inputHeight = 40;
    
    CGRect emailInputFrame = CGRectMake(x,
                                        emailLabel.y + emailLabel.height + kAuthComponentSpace,
                                        inputWidth,
                                        inputHeight);
    [self createEmailInputWithFrame:emailInputFrame];
    [containerView addSubview:self.emailTextField];
    
    [self createErrorLabelWithOrigin:CGPointMake(x,self.emailTextField.y + self.emailTextField.height + kAuthComponentSpace * 2)];
    [containerView addSubview:self.errorLabel];
    
    [self createContinueButtonToContainerView:containerView];
    
}

- (CustomLabel *)createTitleLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"Forgot Password";
    UIFont *titleFont = kSemiBold32;
    
    CGFloat titleWidth = self.view.width - 2*origin.x;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:kPrimaryColor];
    
    return label;
}

- (CustomLabel *)createDescriptionLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"Enter the email address that you registered with your account. Reset password email will be sent to it.";
    UIFont *titleFont = kRegular18;
    
    CGFloat titleWidth = self.view.width - 2*origin.x;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    
    return label;
}

- (CustomLabel *)createEmailLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"";
    UIFont *titleFont = kSemiBold16;
    
    CGFloat titleWidth = self.view.width - 20;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    
    return label;
}

- (void)createEmailInputWithFrame:(CGRect)frame {
    self.emailTextField = [[InputTextField alloc] initWithFrame:frame];
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.emailTextField.placeholder = @"example@email.com";
    
    self.emailTextField.text = @"";
    self.emailTextField.textColor = UIColor.blackColor;
}

- (void)createErrorLabelWithOrigin:(CGPoint)origin {
    UIFont *titleFont = kRegular16;
    
    self.errorLabel = [[ErrorLabel alloc] initWithOrigin:origin
                                                maxWidth:kMaxErrorWidth
                                               titleFont:titleFont];
}

- (void)createContinueButtonToContainerView:(UIView *)containerView {
    CGFloat x = (self.view.width - kButtonWidth) / 2;
//    CGFloat y = self.view.height - kButtonHeight - kAuthBottomPadding;
    CGFloat y = self.errorLabel.y + self.errorLabel.height + kAuthComponentSpace * 2;
    CGRect frame = CGRectMake(x, y, kButtonWidth, kButtonHeight);
    
    UIFont *titleFont = kSemiBold20;
    
    self.continueButton = [[CustomButton alloc] initWithFrame:frame
                                                        title:@"Continue"
                                                   titleColor:UIColor.whiteColor
                                                    titleFont:titleFont
                                              backgroundColor:kPrimaryColor
                                                  borderColor:kPrimaryColor.CGColor];
    [self.continueButton addTarget:self action:@selector(continueButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [containerView addSubview:self.continueButton];
}

#pragma mark - OnClicked

- (void)continueButtonClicked:(CustomButton *)sender {
    
    NSString *email = self.emailTextField.text;
    
    if ([email isEqualToString:@""]) {
        [self.errorLabel setErrorLabelWithTitle:@"Please input email"];
        [sender showAndHideLoadingInDuration:0.1f];
        return;
    }
    [self.errorLabel setErrorLabelWithTitle:@""];
    [sender showLoading];
    
    NSDictionary *info = [[NSDictionary alloc] initWithObjects:@[email] forKeys:@[@"email"]];
    
    [self.forgotPasswordHandler forgotPasswordWithInfo:info];
}

#pragma mark - ForgotPasswordHandlerDelegate

- (void)onFinishForgotPasswordWithErrorCode:(ForgotPasswordHandlerErrorCode)errorCode {
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (errorCode) {
            case ForgotPasswordHandlerErrorNotValidData:
                // Toast not valid data
                NSLog(@"[ERROR] %s: NOT VALID DATA", __func__);
                [self.continueButton hideLoading];
                [self.errorLabel setErrorLabelWithTitle:@"Invalid email"];
                break;
            case ForgotPasswordHandlerErrorServerError:
                // Toast server error
                NSLog(@"[ERROR] %s: SERVER ERROR", __func__);
                [self.continueButton hideLoading];
                [self.errorLabel setErrorLabelWithTitle:@"Something went wrong. Please try again"];
                break;
            case ForgotPasswordHandlerErrorNetworkError:
                // Toast network error
                NSLog(@"[ERROR] %s: NETWORK ERROR", __func__);
                [self.continueButton hideLoading];
                [self.errorLabel setErrorLabelWithTitle:@"Something went wrong. Please try again"];
                break;
            case ForgotPasswordHandlerErrorInvalidURL:
                // Toast invalid URL error
                NSLog(@"[ERROR] %s: INVALID URL ERROR", __func__);
                [self.continueButton hideLoading];
                [self.errorLabel setErrorLabelWithTitle:@"Something went wrong. Please try again"];
                break;
            default:
                NSLog(@"[INFO] %s: GOT THE TOKEN", __func__);
                [self.continueButton hideLoading];
                [self showAlertSuccessInfo];
                break;
        }
    });
}

- (void)showAlertSuccessInfo {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:@"The reset password link has been sent to your email. Please check your email to reset password"
                                                                      preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:NO completion:nil];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
    [alertController addAction:okAction];

    // Present the alert controller
    [self presentViewController:alertController animated:YES completion:nil];

}

#pragma mark -
- (ForgotPasswordHandler *)forgotPasswordHandler {
    if (_forgotPasswordHandler) {
        return _forgotPasswordHandler;
    }
    _forgotPasswordHandler = [[ForgotPasswordHandler alloc] init];
    return _forgotPasswordHandler;
}

@end
