//
//  SignUpViewController.m
//  VSGA
//
//  Created by LAP14011 on 07/02/2023.
//

#import "SignUpViewController.h"
#import "UtilityClass.h"
#import "AppDelegate.h"

#import "Macro.h"

#import "UIView+Extension.h"
#import "NSString+Extension.h"
#import "UIImage+Extension.h"

#import "InputTextField.h"
#import "CustomButton.h"
#import "CustomLabel.h"
#import "ErrorLabel.h"

#import "ThirdPartySignInView.h"

#import "SignUpHandler.h"

const NSString *kPasswordPattern = @"^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[@#$%^&+=?])[A-Za-z\\d@#$%^&+=?]{8,}$";

const NSString *kEmailPattern = @"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}";

@interface SignUpViewController() <SignUpHandlerDelegate, ThirdPartySignInDelegate, UITextFieldDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) InputTextField *emailTextField;

@property (nonatomic, strong) InputTextField *usernameTextField;

@property (nonatomic, strong) InputTextField *passwordTextField;

@property (nonatomic, strong) CustomButton *signUpButton;

@property (nonatomic, strong) ErrorLabel *errorLabel;

@property (nonatomic, strong) UIImageView *passwordIcon;

@property (nonatomic, strong) SignUpHandler *signUpHandler;

@property (nonatomic, assign) BOOL isShowPassword;

@property (nonatomic, strong) InputTextField *activeTextField;

@property (nonatomic, assign) UIEdgeInsets originalContentInset;

@property (nonatomic, strong) ThirdPartySignInView *thirdPartySignInView;

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createView];
    [self setupGestures];
    [self setupDelegates];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = FALSE;
    
    // Register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.errorLabel setErrorLabelWithTitle:@""];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Create View

- (void)createView {
    self.view.backgroundColor = UIColor.whiteColor;
    
    ///Sign up form
    CGFloat statusBarHeight = [UtilityClass getStatusBarHeight];
    CGFloat navigationBarHeight = self.navigationController.navigationBar.frame.size.height;
    CGFloat totalBarHeight = statusBarHeight + navigationBarHeight;
    CGFloat safeAreaBottomHeight = [UtilityClass getSafeAreaBottomHeight];
    
    CGFloat scrollViewHeight = self.view.height - totalBarHeight - safeAreaBottomHeight;
    CGRect signUpFormFrame = CGRectMake(0,
                                        totalBarHeight,
                                        self.view.width,
                                        scrollViewHeight);
    [self createSignUpFormWithFrame:signUpFormFrame];
}

- (void)createSignUpFormWithFrame:(CGRect)frame {
    CGFloat x = 20;
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:frame];
    self.scrollView.contentSize = frame.size;
    [self.view addSubview:self.scrollView];
    
    CGFloat yTitle;
    if (frame.size.height < 600) {
        yTitle = 0;
    }
    else {
        yTitle = frame.size.height/10;
    }
    
    CustomLabel *titleLabel = [self createTitleLabelWithOrigin:CGPointMake(x, yTitle)];
    [self.scrollView addSubview:titleLabel];
    
    CustomLabel *descLabel = [self createDescriptionLabelWithOrigin:CGPointMake(x, titleLabel.y + titleLabel.height + kAuthComponentSpace)];
    [self.scrollView addSubview:descLabel];
    
    
    CustomLabel *emailLabel = [self createUsernameLabelWithOrigin:CGPointMake(x, descLabel.y + descLabel.height + kAuthComponentSpace * 2)];
    [self.scrollView addSubview:emailLabel];
    
    CGFloat inputWidth = kButtonWidth;
    CGFloat inputHeight = 40;
    
    CGRect emailInputFrame = CGRectMake(x,
                                        emailLabel.y + emailLabel.height + kAuthComponentSpace,
                                        inputWidth,
                                        inputHeight);
    [self createEmailInputWithFrame:emailInputFrame];
    [self.scrollView addSubview:self.emailTextField];
    
    CustomLabel *usernameLabel = [self createUsernameLabelWithOrigin:CGPointMake(x, self.emailTextField.y + self.emailTextField.height + kAuthComponentSpace)];
    [self.scrollView addSubview:usernameLabel];
    
    CGRect usernameInputFrame = CGRectMake(x,
                                           usernameLabel.y + usernameLabel.height + kAuthComponentSpace,
                                           inputWidth,
                                           inputHeight);
    [self createUsernameInputWithFrame:usernameInputFrame];
    [self.scrollView addSubview:self.usernameTextField];
    
    CustomLabel *passwordLabel = [self createPasswordLabelWithOrigin:CGPointMake(x, self.usernameTextField.y + self.usernameTextField.height + kAuthComponentSpace)];
    [self.scrollView addSubview:passwordLabel];
    
    CGRect passwordInputFrame = CGRectMake(x,
                                           passwordLabel.y + passwordLabel.height + kAuthComponentSpace,
                                           inputWidth,
                                           inputHeight);
    [self createPasswordInputWithFrame:passwordInputFrame];
    [self.scrollView addSubview:self.passwordTextField];
    
    [self createErrorLabelWithOrigin:CGPointMake(x,self.passwordTextField.y + self.passwordTextField.height + kAuthComponentSpace * 2)];
    [self.scrollView addSubview:self.errorLabel];
    
    [self createSignUpButtonWithOrigin:CGPointMake((self.view.width - kButtonWidth) / 2,self.errorLabel.y + self.errorLabel.height + kAuthComponentSpace * 4)];
    [self.scrollView addSubview:self.signUpButton];
    
    CGFloat thirdPartyHeight = kButtonHeight + kAuthOrLabelHeight + kAuthBottomPadding + kAuthComponentSpace;
    CGRect thirdPartyFrame = CGRectMake(0,
                                        self.signUpButton.bottom + kAuthComponentSpace,
                                        self.view.width,
                                        thirdPartyHeight);
    self.thirdPartySignInView = [[ThirdPartySignInView alloc] initWithFrame:thirdPartyFrame
                                                                buttonWidth:kButtonWidth
                                                               buttonHeight:kButtonHeight
                                                         withViewController:self];
    [self.scrollView addSubview:self.thirdPartySignInView];
    
    CGFloat blankSpace = self.scrollView.height - self.thirdPartySignInView.bottom;
    self.scrollView.height = self.scrollView.height - blankSpace;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, self.scrollView.height);
    self.originalContentInset = self.scrollView.contentInset;
}

- (CustomLabel *)createTitleLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"Sign up";
    UIFont *titleFont = kSemiBold32;
    
    CGFloat titleWidth = self.view.width - 2*origin.x;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:kPrimaryColor];
    
    return label;
}

- (CustomLabel *)createDescriptionLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"Sign up to VStudy with your email, username and password.";
    UIFont *titleFont = kRegular18;
    
    CGFloat titleWidth = self.view.width - 2*origin.x;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    
    return label;
}

- (CustomLabel *)createEmailLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"";
    UIFont *titleFont = kSemiBold16;
    
    CGFloat titleWidth = self.view.width - 2*origin.x;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    
    return label;
}

- (void)createEmailInputWithFrame:(CGRect)frame {
    self.emailTextField = [[InputTextField alloc] initWithFrame:frame];
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.emailTextField.placeholder = @"Email";
    self.emailTextField.text = @"";
    self.emailTextField.textColor = UIColor.blackColor;
    self.emailTextField.font = kRegular16;
}

- (CustomLabel *)createUsernameLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"";
    UIFont *titleFont = kSemiBold16;
    
    CGFloat titleWidth = self.view.width - 2*origin.x;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    
    return label;
}

- (void)createUsernameInputWithFrame:(CGRect)frame {
    self.usernameTextField = [[InputTextField alloc] initWithFrame:frame];
    self.usernameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.usernameTextField.placeholder = @"Username";
    
    self.usernameTextField.text = @"";
    self.usernameTextField.textColor = UIColor.blackColor;
    self.usernameTextField.font = kRegular16;
}

- (CustomLabel *)createPasswordLabelWithOrigin:(CGPoint)origin {
    NSString *title = @"";
    UIFont *titleFont = kSemiBold16;
    
    CGFloat titleWidth = self.view.width - 2*origin.x;
    CGSize titleSize = [title sizeOfStringWithStyledFont:titleFont withSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    
    CustomLabel *label = [[CustomLabel alloc]
                          initWithFrame:CGRectMake(origin.x,
                                                   origin.y,
                                                   titleSize.width,
                                                   titleSize.height)
                          title:title
                          titleFont:titleFont
                          titleColor:UIColor.blackColor];
    
    return label;
}

- (void)createPasswordInputWithFrame:(CGRect)frame {
    self.passwordTextField = [[InputTextField alloc] initWithFrame:frame];
    self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.passwordTextField.placeholder = @"Password";
    self.passwordTextField.text = @"";
    self.passwordTextField.textColor = UIColor.blackColor;
    self.passwordTextField.font = kRegular16;
    
    self.passwordTextField.rightViewMode = UITextFieldViewModeAlways;
    
    CGFloat iconSize = 20;
    CGFloat iconMargin = 10;
    // Create a container view for the right view
    UIView *rightViewContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, iconSize + iconMargin, iconSize)];
    
    self.passwordIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, iconSize, iconSize)];
    UIImage *image = kHidePassIcon;
    UIImage *resizedImage = [image resizeWithTargetSize:self.passwordIcon.size];
    self.passwordIcon.image = resizedImage;
    self.passwordIcon.contentMode = UIViewContentModeScaleAspectFit;
    self.passwordTextField.secureTextEntry = TRUE;
    self.isShowPassword = FALSE;
    
    [self.passwordIcon setUserInteractionEnabled:YES];
    
    [rightViewContainer addSubview:self.passwordIcon];
    
    /// tap
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(passwordIconClicked:)];
    [self.passwordIcon addGestureRecognizer:tapRecognizer];
    tapRecognizer.cancelsTouchesInView = NO;
    
    self.passwordTextField.rightView = rightViewContainer;
    
}

- (void)createSignUpButtonWithOrigin:(CGPoint)origin {
    CGRect frame = CGRectMake(origin.x, origin.y, kButtonWidth, kButtonHeight);
    
    UIFont *titleFont = kSemiBold18;
    
    self.signUpButton = [[CustomButton alloc] initWithFrame:frame
                                                      title:@"Sign Up"
                                                 titleColor:UIColor.whiteColor
                                                  titleFont:titleFont backgroundColor:kPrimaryColor
                                                borderColor:kPrimaryColor.CGColor];
    [self.signUpButton addTarget:self action:@selector(signUpButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)createErrorLabelWithOrigin:(CGPoint)origin {
    UIFont *titleFont = kRegular16;
    
    self.errorLabel = [[ErrorLabel alloc] initWithOrigin:origin
                                                maxWidth:kMaxErrorWidth
                                               titleFont:titleFont];
    
}

#pragma mark - Setup Gestures

- (void)setupGestures {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
    NSArray *textFields = @[self.emailTextField, self.usernameTextField, self.passwordTextField];
    
    for (UITextField *textField in textFields) {
        textField.delegate = self;
    }
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - Setup Delegated

- (void)setupDelegates {
    self.emailTextField.delegate = self;
    self.usernameTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    self.signUpHandler.delegate = self;
    self.thirdPartySignInView.delegate = self;
}

#pragma mark - Keyboard

- (void)keyboardWillShow:(NSNotification *)notification {
    // Get the keyboard frame
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    // Calculate the blank space between the scroll view and the sign-up button
    CGFloat blankSpace = CGRectGetMaxY(self.scrollView.frame) - CGRectGetMaxY(self.thirdPartySignInView.frame);
    
    // Calculate the new content inset
    UIEdgeInsets contentInset = self.originalContentInset;
    contentInset.bottom = CGRectGetHeight(keyboardFrame) - blankSpace;
    
    // Update the scroll view's content inset
    self.scrollView.contentInset = contentInset;
    self.scrollView.scrollIndicatorInsets = contentInset;
    
    // Scroll to the active text field
    if (self.activeTextField) {
        CGRect visibleRect = self.activeTextField.frame;
        visibleRect.size.height += 16; // Add some extra space if needed
        [self.scrollView scrollRectToVisible:visibleRect animated:YES];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification {
    self.scrollView.contentInset = self.originalContentInset;
    self.scrollView.scrollIndicatorInsets = self.originalContentInset;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    UITapGestureRecognizer *tapGesture = (UITapGestureRecognizer *)self.view.gestureRecognizers.firstObject;
    tapGesture.enabled = YES;
    self.activeTextField = (InputTextField *)textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    UITapGestureRecognizer *tapGesture = (UITapGestureRecognizer *)self.view.gestureRecognizers.firstObject;
    tapGesture.enabled = [self isAnyTextFieldEditing];
    self.activeTextField = nil;
}

- (BOOL)isAnyTextFieldEditing {
    NSArray *textFields = @[self.emailTextField, self.usernameTextField, self.passwordTextField];
    
    for (UITextField *textField in textFields) {
        if (textField.isEditing) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.emailTextField) {
        [self.usernameTextField becomeFirstResponder];
    } else if (textField == self.usernameTextField) {
        [self.passwordTextField becomeFirstResponder];
    } else if (textField == self.passwordTextField) {
        // The last text field, perform any desired action
        [textField resignFirstResponder]; // Dismiss the keyboard
        [self signUpButtonClicked:nil];
    }
    
    return YES;
}

#pragma mark - On clicked

- (void)passwordIconClicked:(UITapGestureRecognizer *)tapRecognizer {
    UIImage *newImage;
    self.isShowPassword = !self.isShowPassword;
    if (self.isShowPassword) {
        newImage = kShowPassIcon;
        self.passwordTextField.secureTextEntry = FALSE;
    }
    else {
        newImage = kHidePassIcon;
        self.passwordTextField.secureTextEntry = TRUE;
    }
    UIImage *resizedImage = [newImage resizeWithTargetSize:self.passwordIcon.size];
    
    [UIView transitionWithView:self.view
                      duration:0.1
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
        self.passwordIcon.image = resizedImage;
    } completion:nil];
}

- (void)signUpButtonClicked:(CustomButton *)sender {
    
    NSString *email = [self.emailTextField.text lowercaseString];
    NSString *username = [self.usernameTextField.text lowercaseString];
    NSString *password = self.passwordTextField.text;
    
    if ([email isEqualToString:@""] || [username isEqualToString:@""] || [password isEqualToString:@""]) {
        [self.errorLabel setErrorLabelWithTitle:@"Enter email, username, and password"];
        [sender showAndHideLoadingInDuration:0.1f];
        return;
    }
    
    NSPredicate *passwordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", kPasswordPattern];
       BOOL isPasswordValid = [passwordPredicate evaluateWithObject:password];
    if (!isPasswordValid) {
        [self.errorLabel setErrorLabelWithTitle:@"Password at least 8 characters, one uppercase letter, lowercase letter, number and special letter"];
        [sender showAndHideLoadingInDuration:0.1f];
        return;
    }
    
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", kEmailPattern];

    BOOL isValidEmail = [emailPredicate evaluateWithObject:email];
    if (!isValidEmail) {
        [self.errorLabel setErrorLabelWithTitle:@"Invalid email"];
        [sender showAndHideLoadingInDuration:0.1f];
        return;
    }
    
    NSDictionary *info = [[NSDictionary alloc] initWithObjects:@[username,password,email] forKeys:@[@"username",@"password",@"email"]];
    
    [self.errorLabel setErrorLabelWithTitle:@""];
    [sender showLoading];
    [self.signUpHandler signUpWithInfo:info];
}

#pragma mark - SignUpHandlerDelegate

- (void)onFinishSignUpWithErrorCode:(SignUpHandlerErrorCode)errorCode message:(NSString *)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (errorCode) {
            case SignUpHandlerErrorNotValidData:
                // Toast not valid data
                NSLog(@"[ERROR] %s: NOT VALID DATA", __func__);
                [self.signUpButton hideLoading];
                [self.errorLabel setErrorLabelWithTitle:message];
                break;
            default:
                // Switch to home view
                NSLog(@"[INFO] %s: SWITCH TO HOMEVIEW", __func__);
                [self.signUpButton hideLoading];
                [self showAlert:@"Sign up successful"];
                break;
        }
    });
}

#pragma mark - ThirdPartySignInDelegate

- (void)onFinishThirdPartySignInWithErrorCode:(ThirdPartySignInHandlerErrorCode)errorCode {
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (errorCode) {
            case ThirdPartySignInErrorFail:
                // Toast not valid data
                NSLog(@"[ERROR] %s: FAILED", __func__);
                [self.signUpButton hideLoading];
                [self.errorLabel setErrorLabelWithTitle:@"Something went wrong. Please try again"];
                break;
            case ThirdPartySignInCancel:
                [self.signUpButton hideLoading];
                break;
            default:
                // Switch to home view
                NSLog(@"[INFO] %s: SWITCH TO HOMEVIEW", __func__);
                [AppDelegate.shared updateView];
                break;
        }
    });
}

#pragma mark - Alert

- (void)showAlert:(NSString *)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIView *firstSubview = alert.view.subviews.firstObject;
    UIView *alertContentView = firstSubview.subviews.firstObject;
    for (UIView *subSubView in alertContentView.subviews) {
        subSubView.backgroundColor = kPrimaryColor;
    }
    NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:message];
    [title addAttribute:NSForegroundColorAttributeName
                  value:[UIColor whiteColor]
                  range:NSMakeRange(0,title.length)];
    [alert setValue:title forKey:@"attributedTitle"];
    
    [self presentViewController:alert animated:YES completion:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:^{
        }];
        [AppDelegate.shared updateView];
    });
}

#pragma mark - Custom Accessors

- (SignUpHandler *)signUpHandler {
    if (_signUpHandler) {
        return _signUpHandler;
    }
    _signUpHandler = [[SignUpHandler alloc] init];
    return _signUpHandler;
}

@end
