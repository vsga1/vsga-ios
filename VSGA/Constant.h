//
//  Constant.h
//  VStudy
//
//  Created by TamNVM on 22/06/2023.
//

#import <Foundation/Foundation.h>

extern const CGFloat kTopicViewItemSpacing;
extern const CGFloat kTopicViewItemPadding;
extern const CGFloat kTopicViewContentInset;

extern const CGFloat kTopicViewDeleteButtonSize;
extern const CGFloat kTopicViewDeleteButtonHPadding;

extern const CGFloat kTopicViewMaxSelectedCount;
