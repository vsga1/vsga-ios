//
//  Macro.h
//  VSGA
//
//  Created by LAP14011 on 06/02/2023.
//

#ifndef Macro_h
#define Macro_h
/// API
#define kNetworkError 150900
#define kServerError 150901
#define kNoDataError 150902
#define kNoError 0

#define kHost @"https://vstudy.redsand.vn/vstudy"

#define kSignInURL [kHost stringByAppendingString:@"/login"]
#define kSignUpURL [kHost stringByAppendingString:@"/v1/account/registration"]
#define kForgotPasswordURL [kHost stringByAppendingString:@"/v1/account/reset-password"]
#define kResetPasswordURL [kHost stringByAppendingString:@"/v1/account/password"]

#define kGetRoomListURL [kHost stringByAppendingString:@"/v1/room"]
#define kGetLatestRoomListURL [kHost stringByAppendingString:@"/v1/room/latest"]
#define kSearchRoomListURL [kHost stringByAppendingString:@"/v1/room/search"]
#define kJoinRoomEventURL [kHost stringByAppendingString:@"/v1/event/join-room"]
#define kCreateNewRoomURL [kHost stringByAppendingString:@"/v1/room"]

#define kGetSuggestTopicURL [kHost stringByAppendingString:@"/v1/topic/suggest"]
#define kSearchTopicURL [kHost stringByAppendingString:@"/v1/topic"]

#define kGetRoomNoteURL [kHost stringByAppendingString:@"/v1/event/note"]
#define kPutRoomNoteURL [kHost stringByAppendingString:@"/v1/event/note"]

#define kTaskURL [kHost stringByAppendingString:@"/v1/task"]
#define kRoomTaskDoneURL [kHost stringByAppendingString:@"/v1/task/done"]

#define kPromoteMember [kHost stringByAppendingString:@"/v1/event/promote"]
#define kDemoteMember [kHost stringByAppendingString:@"/v1/event/demote"]

#define kGetProfile [kHost stringByAppendingString:@"/v1/account/profile"]
/// Socket
//For real device
//#define kSocketHost @"http://10.0.0.199:5000/"

//Local
#define kSocketHost @"http://localhost:8080"

//Link DHUY
//#define kSocketHost @"https://develop-vstudy-socket.onrender.com"

//Link GHUY
//#define kSocketHost @"https://vsga-socket.onrender.com"
#define kJoinRoomEvent @"join room"
#define kOutRoomEvent @"out room"
#define kRoomDataEvent @"room data"

#define kSendMessageEvent @"send message"
#define kReceiveMessageEvent @"receive message"

#define kOpenEditNote @"open edit note"
#define kCloseEditNote @"close edit note"

#define kGetNewRoomTask @"get new task"
#define kDoneRoomTask @"done task"

#define kUpdateRoleEvent @"update role"

/// Voice call

#define kVoiceCallAppID @"4d8cb6c19d9444588a294322da62042e"
#define kVoiceCallPrimaryCertificate @"5bfb6315695f4712a5d4eeb7d2509d22"

/// Color
#define kPrimaryColor [UIColor colorWithRed:(59/255.0) green:(172/255.0) blue:(182/255.0) alpha:1]
#define kBackgroundColor [UIColor colorWithRed:(240/255.0) green:(244/255.0) blue:(250/255.0) alpha:1]
#define kWhiteBackgroundColor [UIColor colorWithRed:(250/255.0) green:(249/255.0) blue:(246/255.0) alpha:1]
#define kTimestampBGColor [UIColor colorWithRed:(195/255.0) green:(195/255.0) blue:(195/255.0) alpha:1]

#define kGreenColor [UIColor colorWithRed:(150/255.0) green:(195/255.0) blue:(98/255.0) alpha:1]
/// Font
//#define kRegular14 [UIFont fontWithName:@"Poppins-Regular" size:14]
//#define kRegular16 [UIFont fontWithName:@"Poppins-Regular" size:16]
//#define kRegular18 [UIFont fontWithName:@"Poppins-Regular" size:18]
//
//#define kMedium14 [UIFont fontWithName:@"Poppins-Medium" size:14]
//#define kMedium16 [UIFont fontWithName:@"Poppins-Medium" size:16]
//#define kMedium18 [UIFont fontWithName:@"Poppins-Medium" size:18]
//#define kMedium20 [UIFont fontWithName:@"Poppins-Medium" size:20]
//
//#define kSemiBold16 [UIFont fontWithName:@"Poppins-SemiBold" size:16]
//#define kSemiBold18 [UIFont fontWithName:@"Poppins-SemiBold" size:18]
//#define kSemiBold20 [UIFont fontWithName:@"Poppins-SemiBold" size:20]
//#define kSemiBold24 [UIFont fontWithName:@"Poppins-SemiBold" size:24]
//#define kSemiBold32 [UIFont fontWithName:@"Poppins-SemiBold" size:32]

//#define kRegular14 [UIFont fontWithName:@"Nunito-Regular" size:14]
//#define kRegular16 [UIFont fontWithName:@"Nunito-Regular" size:16]
//#define kRegular18 [UIFont fontWithName:@"Nunito-Regular" size:18]
//
//#define kMedium14 [UIFont fontWithName:@"Nunito-Medium" size:14]
//#define kMedium16 [UIFont fontWithName:@"Nunito-Medium" size:16]
//#define kMedium18 [UIFont fontWithName:@"Nunito-Medium" size:18]
//#define kMedium20 [UIFont fontWithName:@"Nunito-Medium" size:20]
//
//#define kSemiBold16 [UIFont fontWithName:@"Nunito-SemiBold" size:16]
//#define kSemiBold18 [UIFont fontWithName:@"Nunito-SemiBold" size:18]
//#define kSemiBold20 [UIFont fontWithName:@"Nunito-SemiBold" size:20]
//#define kSemiBold24 [UIFont fontWithName:@"Nunito-SemiBold" size:24]
//#define kSemiBold32 [UIFont fontWithName:@"Nunito-SemiBold" size:32]

#define kRegular14 [UIFont fontWithName:@"Rubik-Regular" size:14]
#define kRegular16 [UIFont fontWithName:@"Rubik-Regular" size:16]
#define kRegular18 [UIFont fontWithName:@"Rubik-Regular" size:18]

#define kMedium14 [UIFont fontWithName:@"Rubik-Medium" size:14]
#define kMedium16 [UIFont fontWithName:@"Rubik-Medium" size:16]
#define kMedium18 [UIFont fontWithName:@"Rubik-Medium" size:18]
#define kMedium20 [UIFont fontWithName:@"Rubik-Medium" size:20]

#define kSemiBold16 [UIFont fontWithName:@"Rubik-SemiBold" size:16]
#define kSemiBold18 [UIFont fontWithName:@"Rubik-SemiBold" size:18]
#define kSemiBold20 [UIFont fontWithName:@"Rubik-SemiBold" size:20]
#define kSemiBold24 [UIFont fontWithName:@"Rubik-SemiBold" size:24]
#define kSemiBold32 [UIFont fontWithName:@"Rubik-SemiBold" size:32]

/// General
#define kStatusBarFrame [[[[UIApplication sharedApplication] windows] firstObject] windowScene].statusBarManager.statusBarFrame
#define kButtonWidth (self.view.width - 40)
#define kButtonHeight (self.view.height/15)

#define kMaxErrorWidth (self.view.width)

#define kNavigationBarHeight 44

/// Authenticate
#define kAuthOrLabelWidth 50
#define kAuthOrLabelHeight 20

#define kAuthBottomPadding 40
#define kAuthComponentSpace 10

#define kHidePassIcon [UIImage imageNamed:@"HidePassIcon"]
#define kShowPassIcon [UIImage imageNamed:@"ShowPassIcon"]

#define kUnCheckIcon [UIImage imageNamed:@"UnCheckIcon"]
#define kCheckIcon [UIImage imageNamed:@"CheckIcon"]

#define kLoadRoomSize 10

/// Room
#define kRoomCellHeight 200
#define kRoomCellPadding 10

#define RoomDetailViewSpacing 30

/// Member List
#define kMemberAvatarSize 40
#define kMenuButtonSize 30

/// Chat

#define kMessageCollectionViewInset 20

/// Cell of Chat
static NSString *const kAvatarLayout = @"avatarLayout";
static NSString *const kMessageNameLayout = @"messageNameLayout";
static NSString *const kBubbleLayout = @"bubbleLayout";
static NSString *const kMainContentLayout = @"mainContentLayout";
static NSString *const kTimestampLayout = @"timestampLayout";

/// Timestamp

#define kOutsideTimestampMargin 5
#define kOutsideTimestampPadding 3
#define kOutsideTimestampCornerRadius 11

#define kTimestampShowLess @"HH:mm"
#define kTimestampTodayText @" Hôm nay"
#define kTimestampShowFull @"HH:mm dd/MM/yy"

/// Message name

#define kMessageNameMargin 5

/// Bubble message

#define kChatAvatarSize 30
#define kChatAvatarMargin 10

#define kBubbleTrailingPadding 2*kChatAvatarMargin
#define kBubbleMinWidth 40
#define kBubbleMinBlankSpace 60

#define kBubbleCornerRadius 13
#define kBubbleBorderColor [UIColor colorWithRed:(195/255.0) green:(195/255.0) blue:(195/255.0) alpha:1].CGColor
#define kBubbleBorderWidth 1

#define kTextMessagePadding 10

/// Chat Bar Text View

#define kChatBarTextViewPadding 5
#define kChatBarTextViewMargin 10

#define kChatBarContainerPadding 15
#define kChatBarContainerSpacing 5

#define kReceiverContainerPadding 10
#define kReceiverContainerSpacing 8

#endif /* Macro_h */
